plugins {
    id("java-library")
    kotlin("plugin.serialization") apply true
}

val logbackVersion: String by project
val slf4jApiVersion: String by project
val kotlinLoggingVersion: String by project
val kotlinSerializationVersion: String by project
val ktorVersion: String by project

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core")
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:$kotlinSerializationVersion")
    implementation("io.ktor:ktor-client-core:$ktorVersion")
    implementation("io.ktor:ktor-client-cio:$ktorVersion")

    implementation(project(":tax-content-service-model"))

    // Logging
    implementation("ch.qos.logback:logback-classic:$logbackVersion")
    implementation("org.slf4j:slf4j-api:$slf4jApiVersion")
    implementation("io.github.microutils:kotlin-logging:$kotlinLoggingVersion")
}
