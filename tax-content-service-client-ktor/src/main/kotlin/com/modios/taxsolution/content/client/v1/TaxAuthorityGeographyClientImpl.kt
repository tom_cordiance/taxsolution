package com.modios.taxsolution.content.client.v1

import com.modios.taxsolution.content.model.v1.TaxAuthorityGeography
import io.ktor.client.HttpClient
import io.ktor.client.request.get
import io.ktor.client.request.post
import io.ktor.client.statement.HttpResponse
import io.ktor.client.statement.readText
import java.util.Properties

class TaxAuthorityGeographyClientImpl(private val httpClient: HttpClient, properties: Properties) :
    TaxAuthorityGeographyClient {

    private val urlPropertyName = "cordiance.tax-content-service.url"

    private val serverAddress: String = properties.getProperty(urlPropertyName)
        ?: throw RuntimeException("$urlPropertyName was not set in configuration")

    private val baseUrl = "$serverAddress/api/v1/taxauthority-geography"

    override suspend fun create(
        taxContentProviderId: Long,
        geographyId: Long,
        taxAuthorityId: Long
    ): TaxAuthorityGeography {
        return httpClient.post("$baseUrl/$taxContentProviderId/$geographyId/$taxAuthorityId")
    }

    override suspend fun getByGeographyId(geographyId: Long): List<TaxAuthorityGeography> {
        return httpClient.get("$baseUrl/geography/$geographyId")
    }

    override suspend fun getByTaxAuthorityId(taxAuthorityId: Long): List<TaxAuthorityGeography> {
        return httpClient.get("$baseUrl/tax-authority/$taxAuthorityId")
    }

    override suspend fun getByGeographyIdAndTaxAuthorityId(
        geographyId: Long,
        taxAuthorityId: Long
    ): TaxAuthorityGeography? {
        val response: HttpResponse = httpClient.get("$baseUrl/$geographyId/$taxAuthorityId")
        return decodeNullable(response.readText())
    }
}
