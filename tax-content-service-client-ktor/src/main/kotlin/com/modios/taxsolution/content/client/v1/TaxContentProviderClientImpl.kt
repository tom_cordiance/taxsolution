package com.modios.taxsolution.content.client.v1

import com.modios.taxsolution.content.model.v1.TaxContentProvider
import io.ktor.client.HttpClient
import io.ktor.client.request.get
import io.ktor.client.request.post
import io.ktor.client.statement.HttpResponse
import io.ktor.client.statement.readText
import java.util.Properties

class TaxContentProviderClientImpl(private val httpClient: HttpClient, properties: Properties) :
    TaxContentProviderClient {

    private val urlPropertyName = "cordiance.tax-content-service.url"

    private val serverAddress: String = properties.getProperty(urlPropertyName)
        ?: throw RuntimeException("$urlPropertyName was not set in configuration")

    private val baseUrl = "$serverAddress/api/v1/taxcontentprovider"

    override suspend fun create(name: String): TaxContentProvider {
        return httpClient.post("$baseUrl/name/$name")
    }

    override suspend fun get(name: String): TaxContentProvider? {
        val response: HttpResponse = httpClient.get("$baseUrl/name/$name")
        return decodeNullable(response.readText())
    }

    override suspend fun get(id: Long): TaxContentProvider? {
        val response: HttpResponse = httpClient.get("$baseUrl/id/$id")
        return decodeNullable(response.readText())
    }
}
