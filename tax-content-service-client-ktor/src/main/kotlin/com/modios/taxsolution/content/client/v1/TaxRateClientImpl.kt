package com.modios.taxsolution.content.client.v1

import com.modios.taxsolution.content.model.v1.TaxRate
import io.ktor.client.HttpClient
import io.ktor.client.request.get
import io.ktor.client.request.post
import io.ktor.client.request.put
import io.ktor.http.ContentType
import io.ktor.http.contentType
import java.util.Properties

class TaxRateClientImpl(private val httpClient: HttpClient, properties: Properties) : TaxRateClient {

    private val urlPropertyName = "cordiance.tax-content-service.url"

    private val serverAddress: String = properties.getProperty(urlPropertyName)
        ?: throw RuntimeException("$urlPropertyName was not set in configuration")

    private val baseUrl = "$serverAddress/api/v1/tax-rate"

    override suspend fun create(taxRate: TaxRate): TaxRate {
        return httpClient.post(baseUrl) {
            contentType(ContentType.Application.Json)
            body = taxRate
        }
    }

    override suspend fun findByTaxAuthorityId(taxAuthorityId: Long): List<TaxRate> {
        return httpClient.get("$baseUrl/authority/$taxAuthorityId")
    }

    override suspend fun findById(id: Long): TaxRate? {
        return httpClient.get("$baseUrl/$id")
    }

    override suspend fun update(taxRate: TaxRate): TaxRate {
        return httpClient.put(baseUrl) {
            contentType(ContentType.Application.Json)
            body = taxRate
        }
    }
}
