package com.modios.taxsolution.content.client.v1

import com.modios.taxsolution.content.model.v1.Geography
import com.modios.taxsolution.content.model.v1.GeographyClassification
import io.ktor.client.HttpClient
import io.ktor.client.request.get
import io.ktor.client.request.parameter
import io.ktor.client.request.post
import io.ktor.client.statement.HttpResponse
import io.ktor.client.statement.readText
import io.ktor.http.ContentType
import io.ktor.http.contentType
import java.util.Properties

class GeographyClientImpl(private val httpClient: HttpClient, properties: Properties) : GeographyClient {

    companion object {
        val URL_PROPERTY_NAME = "cordiance.tax-content-service.url"
    }

    private val serverAddress: String = properties.getProperty(URL_PROPERTY_NAME)
        ?: throw RuntimeException("$URL_PROPERTY_NAME was not set in configuration")

    private val baseUrl = "$serverAddress/api/v1/geography"

    override suspend fun create(geography: Geography): Geography {
        return httpClient.post(baseUrl) {
            contentType(ContentType.Application.Json)
            body = geography
        }
    }

    override suspend fun create(
        taxContentProviderId: Long,
        classificationId: Int,
        parentId: Long,
        name: String,
        code: String
    ): Geography {
        return httpClient.post(baseUrl) {
            contentType(ContentType.Application.Json)
            body = Geography(
                id = 0L,
                taxContentProviderId = taxContentProviderId,
                classification = GeographyClassification.fromId(classificationId),
                parentId = parentId,
                name = name,
                code = code
            )
        }
    }

    override suspend fun getAllByName(name: String): List<Geography> {
        return httpClient.get("$baseUrl/name") {
            parameter("name", name)
        }
    }

    override suspend fun getByTaxContentProviderIdAndParentIdAndName(
        taxContentProviderId: Long,
        parentId: Long,
        name: String
    ): Geography? {
        val response: HttpResponse = httpClient.get("$baseUrl/parentAndName") {
            parameter("taxContentProviderId", taxContentProviderId)
            parameter("parentId", parentId)
            parameter("name", name)
        }

        return decodeNullable(response.readText())
    }

    override suspend fun getByTaxContentProviderIdAndParentIdAndCode(
        taxContentProviderId: Long,
        parentId: Long,
        code: String
    ): Geography? {
        val response: HttpResponse = httpClient.get("$baseUrl/parentAndCode") {
            parameter("taxContentProviderId", taxContentProviderId)
            parameter("parentId", parentId)
            parameter("code", code)
        }

        return decodeNullable(response.readText())
    }

    override suspend fun getByTaxContentProviderIdAndParentId(
        taxContentProviderId: Long,
        parentId: Long
    ): List<Geography> {
        return httpClient.get("$baseUrl/providerAndParent") {
            parameter("taxContentProviderId", taxContentProviderId)
            parameter("parentId", parentId)
        }
    }
}
