package com.modios.taxsolution.content.client.v1

import com.modios.taxsolution.content.model.v1.TaxAuthority
import io.ktor.client.HttpClient
import io.ktor.client.request.get
import io.ktor.client.request.post
import io.ktor.client.statement.HttpResponse
import io.ktor.client.statement.readText
import io.ktor.http.ContentType
import io.ktor.http.contentType
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import java.time.LocalDate
import java.util.Properties

class TaxAuthorityClientImpl(private val httpClient: HttpClient, properties: Properties) : TaxAuthorityClient {

    private val urlPropertyName = "cordiance.tax-content-service.url"

    private val serverAddress: String = properties.getProperty(urlPropertyName)
        ?: throw RuntimeException("$urlPropertyName was not set in configuration")

    private val baseUrl = "$serverAddress/api/v1/taxauthority"

    override suspend fun create(
        taxContentProviderId: Long,
        name: String,
        startDate: LocalDate,
        endDate: LocalDate?
    ): TaxAuthority {
        return httpClient.post(baseUrl) {
            contentType(ContentType.Application.Json)
            body = TaxAuthority(
                taxContentProviderId = taxContentProviderId,
                id = 0,
                name = name,
                startDate = startDate,
                endDate = endDate
            )
        }
    }

    override suspend fun getById(taxAuthorityId: Long): TaxAuthority? {
        val response: HttpResponse = httpClient.get("$baseUrl/$taxAuthorityId")
        return decodeNullable(response.readText())
    }

    override suspend fun getByTaxContentProviderIdAndName(taxContentProviderId: Long, name: String): TaxAuthority? {
        val response: HttpResponse = httpClient.get("$baseUrl/$taxContentProviderId/$name")
        return decodeNullable(response.readText())
    }
}

inline fun <reified T> decodeNullable(json: String): T? {
    return when {
        json.isBlank() -> null
        else -> Json.decodeFromString(json)
    }
}
