pluginManagement {
    repositories {
        gradlePluginPortal()
        mavenLocal()
        maven { url = uri("https://repo.spring.io/milestone") }
    }
    // required if plugins have not been published properly
    // https://docs.gradle.org/current/userguide/custom_plugins.html#note_for_plugins_published_without_java_gradle_plugin
    resolutionStrategy {
        eachPlugin {
            if (requested.id.id == "org.springframework.boot") {
                useModule("org.springframework.boot:spring-boot-gradle-plugin:${requested.version}")
            }
        }
    }
}
rootProject.name = "taxsolution"

include(
    "ctrie",
    "data-service-common",
    "admin-service-model",
    "admin-service-client",
    "admin-service-client-ktor",
    "admin-service",
    "admin-client",
    "admin-client-ktor",
    "tax-content-service-model",
    "tax-content-service-client",
    "tax-content-service-client-ktor",
    "tax-content-service",
    "tax-content-client",
    "tax-content-client-ktor",
    "tax-configuration-service-model",
    "tax-configuration-service-client",
    "tax-configuration-service",
    "tax-compliance-service-model",
    "tax-compliance-service-client",
    "tax-compliance-service",
    "calc-service-model",
    "calc-service-client",
    "calc-service-core",
    "calc-service-ktor",
    "calc-service-spring",
    "calc-client",
    "calc-client-ktor",
    "registry-service-model",
    "registry-service-dataaccess",
    "registry-service-client",
    "registry-service"
)
