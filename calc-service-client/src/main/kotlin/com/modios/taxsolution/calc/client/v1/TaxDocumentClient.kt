package com.modios.taxsolution.calc.client.v1

import com.modios.taxsolution.calc.model.v1.TaxDocument
import com.modios.taxsolution.calc.model.v1.TaxDocumentResult
import kotlinx.coroutines.flow.single
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.bodyToFlow

@RestController
@RequestMapping("/calc/standard/v1")
class TaxDocumentClient(private val calcWebClient: WebClient) {

    @PostMapping("/calc/standard/v1/jackson")
    suspend fun calcJackson(taxDocument: TaxDocument): TaxDocumentResult {
        return calcWebClient
            .post()
            .uri("/calc/standard/v1/jackson")
            .accept(MediaType.APPLICATION_JSON)
            .bodyValue(taxDocument)
            .retrieve()
            .bodyToFlow<TaxDocumentResult>()
            .single()
    }

    @PostMapping("/calc/standard/v1/kotlin")
    suspend fun calcKotlin(taxDocument: TaxDocument): String {
        return calcWebClient
            .post()
            .uri("/calc/standard/v1/kotlin")
            .accept(MediaType.APPLICATION_JSON)
            .bodyValue(Json.encodeToString(taxDocument))
            .retrieve()
            .bodyToFlow<String>()
            .single()
    }

    @PostMapping("/calc/standard/v1")
    suspend fun calc(taxDocument: TaxDocument): String {
        return calcWebClient
            .post()
            .uri("/calc/standard/v1")
            .accept(MediaType.APPLICATION_JSON)
            .bodyValue(Json.encodeToString(taxDocument))
            .retrieve()
            .bodyToFlow<String>()
            .single()
    }
}
