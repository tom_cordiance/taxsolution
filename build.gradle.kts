import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("org.springframework.boot") version "2.6.3" apply false
    id("io.spring.dependency-management") version "1.0.11.RELEASE" apply false
    id("org.jlleitschuh.gradle.ktlint") version "10.2.1" apply true
    id("org.jlleitschuh.gradle.ktlint-idea") version "10.2.1" apply true
    id("org.jetbrains.kotlin.jvm") version "1.6.10" apply false
    kotlin("plugin.spring") version "1.6.10" apply false
    kotlin("plugin.serialization") version "1.6.10" apply false
    idea apply true
}

allprojects {
    group = "com.modios"
    version = "2022.1.1"
    // version = java.time.LocalDateTime.now()
    //     .let { String.format("2021.1.%02d%02d%02d%02d", it.monthValue, it.dayOfMonth, it.hour, it.minute) }

    apply(plugin = "org.jlleitschuh.gradle.ktlint")
    apply(plugin = "org.jetbrains.kotlin.jvm")
    apply(plugin = "org.gradle.idea")

    repositories {
        jcenter()
        mavenCentral()
        mavenLocal()

        maven(url = "https://repo.spring.io/milestone")
    }

    dependencies {
        // Align versions of all Kotlin and Spring components
        platform("org.jetbrains.kotlin:kotlin-bom")
        enforcedPlatform(org.springframework.boot.gradle.plugin.SpringBootPlugin.BOM_COORDINATES)
    }

    tasks.withType<JavaCompile> {
        sourceCompatibility = JavaVersion.VERSION_11.toString()
        targetCompatibility = JavaVersion.VERSION_11.toString()
    }

    tasks.withType<Test> {
        useJUnitPlatform()
    }

    tasks.withType<KotlinCompile> {
        kotlinOptions {
            freeCompilerArgs = listOf("-Xjsr305=strict", "-Xopt-in=kotlin.RequiresOptIn")
            jvmTarget = JavaVersion.VERSION_11.toString()
        }
    }

    tasks.register(name = "printAllDendencies", type = DependencyReportTask::class) {}

    tasks.register("writeVersion") {
        doLast {
            java.io.FileOutputStream("$buildDir/version.txt").use {
                it.write("$version".toByteArray())
            }
        }
    }
}

subprojects {
    repositories {
        jcenter()
        mavenCentral()
    }
}
