Mac OSX - Use Homebrew to install
> brew install postgresql

To start postgresql via Homebrew
>  brew services start postgresql

Upgrade via Homebrew
Assuming you just did an upgrade without noticing that postgresql was in the list...
> brew postgresql-upgrade-database