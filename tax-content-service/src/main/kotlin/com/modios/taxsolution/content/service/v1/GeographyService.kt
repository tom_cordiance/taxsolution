package com.modios.taxsolution.content.service.v1

import com.modios.taxsolution.content.dataaccess.Geography
import com.modios.taxsolution.content.dataaccess.GeographyBuilder
import com.modios.taxsolution.content.dataaccess.GeographyRepository
import com.modios.taxsolution.content.model.v1.GeographyClassification
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.toList
import mu.KotlinLogging
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("api/v1/geography")
class GeographyService(
    private val geographyBuilder: GeographyBuilder,
    private val geographyRepository: GeographyRepository
) {
    private val logger = KotlinLogging.logger { }

    @PostMapping
    suspend fun create(@RequestBody requestGeography: com.modios.taxsolution.content.model.v1.Geography): com.modios.taxsolution.content.model.v1.Geography {
        val persistedGeography = geographyBuilder.buildNew(
            taxContentProviderId = requestGeography.taxContentProviderId,
            classificationId = requestGeography.classification.id,
            parentId = requestGeography.parentId,
            name = requestGeography.name,
            code = requestGeography.code,
        )
        geographyRepository.save(persistedGeography)

        logger.debug { "created: $persistedGeography" }

        return persistedGeography.asServiceGeography()
    }

    @GetMapping("/name")
    suspend fun getByName(@RequestParam name: String): Flow<com.modios.taxsolution.content.model.v1.Geography> {
        val result = geographyRepository.findAllByName(name).toList().map { it.asServiceGeography() }
        logger.info { "getByName($name): $result" }
        return result.asFlow()
    }

    @GetMapping("/providerAndParent")
    suspend fun getByTaxContentProviderIdAndParentId(
        @RequestParam taxContentProviderId: Long,
        @RequestParam parentId: Long
    ): Flow<com.modios.taxsolution.content.model.v1.Geography> {
        return geographyRepository.findByTaxContentProviderIdAndParentId(
            taxContentProviderId = taxContentProviderId,
            parentId = parentId
        ).map { it.asServiceGeography() }
    }

    @GetMapping("/parentAndName")
    suspend fun getByTaxContentProviderIdAndParentIdAndName(
        @RequestParam taxContentProviderId: Long,
        @RequestParam parentId: Long,
        @RequestParam name: String
    ): com.modios.taxsolution.content.model.v1.Geography? {
        return geographyRepository.findByTaxContentProviderIdAndParentIdAndName(
            taxContentProviderId = taxContentProviderId,
            parentId = parentId,
            name = name
        )?.asServiceGeography()
    }

    @GetMapping("/parentAndCode")
    suspend fun getByTaxContentProviderIdAndParentIdAndCode(
        @RequestParam taxContentProviderId: Long,
        @RequestParam parentId: Long,
        @RequestParam code: String
    ): com.modios.taxsolution.content.model.v1.Geography? {
        return geographyRepository.findByTaxContentProviderIdAndParentIdAndCode(
            taxContentProviderId = taxContentProviderId,
            parentId = parentId,
            code = code
        )?.asServiceGeography()
    }

    companion object {
        const val SERVICE_CONTEXT = "service_context"
        const val GEOGRAPHY_SERVICE_CONTEXT = "geography_service"
    }
}

fun Geography.asServiceGeography(): com.modios.taxsolution.content.model.v1.Geography {
    return com.modios.taxsolution.content.model.v1.Geography(
        taxContentProviderId = this.taxContentProviderId,
        classification = GeographyClassification.fromId(this.classificationId),
        parentId = this.parentId,
        id = this.id,
        name = this.name,
        code = this.code
    )
}
