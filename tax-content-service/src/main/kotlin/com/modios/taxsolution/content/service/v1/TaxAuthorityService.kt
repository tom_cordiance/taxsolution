package com.modios.taxsolution.content.service.v1

import com.modios.service.IdGenerator
import com.modios.taxsolution.content.dataaccess.TaxAuthority
import com.modios.taxsolution.content.dataaccess.TaxAuthorityRepository
import mu.KotlinLogging
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

interface TaxAuthorityService {

    suspend fun create(taxAuthority: com.modios.taxsolution.content.model.v1.TaxAuthority): com.modios.taxsolution.content.model.v1.TaxAuthority

    suspend fun getById(taxAuthorityId: Long): com.modios.taxsolution.content.model.v1.TaxAuthority?

    suspend fun getByTaxContentProviderIdAndName(
        taxContentProviderId: Long,
        name: String
    ): com.modios.taxsolution.content.model.v1.TaxAuthority?
}

@RestController
@RequestMapping("/api/v1/taxauthority")
class TaxAuthorityServiceImpl(private val taxAuthorityRepository: TaxAuthorityRepository) : TaxAuthorityService {

    private val logger = KotlinLogging.logger { }

    @PostMapping
    override suspend fun create(
        @RequestBody taxAuthority: com.modios.taxsolution.content.model.v1.TaxAuthority
    ): com.modios.taxsolution.content.model.v1.TaxAuthority {
        logger.debug { "create($taxAuthority)" }

        val persistedTaxAuthority = TaxAuthority(
            id = IdGenerator.instance.getNextId(),
            taxContentProviderId = taxAuthority.taxContentProviderId,
            name = taxAuthority.name,
            startDate = taxAuthority.startDate,
            endDate = taxAuthority.endDate,
            createNew = true
        )
        taxAuthorityRepository.save(persistedTaxAuthority)

        logger.debug { "created: $persistedTaxAuthority" }

        return persistedTaxAuthority.asServiceTaxAuthority()
    }

    @GetMapping("{taxAuthorityId}")
    override suspend fun getById(@PathVariable taxAuthorityId: Long): com.modios.taxsolution.content.model.v1.TaxAuthority? {
        logger.debug { "getById($taxAuthorityId)" }

        return taxAuthorityRepository.findById(taxAuthorityId)?.asServiceTaxAuthority()
    }

    @GetMapping("{taxContentProviderId}/{name}")
    override suspend fun getByTaxContentProviderIdAndName(
        @PathVariable taxContentProviderId: Long,
        @PathVariable name: String
    ): com.modios.taxsolution.content.model.v1.TaxAuthority? {
        logger.debug { "getByTaxContentProviderIdAndName($taxContentProviderId, $name)" }

        return taxAuthorityRepository
            .findByTaxContentProviderIdAndName(taxContentProviderId, name)
            ?.asServiceTaxAuthority()
    }
}

fun TaxAuthority.asServiceTaxAuthority(): com.modios.taxsolution.content.model.v1.TaxAuthority {
    return com.modios.taxsolution.content.model.v1.TaxAuthority(
        taxContentProviderId = taxContentProviderId,
        id = id,
        name = name,
        startDate = startDate,
        endDate = endDate
    )
}
