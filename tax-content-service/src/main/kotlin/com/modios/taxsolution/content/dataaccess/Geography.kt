package com.modios.taxsolution.content.dataaccess

import com.modios.service.IdGenerator
import com.modios.taxsolution.dataaccess.PersistableEntity
import org.springframework.data.annotation.Id
import org.springframework.data.annotation.PersistenceConstructor
import org.springframework.data.annotation.Transient
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import org.springframework.stereotype.Component

@Table("ts_geography")
data class Geography(
    @Id val id: Long,
    @Column val taxContentProviderId: Long,
    @Column val classificationId: Int,
    @Column val parentId: Long,
    @Column val name: String,
    @Column val code: String,
    @Transient val createNew: Boolean = false
) : PersistableEntity(id, createNew) {

    @PersistenceConstructor
    constructor(
        id: Long,
        taxContentProviderId: Long,
        classificationId: Int,
        parentId: Long,
        name: String,
        code: String
    ) : this(
        id = id,
        taxContentProviderId = taxContentProviderId,
        classificationId = classificationId,
        parentId = parentId,
        name = name,
        code = code,
        createNew = false
    )
}

@Component
class GeographyBuilder(private val idGenerator: IdGenerator) {
    fun buildNew(
        taxContentProviderId: Long,
        classificationId: Int,
        parentId: Long,
        name: String,
        code: String
    ): Geography {
        return Geography(
            id = idGenerator.getNextId(),
            taxContentProviderId = taxContentProviderId,
            classificationId = classificationId,
            parentId = parentId,
            name = name,
            code = code,
            createNew = true
        )
    }
}
