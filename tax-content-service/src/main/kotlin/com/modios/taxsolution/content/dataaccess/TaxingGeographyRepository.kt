package com.modios.taxsolution.content.dataaccess

import kotlinx.coroutines.flow.Flow
import org.springframework.data.repository.kotlin.CoroutineCrudRepository

interface TaxingGeographyRepository : CoroutineCrudRepository<TaxingGeography, Long> {

    suspend fun findByGeographyId(geographyId: Long): Flow<TaxingGeography>

    suspend fun findByTaxAuthorityId(taxAuthorityId: Long): Flow<TaxingGeography>

    suspend fun findByGeographyIdAndTaxAuthorityId(geographyId: Long, taxAuthorityId: Long): TaxingGeography?
}
