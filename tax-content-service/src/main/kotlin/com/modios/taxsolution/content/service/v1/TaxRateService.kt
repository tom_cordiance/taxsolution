package com.modios.taxsolution.content.service.v1

import com.modios.service.IdGenerator
import com.modios.taxsolution.content.dataaccess.TaxRate
import com.modios.taxsolution.content.dataaccess.TaxRateRepository
import com.modios.taxsolution.content.model.v1.TaxContentType
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.runBlocking
import mu.KotlinLogging
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.net.URL

interface TaxRateService {

    suspend fun create(taxRate: com.modios.taxsolution.content.model.v1.TaxRate): com.modios.taxsolution.content.model.v1.TaxRate

    suspend fun findByTaxAuthorityId(taxAuthorityId: Long): List<com.modios.taxsolution.content.model.v1.TaxRate>
    suspend fun findById(id: Long): com.modios.taxsolution.content.model.v1.TaxRate?

    suspend fun update(taxRate: com.modios.taxsolution.content.model.v1.TaxRate): com.modios.taxsolution.content.model.v1.TaxRate
}

@RestController
@RequestMapping("api/v1/tax-rate")
class TaxRateServiceImpl(
    private val taxRateRepository: TaxRateRepository,
    private val taxContentChangeService: TaxContentChangeService,
) : TaxRateService {

    private val logger = KotlinLogging.logger { }

    init {
        runBlocking { taxContentChangeService.addListener(URL("http://0.0.0.0:8800/calc/standard/v1/taxcontentmessage")) }
    }

    @PostMapping
    override suspend fun create(@RequestBody taxRate: com.modios.taxsolution.content.model.v1.TaxRate): com.modios.taxsolution.content.model.v1.TaxRate {
        logger.debug { "create($taxRate)" }

        val persistedTaxRate = TaxRate(
            id = IdGenerator.instance.getNextId(),
            taxContentProviderId = taxRate.taxContentProviderId,
            taxAuthorityId = taxRate.taxAuthorityId,
            code = taxRate.code,
            rate = taxRate.rate,
            startDate = taxRate.startDate,
            endDate = taxRate.endDate,
            createNew = true
        )

        taxRateRepository.save(persistedTaxRate)
        taxContentChangeService.addTaxContentUpdate(
            TaxContentUpdate(
                taxContentType = TaxContentType.TAX_RATE,
                parentId = taxRate.taxAuthorityId,
                id = taxRate.id
            )
        )

        logger.debug { "created: $persistedTaxRate" }

        return persistedTaxRate.asServiceTaxRate()
    }

    @GetMapping("/authority/{taxAuthorityId}")
    override suspend fun findByTaxAuthorityId(@PathVariable taxAuthorityId: Long): List<com.modios.taxsolution.content.model.v1.TaxRate> {
        logger.debug { "findByTaxAuthorityId($taxAuthorityId)" }

        val result = taxRateRepository.findByTaxAuthorityId(taxAuthorityId).toList()
        logger.debug { "found ${result.size} TaxRates" }

        return result.map { it.asServiceTaxRate() }
    }

    @GetMapping("{id}")
    override suspend fun findById(id: Long): com.modios.taxsolution.content.model.v1.TaxRate? {
        logger.debug { "findById($id)" }

        val result = taxRateRepository.findById(id)
        logger.debug { "found $result TaxRate" }

        return result?.asServiceTaxRate()
    }

    @PutMapping
    override suspend fun update(@RequestBody taxRate: com.modios.taxsolution.content.model.v1.TaxRate): com.modios.taxsolution.content.model.v1.TaxRate {
        logger.debug { "update($taxRate)" }

        val persistedTaxRate = taxRate.asPersistedTaxRate(createNew = false)
        taxRateRepository.save(persistedTaxRate)
        taxContentChangeService.addTaxContentUpdate(
            TaxContentUpdate(
                taxContentType = TaxContentType.TAX_RATE,
                parentId = taxRate.taxAuthorityId,
                id = taxRate.id
            )
        )

        return persistedTaxRate.asServiceTaxRate()
    }
}

fun TaxRate.asServiceTaxRate(): com.modios.taxsolution.content.model.v1.TaxRate {
    return com.modios.taxsolution.content.model.v1.TaxRate(
        taxContentProviderId = this.taxContentProviderId,
        taxAuthorityId = this.taxAuthorityId,
        id = this.id,
        code = this.code,
        rate = this.rate,
        startDate = this.startDate,
        endDate = this.endDate
    )
}

fun com.modios.taxsolution.content.model.v1.TaxRate.asPersistedTaxRate(
    createNew: Boolean,
    newId: Long? = null
): TaxRate {
    return TaxRate(
        taxContentProviderId = this.taxContentProviderId,
        taxAuthorityId = this.taxAuthorityId,
        id = when (newId) {
            null -> this.id
            else -> newId
        },
        code = this.code,
        rate = this.rate,
        startDate = this.startDate,
        endDate = this.endDate,
        createNew = createNew
    )
}
