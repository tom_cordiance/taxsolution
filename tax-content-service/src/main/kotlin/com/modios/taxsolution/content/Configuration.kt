package com.modios.taxsolution.content

import com.bugsnag.Bugsnag
import com.modios.service.IdGenerator
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.reactive.function.client.WebClient
import springfox.documentation.builders.PathSelectors
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spring.web.plugins.Docket

@Configuration
class Configuration {
    @Bean
    fun idGenerator(): IdGenerator = IdGenerator.instance

    @Bean
    fun bugsnagApiKey() = "48bd727b944f3e08bc2ead7c8e374cb8"

    @Bean
    fun bugSnag(bugsnagApiKey: String) = Bugsnag(bugsnagApiKey)

    @Bean
    fun springfox() =
        Docket(DocumentationType.OAS_30).select().apis(RequestHandlerSelectors.any()).paths(PathSelectors.any()).build()

    @Bean
    fun taxContentMessagingWebClient() = WebClient.builder().build()
}
