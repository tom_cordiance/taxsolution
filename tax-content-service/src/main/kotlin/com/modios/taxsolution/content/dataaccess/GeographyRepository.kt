package com.modios.taxsolution.content.dataaccess

import kotlinx.coroutines.flow.Flow
import org.springframework.data.repository.kotlin.CoroutineCrudRepository

interface GeographyRepository : CoroutineCrudRepository<Geography, Long> {
    suspend fun findAllByTaxContentProviderId(taxContentProviderId: Long): Flow<Geography>

    suspend fun findAllByName(name: String): Flow<Geography>

    suspend fun findByTaxContentProviderIdAndName(taxContentProviderId: Long, name: String): Flow<Geography>

    suspend fun findByTaxContentProviderIdAndParentId(
        taxContentProviderId: Long,
        parentId: Long
    ): Flow<Geography>

    suspend fun findByTaxContentProviderIdAndParentIdAndCode(
        taxContentProviderId: Long,
        parentId: Long,
        code: String
    ): Geography?

    suspend fun findByTaxContentProviderIdAndParentIdAndName(
        taxContentProviderId: Long,
        parentId: Long,
        name: String
    ): Geography?
}
