package com.modios.taxsolution.content.dataaccess

import com.modios.taxsolution.dataaccess.PersistableEntity
import org.springframework.data.annotation.Id
import org.springframework.data.annotation.PersistenceConstructor
import org.springframework.data.annotation.Transient
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import java.math.BigDecimal
import java.time.LocalDate

@Table("ts_tax_rates")
data class TaxRate(
    @Id val id: Long,
    @Column val taxContentProviderId: Long,
    @Column val taxAuthorityId: Long,
    @Column val code: String,
    @Column val rate: BigDecimal,
    @Column val startDate: LocalDate,
    @Column val endDate: LocalDate?,
    @Transient val createNew: Boolean
) : PersistableEntity(id, createNew) {

    @PersistenceConstructor
    constructor(
        id: Long,
        taxContentProviderId: Long,
        taxAuthorityId: Long,
        code: String,
        rate: BigDecimal,
        startDate: LocalDate,
        endDate: LocalDate?
    ) : this(
        id = id,
        taxContentProviderId = taxContentProviderId,
        taxAuthorityId = taxAuthorityId,
        code = code,
        rate = rate,
        startDate = startDate,
        endDate = endDate,
        createNew = false
    )
}
