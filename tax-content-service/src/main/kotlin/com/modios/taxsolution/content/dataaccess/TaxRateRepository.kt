package com.modios.taxsolution.content.dataaccess

import kotlinx.coroutines.flow.Flow
import org.springframework.data.repository.kotlin.CoroutineCrudRepository

interface TaxRateRepository : CoroutineCrudRepository<TaxRate, Long> {

    suspend fun findByTaxAuthorityId(taxAuthorityId: Long): Flow<TaxRate>
}
