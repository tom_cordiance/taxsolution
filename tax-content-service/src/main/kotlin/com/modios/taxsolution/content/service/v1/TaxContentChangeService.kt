package com.modios.taxsolution.content.service.v1

import com.modios.taxsolution.content.model.v1.MessageType
import com.modios.taxsolution.content.model.v1.TaxContentID
import com.modios.taxsolution.content.model.v1.TaxContentMessage
import com.modios.taxsolution.content.model.v1.TaxContentType
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.sync.Mutex
import mu.KotlinLogging
import org.springframework.http.MediaType
import org.springframework.stereotype.Service
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.awaitBodilessEntity
import reactor.core.publisher.Mono
import java.net.URL
import kotlin.concurrent.fixedRateTimer

data class TaxContentUpdate(
    val taxContentType: TaxContentType,
    val parentId: Long,
    val id: Long
)

interface TaxContentChangeService {
    suspend fun addListener(url: URL)
    suspend fun addTaxContentUpdate(taxContentUpdate: TaxContentUpdate)
}

@Service
class TaxContentChangeServiceImpl(private val taxContentMessagingWebClient: WebClient) :
    TaxContentChangeService {

    private val logger = KotlinLogging.logger { }

    // TODO: make configurable
    private val timerPeriod = 30000L

    private val clearAllThreshold = 10

    private val listenerSet = mutableSetOf<URL>()

    private val taxContentUpdateList = mutableListOf<TaxContentUpdate>()

    private val taxContentUpdateMutex = Mutex()

    private val taxContentUpdateChannel = Channel<List<TaxContentUpdate>>(capacity = Channel.BUFFERED)

    init {
        CoroutineScope(Dispatchers.IO)
            .launch {
                for (channelTaxContentUpdateList in taxContentUpdateChannel) {
                    logger.info { "tax update channel message: $channelTaxContentUpdateList" }
                    // start by grouping by tax content type...
                    channelTaxContentUpdateList
                        .groupBy { it.taxContentType }
                        .forEach { taxContentType, taxContentUpdateListForType ->
                            // group by parent ID
                            val taxContentUpdateByParentLists = taxContentUpdateListForType.groupBy { it.parentId }

                            taxContentUpdateByParentLists
                                .forEach { parentId, taxContentUpdateListForParent ->
                                    val message = when (taxContentUpdateListForParent.size >= clearAllThreshold) {
                                        true -> TaxContentMessage(
                                            taxContentType = taxContentType,
                                            messageType = MessageType.CLEAR_ALL,
                                            taxContentIdList = emptyList()
                                        )
                                        false -> TaxContentMessage(
                                            taxContentType = taxContentType,
                                            messageType = MessageType.CLEAR_THESE,
                                            taxContentIdList = taxContentUpdateListForParent.map {
                                                TaxContentID(
                                                    parentId = parentId,
                                                    id = parentId
                                                )
                                            }
                                        )
                                    }

                                    listenerSet.forEach {
                                        this.launch {
                                            sendMessage(url = it, taxContentMessage = message)
                                        }
                                    }
                                }
                        }
                }
            }
    }

    private val timer = fixedRateTimer(period = timerPeriod) {
        runBlocking {
            try {
                taxContentUpdateMutex.lock()
                logger.info { "sending tax content update messages[${taxContentUpdateList.size}]..." }
                taxContentUpdateChannel.send(taxContentUpdateList.map { it })
                taxContentUpdateList.clear()
            } finally {
                taxContentUpdateMutex.unlock()
            }
        }
    }

    override suspend fun addListener(url: URL) {
        listenerSet.add(url)
    }

    override suspend fun addTaxContentUpdate(taxContentUpdate: TaxContentUpdate) {
        try {
            taxContentUpdateMutex.lock()
            logger.info { "addTaxContentUpdate($taxContentUpdate)" }
            taxContentUpdateList.add(taxContentUpdate)
        } finally {
            taxContentUpdateMutex.unlock()
        }
    }

    private suspend fun sendMessage(url: URL, taxContentMessage: TaxContentMessage) {
        logger.info { "sendMessage($url, $taxContentMessage)" }
        taxContentMessagingWebClient
            .post()
            .uri(url.toURI())
            .accept(MediaType.APPLICATION_JSON)
            .body(Mono.just(taxContentMessage), TaxContentMessage::class.java)
            .retrieve()
            .awaitBodilessEntity()
    }
}
