package com.modios.taxsolution.content

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class TaxContentServiceApp

fun main(args: Array<String>) {
    runApplication<TaxContentServiceApp>(*args)
}
