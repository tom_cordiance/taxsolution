package com.modios.taxsolution.content.dataaccess

import org.springframework.data.repository.kotlin.CoroutineCrudRepository

interface TaxContentProviderRepository : CoroutineCrudRepository<TaxContentProvider, Long> {

    suspend fun findByName(name: String): TaxContentProvider?
}
