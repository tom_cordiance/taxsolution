package com.modios.taxsolution.content.dataaccess

import com.modios.taxsolution.dataaccess.PersistableEntity
import org.springframework.data.annotation.Id
import org.springframework.data.annotation.PersistenceConstructor
import org.springframework.data.annotation.Transient
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import java.time.LocalDate

@Table("ts_tax_authorities")
data class TaxAuthority(
    @Id val id: Long,
    @Column val taxContentProviderId: Long,
    @Column val name: String,
    @Column val startDate: LocalDate,
    @Column val endDate: LocalDate?,
    @Transient val createNew: Boolean
) : PersistableEntity(id, createNew) {

    @PersistenceConstructor
    constructor(id: Long, taxContentProviderId: Long, name: String, startDate: LocalDate, endDate: LocalDate?) : this(
        id = id,
        taxContentProviderId = taxContentProviderId,
        name = name,
        startDate = startDate,
        endDate = endDate,
        createNew = false
    )
}
