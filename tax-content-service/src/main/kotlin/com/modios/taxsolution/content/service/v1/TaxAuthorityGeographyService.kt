package com.modios.taxsolution.content.service.v1

import com.modios.service.IdGenerator
import com.modios.taxsolution.content.dataaccess.TaxingGeography
import com.modios.taxsolution.content.dataaccess.TaxingGeographyRepository
import kotlinx.coroutines.flow.toList
import mu.KotlinLogging
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

interface TaxAuthorityGeographyService {

    suspend fun create(
        taxContentProviderId: Long,
        geographyId: Long,
        taxAuthorityId: Long
    ): com.modios.taxsolution.content.model.v1.TaxAuthorityGeography

    suspend fun getByGeographyId(geographyId: Long): List<com.modios.taxsolution.content.model.v1.TaxAuthorityGeography>

    suspend fun getByTaxAuthorityId(taxAuthorityId: Long): List<com.modios.taxsolution.content.model.v1.TaxAuthorityGeography>

    suspend fun getByGeographyIdAndTaxAuthorityId(
        geographyId: Long,
        taxAuthorityId: Long
    ): com.modios.taxsolution.content.model.v1.TaxAuthorityGeography?
}

@RestController
@RequestMapping("api/v1/taxauthority-geography")
class TaxAuthorityGeographyServiceImpl(private val taxingGeographyRepository: TaxingGeographyRepository) :
    TaxAuthorityGeographyService {

    private val logger = KotlinLogging.logger { }

    @PostMapping("{taxContentProviderId}/{geographyId}/{taxAuthorityId}")
    override suspend fun create(
        @PathVariable taxContentProviderId: Long,
        @PathVariable geographyId: Long,
        @PathVariable taxAuthorityId: Long
    ): com.modios.taxsolution.content.model.v1.TaxAuthorityGeography {
        logger.debug { "create($taxContentProviderId, $geographyId, $taxAuthorityId)" }
        val taxingGeography = TaxingGeography(
            id = IdGenerator.instance.getNextId(),
            taxContentProviderId = taxContentProviderId,
            geographyId = geographyId,
            taxAuthorityId = taxAuthorityId,
            createNew = true
        )

        taxingGeographyRepository.save(taxingGeography)

        logger.debug { "created: $taxingGeography" }

        return taxingGeography.asServiceTaxingGeography()
    }

    @GetMapping("/geography/{geographyId}")
    override suspend fun getByGeographyId(@PathVariable geographyId: Long): List<com.modios.taxsolution.content.model.v1.TaxAuthorityGeography> {
        logger.debug { "getByGeographyId($geographyId)" }

        return taxingGeographyRepository
            .findByGeographyId(geographyId)
            .toList()
            .map { it.asServiceTaxingGeography() }
    }

    @GetMapping("/tax-authority/{taxAuthorityId}")
    override suspend fun getByTaxAuthorityId(@PathVariable taxAuthorityId: Long): List<com.modios.taxsolution.content.model.v1.TaxAuthorityGeography> {
        logger.debug { "getByTaxAuthorityId($taxAuthorityId)" }

        return taxingGeographyRepository
            .findByTaxAuthorityId(taxAuthorityId)
            .toList()
            .map { it.asServiceTaxingGeography() }
    }

    @GetMapping("{geographyId}/{taxAuthorityId}")
    override suspend fun getByGeographyIdAndTaxAuthorityId(
        @PathVariable geographyId: Long,
        @PathVariable taxAuthorityId: Long
    ): com.modios.taxsolution.content.model.v1.TaxAuthorityGeography? {
        logger.debug { "getByGeographyIdAndTaxAuthorityId($geographyId, $taxAuthorityId)" }

        return taxingGeographyRepository
            .findByGeographyIdAndTaxAuthorityId(
                geographyId = geographyId,
                taxAuthorityId = taxAuthorityId
            )?.asServiceTaxingGeography()
    }
}

fun TaxingGeography.asServiceTaxingGeography(): com.modios.taxsolution.content.model.v1.TaxAuthorityGeography {
    return com.modios.taxsolution.content.model.v1.TaxAuthorityGeography(
        id = id,
        taxContentProviderId = taxContentProviderId,
        taxAuthorityId = taxAuthorityId,
        geographyId = geographyId
    )
}
