package com.modios.taxsolution.content.dataaccess

import com.modios.taxsolution.dataaccess.PersistableEntity
import org.springframework.data.annotation.Id
import org.springframework.data.annotation.PersistenceConstructor
import org.springframework.data.annotation.Transient
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table

/**
 * Note: this object really does not need an ID. The real id is a composite ID composed of
 * geographyId and taxAuthorityId. Spring R2DBC does not support this yet...
 */
@Table("ts_taxing_geography")
data class TaxingGeography(
    @Id val id: Long,
    @Column val taxContentProviderId: Long,
    @Column val geographyId: Long,
    @Column val taxAuthorityId: Long,
    @Transient val createNew: Boolean
) : PersistableEntity(id, createNew) {

    @PersistenceConstructor
    constructor(id: Long, taxContentProviderId: Long, geographyId: Long, taxAuthorityId: Long) : this(
        id = id,
        taxContentProviderId = taxContentProviderId,
        geographyId = geographyId,
        taxAuthorityId = taxAuthorityId,
        createNew = false
    )
}
