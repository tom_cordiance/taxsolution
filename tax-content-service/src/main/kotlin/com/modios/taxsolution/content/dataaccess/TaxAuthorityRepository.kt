package com.modios.taxsolution.content.dataaccess

import org.springframework.data.repository.kotlin.CoroutineCrudRepository

interface TaxAuthorityRepository : CoroutineCrudRepository<TaxAuthority, Long> {

    suspend fun findByTaxContentProviderIdAndName(taxContentProviderId: Long, name: String): TaxAuthority?
}
