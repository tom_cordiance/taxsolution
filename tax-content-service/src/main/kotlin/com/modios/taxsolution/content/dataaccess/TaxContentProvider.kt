package com.modios.taxsolution.content.dataaccess

import com.modios.taxsolution.dataaccess.PersistableEntity
import org.springframework.data.annotation.Id
import org.springframework.data.annotation.PersistenceConstructor
import org.springframework.data.annotation.Transient
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table

@Table("ts_tax_content_providers")
data class TaxContentProvider(
    @Id val id: Long,
    @Column val name: String,
    @Transient val createNew: Boolean
) : PersistableEntity(id, createNew) {

    @PersistenceConstructor
    constructor(id: Long, name: String) : this(id = id, name = name, createNew = false)
}
