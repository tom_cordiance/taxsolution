package com.modios.taxsolution.content.service.v1

import com.modios.service.IdGenerator
import com.modios.taxsolution.content.dataaccess.TaxContentProvider
import com.modios.taxsolution.content.dataaccess.TaxContentProviderRepository
import mu.KotlinLogging
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("api/v1/taxcontentprovider")
class TaxContentProviderService(private val taxContentProviderRepository: TaxContentProviderRepository) {

    private val logger = KotlinLogging.logger { }

    @PostMapping("/name/{name}")
    suspend fun create(@PathVariable name: String): com.modios.taxsolution.content.model.v1.TaxContentProvider {
        logger.debug { "create($name)" }

        val taxContentProvider = TaxContentProvider(
            id = IdGenerator.instance.getNextId(),
            name = name,
            createNew = true
        )
        taxContentProviderRepository.save(taxContentProvider)

        logger.debug { "taxContentProvider created: $taxContentProvider" }

        return taxContentProvider.asServiceTaxContentProvider()
    }

    @GetMapping("/name/{name}")
    suspend fun get(@PathVariable name: String): com.modios.taxsolution.content.model.v1.TaxContentProvider? {
        logger.debug { "get($name)" }

        val taxContentProvider = taxContentProviderRepository.findByName(name)
        logger.debug { "taxContentProvider: $taxContentProvider" }

        return taxContentProvider?.asServiceTaxContentProvider()
    }

    @GetMapping("/id/{id}")
    suspend fun get(@PathVariable id: Long): com.modios.taxsolution.content.model.v1.TaxContentProvider? {
        logger.debug { "get($id)" }

        val taxContentProvider = taxContentProviderRepository.findById(id)
        logger.debug { "taxContentProvider: $taxContentProvider" }

        return taxContentProvider?.asServiceTaxContentProvider()
    }
}

fun TaxContentProvider.asServiceTaxContentProvider(): com.modios.taxsolution.content.model.v1.TaxContentProvider {
    return com.modios.taxsolution.content.model.v1.TaxContentProvider(id = this.id, name = this.name)
}
