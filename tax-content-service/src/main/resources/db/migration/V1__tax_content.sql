create table if not exists ts_tax_content_providers (id BIGINT NOT NULL, name VARCHAR(256) NOT NULL);
create unique index if not exists ts_tax_content_provider_id on ts_tax_content_providers (id);
create unique index if not exists ts_tax_content_provider_name on ts_tax_content_providers (name);

create table if not exists ts_geography (tax_content_provider_id BIGINT NOT NULL, id BIGINT NOT NULL, classification_id INT NOT NULL, parent_id BIGINT, name VARCHAR(256) NOT NULL, code VARCHAR(16));
create unique index if not exists ts_geography_id on ts_geography (id);
create unique index if not exists ts_geography_u1 on ts_geography (parent_id, name);
create index if not exists ts_geography_name on ts_geography (name);

create table if not exists ts_tax_authorities (tax_content_provider_id BIGINT NOT NULL, id BIGINT NOT NULL, name VARCHAR(256) NOT NULL, start_date DATE NOT NULL, end_date DATE);
create unique index if not exists ts_tax_authority_id on ts_tax_authorities (id);
create unique index if not exists ts_tax_authority_name on ts_tax_authorities (name);

create table if not exists ts_tax_rates (tax_content_provider_id BIGINT NOT NULL, id BIGINT NOT NULL, tax_authority_id BIGINT NOT NULL, code VARCHAR(256) NOT NULL, rate DECIMAL NOT NULL, start_date DATE NOT NULL, end_date DATE);
create unique index if not exists ts_tax_rate_id on ts_tax_rates (id);
create index if not exists ts_tax_rate_authority_id on ts_tax_rates (tax_authority_id);
create index if not exists ts_tax_rate_provider_authority_id on ts_tax_rates (tax_content_provider_id, tax_authority_id);

create table if not exists ts_taxing_geography (tax_content_provider_id BIGINT NOT NULL, id BIGINT NOT NULL, geography_id BIGINT NOT NULL, tax_authority_id BIGINT NOT NULL);
create unique index if not exists ts_taxing_geography_id on ts_taxing_geography (id);
create index if not exists ts_taxing_geography_geography_authority_id on ts_taxing_geography (geography_id, tax_authority_id);