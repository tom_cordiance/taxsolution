plugins {
    id("org.springframework.boot") apply true
    id("io.spring.dependency-management") apply true
    id("application")
    kotlin("plugin.spring") apply true
    kotlin("plugin.serialization") apply true
}

val logbackVersion: String by project
val logbackEcsEncoderVersion: String by project
val slf4jApiVersion: String by project
val kotlinLoggingVersion: String by project
val bugSnagVersion: String by project
val r2dbcMigrateVersion: String by project
val kotlinSerializationVersion: String by project
val springFoxVersion: String by project

dependencies {
    implementation(enforcedPlatform(org.springframework.boot.gradle.plugin.SpringBootPlugin.BOM_COORDINATES))
    implementation("javax.validation:validation-api:2.0.1.Final")

    implementation("org.springframework.boot:spring-boot-starter-actuator")
    implementation("org.springframework.boot:spring-boot-starter-data-r2dbc")
    implementation("org.springframework.boot:spring-boot-starter-webflux")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-reactor")
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:$kotlinSerializationVersion")

    // Swagger
    implementation("io.springfox:springfox-boot-starter:$springFoxVersion") {
        exclude(group = "org.slf4j", module = "slf4j-api")
    }

    implementation(project(":data-service-common"))
    implementation(project(":tax-content-service-model"))

    // Logging
    implementation("ch.qos.logback:logback-classic:$logbackVersion")
    implementation("org.slf4j:slf4j-api:$slf4jApiVersion")
    implementation("io.github.microutils:kotlin-logging:$kotlinLoggingVersion")
    implementation("com.bugsnag:bugsnag:$bugSnagVersion")
    implementation("com.bugsnag:bugsnag-spring:$bugSnagVersion")

    implementation("io.r2dbc:r2dbc-pool")
    implementation("io.r2dbc:r2dbc-postgresql")
    // db migrations for R2DBC
    implementation("name.nkonev.r2dbc-migrate:r2dbc-migrate-spring-boot-starter:$r2dbcMigrateVersion")
}

application {
    // Define the main class for the application.
    mainClassName = "com.modios.taxsolution.content.TaxContentServiceAppKt"
}

tasks.getByName<org.springframework.boot.gradle.tasks.bundling.BootBuildImage>("bootBuildImage") {
    imageName = "nexus.dev.modios.io/tax-content-service:${project.version}"
    environment = mapOf()
}

tasks.test {
    useJUnitPlatform()
}
