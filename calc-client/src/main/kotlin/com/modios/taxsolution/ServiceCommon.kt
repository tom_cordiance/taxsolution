package com.modios.taxsolution

import com.modios.taxsolution.calc.model.v1.TransactionLocation

object ServiceCommon {

    const val TAX_CONTENT_PROVIDER = "Cordiance US Tax Content"

    const val TENANT_NAME = "Modios"

    const val COMPANY_NAME = "ModCo"

    val ALABAMA_TAXING_LOCATION = TransactionLocation(
        country = USGeography.UNITED_STATES.code,
        state = USGeography.ALABAMA.code
    )
    val ALASKA_TAXING_LOCATION = TransactionLocation(
        country = USGeography.UNITED_STATES.code,
        state = USGeography.ALASKA.code
    )
    val ARIZONA_TAXING_LOCATION = TransactionLocation(
        country = USGeography.UNITED_STATES.code,
        state = USGeography.ARIZONA.code
    )
    val ARKANSAS_TAXING_LOCATION = TransactionLocation(
        country = USGeography.UNITED_STATES.code,
        state = USGeography.ARKANSAS.code
    )
    val CALIFORNIA_TAXING_LOCATION = TransactionLocation(
        country = USGeography.UNITED_STATES.code,
        state = USGeography.CALIFORNIA.code
    )
    val CONNECTICUT_TAXING_LOCATION = TransactionLocation(
        country = USGeography.UNITED_STATES.code,
        state = USGeography.CONNECTICUT.code
    )
    val DELAWARE_TAXING_LOCATION = TransactionLocation(
        country = USGeography.UNITED_STATES.code,
        state = USGeography.DELAWARE.code
    )
    val FLORIDA_TAXING_LOCATION = TransactionLocation(
        country = USGeography.UNITED_STATES.code,
        state = USGeography.FLORIDA.code
    )
    val GEORGIA_TAXING_LOCATION = TransactionLocation(
        country = USGeography.UNITED_STATES.code,
        state = USGeography.GEORGIA.code
    )
    val HAWAII_TAXING_LOCATION = TransactionLocation(
        country = USGeography.UNITED_STATES.code,
        state = USGeography.HAWAII.code
    )
    val IDAHO_TAXING_LOCATION = TransactionLocation(
        country = USGeography.UNITED_STATES.code,
        state = USGeography.IDAHO.code
    )
    val ILLINOIS_TAXING_LOCATION = TransactionLocation(
        country = USGeography.UNITED_STATES.code,
        state = USGeography.ILLINOIS.code
    )
    val INDIANA_TAXING_LOCATION = TransactionLocation(
        country = USGeography.UNITED_STATES.code,
        state = USGeography.INDIANA.code
    )
    val IOWA_TAXING_LOCATION = TransactionLocation(
        country = USGeography.UNITED_STATES.code,
        state = USGeography.IOWA.code
    )
    val KANSAS_TAXING_LOCATION = TransactionLocation(
        country = USGeography.UNITED_STATES.code,
        state = USGeography.KANSAS.code
    )
    val KENTUCKY_TAXING_LOCATION = TransactionLocation(
        country = USGeography.UNITED_STATES.code,
        state = USGeography.KENTUCKY.code
    )
    val LOUISIANA_TAXING_LOCATION = TransactionLocation(
        country = USGeography.UNITED_STATES.code,
        state = USGeography.LOUISIANA.code
    )
    val MAINE_TAXING_LOCATION = TransactionLocation(
        country = USGeography.UNITED_STATES.code,
        state = USGeography.MAINE.code
    )
    val MARYLAND_TAXING_LOCATION = TransactionLocation(
        country = USGeography.UNITED_STATES.code,
        state = USGeography.MARYLAND.code
    )
    val MASSACHUSETTS_TAXING_LOCATION = TransactionLocation(
        country = USGeography.UNITED_STATES.code,
        state = USGeography.MASSACHUSETTS.code
    )
    val MICHIGAN_TAXING_LOCATION = TransactionLocation(
        country = USGeography.UNITED_STATES.code,
        state = USGeography.MICHIGAN.code
    )
    val MINNESOTA_TAXING_LOCATION = TransactionLocation(
        country = USGeography.UNITED_STATES.code,
        state = USGeography.MINNESOTA.code
    )
    val MISSISSIPPI_TAXING_LOCATION = TransactionLocation(
        country = USGeography.UNITED_STATES.code,
        state = USGeography.MISSISSIPPI.code
    )
    val MISSOURI_TAXING_LOCATION = TransactionLocation(
        country = USGeography.UNITED_STATES.code,
        state = USGeography.MISSOURI.code
    )
    val MONTANA_TAXING_LOCATION = TransactionLocation(
        country = USGeography.UNITED_STATES.code,
        state = USGeography.MONTANA.code
    )
    val NEBRASKA_TAXING_LOCATION = TransactionLocation(
        country = USGeography.UNITED_STATES.code,
        state = USGeography.NEBRASKA.code
    )
    val NEVADA_TAXING_LOCATION = TransactionLocation(
        country = USGeography.UNITED_STATES.code,
        state = USGeography.NEVADA.code
    )
    val NEW_HAMPSHIRE_TAXING_LOCATION = TransactionLocation(
        country = USGeography.UNITED_STATES.code,
        state = USGeography.NEW_HAMPSHIRE.code
    )
    val NEW_JERSEY_TAXING_LOCATION = TransactionLocation(
        country = USGeography.UNITED_STATES.code,
        state = USGeography.NEW_JERSEY.code
    )
    val NEW_MEXICO_TAXING_LOCATION = TransactionLocation(
        country = USGeography.UNITED_STATES.code,
        state = USGeography.NEW_MEXICO.code
    )
    val NEW_YORK_TAXING_LOCATION = TransactionLocation(
        country = USGeography.UNITED_STATES.code,
        state = USGeography.NEW_YORK.code
    )
    val NORTH_CAROLINA_TAXING_LOCATION = TransactionLocation(
        country = USGeography.UNITED_STATES.code,
        state = USGeography.NORTH_CAROLINA.code
    )
    val NORTH_DAKOTA_TAXING_LOCATION = TransactionLocation(
        country = USGeography.UNITED_STATES.code,
        state = USGeography.NORTH_DAKOTA.code
    )
    val OHIO_TAXING_LOCATION = TransactionLocation(
        country = USGeography.UNITED_STATES.code,
        state = USGeography.OHIO.code
    )
    val OKLAHOMA_TAXING_LOCATION = TransactionLocation(
        country = USGeography.UNITED_STATES.code,
        state = USGeography.OKLAHOMA.code
    )
    val OREGON_TAXING_LOCATION = TransactionLocation(
        country = USGeography.UNITED_STATES.code,
        state = USGeography.OREGON.code
    )
    val PENNSYLVANIA_TAXING_LOCATION = TransactionLocation(
        country = USGeography.UNITED_STATES.code,
        state = USGeography.PENNSYLVANIA.code
    )
    val RHODE_ISLAND_TAXING_LOCATION = TransactionLocation(
        country = USGeography.UNITED_STATES.code,
        state = USGeography.RHODE_ISLAND.code
    )
    val SOUTH_CAROLINA_TAXING_LOCATION = TransactionLocation(
        country = USGeography.UNITED_STATES.code,
        state = USGeography.SOUTH_CAROLINA.code
    )
    val SOUTH_DAKOTA_TAXING_LOCATION = TransactionLocation(
        country = USGeography.UNITED_STATES.code,
        state = USGeography.SOUTH_DAKOTA.code
    )
    val TENNESSEE_TAXING_LOCATION = TransactionLocation(
        country = USGeography.UNITED_STATES.code,
        state = USGeography.TENNESSEE.code
    )
    val TEXAS_TAXING_LOCATION = TransactionLocation(
        country = USGeography.UNITED_STATES.code,
        state = USGeography.TEXAS.code
    )
    val UTAH_TAXING_LOCATION = TransactionLocation(
        country = USGeography.UNITED_STATES.code,
        state = USGeography.UTAH.code
    )
    val VERMONT_TAXING_LOCATION = TransactionLocation(
        country = USGeography.UNITED_STATES.code,
        state = USGeography.VERMONT.code
    )
    val VIRGINIA_TAXING_LOCATION = TransactionLocation(
        country = USGeography.UNITED_STATES.code,
        state = USGeography.VIRGINIA.code
    )
    val WASHINGTON_TAXING_LOCATION = TransactionLocation(
        country = USGeography.UNITED_STATES.code,
        state = USGeography.WASHINGTON.code
    )
    val WEST_VIRGINIA_TAXING_LOCATION = TransactionLocation(
        country = USGeography.UNITED_STATES.code,
        state = USGeography.WEST_VIRGINIA.code
    )
    val WISCONSIN_TAXING_LOCATION = TransactionLocation(
        country = USGeography.UNITED_STATES.code,
        state = USGeography.WISCONSIN.code
    )
    val WYOMING_TAXING_LOCATION = TransactionLocation(
        country = USGeography.UNITED_STATES.code,
        state = USGeography.WYOMING.code
    )

    val TAXING_LOCATIONS =
        listOf(
            ARIZONA_TAXING_LOCATION,
            CALIFORNIA_TAXING_LOCATION,
            FLORIDA_TAXING_LOCATION,
            NEW_YORK_TAXING_LOCATION,
            TEXAS_TAXING_LOCATION,
            ALABAMA_TAXING_LOCATION,
            ALASKA_TAXING_LOCATION,
            ARIZONA_TAXING_LOCATION,
            ARKANSAS_TAXING_LOCATION,
            CALIFORNIA_TAXING_LOCATION,
            CONNECTICUT_TAXING_LOCATION,
            DELAWARE_TAXING_LOCATION,
            FLORIDA_TAXING_LOCATION,
            GEORGIA_TAXING_LOCATION,
            HAWAII_TAXING_LOCATION,
            IDAHO_TAXING_LOCATION,
            ILLINOIS_TAXING_LOCATION,
            INDIANA_TAXING_LOCATION,
            IOWA_TAXING_LOCATION,
            KANSAS_TAXING_LOCATION,
            KENTUCKY_TAXING_LOCATION,
            LOUISIANA_TAXING_LOCATION,
            MAINE_TAXING_LOCATION,
            MARYLAND_TAXING_LOCATION,
            MASSACHUSETTS_TAXING_LOCATION,
            MICHIGAN_TAXING_LOCATION,
            MINNESOTA_TAXING_LOCATION,
            MISSISSIPPI_TAXING_LOCATION,
            MISSOURI_TAXING_LOCATION,
            MONTANA_TAXING_LOCATION,
            NEBRASKA_TAXING_LOCATION,
            NEVADA_TAXING_LOCATION,
            NEW_HAMPSHIRE_TAXING_LOCATION,
            NEW_JERSEY_TAXING_LOCATION,
            NEW_MEXICO_TAXING_LOCATION,
            NEW_YORK_TAXING_LOCATION,
            NORTH_CAROLINA_TAXING_LOCATION,
            NORTH_DAKOTA_TAXING_LOCATION,
            OHIO_TAXING_LOCATION,
            OKLAHOMA_TAXING_LOCATION,
            OREGON_TAXING_LOCATION,
            PENNSYLVANIA_TAXING_LOCATION,
            RHODE_ISLAND_TAXING_LOCATION,
            SOUTH_CAROLINA_TAXING_LOCATION,
            SOUTH_DAKOTA_TAXING_LOCATION,
            TENNESSEE_TAXING_LOCATION,
            TEXAS_TAXING_LOCATION,
            UTAH_TAXING_LOCATION,
            VERMONT_TAXING_LOCATION,
            VIRGINIA_TAXING_LOCATION,
            WASHINGTON_TAXING_LOCATION,
            WEST_VIRGINIA_TAXING_LOCATION,
            WISCONSIN_TAXING_LOCATION,
            WYOMING_TAXING_LOCATION
        )
}
