package com.modios.taxsolution

import com.modios.taxsolution.calc.client.v1.TaxDocumentClient
import com.modios.taxsolution.calc.model.v1.TaxDocument
import com.modios.taxsolution.calc.model.v1.TaxDocumentItem
import com.modios.taxsolution.calc.model.v1.TaxDocumentResult
import com.modios.taxsolution.calc.model.v1.TransactionLocation
import com.modios.taxsolution.content.client.v1.GeographyClientImpl
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import java.math.BigDecimal
import java.time.LocalDateTime
import java.util.concurrent.Executors
import java.util.concurrent.atomic.AtomicLong
import kotlin.math.abs
import kotlin.random.Random
import kotlin.time.Duration
import kotlin.time.ExperimentalTime
import kotlin.time.measureTimedValue

fun main(args: Array<String>) {
    SpringApplication(TaxCalculationClient::class.java).run(*args)
}

data class CalculationInfo @OptIn(ExperimentalTime::class) constructor(val numberOfCalcs: Int, val duration: Duration) {
    @OptIn(ExperimentalTime::class)
    fun averageCalcTime() = duration.div(numberOfCalcs)
}

@SpringBootApplication
class TaxCalculationClient : CommandLineRunner {

    private val logger = KotlinLogging.logger { }

    @Autowired
    private lateinit var geographyClient: GeographyClientImpl

    @Autowired
    private lateinit var calcClient: TaxDocumentClient

    private val runCalcSet = true
    private val persistResults = true
    private val persistPercentage = 4
    private val numberOfCalculations = 10000
    private val numberOfExecutions = 4
    private val concurrency = 16
    private val kotlinSerialization = true
    private val ktorServer = true

    private val dispatcher = Executors.newFixedThreadPool(concurrency).asCoroutineDispatcher()

    // private lateinit var usStates: List<TransactionLocation>

    private val counter = AtomicLong(0L)

    private fun testTaxDocument(): TaxDocument {
        return TaxDocument(
            tenantName = ServiceCommon.TENANT_NAME,
            companyName = ServiceCommon.COMPANY_NAME,
            persistResult = persistResults && (abs(Random.nextInt() % 100) <= persistPercentage),
            documentDate = LocalDateTime.now(),
            documentNumber = counter.incrementAndGet().toString(),
            shipFrom = ServiceCommon.TAXING_LOCATIONS.random(),
            shipTo = ServiceCommon.TAXING_LOCATIONS.random()
        ).apply {
            taxDocumentItems.add(TaxDocumentItem(index = 1, quantity = 1, amount = BigDecimal.valueOf(100)))
        }
    }

    suspend fun getUSTransactionLocations(): List<TransactionLocation> =
        geographyClient
            .getAllByName("UNITED STATES")
            .first()
            .let { us ->
                geographyClient.getByTaxContentProviderIdAndParentId(
                    taxContentProviderId = us.taxContentProviderId,
                    parentId = us.id
                ).map {
                    TransactionLocation(country = us.code, state = it.code)
                }
            }

    suspend fun testTaxCalculationJackson(): TaxDocumentResult {
        val result = calcClient.calcJackson(testTaxDocument())

        return result
    }

    suspend fun testTaxCalculationKotlin(): TaxDocumentResult {
        val result = Json.decodeFromString<TaxDocumentResult>(calcClient.calcKotlin(testTaxDocument()))

        return result
    }

    suspend fun testTaxCalculation(): TaxDocumentResult {
        val result = Json.decodeFromString<TaxDocumentResult>(calcClient.calc(testTaxDocument()))

        return result
    }

    @OptIn(ExperimentalTime::class)
    private suspend fun doCalc(numberOfCalcs: Int, coroutineScope: CoroutineScope): Deferred<CalculationInfo> =
        coroutineScope.async {
            val timedValue = measureTimedValue {
                (1..numberOfCalcs).forEach {
                    val taxDocumentResult = when (kotlinSerialization) {
                        true -> when (ktorServer) {
                            true -> testTaxCalculation()
                            false -> testTaxCalculationKotlin()
                        }
                        false -> testTaxCalculationJackson()
                    }
//                    logger.info { taxDocumentResult }
                }
            }

            CalculationInfo(numberOfCalcs = numberOfCalcs, duration = timedValue.duration)
        }

    @ExperimentalTime
    private suspend fun runCalcSet() {
        val coroutineScope = CoroutineScope(dispatcher)

        listOf(
            1 to 100,
            1 to 10000,
            4 to 10000,
            8 to 10000,
            16 to 10000,
            32 to 10000
        ).forEach { (executions, numberOfCalcs) ->
            logger.info { "Executing $executions calc sessions, $numberOfCalcs calculations per session" }
            (1..executions)
                .map { doCalc(numberOfCalcs, coroutineScope) }
                .awaitAll()
                .sortedBy { it.averageCalcTime().inWholeMicroseconds }
                .forEach { calculationInfo ->
                    logger.info { "$calculationInfo - average ${calculationInfo.averageCalcTime()}" }
                }
            delay(2500)
        }
    }

    @ExperimentalTime
    private suspend fun runOneSet() {
        CoroutineScope(dispatcher).run {
            (1..numberOfExecutions)
                .map { doCalc(numberOfCalculations, this) }
                .awaitAll()
                .sortedBy { it.averageCalcTime().inWholeMicroseconds }
                .forEach { calculationInfo -> logger.info { "$calculationInfo - average ${calculationInfo.averageCalcTime()}" } }
        }
    }

    @OptIn(ExperimentalTime::class)
    override fun run(vararg args: String?) {
        runBlocking {
            try {
                // usStates = getUSTransactionLocations()

                when (runCalcSet) {
                    true -> runCalcSet()
                    false -> runOneSet()
                }
            } catch (exception: Exception) {
                logger.error(exception) { "Calc error" }
            } finally {
                System.exit(0)
            }
        }
    }
}
