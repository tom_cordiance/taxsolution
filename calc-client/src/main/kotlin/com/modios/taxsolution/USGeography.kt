package com.modios.taxsolution

import com.modios.taxsolution.content.model.v1.Geography
import com.modios.taxsolution.content.model.v1.GeographyClassification

object USGeography {
    fun createGeography(
        classification: GeographyClassification,
        name: String,
        code: String = ""
    ) = Geography(
        taxContentProviderId = 0L,
        id = 0L,
        parentId = 0L,
        classification = classification,
        name = name,
        code = code
    )

    val UNITED_STATES = createGeography(
        classification = GeographyClassification.COUNTRY,
        name = "UNITED STATES",
        code = "US"
    )

    val ALABAMA = createGeography(classification = GeographyClassification.STATE, name = "ALABAMA", code = "AL")
    val ALASKA = createGeography(classification = GeographyClassification.STATE, name = "ALASKA", code = "AK")
    val ARIZONA = createGeography(classification = GeographyClassification.STATE, name = "ARIZONA", code = "AZ")
    val ARKANSAS = createGeography(classification = GeographyClassification.STATE, name = "ARKANSAS", code = "AR")
    val CALIFORNIA = createGeography(classification = GeographyClassification.STATE, name = "CALIFORNIA", code = "CA")
    val CONNECTICUT = createGeography(classification = GeographyClassification.STATE, name = "CONNECTICUT", code = "CT")
    val DELAWARE = createGeography(classification = GeographyClassification.STATE, name = "DELAWARE", code = "DE")
    val FLORIDA = createGeography(classification = GeographyClassification.STATE, name = "FLORIDA", code = "FL")
    val GEORGIA = createGeography(classification = GeographyClassification.STATE, name = "GEORGIA", code = "GA")
    val HAWAII = createGeography(classification = GeographyClassification.STATE, name = "HAWAII", code = "HI")
    val IDAHO = createGeography(classification = GeographyClassification.STATE, name = "IDAHO", code = "ID")
    val ILLINOIS = createGeography(classification = GeographyClassification.STATE, name = "ILLINOIS", code = "IL")
    val INDIANA = createGeography(classification = GeographyClassification.STATE, name = "INDIANA", code = "IN")
    val IOWA = createGeography(classification = GeographyClassification.STATE, name = "IOWA", code = "IA")
    val KANSAS = createGeography(classification = GeographyClassification.STATE, name = "KANSAS", code = "KS")
    val KENTUCKY = createGeography(classification = GeographyClassification.STATE, name = "KENTUCKY", code = "KY")
    val LOUISIANA = createGeography(classification = GeographyClassification.STATE, name = "LOUISIANA", code = "LA")
    val MAINE = createGeography(classification = GeographyClassification.STATE, name = "MAINE", code = "ME")
    val MARYLAND = createGeography(classification = GeographyClassification.STATE, name = "MARYLAND", code = "MD")
    val MASSACHUSETTS =
        createGeography(classification = GeographyClassification.STATE, name = "MASSACHUSETTS", code = "MA")
    val MICHIGAN = createGeography(classification = GeographyClassification.STATE, name = "MICHIGAN", code = "MI")
    val MINNESOTA = createGeography(classification = GeographyClassification.STATE, name = "MINNESOTA", code = "MN")
    val MISSISSIPPI = createGeography(classification = GeographyClassification.STATE, name = "MISSISSIPPI", code = "MS")
    val MISSOURI = createGeography(classification = GeographyClassification.STATE, name = "MISSOURI", code = "MO")
    val MONTANA = createGeography(classification = GeographyClassification.STATE, name = "MONTANA", code = "MT")
    val NEBRASKA = createGeography(classification = GeographyClassification.STATE, name = "NEBRASKA", code = "NE")
    val NEVADA = createGeography(classification = GeographyClassification.STATE, name = "NEVADA", code = "NV")
    val NEW_HAMPSHIRE =
        createGeography(classification = GeographyClassification.STATE, name = "NEW HAMPSHIRE", code = "NH")
    val NEW_JERSEY = createGeography(classification = GeographyClassification.STATE, name = "NEW JERSEY", code = "NJ")
    val NEW_MEXICO = createGeography(classification = GeographyClassification.STATE, name = "NEW MEXICO", code = "NM")
    val NEW_YORK = createGeography(classification = GeographyClassification.STATE, name = "NEW YORK", code = "NY")
    val NORTH_CAROLINA =
        createGeography(classification = GeographyClassification.STATE, name = "NORTH CAROLINA", code = "NC")
    val NORTH_DAKOTA =
        createGeography(classification = GeographyClassification.STATE, name = "NORTH DAKOTA", code = "ND")
    val OHIO = createGeography(classification = GeographyClassification.STATE, name = "OHIO", code = "OH")
    val OKLAHOMA = createGeography(classification = GeographyClassification.STATE, name = "OKLAHOMA", code = "OK")
    val OREGON = createGeography(classification = GeographyClassification.STATE, name = "OREGON", code = "OR")
    val PENNSYLVANIA =
        createGeography(classification = GeographyClassification.STATE, name = "PENNSYLVANIA", code = "PA")
    val RHODE_ISLAND =
        createGeography(classification = GeographyClassification.STATE, name = "RHODE ISLAND", code = "RI")
    val SOUTH_CAROLINA =
        createGeography(classification = GeographyClassification.STATE, name = "SOUTH CAROLINA", code = "SC")
    val SOUTH_DAKOTA =
        createGeography(classification = GeographyClassification.STATE, name = "SOUTH DAKOTA", code = "SD")
    val TENNESSEE = createGeography(classification = GeographyClassification.STATE, name = "TENNESSEE", code = "TN")
    val TEXAS = createGeography(classification = GeographyClassification.STATE, name = "TEXAS", code = "TX")
    val UTAH = createGeography(classification = GeographyClassification.STATE, name = "UTAH", code = "UT")
    val VERMONT = createGeography(classification = GeographyClassification.STATE, name = "VERMONT", code = "VT")
    val VIRGINIA = createGeography(classification = GeographyClassification.STATE, name = "VIRGINIA", code = "VA")
    val WASHINGTON = createGeography(classification = GeographyClassification.STATE, name = "WASHINGTON", code = "WA")
    val WEST_VIRGINIA =
        createGeography(classification = GeographyClassification.STATE, name = "WEST VIRGINIA", code = "WV")
    val WISCONSIN = createGeography(classification = GeographyClassification.STATE, name = "WISCONSIN", code = "WI")
    val WYOMING = createGeography(classification = GeographyClassification.STATE, name = "WYOMING", code = "WY")

    val US_STATES = listOf(
        ALABAMA,
        ALASKA,
        ARIZONA,
        ARKANSAS,
        CALIFORNIA,
        CONNECTICUT,
        DELAWARE,
        FLORIDA,
        GEORGIA,
        HAWAII,
        IDAHO,
        ILLINOIS,
        INDIANA,
        IOWA,
        KANSAS,
        KENTUCKY,
        LOUISIANA,
        MAINE,
        MARYLAND,
        MASSACHUSETTS,
        MICHIGAN,
        MINNESOTA,
        MISSISSIPPI,
        MISSOURI,
        MONTANA,
        NEBRASKA,
        NEVADA,
        NEW_HAMPSHIRE,
        NEW_JERSEY,
        NEW_MEXICO,
        NEW_YORK,
        NORTH_CAROLINA,
        NORTH_DAKOTA,
        OHIO,
        OKLAHOMA,
        OREGON,
        PENNSYLVANIA,
        RHODE_ISLAND,
        SOUTH_CAROLINA,
        SOUTH_DAKOTA,
        TENNESSEE,
        TEXAS,
        UTAH,
        VERMONT,
        VIRGINIA,
        WASHINGTON,
        WEST_VIRGINIA,
        WISCONSIN,
        WYOMING
    )
}
