package com.modios.taxsolution

// import com.fasterxml.jackson.annotation.JsonInclude
// import com.fasterxml.jackson.databind.ObjectMapper
// import com.fasterxml.jackson.databind.module.SimpleModule
// import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
// import com.fasterxml.jackson.module.kotlin.KotlinModule
// import com.modios.taxsolution.calc.model.v1.TaxDocument
// import com.modios.taxsolution.calc.model.v1.TaxDocumentItem
// import com.modios.taxsolution.calc.model.v1.TaxDocumentItemJsonSerializer
// import com.modios.taxsolution.calc.model.v1.TaxDocumentJsonDeserializer
// import com.modios.taxsolution.calc.model.v1.TaxDocumentJsonSerializer
// import com.modios.taxsolution.calc.model.v1.TransactionLocation
// import com.modios.taxsolution.calc.model.v1.TransactionLocationSerializer
import mu.KotlinLogging
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
// import org.springframework.context.annotation.Primary
import org.springframework.web.reactive.function.client.WebClient

@Configuration
class Configuration {

    private val logger = KotlinLogging.logger { }

    @Bean
    fun taxContentWebClient() = WebClient.builder().baseUrl("http://0.0.0.0:8002/").build()

    @Bean
    fun calcWebClient() = WebClient.builder().baseUrl("http://0.0.0.0:8800/").build()

    // @Bean
    // @Primary
    // fun objectMapper(): ObjectMapper = ObjectMapper().also {
    //     logger.info { "Custom object mapper created..." }
    //     it.setSerializationInclusion(JsonInclude.Include.NON_NULL)
    //     it.registerModule(KotlinModule())
    //     it.registerModule(JavaTimeModule())
    //     it.registerModule(
    //         SimpleModule().also {
    //             it.addSerializer(TaxDocument::class.java, TaxDocumentJsonSerializer())
    //             it.addSerializer(TransactionLocation::class.java, TransactionLocationSerializer())
    //             it.addSerializer(TaxDocumentItem::class.java, TaxDocumentItemJsonSerializer())
    //             it.addDeserializer(TaxDocument::class.java, TaxDocumentJsonDeserializer())
    //         }
    //     )
    // }
}
