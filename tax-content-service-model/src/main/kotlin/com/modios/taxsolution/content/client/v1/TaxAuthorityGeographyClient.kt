package com.modios.taxsolution.content.client.v1

import com.modios.taxsolution.content.model.v1.TaxAuthorityGeography

interface TaxAuthorityGeographyClient {
    suspend fun create(
        taxContentProviderId: Long,
        geographyId: Long,
        taxAuthorityId: Long
    ): TaxAuthorityGeography

    suspend fun getByGeographyId(geographyId: Long): List<TaxAuthorityGeography>

    suspend fun getByTaxAuthorityId(taxAuthorityId: Long): List<TaxAuthorityGeography>

    suspend fun getByGeographyIdAndTaxAuthorityId(
        geographyId: Long,
        taxAuthorityId: Long
    ): TaxAuthorityGeography?
}
