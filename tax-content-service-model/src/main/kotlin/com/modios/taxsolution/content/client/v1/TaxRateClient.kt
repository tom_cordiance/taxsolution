package com.modios.taxsolution.content.client.v1

import com.modios.taxsolution.content.model.v1.TaxRate

interface TaxRateClient {

    suspend fun create(taxRate: TaxRate): TaxRate

    suspend fun findByTaxAuthorityId(taxAuthorityId: Long): List<TaxRate>

    suspend fun findById(id: Long): TaxRate?

    suspend fun update(taxRate: TaxRate): TaxRate
}
