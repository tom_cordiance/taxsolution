package com.modios.taxsolution.content.model.v1

import kotlinx.serialization.Serializable

/**
 * Represents the Geography that a TaxAuthority has jurisdiction
 */
@Serializable
data class TaxAuthorityGeography(val id: Long, val taxContentProviderId: Long, val geographyId: Long, val taxAuthorityId: Long)
