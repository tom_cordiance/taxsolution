package com.modios.taxsolution.content.client.v1

import com.modios.taxsolution.content.model.v1.TaxAuthority
import java.time.LocalDate

interface TaxAuthorityClient {
    suspend fun create(
        taxContentProviderId: Long,
        name: String,
        startDate: LocalDate,
        endDate: LocalDate?
    ): TaxAuthority

    suspend fun getById(taxAuthorityId: Long): TaxAuthority?

    suspend fun getByTaxContentProviderIdAndName(
        taxContentProviderId: Long,
        name: String
    ): TaxAuthority?
}
