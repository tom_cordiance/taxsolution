package com.modios.taxsolution.content.client.v1

import com.modios.taxsolution.content.model.v1.TaxContentProvider

interface TaxContentProviderClient {

    suspend fun create(name: String): TaxContentProvider

    suspend fun get(name: String): TaxContentProvider?

    suspend fun get(id: Long): TaxContentProvider?
}
