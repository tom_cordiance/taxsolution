package com.modios.taxsolution.content.client.v1

import com.modios.taxsolution.content.model.v1.Geography

interface GeographyClient {
    suspend fun create(geography: Geography): Geography

    suspend fun create(
        taxContentProviderId: Long,
        classificationId: Int,
        parentId: Long,
        name: String,
        code: String
    ): Geography

    suspend fun getAllByName(name: String): List<Geography>

    suspend fun getByTaxContentProviderIdAndParentIdAndName(
        taxContentProviderId: Long,
        parentId: Long,
        name: String
    ): Geography?

    suspend fun getByTaxContentProviderIdAndParentIdAndCode(
        taxContentProviderId: Long,
        parentId: Long,
        code: String
    ): Geography?

    suspend fun getByTaxContentProviderIdAndParentId(taxContentProviderId: Long, parentId: Long): List<Geography>
}
