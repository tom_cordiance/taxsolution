package com.modios.taxsolution.content.model.v1

import com.modios.service.LocalDateKSerializer
import kotlinx.serialization.Serializable
import java.time.LocalDate

/**
 * Represents an entity that can tax
 */
@Serializable
data class TaxAuthority(
    val taxContentProviderId: Long,
    val id: Long,
    val name: String,
    @kotlinx.serialization.Serializable(with = LocalDateKSerializer::class)
    val startDate: LocalDate,
    @kotlinx.serialization.Serializable(with = LocalDateKSerializer::class)
    val endDate: LocalDate?
)
