package com.modios.taxsolution.content.model.v1

import com.modios.service.BigDecimalKSerializer
import com.modios.service.LocalDateKSerializer
import kotlinx.serialization.Serializable
import java.math.BigDecimal
import java.time.LocalDate

@Serializable
data class TaxRate(
    val taxContentProviderId: Long,
    val taxAuthorityId: Long,
    val id: Long,
    val code: String,
    @kotlinx.serialization.Serializable(with = BigDecimalKSerializer::class)
    val rate: BigDecimal,
    @kotlinx.serialization.Serializable(with = LocalDateKSerializer::class)
    val startDate: LocalDate,
    @kotlinx.serialization.Serializable(with = LocalDateKSerializer::class)
    val endDate: LocalDate?
)
