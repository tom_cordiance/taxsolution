package com.modios.taxsolution.content.model.v1

import kotlinx.serialization.Serializable

@Serializable
data class TaxContentProvider(
    val id: Long,
    val name: String
)
