package com.modios.taxsolution.content.model.v1

import kotlinx.serialization.Serializable

/**
 * Represents geography globally
 */
@Serializable
data class Geography(
    val taxContentProviderId: Long,
    val classification: GeographyClassification,
    val id: Long,
    val parentId: Long,
    val name: String,
    val code: String = ""
) {
    companion object {
        const val WORLD_ID = -1L

        val WORLD = Geography(
            taxContentProviderId = -1L,
            classification = GeographyClassification.WORLD,
            id = WORLD_ID,
            parentId = -1L,
            name = "World",
            code = "WORLD"
        )
    }
}

@Serializable
enum class GeographyClassification(val id: Int) {
    UNKNOWN(-1),
    WORLD(0),
    GROUP(1),
    COUNTRY(2),
    PROVINCE(3),
    STATE(4),
    COUNTY(5),
    CITY(6),
    DISTRICT(7),
    POSTCODE(8),
    GEOCODE(9);

    companion object {
        private val idMap = GeographyClassification.values().associateBy(GeographyClassification::id)

        fun fromId(id: Int): GeographyClassification {
            val geographyClassification = idMap[id]

            return when (geographyClassification) {
                null -> UNKNOWN
                else -> geographyClassification
            }
        }
    }
}
