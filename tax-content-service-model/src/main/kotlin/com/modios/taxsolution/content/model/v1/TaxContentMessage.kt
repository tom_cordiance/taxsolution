package com.modios.taxsolution.content.model.v1

@kotlinx.serialization.Serializable
enum class TaxContentType {
    TAX_AUTHORITY, TAX_AUTHORITY_GEOGRAPHY, GEOGRAPHY, TAX_RATE
}

@kotlinx.serialization.Serializable
enum class MessageType {
    CLEAR_ALL, CLEAR_THESE
}

@kotlinx.serialization.Serializable
data class TaxContentID(
    val parentId: Long,
    val id: Long
)

@kotlinx.serialization.Serializable
data class TaxContentMessage(
    val taxContentType: TaxContentType,
    val messageType: MessageType,
    val taxContentIdList: List<TaxContentID> = emptyList()
)
