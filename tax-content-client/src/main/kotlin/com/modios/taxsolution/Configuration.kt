package com.modios.taxsolution

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.reactive.function.client.WebClient

@Configuration
class Configuration {

    @Bean
    fun taxContentWebClient() = WebClient.builder().baseUrl("http://localhost:8002/").build()
}
