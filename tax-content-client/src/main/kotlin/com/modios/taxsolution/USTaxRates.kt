package com.modios.taxsolution

import com.modios.taxsolution.content.model.v1.TaxRate
import java.math.BigDecimal

object USTaxRates {
    val ALABAMA_STATE_SALES = TaxRate(
        id = 0,
        taxAuthorityId = 0,
        taxContentProviderId = 0,
        code = "SALES",
        rate = BigDecimal.valueOf(0.04),
        startDate = textToDate("01/01/2000"),
        endDate = null
    )

    val ALASKA_STATE_SALES = TaxRate(
        id = 0,
        taxAuthorityId = 0,
        taxContentProviderId = 0,
        code = "SALES",
        rate = BigDecimal.ZERO,
        startDate = textToDate("01/01/2000"),
        endDate = null
    )

    val ARIZONA_STATE_SALES = TaxRate(
        id = 0,
        taxAuthorityId = 0,
        taxContentProviderId = 0,
        code = "SALES",
        rate = BigDecimal.valueOf(0.056),
        startDate = textToDate("01/01/2000"),
        endDate = null
    )
    val ARKANSAS_STATE_SALES = TaxRate(
        id = 0,
        taxAuthorityId = 0,
        taxContentProviderId = 0,
        code = "SALES",
        rate = BigDecimal.valueOf(0.065),
        startDate = textToDate("01/01/2000"),
        endDate = null
    )
    val CALIFORNIA_STATE_SALES = TaxRate(
        id = 0,
        taxAuthorityId = 0,
        taxContentProviderId = 0,
        code = "SALES",
        rate = BigDecimal.valueOf(0.0725),
        startDate = textToDate("01/01/2000"),
        endDate = null
    )
    val CONNECTICUT_STATE_SALES = TaxRate(
        id = 0,
        taxAuthorityId = 0,
        taxContentProviderId = 0,
        code = "SALES",
        rate = BigDecimal.valueOf(0.0635),
        startDate = textToDate("01/01/2000"),
        endDate = null
    )
    val DELAWARE_STATE_SALES = TaxRate(
        id = 0,
        taxAuthorityId = 0,
        taxContentProviderId = 0,
        code = "SALES",
        rate = BigDecimal.ZERO,
        startDate = textToDate("01/01/2000"),
        endDate = null
    )
    val FLORIDA_STATE_SALES = TaxRate(
        id = 0,
        taxAuthorityId = 0,
        taxContentProviderId = 0,
        code = "SALES",
        rate = BigDecimal.valueOf(0.0600),
        startDate = textToDate("01/01/2000"),
        endDate = null
    )
    val GEORGIA_STATE_SALES = TaxRate(
        id = 0,
        taxAuthorityId = 0,
        taxContentProviderId = 0,
        code = "SALES",
        rate = BigDecimal.valueOf(0.04),
        startDate = textToDate("01/01/2000"),
        endDate = null
    )
    val HAWAII_STATE_SALES = TaxRate(
        id = 0,
        taxAuthorityId = 0,
        taxContentProviderId = 0,
        code = "SALES",
        rate = BigDecimal.valueOf(0.04166),
        startDate = textToDate("01/01/2000"),
        endDate = null
    )
    val IDAHO_STATE_SALES = TaxRate(
        id = 0,
        taxAuthorityId = 0,
        taxContentProviderId = 0,
        code = "SALES",
        rate = BigDecimal.valueOf(0.06),
        startDate = textToDate("01/01/2000"),
        endDate = null
    )
    val ILLINOIS_STATE_SALES = TaxRate(
        id = 0,
        taxAuthorityId = 0,
        taxContentProviderId = 0,
        code = "SALES",
        rate = BigDecimal.valueOf(0.0625),
        startDate = textToDate("01/01/2000"),
        endDate = null
    )
    val INDIANA_STATE_SALES = TaxRate(
        id = 0,
        taxAuthorityId = 0,
        taxContentProviderId = 0,
        code = "SALES",
        rate = BigDecimal.valueOf(0.07),
        startDate = textToDate("01/01/2000"),
        endDate = null
    )
    val IOWA_STATE_SALES = TaxRate(
        id = 0,
        taxAuthorityId = 0,
        taxContentProviderId = 0,
        code = "SALES",
        rate = BigDecimal.valueOf(0.06),
        startDate = textToDate("01/01/2000"),
        endDate = null
    )
    val KANSAS_STATE_SALES = TaxRate(
        id = 0,
        taxAuthorityId = 0,
        taxContentProviderId = 0,
        code = "SALES",
        rate = BigDecimal.valueOf(0.065),
        startDate = textToDate("01/01/2000"),
        endDate = null
    )
    val KENTUCKY_STATE_SALES = TaxRate(
        id = 0,
        taxAuthorityId = 0,
        taxContentProviderId = 0,
        code = "SALES",
        rate = BigDecimal.valueOf(0.06),
        startDate = textToDate("01/01/2000"),
        endDate = null
    )
    val LOUISIANA_STATE_SALES = TaxRate(
        id = 0,
        taxAuthorityId = 0,
        taxContentProviderId = 0,
        code = "SALES",
        rate = BigDecimal.valueOf(0.0445),
        startDate = textToDate("01/01/2000"),
        endDate = null
    )
    val MAINE_STATE_SALES = TaxRate(
        id = 0,
        taxAuthorityId = 0,
        taxContentProviderId = 0,
        code = "SALES",
        rate = BigDecimal.valueOf(0.055),
        startDate = textToDate("01/01/2000"),
        endDate = null
    )
    val MARYLAND_STATE_SALES = TaxRate(
        id = 0,
        taxAuthorityId = 0,
        taxContentProviderId = 0,
        code = "SALES",
        rate = BigDecimal.valueOf(0.06),
        startDate = textToDate("01/01/2000"),
        endDate = null
    )
    val MASSACHUSETTS_STATE_SALES = TaxRate(
        id = 0,
        taxAuthorityId = 0,
        taxContentProviderId = 0,
        code = "SALES",
        rate = BigDecimal.valueOf(0.0625),
        startDate = textToDate("01/01/2000"),
        endDate = null
    )
    val MICHIGAN_STATE_SALES = TaxRate(
        id = 0,
        taxAuthorityId = 0,
        taxContentProviderId = 0,
        code = "SALES",
        rate = BigDecimal.valueOf(0.06),
        startDate = textToDate("01/01/2000"),
        endDate = null
    )
    val MINNESOTA_STATE_SALES = TaxRate(
        id = 0,
        taxAuthorityId = 0,
        taxContentProviderId = 0,
        code = "SALES",
        rate = BigDecimal.valueOf(0.06875),
        startDate = textToDate("01/01/2000"),
        endDate = null
    )
    val MISSISSIPPI_STATE_SALES = TaxRate(
        id = 0,
        taxAuthorityId = 0,
        taxContentProviderId = 0,
        code = "SALES",
        rate = BigDecimal.valueOf(0.07),
        startDate = textToDate("01/01/2000"),
        endDate = null
    )
    val MISSOURI_STATE_SALES = TaxRate(
        id = 0,
        taxAuthorityId = 0,
        taxContentProviderId = 0,
        code = "SALES",
        rate = BigDecimal.valueOf(0.04225),
        startDate = textToDate("01/01/2000"),
        endDate = null
    )
    val MONTANA_STATE_SALES = TaxRate(
        id = 0,
        taxAuthorityId = 0,
        taxContentProviderId = 0,
        code = "SALES",
        rate = BigDecimal.ZERO,
        startDate = textToDate("01/01/2000"),
        endDate = null
    )
    val NEBRASKA_STATE_SALES = TaxRate(
        id = 0,
        taxAuthorityId = 0,
        taxContentProviderId = 0,
        code = "SALES",
        rate = BigDecimal.valueOf(0.055),
        startDate = textToDate("01/01/2000"),
        endDate = null
    )
    val NEVADA_STATE_SALES = TaxRate(
        id = 0,
        taxAuthorityId = 0,
        taxContentProviderId = 0,
        code = "SALES",
        rate = BigDecimal.valueOf(0.0685),
        startDate = textToDate("01/01/2000"),
        endDate = null
    )
    val NEW_HAMPSHIRE_STATE_SALES = TaxRate(
        id = 0,
        taxAuthorityId = 0,
        taxContentProviderId = 0,
        code = "SALES",
        rate = BigDecimal.ZERO,
        startDate = textToDate("01/01/2000"),
        endDate = null
    )
    val NEW_JERSEY_STATE_SALES = TaxRate(
        id = 0,
        taxAuthorityId = 0,
        taxContentProviderId = 0,
        code = "SALES",
        rate = BigDecimal.valueOf(0.06625),
        startDate = textToDate("01/01/2000"),
        endDate = null
    )
    val NEW_MEXICO_STATE_SALES = TaxRate(
        id = 0,
        taxAuthorityId = 0,
        taxContentProviderId = 0,
        code = "SALES",
        rate = BigDecimal.valueOf(0.05125),
        startDate = textToDate("01/01/2000"),
        endDate = null
    )
    val NEW_YORK_STATE_SALES = TaxRate(
        id = 0,
        taxAuthorityId = 0,
        taxContentProviderId = 0,
        code = "SALES",
        rate = BigDecimal.valueOf(0.0400),
        startDate = textToDate("01/01/2000"),
        endDate = null
    )
    val NORTH_CAROLINA_STATE_SALES = TaxRate(
        id = 0,
        taxAuthorityId = 0,
        taxContentProviderId = 0,
        code = "SALES",
        rate = BigDecimal.valueOf(0.0475),
        startDate = textToDate("01/01/2000"),
        endDate = null
    )
    val NORTH_DAKOTA_STATE_SALES = TaxRate(
        id = 0,
        taxAuthorityId = 0,
        taxContentProviderId = 0,
        code = "SALES",
        rate = BigDecimal.valueOf(0.05),
        startDate = textToDate("01/01/2000"),
        endDate = null
    )
    val OHIO_STATE_SALES = TaxRate(
        id = 0,
        taxAuthorityId = 0,
        taxContentProviderId = 0,
        code = "SALES",
        rate = BigDecimal.valueOf(0.0575),
        startDate = textToDate("01/01/2000"),
        endDate = null
    )
    val OKLAHOMA_STATE_SALES = TaxRate(
        id = 0,
        taxAuthorityId = 0,
        taxContentProviderId = 0,
        code = "SALES",
        rate = BigDecimal.valueOf(0.045),
        startDate = textToDate("01/01/2000"),
        endDate = null
    )
    val OREGON_STATE_SALES = TaxRate(
        id = 0,
        taxAuthorityId = 0,
        taxContentProviderId = 0,
        code = "SALES",
        rate = BigDecimal.ZERO,
        startDate = textToDate("01/01/2000"),
        endDate = null
    )
    val PENNSYLVANIA_STATE_SALES = TaxRate(
        id = 0,
        taxAuthorityId = 0,
        taxContentProviderId = 0,
        code = "SALES",
        rate = BigDecimal.valueOf(0.06),
        startDate = textToDate("01/01/2000"),
        endDate = null
    )
    val RHODE_ISLAND_STATE_SALES = TaxRate(
        id = 0,
        taxAuthorityId = 0,
        taxContentProviderId = 0,
        code = "SALES",
        rate = BigDecimal.valueOf(0.07),
        startDate = textToDate("01/01/2000"),
        endDate = null
    )
    val SOUTH_CAROLINA_STATE_SALES = TaxRate(
        id = 0,
        taxAuthorityId = 0,
        taxContentProviderId = 0,
        code = "SALES",
        rate = BigDecimal.valueOf(0.06),
        startDate = textToDate("01/01/2000"),
        endDate = null
    )
    val SOUTH_DAKOTA_STATE_SALES = TaxRate(
        id = 0,
        taxAuthorityId = 0,
        taxContentProviderId = 0,
        code = "SALES",
        rate = BigDecimal.valueOf(0.045),
        startDate = textToDate("01/01/2000"),
        endDate = null
    )
    val TENNESSEE_STATE_SALES = TaxRate(
        id = 0,
        taxAuthorityId = 0,
        taxContentProviderId = 0,
        code = "SALES",
        rate = BigDecimal.valueOf(0.07),
        startDate = textToDate("01/01/2000"),
        endDate = null
    )
    val TEXAS_STATE_SALES = TaxRate(
        id = 0,
        taxAuthorityId = 0,
        taxContentProviderId = 0,
        code = "SALES",
        rate = BigDecimal.valueOf(0.0625),
        startDate = textToDate("01/01/2000"),
        endDate = null
    )
    val UTAH_STATE_SALES = TaxRate(
        id = 0,
        taxAuthorityId = 0,
        taxContentProviderId = 0,
        code = "SALES",
        rate = BigDecimal.valueOf(0.0485),
        startDate = textToDate("01/01/2000"),
        endDate = null
    )
    val VERMONT_STATE_SALES = TaxRate(
        id = 0,
        taxAuthorityId = 0,
        taxContentProviderId = 0,
        code = "SALES",
        rate = BigDecimal.valueOf(0.06),
        startDate = textToDate("01/01/2000"),
        endDate = null
    )
    val VIRGINIA_STATE_SALES = TaxRate(
        id = 0,
        taxAuthorityId = 0,
        taxContentProviderId = 0,
        code = "SALES",
        rate = BigDecimal.valueOf(0.043),
        startDate = textToDate("01/01/2000"),
        endDate = null
    )
    val WASHINGTON_STATE_SALES = TaxRate(
        id = 0,
        taxAuthorityId = 0,
        taxContentProviderId = 0,
        code = "SALES",
        rate = BigDecimal.valueOf(0.065),
        startDate = textToDate("01/01/2000"),
        endDate = null
    )
    val WEST_VIRGINIA_STATE_SALES = TaxRate(
        id = 0,
        taxAuthorityId = 0,
        taxContentProviderId = 0,
        code = "SALES",
        rate = BigDecimal.valueOf(0.06),
        startDate = textToDate("01/01/2000"),
        endDate = null
    )
    val WISCONSIN_STATE_SALES = TaxRate(
        id = 0,
        taxAuthorityId = 0,
        taxContentProviderId = 0,
        code = "SALES",
        rate = BigDecimal.valueOf(0.05),
        startDate = textToDate("01/01/2000"),
        endDate = null
    )
    val WYOMING_STATE_SALES = TaxRate(
        id = 0,
        taxAuthorityId = 0,
        taxContentProviderId = 0,
        code = "SALES",
        rate = BigDecimal.valueOf(0.04),
        startDate = textToDate("01/01/2000"),
        endDate = null
    )

    val US_TAX_RATES = mapOf(
        USTaxAuthorities.ALABAMA_STATE.name to ALABAMA_STATE_SALES,
        USTaxAuthorities.ALASKA_STATE.name to ALASKA_STATE_SALES,
        USTaxAuthorities.ARIZONA_STATE.name to ARIZONA_STATE_SALES,
        USTaxAuthorities.ARKANSAS_STATE.name to ARIZONA_STATE_SALES,
        USTaxAuthorities.CALIFORNIA_STATE.name to CALIFORNIA_STATE_SALES,
        USTaxAuthorities.DELAWARE_STATE.name to DELAWARE_STATE_SALES,
        USTaxAuthorities.FLORIDA_STATE.name to FLORIDA_STATE_SALES,
        USTaxAuthorities.GEORGIA_STATE.name to GEORGIA_STATE_SALES,
        USTaxAuthorities.HAWAII_STATE.name to HAWAII_STATE_SALES,
        USTaxAuthorities.IDAHO_STATE.name to IDAHO_STATE_SALES,
        USTaxAuthorities.ILLINOIS_STATE.name to ILLINOIS_STATE_SALES,
        USTaxAuthorities.INDIANA_STATE.name to INDIANA_STATE_SALES,
        USTaxAuthorities.IOWA_STATE.name to IOWA_STATE_SALES,
        USTaxAuthorities.KANSAS_STATE.name to KANSAS_STATE_SALES,
        USTaxAuthorities.KENTUCKY_STATE.name to KENTUCKY_STATE_SALES,
        USTaxAuthorities.LOUISIANA_STATE.name to LOUISIANA_STATE_SALES,
        USTaxAuthorities.MAINE_STATE.name to MAINE_STATE_SALES,
        USTaxAuthorities.MARYLAND_STATE.name to MARYLAND_STATE_SALES,
        USTaxAuthorities.MASSACHUSETTS_STATE.name to MASSACHUSETTS_STATE_SALES,
        USTaxAuthorities.MICHIGAN_STATE.name to MICHIGAN_STATE_SALES,
        USTaxAuthorities.MINNESOTA_STATE.name to MINNESOTA_STATE_SALES,
        USTaxAuthorities.MISSISSIPPI_STATE.name to MISSISSIPPI_STATE_SALES,
        USTaxAuthorities.MISSOURI_STATE.name to MISSOURI_STATE_SALES,
        USTaxAuthorities.MONTANA_STATE.name to MONTANA_STATE_SALES,
        USTaxAuthorities.NEBRASKA_STATE.name to NEBRASKA_STATE_SALES,
        USTaxAuthorities.NEVADA_STATE.name to NEVADA_STATE_SALES,
        USTaxAuthorities.NEW_HAMPSHIRE_STATE.name to NEW_HAMPSHIRE_STATE_SALES,
        USTaxAuthorities.NEW_JERSEY_STATE.name to NEW_JERSEY_STATE_SALES,
        USTaxAuthorities.NEW_MEXICO_STATE.name to NEW_MEXICO_STATE_SALES,
        USTaxAuthorities.NEW_YORK_STATE.name to NEW_YORK_STATE_SALES,
        USTaxAuthorities.NORTH_CAROLINA_STATE.name to NORTH_CAROLINA_STATE_SALES,
        USTaxAuthorities.NORTH_DAKOTA_STATE.name to NORTH_DAKOTA_STATE_SALES,
        USTaxAuthorities.OHIO_STATE.name to OHIO_STATE_SALES,
        USTaxAuthorities.OKLAHOMA_STATE.name to OKLAHOMA_STATE_SALES,
        USTaxAuthorities.OREGON_STATE.name to OREGON_STATE_SALES,
        USTaxAuthorities.PENNSYLVANIA_STATE.name to PENNSYLVANIA_STATE_SALES,
        USTaxAuthorities.RHODE_ISLAND_STATE.name to RHODE_ISLAND_STATE_SALES,
        USTaxAuthorities.SOUTH_CAROLINA_STATE.name to SOUTH_CAROLINA_STATE_SALES,
        USTaxAuthorities.SOUTH_DAKOTA_STATE.name to SOUTH_DAKOTA_STATE_SALES,
        USTaxAuthorities.TENNESSEE_STATE.name to TENNESSEE_STATE_SALES,
        USTaxAuthorities.TEXAS_STATE.name to TEXAS_STATE_SALES,
        USTaxAuthorities.UTAH_STATE.name to UTAH_STATE_SALES,
        USTaxAuthorities.VERMONT_STATE.name to VERMONT_STATE_SALES,
        USTaxAuthorities.VIRGINIA_STATE.name to VIRGINIA_STATE_SALES,
        USTaxAuthorities.WASHINGTON_STATE.name to WASHINGTON_STATE_SALES,
        USTaxAuthorities.WEST_VIRGINIA_STATE.name to WEST_VIRGINIA_STATE_SALES,
        USTaxAuthorities.WISCONSIN_STATE.name to WISCONSIN_STATE_SALES,
        USTaxAuthorities.WYOMING_STATE.name to WYOMING_STATE_SALES
    )
}
