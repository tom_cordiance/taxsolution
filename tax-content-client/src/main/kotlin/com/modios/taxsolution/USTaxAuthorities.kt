package com.modios.taxsolution

import com.modios.taxsolution.content.model.v1.TaxAuthority

object USTaxAuthorities {

    fun createTaxAuthority(name: String) = TaxAuthority(
        taxContentProviderId = 0L,
        id = 0L,
        name = name,
        startDate = textToDate("01/01/2000"),
        endDate = null
    )

    val ALABAMA_STATE = createTaxAuthority("Alabama State Sales and Use")
    val ALASKA_STATE = createTaxAuthority("Alaska State Sales and Use")
    val ARIZONA_STATE = createTaxAuthority("Arizona State Sales and Use")
    val ARKANSAS_STATE = createTaxAuthority("Arkansas State Sales and Use")
    val CALIFORNIA_STATE = createTaxAuthority("California State Sales and Use")
    val CONNECTICUT_STATE = createTaxAuthority("Connecticut State Sales and Use")
    val DELAWARE_STATE = createTaxAuthority("Delaware State Sales and Use")
    val FLORIDA_STATE = createTaxAuthority("Florida State Sales and Use")
    val GEORGIA_STATE = createTaxAuthority("Georgia State Sales and Use")
    val HAWAII_STATE = createTaxAuthority("Hawaii State Sales and Use")
    val IDAHO_STATE = createTaxAuthority("Idaho State Sales and Use")
    val ILLINOIS_STATE = createTaxAuthority("Illinois State Sales and Use")
    val INDIANA_STATE = createTaxAuthority("Indiana State Sales and Use")
    val IOWA_STATE = createTaxAuthority("Iowa State Sales and Use")
    val KANSAS_STATE = createTaxAuthority("Kansas State Sales and Use")
    val KENTUCKY_STATE = createTaxAuthority("Kentucky State Sales and Use")
    val LOUISIANA_STATE = createTaxAuthority("Louisiana State Sales and Use")
    val MAINE_STATE = createTaxAuthority("Main State Sales and Use")
    val MARYLAND_STATE = createTaxAuthority("Maryland State Sales and Use")
    val MASSACHUSETTS_STATE = createTaxAuthority("Massachusetts State Sales and Use")
    val MICHIGAN_STATE = createTaxAuthority("Michigan State Sales and Use")
    val MINNESOTA_STATE = createTaxAuthority("Minnesota State Sales and Use")
    val MISSISSIPPI_STATE = createTaxAuthority("Mississippi State Sales and Use")
    val MISSOURI_STATE = createTaxAuthority("Missouri State Sales and Use")
    val MONTANA_STATE = createTaxAuthority("Montana State Sales and Use")
    val NEBRASKA_STATE = createTaxAuthority("Nebraska State Sales and Use")
    val NEVADA_STATE = createTaxAuthority("Nevada State Sales and Use")
    val NEW_HAMPSHIRE_STATE = createTaxAuthority("New Hampshire State Sales and Use")
    val NEW_JERSEY_STATE = createTaxAuthority("New Jersey State Sales and Use")
    val NEW_MEXICO_STATE = createTaxAuthority("New Mexico State Sales and Use")
    val NEW_YORK_STATE = createTaxAuthority("New York State Sales and Use")
    val NORTH_CAROLINA_STATE = createTaxAuthority("North Carolina State Sales and Use")
    val NORTH_DAKOTA_STATE = createTaxAuthority("North Dakota State Sales and Use")
    val OHIO_STATE = createTaxAuthority("Ohio State Sales and Use")
    val OKLAHOMA_STATE = createTaxAuthority("Oklahoma State Sales and Use")
    val OREGON_STATE = createTaxAuthority("Oregon State Sales and Use")
    val PENNSYLVANIA_STATE = createTaxAuthority("Pennsylvania State Sales and Use")
    val RHODE_ISLAND_STATE = createTaxAuthority("Rhode Island State Sales and Use")
    val SOUTH_CAROLINA_STATE = createTaxAuthority("South Carolina State Sales and Use")
    val SOUTH_DAKOTA_STATE = createTaxAuthority("South Dakota State Sales and Use")
    val TENNESSEE_STATE = createTaxAuthority("Tennessee State Sales and Use")
    val TEXAS_STATE = createTaxAuthority("Texas State Sales and Use")
    val UTAH_STATE = createTaxAuthority("Utah State Sales and Use")
    val VERMONT_STATE = createTaxAuthority("Vermont State Sales and Use")
    val VIRGINIA_STATE = createTaxAuthority("Virginia State Sales and Use")
    val WASHINGTON_STATE = createTaxAuthority("Washington State Sales and Use")
    val WEST_VIRGINIA_STATE = createTaxAuthority("West Virginia State Sales and Use")
    val WISCONSIN_STATE = createTaxAuthority("Wisconsin State Sales and Use")
    val WYOMING_STATE = createTaxAuthority("Wyoming State Sales and Use")

    val US_TAX_AUTHORITIES = listOf(
        ALABAMA_STATE,
        ALASKA_STATE,
        ARIZONA_STATE,
        ARKANSAS_STATE,
        CALIFORNIA_STATE,
        DELAWARE_STATE,
        FLORIDA_STATE,
        GEORGIA_STATE,
        HAWAII_STATE,
        IDAHO_STATE,
        ILLINOIS_STATE,
        INDIANA_STATE,
        IOWA_STATE,
        KANSAS_STATE,
        KENTUCKY_STATE,
        LOUISIANA_STATE,
        MAINE_STATE,
        MARYLAND_STATE,
        MASSACHUSETTS_STATE,
        MICHIGAN_STATE,
        MINNESOTA_STATE,
        MISSISSIPPI_STATE,
        MISSOURI_STATE,
        MONTANA_STATE,
        NEBRASKA_STATE,
        NEVADA_STATE,
        NEW_HAMPSHIRE_STATE,
        NEW_JERSEY_STATE,
        NEW_MEXICO_STATE,
        NEW_YORK_STATE,
        NORTH_CAROLINA_STATE,
        NORTH_DAKOTA_STATE,
        OHIO_STATE,
        OKLAHOMA_STATE,
        OREGON_STATE,
        PENNSYLVANIA_STATE,
        RHODE_ISLAND_STATE,
        SOUTH_CAROLINA_STATE,
        SOUTH_DAKOTA_STATE,
        TENNESSEE_STATE,
        TEXAS_STATE,
        UTAH_STATE,
        VERMONT_STATE,
        VIRGINIA_STATE,
        WASHINGTON_STATE,
        WEST_VIRGINIA_STATE,
        WISCONSIN_STATE,
        WYOMING_STATE,
    )
}
