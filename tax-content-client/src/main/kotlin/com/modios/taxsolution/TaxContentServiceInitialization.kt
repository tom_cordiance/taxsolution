package com.modios.taxsolution

import com.modios.taxsolution.content.client.v1.GeographyClient
import com.modios.taxsolution.content.client.v1.TaxAuthorityClient
import com.modios.taxsolution.content.client.v1.TaxAuthorityGeographyClient
import com.modios.taxsolution.content.client.v1.TaxContentProviderClient
import com.modios.taxsolution.content.client.v1.TaxRateClient
import com.modios.taxsolution.content.model.v1.Geography
import com.modios.taxsolution.content.model.v1.TaxAuthority
import com.modios.taxsolution.content.model.v1.TaxContentProvider
import com.modios.taxsolution.content.model.v1.TaxRate
import kotlinx.coroutines.runBlocking
import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

fun main(args: Array<String>) {
    SpringApplication(TaxContentServiceInitialization::class.java).run(*args)
}

@SpringBootApplication
class TaxContentServiceInitialization : CommandLineRunner {

    private val logger = KotlinLogging.logger { }

    @Autowired
    private lateinit var taxContentProviderClient: TaxContentProviderClient

    @Autowired
    private lateinit var geographyClient: GeographyClient

    @Autowired
    private lateinit var taxAuthorityClient: TaxAuthorityClient

    @Autowired
    private lateinit var taxAuthorityGeographyClient: TaxAuthorityGeographyClient

    @Autowired
    private lateinit var taxRateClient: TaxRateClient

    private suspend fun createUSTaxContentProvider(): TaxContentProvider {
        val taxContentProviderName = "Cordiance US Tax Content"

        logger.info { "getting $taxContentProviderName" }
        var taxContentProvider = taxContentProviderClient.get(taxContentProviderName)
        when (taxContentProvider) {
            null -> {
                logger.info { "could not find $taxContentProviderName, creating..." }
                taxContentProvider = taxContentProviderClient.create(taxContentProviderName)
                logger.info { "tax content provider created: $taxContentProvider" }
            }
            else -> {
                logger.info { "tax content provider found: $taxContentProvider" }
            }
        }

        return taxContentProvider
    }

    private suspend fun createUSGeography(taxContentProviderId: Long): List<Geography> {
        logger.info { "creating US geography..." }

        logger.info { "getting ${USGeography.UNITED_STATES}..." }
        var unitedStates = geographyClient.getByTaxContentProviderIdAndParentIdAndName(
            taxContentProviderId = taxContentProviderId,
            parentId = Geography.WORLD_ID,
            name = USGeography.UNITED_STATES.name
        )

        when (unitedStates) {
            null -> {
                logger.info { "creating ${USGeography.UNITED_STATES}..." }
                unitedStates = geographyClient.create(
                    USGeography.UNITED_STATES.copy(
                        taxContentProviderId = taxContentProviderId,
                        parentId = Geography.WORLD_ID
                    )
                )
                logger.info { "created: $unitedStates" }
            }
            else -> {
                logger.info { "geography found: $unitedStates" }
            }
        }

        logger.info { "creating states..." }
        return USGeography.US_STATES.map { geography ->
            logger.info { "getting $geography..." }
            val foundState = geographyClient.getByTaxContentProviderIdAndParentIdAndName(
                taxContentProviderId = taxContentProviderId,
                parentId = unitedStates.id,
                name = geography.name
            )

            when (foundState) {
                null -> {
                    logger.info { "creating $geography..." }
                    val state = geographyClient.create(
                        geography.copy(
                            taxContentProviderId = taxContentProviderId,
                            parentId = unitedStates.id
                        )
                    )
                    logger.info { "created $state" }

                    state
                }
                else -> {
                    logger.info { "geography found: $foundState" }
                    foundState
                }
            }
        }
    }

    private suspend fun createUSTaxAuthorities(taxContentProviderId: Long): List<TaxAuthority> {
        logger.info { "creating US tax authorities..." }

        return USTaxAuthorities.US_TAX_AUTHORITIES.map { taxAuthority ->
            logger.info { "getting ${taxAuthority.name}..." }
            val foundTaxAuthority = taxAuthorityClient.getByTaxContentProviderIdAndName(
                taxContentProviderId = taxContentProviderId,
                name = taxAuthority.name
            )

            when (foundTaxAuthority) {
                null -> {
                    logger.info { "creating ${taxAuthority.name}" }
                    val createdTaxAuthority = taxAuthorityClient.create(
                        taxContentProviderId = taxContentProviderId,
                        name = taxAuthority.name,
                        startDate = textToDate("01/01/2000"),
                        endDate = null
                    )

                    logger.info { "created $createdTaxAuthority" }
                    createdTaxAuthority
                }
                else -> {
                    logger.info { "found $foundTaxAuthority" }
                    foundTaxAuthority
                }
            }
        }
    }

    private suspend fun createUSTaxRates(taxContentProviderId: Long, taxAuthorityList: List<TaxAuthority>) {
        USTaxRates.US_TAX_RATES.forEach { (authorityName, rate) ->
            val taxAuthority = taxAuthorityList.find { it.name == authorityName }
                ?: throw RuntimeException("TaxAuthority not found: $authorityName")

            val foundRates = taxRateClient.findByTaxAuthorityId(taxAuthorityId = taxAuthority.id)

            when (foundRates.find { it.code == rate.code && it.rate == rate.rate }) {
                null -> {
                    val taxRate = taxRateClient.create(
                        TaxRate(
                            taxContentProviderId = taxContentProviderId,
                            taxAuthorityId = taxAuthority.id,
                            id = 0,
                            code = rate.code,
                            rate = rate.rate,
                            startDate = rate.startDate,
                            endDate = rate.endDate
                        )
                    )

                    logger.info { "created tax rate $taxRate for $authorityName" }
                }
                else -> logger.info { "rate exists for $authorityName: ${rate.rate}/${rate.code}" }
            }
        }
    }

    private suspend fun createTaxingGeography(
        taxContentProviderId: Long,
        geographyList: List<Geography>,
        taxAuthorityList: List<TaxAuthority>
    ) {
        USTaxAuthorityMapping.getAuthorityMapping().forEach { (geo, taxAuth) ->
            val geography = geographyList.find { it.code == geo.code }
                ?: throw RuntimeException("Geography not found: ${geo.code}")
            val taxAuthority = taxAuthorityList.find { it.name == taxAuth.name }
                ?: throw RuntimeException("TaxAuthority not found: ${taxAuth.name}")

            val foundTaxingGeography = taxAuthorityGeographyClient.getByGeographyIdAndTaxAuthorityId(
                taxAuthorityId = taxAuthority.id,
                geographyId = geography.id
            )

            when (foundTaxingGeography) {
                null -> {
                    taxAuthorityGeographyClient.create(
                        taxContentProviderId = taxContentProviderId,
                        geographyId = geography.id,
                        taxAuthorityId = taxAuthority.id
                    )

                    logger.info { "created TaxingGeography for ${geography.name}/${taxAuthority.name}" }
                }
                else -> logger.info { "found TaxingGeography for ${geography.name}/${taxAuthority.name}" }
            }
        }
    }

    override fun run(vararg args: String?) = runBlocking {

        val taxContentProvider = createUSTaxContentProvider()

        val usStates = createUSGeography(taxContentProvider.id)
        val usTaxAuthorities = createUSTaxAuthorities(taxContentProvider.id)
        createUSTaxRates(taxContentProviderId = taxContentProvider.id, taxAuthorityList = usTaxAuthorities)
        createTaxingGeography(
            taxContentProviderId = taxContentProvider.id,
            geographyList = usStates,
            taxAuthorityList = usTaxAuthorities
        )

        System.exit(0)
    }
}
