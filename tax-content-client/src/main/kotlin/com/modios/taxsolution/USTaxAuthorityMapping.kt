package com.modios.taxsolution

import com.modios.taxsolution.content.model.v1.Geography
import com.modios.taxsolution.content.model.v1.TaxAuthority
import mu.KotlinLogging

object USTaxAuthorityMapping {
    private val logger = KotlinLogging.logger { }

    fun getAuthorityMapping(): Map<Geography, TaxAuthority> =
        USGeography
            .US_STATES
            .map { geography ->
                geography to USTaxAuthorities.US_TAX_AUTHORITIES.first {
                    logger.info { "Geography: ${geography.name}  Tax authority: ${it.name}" }
                    it.name.lowercase().startsWith(geography.name.lowercase())
                }
            }.toMap()
}
