package com.modios.taxsolution

import com.modios.taxsolution.calc.model.v1.TaxDocument
import com.modios.taxsolution.calc.model.v1.TaxDocumentItem
import com.modios.taxsolution.calc.model.v1.TaxDocumentResult
import com.modios.taxsolution.calc.model.v1.TransactionLocation
import com.modios.taxsolution.content.client.v1.GeographyClientImpl
import io.ktor.client.HttpClient
import io.ktor.client.engine.cio.CIO
import io.ktor.client.engine.cio.endpoint
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.request.post
import io.ktor.http.ContentType
import io.ktor.http.contentType
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import mu.KotlinLogging
import java.math.BigDecimal
import java.time.LocalDateTime
import java.util.Properties
import java.util.concurrent.Executors
import kotlin.time.Duration
import kotlin.time.ExperimentalTime
import kotlin.time.measureTimedValue

fun main(args: Array<String>) {
    TaxCalculationClient().run(*args)
}

data class CalculationInfo @OptIn(ExperimentalTime::class) constructor(val numberOfCalcs: Int, val duration: Duration) {
    @OptIn(ExperimentalTime::class)
    fun averageCalcTime() = duration.div(numberOfCalcs)
}

class TaxCalculationClient {

    private val baseUrl = "http://localhost:8800/"

    private val logger = KotlinLogging.logger { }

    private val httpClient = HttpClient(CIO) {
        install(JsonFeature)
        expectSuccess = true
        engine {
            maxConnectionsCount = 1000
            endpoint {
                // this: EndpointConfig
                maxConnectionsPerRoute = 100
                pipelineMaxSize = 20
                keepAliveTime = 5000
                connectTimeout = 5000
                connectAttempts = 5
            }
        }
    }

    private val geographyClient = GeographyClientImpl(
        httpClient = httpClient,
        properties = Properties().also {
            it.setProperty(
                GeographyClientImpl.URL_PROPERTY_NAME,
                "http://localhost:8002"
            )
        }
    )

    private val concurrency = 16
    private val dispatcher = Executors.newFixedThreadPool(concurrency).asCoroutineDispatcher()

    private lateinit var usTaxingLocations: List<TransactionLocation>

    suspend fun getUSTransactionLocations(): List<TransactionLocation> =
        geographyClient
            .getAllByName("UNITED STATES")
            .first()
            .let { us ->
                geographyClient.getByTaxContentProviderIdAndParentId(
                    taxContentProviderId = us.taxContentProviderId,
                    parentId = us.id
                ).map {
                    TransactionLocation(country = us.code, state = it.code)
                }
            }

    private fun testTaxDocument(): TaxDocument {
        return TaxDocument(
            tenantName = ServiceCommon.TENANT_NAME,
            companyName = ServiceCommon.COMPANY_NAME,
            persistResult = false,
            documentDate = LocalDateTime.now(),
            documentNumber = "abc123",
            shipFrom = usTaxingLocations.random(),
            shipTo = usTaxingLocations.random()
        ).apply {
            taxDocumentItems.add(TaxDocumentItem(index = 1, quantity = 1, amount = BigDecimal.valueOf(100)))
        }
    }

    suspend fun taxCalculation(webClient: HttpClient): TaxDocumentResult = coroutineScope {
        val result: Deferred<TaxDocumentResult> = try {
            async {
                webClient.post("$baseUrl/calc/standard/v1") {
                    contentType(ContentType.Application.Json)
                    body = testTaxDocument()
                }
            }
        } catch (e: Exception) {
            logger.error(e) { "Calc error" }
            throw e
        }

        result.await()
    }

    @OptIn(ExperimentalTime::class)
    private suspend fun doCalc(numberOfCalculations: Int): Deferred<CalculationInfo> = coroutineScope {
        async {
            val calcHttpClient = HttpClient(CIO) {
                install(JsonFeature)
                expectSuccess = true
                engine {
                    maxConnectionsCount = 1000
                    endpoint {
                        // this: EndpointConfig
                        maxConnectionsPerRoute = 100
                        pipelineMaxSize = 20
                        keepAliveTime = 5000
                        connectTimeout = 5000
                        connectAttempts = 5
                    }
                }
            }

            val timedValue = measureTimedValue {
                (1..numberOfCalculations).forEach {
                    taxCalculation(calcHttpClient)
                }
            }

            CalculationInfo(numberOfCalcs = numberOfCalculations, duration = timedValue.duration)
        }
    }

    @OptIn(ExperimentalTime::class)
    fun run(vararg args: String?) {
        runBlocking {
            try {
                usTaxingLocations = getUSTransactionLocations()

                CoroutineScope(dispatcher).run {
                    listOf(
                        1 to 100,
                        1 to 10000,
                        4 to 10000,
                        8 to 10000,
                        16 to 10000,
                        32 to 10000
                    ).forEach { (executions, numberOfCalcs) ->
                        logger.info { "Executing $executions calc sessions, $numberOfCalcs calculations per session" }
                        (1..executions)
                            .map { doCalc(numberOfCalcs) }
                            .awaitAll()
                            .sortedBy { it.averageCalcTime().inWholeMicroseconds }
                            .forEach { calculationInfo ->
                                logger.info { "$calculationInfo - average ${calculationInfo.averageCalcTime()}" }
                                delay(2500)
                            }
                    }
                }
            } catch (exception: Exception) {
                logger.error(exception) { "Calc error" }
            } finally {
                System.exit(0)
            }
        }
    }
}
