package com.modios.taxsolution

import com.modios.taxsolution.content.model.v1.Geography
import com.modios.taxsolution.content.model.v1.GeographyClassification

// TODO: can the client pull the US states for calcs?-
object USGeography {
    fun createGeography(
        classification: GeographyClassification,
        name: String,
        code: String = ""
    ) = Geography(
        taxContentProviderId = 0L,
        id = 0L,
        parentId = 0L,
        classification = classification,
        name = name,
        code = code
    )

    val UNITED_STATES = createGeography(
        classification = GeographyClassification.COUNTRY,
        name = "UNITED STATES",
        code = "US"
    )

    val ALASKA = createGeography(classification = GeographyClassification.STATE, name = "ALASKA", code = "AK")
    val CALIFORNIA = createGeography(classification = GeographyClassification.STATE, name = "CALIFORNIA", code = "CA")
    val FLORIDA = createGeography(classification = GeographyClassification.STATE, name = "FLORIDA", code = "FL")
    val NEW_YORK = createGeography(classification = GeographyClassification.STATE, name = "NEW YORK", code = "NY")
    val TEXAS = createGeography(classification = GeographyClassification.STATE, name = "TEXAS", code = "TX")
}
