package com.modios.taxsolution

import com.modios.taxsolution.calc.model.v1.TransactionLocation

object ServiceCommon {

    const val TAX_CONTENT_PROVIDER = "Cordiance US Tax Content"

    const val TENANT_NAME = "Modios"

    const val COMPANY_NAME = "ModCo"

    val ARIZONA_TAXING_LOCATION = TransactionLocation(
        country = USGeography.UNITED_STATES.code,
        state = USGeography.ALASKA.code
    )

    val CALIFORNIA_TAXING_LOCATION = TransactionLocation(
        country = USGeography.UNITED_STATES.code,
        state = USGeography.CALIFORNIA.code
    )

    val FLORIDA_TAXING_LOCATION = TransactionLocation(
        country = USGeography.UNITED_STATES.code,
        state = USGeography.FLORIDA.code
    )

    val NEW_YORK_TAXING_LOCATION = TransactionLocation(
        country = USGeography.UNITED_STATES.code,
        state = USGeography.NEW_YORK.code
    )

    val TEXAS_TAXING_LOCATION = TransactionLocation(
        country = USGeography.UNITED_STATES.code,
        state = USGeography.TEXAS.code
    )

    val TAXING_LOCATIONS = listOf(ARIZONA_TAXING_LOCATION, CALIFORNIA_TAXING_LOCATION, FLORIDA_TAXING_LOCATION, NEW_YORK_TAXING_LOCATION, TEXAS_TAXING_LOCATION)
}
