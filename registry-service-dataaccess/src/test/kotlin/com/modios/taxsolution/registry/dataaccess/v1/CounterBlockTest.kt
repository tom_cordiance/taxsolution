package com.modios.taxsolution.registry.dataaccess.v1

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import java.util.UUID

class CounterBlockTest {

    @Test
    fun testBlockNext() {
        val block = CounterBlock(idCounterBlockUUID = UUID.randomUUID(), start = 1, end = 100, blockSize = 5)

        Assertions.assertEquals(1L, block.getNext())
        Assertions.assertEquals(2L, block.getNext())
        Assertions.assertEquals(3L, block.getNext())
        Assertions.assertEquals(4L, block.getNext())
        Assertions.assertEquals(5L, block.getNext())
        Assertions.assertEquals(-1L, block.getNext())
    }

    @Test
    fun testGetNextCounterBlock() {
        val block = CounterBlock(idCounterBlockUUID = UUID.randomUUID(), start = 1, end = 500, blockSize = 100)
        (1..100).forEach { block.getNext() }

        Assertions.assertEquals(CounterBlock.ID_BLOCK_EXHAUSTED, block.getNext())
        val nextBlock = block.getNextCounterBlock()

        Assertions.assertEquals(101L, nextBlock.getNext())
    }

    @Test
    fun testBlockExhaustion() {
        val block = CounterBlock(idCounterBlockUUID = UUID.randomUUID(), start = 1, end = 100, blockSize = 100)
        (1..100L).forEach { Assertions.assertEquals(it, block.getNext()) }

        Assertions.assertEquals(CounterBlock.MAXIMUM_ID_REACHED, block.getNext())
    }
}
