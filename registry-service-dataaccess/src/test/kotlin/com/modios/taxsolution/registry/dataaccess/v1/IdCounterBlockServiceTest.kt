package com.modios.taxsolution.registry.dataaccess.v1

import io.mockk.Runs
import io.mockk.coEvery
import io.mockk.just
import io.mockk.mockk
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import java.util.UUID

class IdCounterBlockServiceTest {

    @Test
    fun testInitialization() = runBlocking {
        val idCounterBlockService = IdCounterBlockServiceImpl(idCounterBlockRepository, 100L)

        Assertions.assertEquals(301L, idCounterBlockService.getNextId(blockName1))
        Assertions.assertEquals(3001L, idCounterBlockService.getNextId(blockName2))

        (1L..100L).forEach { Assertions.assertEquals(it + 400L, idCounterBlockService.getNextId(blockName3)) }
        Assertions.assertThrows(IdBlockExhaustedException::class.java) {
            runBlocking {
                idCounterBlockService.getNextId(blockName3)
            }
        }

        Assertions.assertThrows(UninitializedIdBlockException::class.java) {
            runBlocking { idCounterBlockService.getNextId(blockName4) }
        }

        return@runBlocking
    }

    companion object {
        val blockName1 = "foo"
        val blockName2 = "bar"
        val blockName3 = "snert"
        val blockName4 = "something"

        val idCounterBlock1 = IdCounterBlock(
            blockUUID = UUID.randomUUID(),
            serviceKey = UUID.randomUUID(),
            name = blockName1,
            blockStart = 100,
            blockEnd = 500,
            current = 300
        )
        val idCounterBlock2 = IdCounterBlock(
            blockUUID = UUID.randomUUID(),
            serviceKey = UUID.randomUUID(),
            name = blockName2,
            blockStart = 1000,
            blockEnd = 5000,
            current = 3000
        )
        val idCounterBlock3 = IdCounterBlock(
            blockUUID = UUID.randomUUID(),
            serviceKey = UUID.randomUUID(),
            name = blockName3,
            blockStart = 100,
            blockEnd = 500,
            current = 400
        )

        val idCounterBlockRepository = mockk<IdCounterBlockRepository>().also {
            coEvery { it.findAll() } returns flowOf(idCounterBlock1, idCounterBlock2, idCounterBlock3)
            coEvery { it.updateCurrent(ofType(String::class), ofType(Long::class)) } just Runs
        }
    }
}
