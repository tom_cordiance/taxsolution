package com.modios.taxsolution.registry.dataaccess.v1

import org.springframework.data.annotation.Id
import org.springframework.data.r2dbc.repository.Query
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import org.springframework.data.repository.kotlin.CoroutineCrudRepository
import java.util.UUID

interface IdCounterBlockRepository : CoroutineCrudRepository<IdCounterBlock, UUID> {

    @Query("update CE_COUNTER_BLOCK set current=:current where name=:name")
    suspend fun updateCurrent(name: String, current: Long)
}

@Table("CE_COUNTER_BLOCK")
data class IdCounterBlock(
    @Id val blockUUID: UUID,
    @Column val serviceKey: UUID,
    @Column val name: String,
    @Column val blockStart: Long,
    @Column val blockEnd: Long,
    @Column val current: Long
)
