package com.modios.taxsolution.registry.dataaccess.v1

import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.runBlocking
import mu.KLogger
import mu.KotlinLogging
import org.springframework.stereotype.Component
import java.util.UUID

interface IdCounterBlockService {

    /**
     * Get the next ID for the block. If the block has been completely used and not replenished an exception will be thrown.
     *
     * @throws IdBlockExhaustedException if the IDBlock has been exhausted
     */
    suspend fun getNextId(blockName: String): Long
}

/**
 * @param idCounterBlockRepository the source of persisted IDBlock data
 * @param blockSize the size of a counter block
 */
@Component
class IdCounterBlockServiceImpl(
    private val idCounterBlockRepository: IdCounterBlockRepository,
    private val blockSize: Long
) : IdCounterBlockService {

    private val logger = KotlinLogging.logger { }

    private val counterBlockMap = mutableMapOf<String, CounterBlock>()

    init {
        logger.info { "Initializing counter block service..." }
        // runBlocking needed for 1) blocking this thread until completion and 2) the toList() call
        runBlocking {
            idCounterBlockRepository
                .findAll()
                .onEach {
                    // on initialization the current counters need to be updated to reserve the next segment
                    idCounterBlockRepository.updateCurrent(
                        name = it.name,
                        current = it.current + blockSize
                    )
                }
                .map {
                    // create the mapping pairs for the in-memory generation (map)
                    it.name to CounterBlock(
                        idCounterBlockUUID = it.blockUUID,
                        start = it.current + 1,
                        end = it.blockEnd,
                        blockSize = blockSize
                    )
                }
                .toList(mutableListOf())
                .also { counterBlockMap.putAll(it.toMap()) }
        }

        logger.info { "Counter block service initialized" }
    }

    /**
     * Get the next ID for the block. If the block has been completely used and not replenished an exception will be thrown.
     *
     * @throws IdBlockExhaustedException if the IDBlock has been exhausted
     */
    override suspend fun getNextId(blockName: String): Long {
        val counterBlock = counterBlockMap[blockName]
        return when (counterBlock) {
            null -> {
                // this should never happen, is an initialization error
                // counter blocks need to be requested before use from the registry service
                throw UninitializedIdBlockException(blockName)
            }
            else -> {
                val next = counterBlock.getNext()
                when (next) {
                    CounterBlock.MAXIMUM_ID_REACHED -> {
                        throw IdBlockExhaustedException(blockName)
                    }
                    CounterBlock.ID_BLOCK_EXHAUSTED -> {
                        // need to persist next block segment
                        val nextCounterBlock = counterBlock.getNextCounterBlock()
                        idCounterBlockRepository.updateCurrent(
                            name = blockName,
                            current = counterBlock.start + blockSize
                        )
                        counterBlockMap[blockName] = nextCounterBlock
                        return nextCounterBlock.getNext()
                    }
                    else -> {
                        next
                    }
                }
            }
        }
    }
}

/**
 * @param idCounterBlockUUID the UUID of the counter block
 * @param start the start of the counter block
 * @param end the end of the counter block, no ID values can be generated beyond this value
 * @param blockSize the size of the block
 */
data class CounterBlock(val idCounterBlockUUID: UUID, val start: Long, val end: Long, val blockSize: Long) {
    private var next = start

    fun getNextCounterBlock() = copy(start = start + blockSize)

    /**
     * -1 means block has been exhausted
     */
    fun getNext(): Long {
        return when (next > end) {
            true -> return MAXIMUM_ID_REACHED
            false -> {
                when (next < start + blockSize) {
                    true -> next++
                    false -> ID_BLOCK_EXHAUSTED
                }
            }
        }
    }

    companion object {
        const val ID_BLOCK_EXHAUSTED = -1L
        const val MAXIMUM_ID_REACHED = -2L
    }
}

class IdBlockExhaustedException(blockName: String) :
    RuntimeException("The ID Block $blockName has been exhausted. Check the log file for IDBlockService issues.")

class UninitializedIdBlockException(blockName: String) :
    RuntimeException("The ID Block $blockName not been initialized. Validate that block initialization has executed and this block is included.")

// TODO: move to common library
/**
 * Targeted to SLF4J
 */
inline fun <reified T> T.logger(): KLogger {
    return KotlinLogging.logger { }
}
