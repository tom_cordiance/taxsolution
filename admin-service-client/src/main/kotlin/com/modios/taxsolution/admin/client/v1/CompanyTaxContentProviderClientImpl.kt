package com.modios.taxsolution.admin.client.v1

import com.modios.taxsolution.model.v1.CompanyTaxContentProvider
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.toList
import mu.KotlinLogging
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.awaitBody
import org.springframework.web.reactive.function.client.bodyToFlow

@RestController
class CompanyTaxContentProviderClientImpl(private val adminWebClient: WebClient) :
    CompanyTaxContentProviderClient {

    private val baseUri = "/api/v1/company-tax-content-provider"

    private val logger = KotlinLogging.logger { }

    override suspend fun create(companyId: Long, taxContentProviderId: Long): CompanyTaxContentProvider {
        return adminWebClient
            .post()
            .uri("$baseUri/$companyId/$taxContentProviderId")
            .accept(MediaType.APPLICATION_JSON)
            .retrieve()
            .awaitBody()
    }

    override suspend fun getByCompanyId(companyId: Long): List<CompanyTaxContentProvider> {
        return adminWebClient
            .get()
            .uri("$baseUri/$companyId")
            .accept(MediaType.APPLICATION_JSON)
            .retrieve()
            .bodyToFlow<CompanyTaxContentProvider>()
            .catch { logger.error(it) { "A problem occurred while getting by company Id" } }
            .toList()
    }
}
