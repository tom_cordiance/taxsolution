package com.modios.taxsolution.admin.client.v1

import com.modios.taxsolution.model.v1.Tenant
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.awaitBody
import org.springframework.web.reactive.function.client.awaitBodyOrNull

@RestController
class TenantClientImpl(private val adminWebClient: WebClient) :
    TenantClient {

    private val baseUri = "/api/v1/tenant"

    override suspend fun createTenant(name: String): Tenant =
        adminWebClient
            .post()
            .uri("$baseUri/$name")
            .accept(MediaType.APPLICATION_JSON)
            .retrieve()
            .awaitBody()

    override suspend fun getTenant(name: String): Tenant? =
        adminWebClient
            .get()
            .uri("$baseUri/$name")
            .accept(MediaType.APPLICATION_JSON)
            .retrieve()
            .awaitBodyOrNull()

    override suspend fun getTenant(name: String, jwt: String): Tenant? =
        adminWebClient
            .get()
            .uri("$baseUri/$name")
            .accept(MediaType.APPLICATION_JSON)
            .headers { headers -> headers.setBearerAuth(jwt) }
            .retrieve()
            .awaitBodyOrNull()
}
