package com.modios.taxsolution.admin.client.v1

import com.modios.taxsolution.model.v1.Company
import kotlinx.coroutines.flow.toList
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.awaitBody
import org.springframework.web.reactive.function.client.awaitBodyOrNull
import org.springframework.web.reactive.function.client.bodyToFlow

@RestController
class CompanyClientImpl(private val adminWebClient: WebClient) :
    CompanyClient {

    private val baseUri = "/api/v1/company"

    override suspend fun create(tenantId: Long, parentCompanyId: Long, name: String): Company =
        adminWebClient
            .post()
            .uri("$baseUri/$tenantId/$parentCompanyId/$name")
            .accept(MediaType.APPLICATION_JSON)
            .retrieve()
            .awaitBody()

    override suspend fun getCompanies(tenantId: Long): List<Company> =
        adminWebClient
            .get()
            .uri("$baseUri/$tenantId")
            .accept(MediaType.APPLICATION_JSON)
            .retrieve()
            .bodyToFlow<Company>()
            .toList()

    override suspend fun getCompany(tenantId: Long, name: String): Company? =
        adminWebClient
            .get()
            .uri("$baseUri/$tenantId/$name")
            .accept(MediaType.APPLICATION_JSON)
            .retrieve()
            .awaitBodyOrNull()
}
