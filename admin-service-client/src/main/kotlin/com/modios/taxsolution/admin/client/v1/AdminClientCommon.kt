package com.modios.taxsolution.admin.client.v1

object AdminClientCommon {
    const val TOP_LEVEL_PARENT_ID: Long = -1L
}
