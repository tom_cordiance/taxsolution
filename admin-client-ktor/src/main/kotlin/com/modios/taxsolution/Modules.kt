package com.modios.taxsolution

import com.modios.taxsolution.admin.client.v1.CompanyClient
import com.modios.taxsolution.admin.client.v1.CompanyClientImpl
import com.modios.taxsolution.admin.client.v1.CompanyTaxContentProviderClient
import com.modios.taxsolution.admin.client.v1.CompanyTaxContentProviderClientImpl
import com.modios.taxsolution.admin.client.v1.TenantClient
import com.modios.taxsolution.admin.client.v1.TenantClientImpl
import com.modios.taxsolution.content.client.v1.GeographyClient
import com.modios.taxsolution.content.client.v1.GeographyClientImpl
import com.modios.taxsolution.content.client.v1.TaxAuthorityClient
import com.modios.taxsolution.content.client.v1.TaxAuthorityClientImpl
import com.modios.taxsolution.content.client.v1.TaxAuthorityGeographyClient
import com.modios.taxsolution.content.client.v1.TaxAuthorityGeographyClientImpl
import com.modios.taxsolution.content.client.v1.TaxContentProviderClient
import com.modios.taxsolution.content.client.v1.TaxContentProviderClientImpl
import com.modios.taxsolution.content.client.v1.TaxRateClient
import com.modios.taxsolution.content.client.v1.TaxRateClientImpl
import io.ktor.client.HttpClient
import io.ktor.client.engine.cio.CIO
import io.ktor.client.features.json.JsonFeature
import org.koin.dsl.module
import java.util.Properties

val modules = module {
    single {
        Properties().apply {
            this.put("cordiance.admin-service.url", "http://localhost:8001")
            this.put("cordiance.tax-content-service.url", "http://localhost:8002")
        } as Properties
    }

    single {
        HttpClient(CIO) {
            install(JsonFeature)
            expectSuccess = true
            engine {
            }
        }
    }

    single { TenantClientImpl(get(), get()) as TenantClient }
    single { CompanyClientImpl(get(), get()) as CompanyClient }
    single { CompanyTaxContentProviderClientImpl(get(), get()) as CompanyTaxContentProviderClient }

    single { GeographyClientImpl(get(), get()) as GeographyClient }
    single { TaxAuthorityClientImpl(get(), get()) as TaxAuthorityClient }
    single { TaxAuthorityGeographyClientImpl(get(), get()) as TaxAuthorityGeographyClient }
    single { TaxContentProviderClientImpl(get(), get()) as TaxContentProviderClient }
    single { TaxRateClientImpl(get(), get()) as TaxRateClient }
}
