package com.modios.taxsolution.model.v1

import kotlinx.serialization.Serializable

@Serializable
data class CompanyTaxContentProvider(val id: Long, val companyId: Long, val taxContentProviderId: Long)
