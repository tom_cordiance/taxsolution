package com.modios.taxsolution.model.v1

import com.modios.service.LocalDateKSerializer
import kotlinx.serialization.Serializable
import java.time.LocalDate

@Serializable
data class Tenant(
    val id: Long,
    val name: String,
    @kotlinx.serialization.Serializable(with = LocalDateKSerializer::class)
    val activeFrom: LocalDate,
    @kotlinx.serialization.Serializable(with = LocalDateKSerializer::class)
    val activeTo: LocalDate?
)
