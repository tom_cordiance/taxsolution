package com.modios.taxsolution.model.v1

import kotlinx.serialization.Serializable

@Serializable
data class Company(
    val tenantId: Long,
    val parentCompanyId: Long?,
    val id: Long,
    val name: String
)
