package com.modios.taxsolution.admin.client.v1

import com.modios.taxsolution.model.v1.Company

interface CompanyClient {
    suspend fun create(tenantId: Long, parentCompanyId: Long, name: String): Company

    suspend fun getCompanies(tenantId: Long): List<Company>

    suspend fun getCompany(tenantId: Long, name: String): Company?
}
