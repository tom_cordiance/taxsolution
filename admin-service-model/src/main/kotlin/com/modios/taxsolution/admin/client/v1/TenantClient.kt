package com.modios.taxsolution.admin.client.v1

import com.modios.taxsolution.model.v1.Tenant

interface TenantClient {
    suspend fun createTenant(name: String): Tenant

    suspend fun getTenant(name: String): Tenant?

    suspend fun getTenant(name: String, jwt: String): Tenant?
}
