package com.modios.taxsolution.admin.client.v1

import com.modios.taxsolution.model.v1.CompanyTaxContentProvider

interface CompanyTaxContentProviderClient {
    suspend fun create(
        companyId: Long,
        taxContentProviderId: Long
    ): CompanyTaxContentProvider

    suspend fun getByCompanyId(companyId: Long): List<CompanyTaxContentProvider>
}
