# Installation of ELK via HomeBrew

## Install Elastic
% brew tap elastic/tap
% brew install elastic/tap/elasticsearch-full

Data:    /usr/local/var/lib/elasticsearch/elasticsearch_tom/
Logs:    /usr/local/var/log/elasticsearch/elasticsearch_tom.log
Plugins: /usr/local/var/elasticsearch/plugins/
Config:  /usr/local/etc/elasticsearch/

To have launchd start elastic/tap/elasticsearch-full now and restart at login:
brew services start elastic/tap/elasticsearch-full
Or, if you don't want/need a background service you can just run:
elasticsearch

## Install Kibana
% brew tap elastic/tap
% brew install elastic/tap/kibana-full

Config: /usr/local/etc/kibana/
If you wish to preserve your plugins upon upgrade, make a copy of
/usr/local/opt/kibana-full/plugins before upgrading, and copy it into the
new keg location after upgrading.

To have launchd start elastic/tap/kibana-full now and restart at login:
brew services start elastic/tap/kibana-full
Or, if you don't want/need a background service you can just run:
kibana

Kibana UI: http://localhost:5601

## Install Filebeat
% brew tap elastic/tap
% brew install elastic/tap/filebeat-full

To have launchd start elastic/tap/filebeat-full now and restart at login:
brew services start elastic/tap/filebeat-full
Or, if you don't want/need a background service you can just run:
filebeat