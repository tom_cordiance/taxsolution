package com.modios.taxsolution.registry.v1

import com.modios.taxsolution.registry.model.v1.IdBlock
import kotlinx.coroutines.flow.single
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.bodyToFlow
import java.util.UUID

@RestController
class RegistryClient(private val idWebClient: WebClient) : RegistryService {

    private val baseUri = "/api/v1/id"

    suspend fun getIdBlock(tenantId: Long, blockSize: Long): IdBlock {
        return idWebClient
            .get()
            .uri("$baseUri/$tenantId/$blockSize")
            .accept(MediaType.APPLICATION_JSON)
            .retrieve()
            .bodyToFlow<IdBlock>()
            .single()
    }

    /**
     * Need a way to identify valid service instances
     *
     * @param serviceName - the name of the service
     */
    override suspend fun registerService(serviceName: String): UUID {
        TODO("Not yet implemented")
    }

    /**
     * Allows a service to reserve a block of IDs.  These are permanently reserved and unavailable to other callers.
     *
     * @param serviceUUID the key of the registered service
     * @param blockName the name of the counter, i.e. tablename
     * @param blockSize the size of the block
     */
    override suspend fun reserveIdBlock(serviceUUID: UUID, blockName: String, blockSize: Long): IdBlock {
        TODO("Not yet implemented")
    }

    /**
     * Allow a reserved ID block to be able to be released and reused
     */
    override suspend fun releaseIdBlock(serviceKey: UUID, blockUUID: UUID) {
        TODO("Not yet implemented")
    }
}
