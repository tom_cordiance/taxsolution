# Next Generation Indirect Tax Engine

## General Philosophy

- Extension/evolution of tax verticals/markets/domains independently of each other
- Deployment of updates without an end-user service disruption
- CPU-bound performance - focus of in-memory operations / vertical (more CPU) and horizontal (instance clustering) scalability
- Global tax support
- Muti-tenant support - allow for mutiple tenants to be deployed and supported in a single implementation/cluster
- Data generated in-place, meaning there are no external services used to manage data. 

## Architectural Philosophy

- Generic tax document approach
  - Calculation based up on the contents of a tax document
  - A tax document is composed of 
    - Metadata elements that define type and version (this is optional, as the type can be managed by engine/modules, see below)
    - a generic set of document elements
    - one or more generic document items
    - a tax document is only strictly defined as above; it is the responsiblity of the client and engine/modules to manage the types/versions of tax documents
- Decoupled architecture
  - Web services for tax content management
  - Web services for company tax footprint/configuration management
  - Web services for tax result management
  - Modular engine design
    - Independent modules perform tax operations
    - Engines composed of one or more tax modules
    - Engines can be deployed with many configurations based upon the modules used to compose the engine
- In-memory architecture for calculations
  - A focus on keeping tax calculations CPU-bound
  - Data caching to allow a focus on decoupling and staying CPU-bound
- Asynchronous architecture
  - Language-level implementations that focus on maximizing throughput via asynchronous operations
  - Implementation focus on calculation performance and throughput by avoiding direct IO-bound operations (asynchronous handling of tax results for example)
- Stateless server/service architecture
  - Versioned service endpoints - service interfaces/contracts honored allowing services upgrades without requiring client changes (this is independent of version end-of-life)
  - Rolling updates or canary deployments - updates can be done in-place
- Clusterable services supporting multiple tenants
  - Pod - finite group of clustered instances that support a finite number of tenants - something like 16 instances clustered to support 5-30 tenants
  - Supports migration of tenants seamlessly from one pod to another, assuming pod support of tenant taxing requirements (tax vertical/version/etc)

## Components

### Data Cache

Custom Cordiance cache to support in-memory focus and CPU-bound calculation operations

* Based on Ctrie architecture - see https://dzone.com/articles/lose-the-lock-while-embracing-concurrency-brave-ne and https://github.com/lgzh1215/Ctrie.
  * lock-free
  * High throughput
* Implemented with a focus on decoupling
  * Cache decoupled from entity frameworks
  * Services/repositories coordinate cache operations
  * Immutable instances cached
  * Instances wrapped in cache result sets allowing for Null/Singleton/Plural result sets to be identified

## Notes

ID generation
* needs to be portable, hence unique
* central ID block service for reserving ID blocks
* Registry service is NEVER available publicly, only privately to the Tax Engine services

## Project To-Do List

* Persisting of tax results (audit)
  * ~~Needs to not appreciably increase calc time, so~~
  * ~~Most likely has a calc internal component for short-term persistence that then is processed
    and persistend long term~~
  * ~~Something like internal quick-persist (Kafka) then async process to long-term-persist (Some DB)~~
  * Note - time-series candidate for retrieval, reports
  
* ~~Service caches on calc side functional~~

* Error handling
  * ~~missing data - 404 service call~~
  * exceptions during service call
  * exceptions during calc - not service related
  * etc
  
* Metrics
  * ~~Using Micrometer, supports many standard Metrics servers~~
  * ~~What is required on application side?~~
  
* Logging
  * ~~Elk~~
  * ~~Elk logger - see Elk PoC~~
  
* ~~Calc Coroutines~~
  * ~~Need a pipeline~~
  * ~~Need a reactive stream model - only accept calc requests if able to process~~
  
* Data Unique Identification
  
  * Numeric unique ID generation - see article
* Data Management
  
  * Test based upon start/active dates.  New data active date set to MAX_DATE to indicate not active.  Or all records have an active field that when enabled becomes production data.
  * Promotion - test to production - set start/active dates
    * Why not do UAT in place? Different tables, promote is internal move.
    * Must be seamless - no extra work to the end user
    * Allow for demotion? Meaning prod data migrates back to test as next starting point?
  * Migration - one pod to another
    * Is there a use case for multiple pods and the migration of a client to another pod?
    * If so then it needs to be seamless - no extra work for the client
  
* Calc requirements
  * Forward calcs
  * Reverse calcs - need to spec this
  
* Automated integration testing 
  * Needs to be standard part of build
  * Needs to be true integration test - running services launched into some environment
  
* Tax calc messages
  * Need to be defined as constants, not implemented inline
  * Need to be globalized to support localization
  
* Services

  * Admin - tenants, tenant companies/taxpayers/legal entities, licensing, service keys, etc
  * Content - standard tax data
  * Company configuration - company-specific tax data
  * Calculation - performs tax calculations
  * Tax Result Intermediate Storage - where raw tax results are stored, intermediate storage before Compliance service. Implemented using Kafka or something similar (write data serially/read data serially)
  * Compliance - tax result storage for reports and returns

* Implementation

  * Kafka for Tax Result Intermediate Storage
  * Cassandra for Compliance service - tax result data is natively time-series data - tax results per authority over time - plus handles large amounts of data
  * Spring supports both
  * Need to create bare-iron, production ready, creation scripts for these
