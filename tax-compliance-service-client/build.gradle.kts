plugins {
    id("org.springframework.boot") apply false
    id("java-library")
    kotlin("plugin.spring") apply true
}

val logbackVersion: String by project
val slf4jApiVersion: String by project
val kotlinLoggingVersion: String by project

dependencies {
    implementation(enforcedPlatform(org.springframework.boot.gradle.plugin.SpringBootPlugin.BOM_COORDINATES))
    implementation("org.springframework.boot:spring-boot-starter-webflux")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core")

    implementation(project(":calc-service-model"))

    // Logging
    implementation("ch.qos.logback:logback-classic:$logbackVersion")
    implementation("org.slf4j:slf4j-api:$slf4jApiVersion")
    implementation("io.github.microutils:kotlin-logging:$kotlinLoggingVersion")
}
