package com.modios.taxsolution.content.client.v1

import com.modios.taxsolution.content.model.v1.TaxContentProvider
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.awaitBody
import org.springframework.web.reactive.function.client.awaitBodyOrNull

@RestController
class TaxContentProviderClientImpl(private val taxContentWebClient: WebClient) :
    TaxContentProviderClient {
    private val baseUri = "/api/v1/taxcontentprovider"

    override suspend fun create(name: String): TaxContentProvider {
        return taxContentWebClient
            .post()
            .uri("$baseUri/name/$name")
            .accept(MediaType.APPLICATION_JSON)
            .retrieve()
            .awaitBody()
    }

    override suspend fun get(name: String): TaxContentProvider? {
        return taxContentWebClient
            .get()
            .uri("$baseUri/name/$name")
            .accept(MediaType.APPLICATION_JSON)
            .retrieve()
            .awaitBodyOrNull()
    }

    override suspend fun get(id: Long): TaxContentProvider? {
        return taxContentWebClient
            .get()
            .uri("$baseUri/id/$id")
            .accept(MediaType.APPLICATION_JSON)
            .retrieve()
            .awaitBodyOrNull()
    }
}
