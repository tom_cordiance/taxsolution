package com.modios.taxsolution.content.client.v1

import com.modios.taxsolution.content.model.v1.TaxAuthorityGeography
import kotlinx.coroutines.flow.toList
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.awaitBody
import org.springframework.web.reactive.function.client.awaitBodyOrNull
import org.springframework.web.reactive.function.client.bodyToFlow

@RestController
class TaxAuthorityGeographyClientImpl(private val taxContentWebClient: WebClient) : TaxAuthorityGeographyClient {

    private val baseUri = "/api/v1/taxauthority-geography"

    override suspend fun create(taxContentProviderId: Long, geographyId: Long, taxAuthorityId: Long): TaxAuthorityGeography {
        return taxContentWebClient
            .post()
            .uri("$baseUri/$taxContentProviderId/$geographyId/$taxAuthorityId")
            .accept(MediaType.APPLICATION_JSON)
            .retrieve()
            .awaitBody()
    }

    override suspend fun getByGeographyId(geographyId: Long): List<TaxAuthorityGeography> {
        return taxContentWebClient
            .get()
            .uri("$baseUri/geography/$geographyId")
            .accept(MediaType.APPLICATION_JSON)
            .retrieve()
            .bodyToFlow<TaxAuthorityGeography>()
            .toList()
    }

    override suspend fun getByTaxAuthorityId(taxAuthorityId: Long): List<TaxAuthorityGeography> {
        return taxContentWebClient
            .get()
            .uri("$baseUri/tax-authority/$taxAuthorityId")
            .accept(MediaType.APPLICATION_JSON)
            .retrieve()
            .bodyToFlow<TaxAuthorityGeography>()
            .toList()
    }

    override suspend fun getByGeographyIdAndTaxAuthorityId(
        geographyId: Long,
        taxAuthorityId: Long
    ): TaxAuthorityGeography? {
        return taxContentWebClient
            .get()
            .uri("$baseUri/$geographyId/$taxAuthorityId")
            .accept(MediaType.APPLICATION_JSON)
            .retrieve()
            .awaitBodyOrNull()
    }
}
