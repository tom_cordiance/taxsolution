package com.modios.taxsolution.content.client.v1

import com.modios.taxsolution.content.model.v1.TaxRate
import kotlinx.coroutines.flow.toList
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.awaitBody
import org.springframework.web.reactive.function.client.awaitBodyOrNull
import org.springframework.web.reactive.function.client.bodyToFlow
import reactor.core.publisher.Mono

@RestController
class TaxRateClientImpl(private val taxContentWebClient: WebClient) : TaxRateClient {

    private val baseUri = "/api/v1/tax-rate"

    override suspend fun create(taxRate: TaxRate): TaxRate {
        return taxContentWebClient
            .post()
            .uri(baseUri)
            .accept(MediaType.APPLICATION_JSON)
            .body(Mono.just(taxRate), TaxRate::class.java)
            .retrieve()
            .awaitBody()
    }

    override suspend fun findByTaxAuthorityId(taxAuthorityId: Long): List<TaxRate> {
        return taxContentWebClient
            .get()
            .uri("$baseUri/authority/$taxAuthorityId")
            .accept(MediaType.APPLICATION_JSON)
            .retrieve()
            .bodyToFlow<TaxRate>()
            .toList()
    }

    override suspend fun findById(id: Long): TaxRate? {
        return taxContentWebClient
            .get()
            .uri("$baseUri/$id")
            .accept(MediaType.APPLICATION_JSON)
            .retrieve()
            .awaitBodyOrNull()
    }

    override suspend fun update(taxRate: TaxRate): TaxRate {
        return taxContentWebClient
            .put()
            .uri(baseUri)
            .accept(MediaType.APPLICATION_JSON)
            .body(Mono.just(taxRate), TaxRate::class.java)
            .retrieve()
            .awaitBody()
    }
}
