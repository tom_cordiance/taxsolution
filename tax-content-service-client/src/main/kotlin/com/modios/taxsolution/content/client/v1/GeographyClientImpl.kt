package com.modios.taxsolution.content.client.v1

import com.modios.taxsolution.content.model.v1.Geography
import com.modios.taxsolution.content.model.v1.GeographyClassification
import kotlinx.coroutines.flow.toList
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.awaitBody
import org.springframework.web.reactive.function.client.awaitBodyOrNull
import org.springframework.web.reactive.function.client.bodyToFlow
import reactor.core.publisher.Mono

@RestController
class GeographyClientImpl(private val taxContentWebClient: WebClient) : GeographyClient {

    private val baseUri = "/api/v1/geography"

    override suspend fun create(geography: Geography): Geography {
        return taxContentWebClient
            .post()
            .uri(baseUri)
            .accept(MediaType.APPLICATION_JSON)
            .body(Mono.just(geography), Geography::class.java)
            .retrieve()
            .awaitBody()
    }

    override suspend fun create(
        taxContentProviderId: Long,
        classificationId: Int,
        parentId: Long,
        name: String,
        code: String
    ): Geography {
        val geography = Geography(
            id = 0L,
            taxContentProviderId = taxContentProviderId,
            classification = GeographyClassification.fromId(classificationId),
            parentId = parentId,
            name = name,
            code = code
        )

        return create(geography)
    }

    override suspend fun getAllByName(name: String): List<Geography> {
        return taxContentWebClient
            .get()
            .uri { uriBuilder ->
                uriBuilder
                    .path("$baseUri/name")
                    .queryParam("name", name)
                    .build()
            }
            .accept(MediaType.APPLICATION_JSON)
            .retrieve()
            .bodyToFlow<Geography>()
            .toList()
    }

    override suspend fun getByTaxContentProviderIdAndParentIdAndName(
        taxContentProviderId: Long,
        parentId: Long,
        name: String
    ): Geography? {
        return taxContentWebClient
            .get()
            .uri { uriBuilder ->
                uriBuilder
                    .path("$baseUri/parentAndName")
                    .queryParam("taxContentProviderId", taxContentProviderId)
                    .queryParam("parentId", parentId)
                    .queryParam("name", name)
                    .build()
            }
            .accept(MediaType.APPLICATION_JSON)
            .retrieve()
            .awaitBodyOrNull()
    }

    override suspend fun getByTaxContentProviderIdAndParentIdAndCode(
        taxContentProviderId: Long,
        parentId: Long,
        code: String
    ): Geography? {
        return taxContentWebClient
            .get()
            .uri { uriBuilder ->
                uriBuilder
                    .path("$baseUri/parentAndCode")
                    .queryParam("taxContentProviderId", taxContentProviderId)
                    .queryParam("parentId", parentId)
                    .queryParam("code", code)
                    .build()
            }
            .accept(MediaType.APPLICATION_JSON)
            .retrieve()
            .awaitBodyOrNull()
    }

    override suspend fun getByTaxContentProviderIdAndParentId(
        taxContentProviderId: Long,
        parentId: Long
    ): List<Geography> {
        return taxContentWebClient
            .get()
            .uri { uriBuilder ->
                uriBuilder
                    .path("$baseUri/providerAndParent")
                    .queryParam("taxContentProviderId", taxContentProviderId)
                    .queryParam("parentId", parentId)
                    .build()
            }
            .accept(MediaType.APPLICATION_JSON)
            .retrieve()
            .bodyToFlow<Geography>()
            .toList()
    }
}
