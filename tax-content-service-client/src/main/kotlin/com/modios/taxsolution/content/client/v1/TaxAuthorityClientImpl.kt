package com.modios.taxsolution.content.client.v1

import com.modios.taxsolution.content.model.v1.TaxAuthority
import kotlinx.coroutines.flow.singleOrNull
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.awaitBody
import org.springframework.web.reactive.function.client.awaitBodyOrNull
import org.springframework.web.reactive.function.client.bodyToFlow
import reactor.core.publisher.Mono
import java.time.LocalDate

@RestController
class TaxAuthorityClientImpl(private val taxContentWebClient: WebClient) : TaxAuthorityClient {

    private val baseUri = "/api/v1/taxauthority"

    override suspend fun create(
        taxContentProviderId: Long,
        name: String,
        startDate: LocalDate,
        endDate: LocalDate?
    ): TaxAuthority {
        val taxAuthority = TaxAuthority(
            taxContentProviderId = taxContentProviderId,
            id = 0,
            name = name,
            startDate = startDate,
            endDate = endDate
        )
        return taxContentWebClient
            .post()
            .uri(baseUri)
            .accept(MediaType.APPLICATION_JSON)
            .body(Mono.just(taxAuthority), TaxAuthority::class.java)
            .retrieve()
            .awaitBody()
    }

    override suspend fun getById(taxAuthorityId: Long): TaxAuthority? {
        return taxContentWebClient
            .get()
            .uri("$baseUri/$taxAuthorityId")
            .accept(MediaType.APPLICATION_JSON)
            .retrieve()
            .awaitBodyOrNull()
    }

    override suspend fun getByTaxContentProviderIdAndName(taxContentProviderId: Long, name: String): TaxAuthority? {
        return taxContentWebClient
            .get()
            .uri("$baseUri/$taxContentProviderId/$name")
            .accept(MediaType.APPLICATION_JSON)
            .retrieve()
            .bodyToFlow<TaxAuthority>()
            .singleOrNull()
    }
}
