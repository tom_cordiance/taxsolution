create table if not exists ts_tenants (id BIGINT NOT NULL, name VARCHAR(256) NOT NULL, active_from DATE NOT NULL, active_to DATE);
create unique index if not exists ts_tenant_id on ts_tenants (id);
create unique index if not exists ts_tenant_name on ts_tenants (name);

create table if not exists ts_companies (id BIGINT NOT NULL, tenant_id BIGINT NOT NULL, parent_company_id BIGINT, name VARCHAR(256) NOT NULL);
create unique index if not exists ts_company_id on ts_companies (id);
create unique index if not exists ts_company_name on ts_companies (name);

create table if not exists ts_company_tax_content_providers (id BIGINT NOT NULL, company_id BIGINT NOT NULL, tax_content_provider_id BIGINT NOT NULL);
create unique index if not exists ts_company_tax_content_provider_id on ts_company_tax_content_providers (id);
create index if not exists ts_company_tax_content_provider_n1 on ts_company_tax_content_providers (company_id, tax_content_provider_id);
