package com.modios.taxsolution

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class AdminServiceApp

fun main(args: Array<String>) {
    runApplication<AdminServiceApp>(*args)
}
