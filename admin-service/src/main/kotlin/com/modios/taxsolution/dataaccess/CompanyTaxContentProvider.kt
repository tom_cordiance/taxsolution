package com.modios.taxsolution.dataaccess

import org.springframework.data.annotation.Id
import org.springframework.data.annotation.PersistenceConstructor
import org.springframework.data.annotation.Transient
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table

@Table("ts_company_tax_content_providers")
data class CompanyTaxContentProvider(
    @Id val id: Long,
    @Column val companyId: Long,
    @Column val taxContentProviderId: Long,
    @Transient val createNew: Boolean
) : PersistableEntity(id, createNew) {

    @PersistenceConstructor
    constructor(id: Long, companyId: Long, taxContentProviderId: Long) : this(
        id = id,
        companyId = companyId,
        taxContentProviderId = taxContentProviderId,
        createNew = false
    )
}
