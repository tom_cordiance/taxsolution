package com.modios.taxsolution.dataaccess

import org.springframework.data.annotation.Id
import org.springframework.data.annotation.PersistenceConstructor
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table

/**
 * Note: parentCompanyId is only for visual representation and data relations,
 * a company name is the unique identifier by tenant.
 */
@Table("ts_companies")
class Company(
    @Id val id: Long,
    @Column val tenantId: Long,
    @Column val parentCompanyId: Long? = null,
    @Column val name: String,
    createNew: Boolean = false
) : PersistableEntity(id, createNew) {

    @PersistenceConstructor
    constructor(
        id: Long,
        tenantId: Long,
        parentCompanyId: Long,
        name: String
    ) : this(id = id, tenantId = tenantId, parentCompanyId = parentCompanyId, name = name, createNew = false)
}
