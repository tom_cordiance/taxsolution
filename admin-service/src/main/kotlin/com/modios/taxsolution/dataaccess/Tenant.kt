package com.modios.taxsolution.dataaccess

import org.springframework.data.annotation.Id
import org.springframework.data.annotation.PersistenceConstructor
import org.springframework.data.annotation.Transient
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import java.time.LocalDate

@Table("ts_tenants")
data class Tenant(
    @Id val id: Long,
    @Column val name: String,
    @Column val activeFrom: LocalDate = LocalDate.now(),
    @Column val activeTo: LocalDate? = null,
    @Transient val createNew: Boolean = false
) : PersistableEntity(id, createNew) {

    @PersistenceConstructor
    constructor(
        id: Long,
        name: String,
        activeFrom: LocalDate,
        activeTo: LocalDate?
    ) : this(id = id, name = name, activeFrom = activeFrom, activeTo = activeTo, createNew = false)
}
