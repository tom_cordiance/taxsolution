package com.modios.taxsolution.dataaccess

import kotlinx.coroutines.flow.Flow
import org.springframework.data.repository.kotlin.CoroutineCrudRepository

interface CompanyTaxContentProviderRepository : CoroutineCrudRepository<CompanyTaxContentProvider, Long> {

    suspend fun findByCompanyId(companyId: Long): Flow<CompanyTaxContentProvider>
}
