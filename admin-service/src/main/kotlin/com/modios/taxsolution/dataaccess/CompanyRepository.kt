package com.modios.taxsolution.dataaccess

import kotlinx.coroutines.flow.Flow
import org.springframework.data.repository.kotlin.CoroutineCrudRepository

interface CompanyRepository : CoroutineCrudRepository<Company, Long> {

    suspend fun findByTenantIdAndName(tenantId: Long, name: String): Company?

    suspend fun findAllByTenantId(tenantId: Long): Flow<Company>
}
