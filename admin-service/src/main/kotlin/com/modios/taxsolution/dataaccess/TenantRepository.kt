package com.modios.taxsolution.dataaccess

import org.springframework.data.repository.kotlin.CoroutineCrudRepository

interface TenantRepository : CoroutineCrudRepository<Tenant, Long> {

    suspend fun findByName(name: String): Tenant?
}
