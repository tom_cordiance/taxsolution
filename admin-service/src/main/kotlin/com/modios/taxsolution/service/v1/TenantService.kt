package com.modios.taxsolution.service.v1

import com.modios.service.IdGenerator
import com.modios.taxsolution.dataaccess.Tenant
import com.modios.taxsolution.dataaccess.TenantRepository
import mu.KotlinLogging
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/v1/tenant")
class TenantService(private val tenantRepository: TenantRepository) {

    private val logger = KotlinLogging.logger { }

    @PostMapping("{name}")
    suspend fun createTenant(@PathVariable name: String): com.modios.taxsolution.model.v1.Tenant {
        logger.debug { "createTenant($name)" }

        val tenant = Tenant(
            id = IdGenerator.instance.getNextId(),
            name = name,
            createNew = true
        )
        tenantRepository.save(tenant)
        logger.debug { "tenant created: $tenant" }

        return tenant.asServiceTenant()
    }

    @GetMapping("{name}")
    suspend fun getTentant(@PathVariable name: String): com.modios.taxsolution.model.v1.Tenant? {
        return tenantRepository.findByName(name)?.asServiceTenant()
    }
}

fun Tenant.asServiceTenant(): com.modios.taxsolution.model.v1.Tenant {
    return com.modios.taxsolution.model.v1.Tenant(
        id = this.entityId,
        name = this.name,
        activeFrom = this.activeFrom,
        activeTo = this.activeTo
    )
}
