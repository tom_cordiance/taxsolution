package com.modios.taxsolution.service.v1

import com.modios.service.IdGenerator
import com.modios.taxsolution.dataaccess.Company
import com.modios.taxsolution.dataaccess.CompanyRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import mu.KotlinLogging
import org.springframework.transaction.ReactiveTransactionManager
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/v1/company")
class CompanyService(
    private val transactionManager: ReactiveTransactionManager,
    private val companyRepository: CompanyRepository
) {

    private val logger = KotlinLogging.logger { }

    @PostMapping("{tenantId}/{parentCompanyId}/{name}")
    suspend fun create(
        @PathVariable tenantId: Long,
        @PathVariable parentCompanyId: Long,
        @PathVariable name: String
    ): com.modios.taxsolution.model.v1.Company {
        logger.debug { "create($name)" }

        val company = Company(
            tenantId = tenantId,
            parentCompanyId = parentCompanyId,
            id = IdGenerator.instance.getNextId(),
            name = name,
            createNew = true
        )

        companyRepository.save(company)

        return company.asServiceCompany()
    }

    @GetMapping("{tenantId}")
    suspend fun getCompanies(@PathVariable tenantId: Long): Flow<com.modios.taxsolution.model.v1.Company> {
        return companyRepository.findAllByTenantId(tenantId).map { it.asServiceCompany() }
    }

    @GetMapping("{tenantId}/{name}")
    suspend fun getCompany(
        @PathVariable tenantId: Long,
        @PathVariable name: String
    ): com.modios.taxsolution.model.v1.Company? {
        return companyRepository.findByTenantIdAndName(tenantId, name)?.asServiceCompany()
    }
}

fun Company.asServiceCompany(): com.modios.taxsolution.model.v1.Company {
    return com.modios.taxsolution.model.v1.Company(
        tenantId = this.tenantId,
        parentCompanyId = this.parentCompanyId,
        id = this.id,
        name = this.name
    )
}
