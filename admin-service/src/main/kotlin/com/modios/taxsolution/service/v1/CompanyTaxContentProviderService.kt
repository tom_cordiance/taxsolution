package com.modios.taxsolution.service.v1

import com.modios.service.IdGenerator
import com.modios.taxsolution.dataaccess.CompanyTaxContentProvider
import com.modios.taxsolution.dataaccess.CompanyTaxContentProviderRepository
import kotlinx.coroutines.flow.toList
import mu.KotlinLogging
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

interface CompanyTaxContentProviderService {

    suspend fun create(
        companyId: Long,
        taxContentProviderId: Long
    ): com.modios.taxsolution.model.v1.CompanyTaxContentProvider

    suspend fun getByCompanyId(companyId: Long): List<com.modios.taxsolution.model.v1.CompanyTaxContentProvider>
}

@RestController
@RequestMapping("api/v1/company-tax-content-provider")
class CompanyTaxContentProviderServiceImpl(private val companyTaxContentProviderRepository: CompanyTaxContentProviderRepository) :
    CompanyTaxContentProviderService {

    private val logger = KotlinLogging.logger { }

    @PostMapping("{companyId}/{taxContentProviderId}")
    override suspend fun create(
        @PathVariable companyId: Long,
        @PathVariable taxContentProviderId: Long
    ): com.modios.taxsolution.model.v1.CompanyTaxContentProvider {
        logger.debug { "create($companyId, $taxContentProviderId)" }

        val companyTaxContentProvider = CompanyTaxContentProvider(
            id = IdGenerator.instance.getNextId(),
            companyId = companyId,
            taxContentProviderId = taxContentProviderId,
            createNew = true
        )

        companyTaxContentProviderRepository.save(companyTaxContentProvider)

        logger.debug { "created: $companyTaxContentProvider" }

        return companyTaxContentProvider.asServiceCompanyTaxContentProvider()
    }

    @GetMapping("{companyId}")
    override suspend fun getByCompanyId(@PathVariable companyId: Long): List<com.modios.taxsolution.model.v1.CompanyTaxContentProvider> {
        logger.debug { "getByCompanyId($companyId)" }

        val result = companyTaxContentProviderRepository.findByCompanyId(companyId).toList()
        logger.debug { "found ${result.size} company tax content providers" }

        return result.map { it.asServiceCompanyTaxContentProvider() }
    }
}

fun CompanyTaxContentProvider.asServiceCompanyTaxContentProvider(): com.modios.taxsolution.model.v1.CompanyTaxContentProvider {
    return com.modios.taxsolution.model.v1.CompanyTaxContentProvider(
        id = this.id,
        companyId = this.companyId,
        taxContentProviderId = this.taxContentProviderId
    )
}
