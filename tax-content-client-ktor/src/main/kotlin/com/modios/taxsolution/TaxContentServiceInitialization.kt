package com.modios.taxsolution

import com.modios.taxsolution.content.client.v1.GeographyClient
import com.modios.taxsolution.content.client.v1.TaxAuthorityClient
import com.modios.taxsolution.content.client.v1.TaxAuthorityGeographyClient
import com.modios.taxsolution.content.client.v1.TaxContentProviderClient
import com.modios.taxsolution.content.client.v1.TaxRateClient
import com.modios.taxsolution.content.model.v1.Geography
import com.modios.taxsolution.content.model.v1.TaxAuthority
import com.modios.taxsolution.content.model.v1.TaxContentProvider
import com.modios.taxsolution.content.model.v1.TaxRate
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import mu.KotlinLogging
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import org.koin.core.context.startKoin

fun main(args: Array<String>) {
    startKoin {
        // declare modules
        modules(modules)
    }

    val doUpdate = args.any { it == "update" }
    val continuous = args.any { it == "continuous" }
    TaxContentServiceInitialization().run(doUpdate = doUpdate, continuous = continuous)
}

class TaxContentServiceInitialization : KoinComponent {

    private val logger = KotlinLogging.logger { }

    private val taxContentProviderClient: TaxContentProviderClient by inject()

    private val geographyClient: GeographyClient by inject()

    private val taxAuthorityClient: TaxAuthorityClient by inject()

    private val taxAuthorityGeographyClient: TaxAuthorityGeographyClient by inject()

    private val taxRateClient: TaxRateClient by inject()

    private suspend fun createUSTaxContentProvider(): TaxContentProvider {
        val taxContentProviderName = "Cordiance US Tax Content"

        logger.info { "getting $taxContentProviderName" }
        var taxContentProvider = taxContentProviderClient.get(taxContentProviderName)
        when (taxContentProvider) {
            null -> {
                logger.info { "could not find $taxContentProviderName, creating..." }
                taxContentProvider = taxContentProviderClient.create(taxContentProviderName)
                logger.info { "tax content provider created: $taxContentProvider" }
            }
            else -> {
                logger.info { "tax content provider found: $taxContentProvider" }
            }
        }

        return taxContentProvider
    }

    private suspend fun createUSGeography(taxContentProviderId: Long): List<Geography> {
        logger.info { "creating US geography..." }

        logger.info { "getting ${USGeography.UNITED_STATES}..." }
        var unitedStates = geographyClient.getByTaxContentProviderIdAndParentIdAndName(
            taxContentProviderId = taxContentProviderId,
            parentId = Geography.WORLD_ID,
            name = USGeography.UNITED_STATES.name
        )

        when (unitedStates) {
            null -> {
                logger.info { "creating ${USGeography.UNITED_STATES}..." }
                unitedStates = geographyClient.create(
                    USGeography.UNITED_STATES.copy(
                        taxContentProviderId = taxContentProviderId,
                        parentId = Geography.WORLD_ID
                    )
                )
                logger.info { "created: $unitedStates" }
            }
            else -> {
                logger.info { "geography found: $unitedStates" }
            }
        }

        logger.info { "creating states..." }
        return USGeography.US_STATES.map { geography ->
            logger.info { "getting $geography..." }
            val foundState = geographyClient.getByTaxContentProviderIdAndParentIdAndName(
                taxContentProviderId = taxContentProviderId,
                parentId = unitedStates.id,
                name = geography.name
            )

            when (foundState) {
                null -> {
                    logger.info { "creating $geography..." }
                    val state = geographyClient.create(
                        geography.copy(
                            taxContentProviderId = taxContentProviderId,
                            parentId = unitedStates.id
                        )
                    )
                    logger.info { "created $state" }

                    state
                }
                else -> {
                    logger.info { "geography found: $foundState" }
                    foundState
                }
            }
        }
    }

    private suspend fun createUSTaxAuthorities(taxContentProviderId: Long): List<TaxAuthority> {
        logger.info { "creating US tax authorities..." }

        return USTaxAuthorities.US_TAX_AUTHORITIES.map { taxAuthority ->
            logger.info { "getting ${taxAuthority.name}..." }
            val foundTaxAuthority = taxAuthorityClient.getByTaxContentProviderIdAndName(
                taxContentProviderId = taxContentProviderId,
                name = taxAuthority.name
            )

            when (foundTaxAuthority) {
                null -> {
                    logger.info { "creating ${taxAuthority.name}" }
                    val createdTaxAuthority = taxAuthorityClient.create(
                        taxContentProviderId = taxContentProviderId,
                        name = taxAuthority.name,
                        startDate = textToDate("01/01/2000"),
                        endDate = null
                    )

                    logger.info { "created $createdTaxAuthority" }
                    createdTaxAuthority
                }
                else -> {
                    logger.info { "found $foundTaxAuthority" }
                    foundTaxAuthority
                }
            }
        }
    }

    private suspend fun createUSTaxRates(taxContentProviderId: Long, taxAuthorityList: List<TaxAuthority>) {
        USTaxRates.US_TAX_RATES.forEach { (authorityName, rate) ->
            val taxAuthority = taxAuthorityList.find { it.name == authorityName }
                ?: throw RuntimeException("TaxAuthority not found: $authorityName")

            val foundRates = taxRateClient.findByTaxAuthorityId(taxAuthorityId = taxAuthority.id)

            when (foundRates.find { it.code == rate.code }) {
                null -> {
                    val taxRate = taxRateClient.create(
                        TaxRate(
                            taxContentProviderId = taxContentProviderId,
                            taxAuthorityId = taxAuthority.id,
                            id = 0,
                            code = rate.code,
                            rate = rate.rate,
                            startDate = rate.startDate,
                            endDate = rate.endDate
                        )
                    )

                    logger.info { "created tax rate $taxRate for $authorityName" }
                }
                else -> {
                    val existing = foundRates.firstOrNull { it.code == rate.code && it.rate != rate.rate }
                    when (existing) {
                        null -> logger.info { "rate exists for $authorityName and is correct: ${rate.rate}/${rate.code}" }
                        else -> {
                            logger.info { "rate exists for $authorityName but needs to be updated: ${rate.rate}/${rate.code}" }
                            taxRateClient.update(
                                TaxRate(
                                    taxContentProviderId = taxContentProviderId,
                                    taxAuthorityId = taxAuthority.id,
                                    id = existing.id,
                                    code = rate.code,
                                    rate = rate.rate,
                                    startDate = rate.startDate,
                                    endDate = rate.endDate
                                )
                            )
                        }
                    }
                }
            }
        }
    }

    private suspend fun updateUSTaxRates(taxContentProviderId: Long) {
        USTaxRateUpdates.US_TAX_RATE_UPDATES.forEach { (authorityName, rate) ->
            val taxAuthority = taxAuthorityClient.getByTaxContentProviderIdAndName(
                taxContentProviderId = taxContentProviderId,
                name = authorityName
            )
                ?: throw RuntimeException("TaxAuthority not found: $authorityName")

            val foundRates = taxRateClient.findByTaxAuthorityId(taxAuthorityId = taxAuthority.id)
            val foundRate = foundRates.find { it.code == rate.code }
            when (foundRate) {
                null -> logger.info { "rate not found for $authorityName: code ${rate.code}" }
                else -> {
                    val taxRate = taxRateClient.update(
                        TaxRate(
                            taxContentProviderId = taxContentProviderId,
                            taxAuthorityId = taxAuthority.id,
                            id = foundRate.id,
                            code = rate.code,
                            rate = rate.rate,
                            startDate = rate.startDate,
                            endDate = rate.endDate
                        )
                    )

                    logger.info { "updated tax rate $taxRate for $authorityName" }
                }
            }
        }
    }

    private suspend fun createTaxingGeography(
        taxContentProviderId: Long,
        geographyList: List<Geography>,
        taxAuthorityList: List<TaxAuthority>
    ) {
        USTaxAuthorityMapping.getAuthorityMapping().forEach { (geo, taxAuth) ->
            val geography = geographyList.find { it.code == geo.code }
                ?: throw RuntimeException("Geography not found: ${geo.code}")
            val taxAuthority = taxAuthorityList.find { it.name == taxAuth.name }
                ?: throw RuntimeException("TaxAuthority not found: ${taxAuth.name}")

            val foundTaxingGeography = taxAuthorityGeographyClient.getByGeographyIdAndTaxAuthorityId(
                taxAuthorityId = taxAuthority.id,
                geographyId = geography.id
            )

            when (foundTaxingGeography) {
                null -> {
                    taxAuthorityGeographyClient.create(
                        taxContentProviderId = taxContentProviderId,
                        geographyId = geography.id,
                        taxAuthorityId = taxAuthority.id
                    )

                    logger.info { "created TaxingGeography for ${geography.name}/${taxAuthority.name}" }
                }
                else -> logger.info { "found TaxingGeography for ${geography.name}/${taxAuthority.name}" }
            }
        }
    }

    fun run(doUpdate: Boolean = false, continuous: Boolean = false) = runBlocking {
        val taxContentProvider = createUSTaxContentProvider()

        when (continuous) {
            true -> {
                var create = true
                while (true) {
                    if (create) {
                        create = false
                        val usTaxAuthorities = createUSTaxAuthorities(taxContentProvider.id)
                        createUSTaxRates(
                            taxContentProviderId = taxContentProvider.id,
                            taxAuthorityList = usTaxAuthorities
                        )
                    } else {
                        create = true
                        updateUSTaxRates(taxContentProvider.id)
                    }

                    delay(20000)
                }
            }
            false -> {
                when (doUpdate) {
                    false -> {
                        val usStates = createUSGeography(taxContentProvider.id)
                        val usTaxAuthorities = createUSTaxAuthorities(taxContentProvider.id)
                        createUSTaxRates(
                            taxContentProviderId = taxContentProvider.id,
                            taxAuthorityList = usTaxAuthorities
                        )
                        createTaxingGeography(
                            taxContentProviderId = taxContentProvider.id,
                            geographyList = usStates,
                            taxAuthorityList = usTaxAuthorities
                        )
                    }
                    true -> {
                        updateUSTaxRates(taxContentProvider.id)
                    }
                }
            }
        }
    }
}
