package com.modios.taxsolution

import com.modios.taxsolution.content.model.v1.Geography
import com.modios.taxsolution.content.model.v1.TaxAuthority
import mu.KotlinLogging

object USTaxAuthorityMapping {
    private val logger = KotlinLogging.logger { }

    fun getAuthorityMapping(): Map<Geography, TaxAuthority> =
        USGeography
            .US_STATES
            .map { geography ->
                val authority = USTaxAuthorities.US_TAX_AUTHORITIES.first {
                    it.name.lowercase().startsWith(geography.name.lowercase())
                }
                logger.info { "Authority mapping: ${authority.name} to ${geography.name}" }
                geography to authority
            }.toMap()
}
