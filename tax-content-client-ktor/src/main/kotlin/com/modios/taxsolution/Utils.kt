package com.modios.taxsolution

import java.time.LocalDate
import java.time.format.DateTimeFormatter

/**
 * Date pattern: MM/dd/yyyy
 */
fun textToDate(dateText: String): LocalDate {
    return LocalDate.parse(dateText, DateTimeFormatter.ofPattern("MM/dd/yyyy"))
}
