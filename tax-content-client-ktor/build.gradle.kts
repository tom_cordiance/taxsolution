plugins {
    id("application")
    kotlin("plugin.serialization") apply true
}

val logbackVersion: String by project
val slf4jApiVersion: String by project
val kotlinLoggingVersion: String by project
val kotlinSerializationVersion: String by project
val ktorVersion: String by project
val koinVersion: String by project

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:$kotlinSerializationVersion")
    implementation("io.ktor:ktor-client-core:$ktorVersion")
    implementation("io.ktor:ktor-client-cio:$ktorVersion")
    implementation("io.ktor:ktor-client-serialization:$ktorVersion")

    implementation("io.insert-koin:koin-core:$koinVersion")
    implementation("io.insert-koin:koin-ktor:$koinVersion")

    implementation(project(":tax-content-service-model"))
    implementation(project(":tax-content-service-client-ktor"))

    // Logging
    implementation("ch.qos.logback:logback-classic:$logbackVersion")
    implementation("org.slf4j:slf4j-api:$slf4jApiVersion")
    implementation("io.github.microutils:kotlin-logging:$kotlinLoggingVersion")
}

application {
    // Define the main class for the application.
    mainClassName = "com.modios.taxsolution.TaxContentServiceInitializationKt"
}
