plugins {
    id("java-library")
}

val logbackVersion: String by project
val logbackEcsEncoderVersion: String by project
val slf4jApiVersion: String by project
val kotlinLoggingVersion: String by project
val junitJupiterVersion: String by project
val mockkVersion: String by project
val kotlinCoroutineVersion: String by project

dependencies {
    // Align versions of all Kotlin components
    implementation(platform("org.jetbrains.kotlin:kotlin-bom"))

    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$kotlinCoroutineVersion")

    implementation(project(":ctrie"))
    implementation(project(":data-service-common"))
    implementation(project(":calc-service-model"))
    implementation(project(":admin-service-model"))
    implementation(project(":tax-content-service-model"))

    // Metrics
    implementation("io.micrometer:micrometer-core:1.7.5")

    // Logging
    implementation("ch.qos.logback:logback-classic:$logbackVersion")
    implementation("org.slf4j:slf4j-api:$slf4jApiVersion")
    implementation("net.logstash.logback:logstash-logback-encoder:6.6")
    implementation("io.github.microutils:kotlin-logging:$kotlinLoggingVersion")

    testImplementation("org.junit.jupiter:junit-jupiter:$junitJupiterVersion")
    testImplementation("io.mockk:mockk:$mockkVersion")
}

tasks.test {
    useJUnitPlatform()
}
