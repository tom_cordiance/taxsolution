package com.modios.taxsolution.service.data.cache

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import mu.KotlinLogging
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import kotlin.random.Random

class ServiceCacheLRUTest {

    private val logger = KotlinLogging.logger { }

    suspend fun loadCache(serviceCache: ServiceCache<Int>, count: Int): List<Int> =
        (1..count).map {
            val value = Random.nextInt()
            serviceCache.getOrPutValue(ServiceCacheTestCacheKey(value)) {
                value
            }
            value
        }

    @Test
    fun test() {
        runBlocking {
            val maxCapacity = 500
            val serviceCache = ServiceCacheLRU<Int>(maxCapacity = maxCapacity)

            try {
                loadCache(serviceCache, 1000)

                logger.info { "Pausing test thread..." }
                delay(2500)
                logger.info { "...test thread continuing" }
            } finally {
                serviceCache.stop()
            }
        }
    }

    @Test
    fun testCoroutines() {
        val serviceCache = ServiceCacheLRU<Int>(maxCapacity = 1000)

        runBlocking {
            try {
                (1..10).map {
                    CoroutineScope(Dispatchers.Default).async {
                        logger.info { "Loading cache..." }
                        loadCache(serviceCache, 1000)
                    }
                }.awaitAll()

                logger.info { "Pausing test thread..." }
                delay(2500)
                logger.info { "...test thread continuing" }
            } finally {
                serviceCache.stop()
            }
        }
    }

    @Test
    fun testLRU() {
        runBlocking {
            val maxCapacity = 500
            val serviceCache = ServiceCacheLRU<Int>(maxCapacity = maxCapacity)

            try {
                val values = loadCache(serviceCache, 1000)

                val valuesUsed = values.take(500).sorted()
                (1..10000).forEach {
                    val value = valuesUsed.random()
                    serviceCache.getOrPutValue(ServiceCacheTestCacheKey(value)) { value }
                }

                val statistics = serviceCache.statistics()

                Assertions.assertEquals(maxCapacity, statistics.cacheEntries.size)
                val cachedValues = statistics.cacheEntries.map { cachEntry ->
                    Assertions.assertTrue(cachEntry is LRUCacheEntry<*>)
                    (cachEntry as LRUCacheEntry<*>).let {
                        when (it.cacheEntry) {
                            is SingleCacheEntry<*> -> (it.cacheEntry as SingleCacheEntry<*>).value as Int
                            else -> Assertions.fail("Invalid cache entry: ${it.cacheEntry}")
                        }
                    }
                }.sorted()

                delay(500)

                Assertions.assertEquals(valuesUsed.size, cachedValues.size)
                (0..valuesUsed.size - 1).forEach {
                    Assertions.assertEquals(valuesUsed[it], cachedValues[it])
                }
            } finally {
                serviceCache.stop()
            }
        }
    }

    @Test
    fun testLRUCoroutine() {
        runBlocking {
            val maxCapacity = 500
            val serviceCache = ServiceCacheLRU<Int>(maxCapacity = maxCapacity)
            val coroutineCount = 10

            try {
                val allValuesUsed = (1..coroutineCount).map {
                    CoroutineScope(Dispatchers.Default).async {
                        val values = loadCache(serviceCache, 1000)

                        val valuesUsed = values.take(500 / coroutineCount)
                        (1..10000).forEach {
                            val value = valuesUsed.random()
                            serviceCache.getOrPutValue(ServiceCacheTestCacheKey(value)) { value }
                        }
                        valuesUsed
                    }
                }.awaitAll().flatMap { it }.sortedBy { it }

                delay(500)

                val statistics = serviceCache.statistics()

                Assertions.assertEquals(maxCapacity, statistics.cacheEntries.size)
                val cachedValues = statistics.cacheEntries.map { cachEntry ->
                    Assertions.assertTrue(cachEntry is LRUCacheEntry<*>)
                    (cachEntry as LRUCacheEntry<*>).let {
                        when (it.cacheEntry) {
                            is SingleCacheEntry<*> -> (it.cacheEntry as SingleCacheEntry<*>).value as Int
                            else -> Assertions.fail("Invalid cache entry: ${it.cacheEntry}")
                        }
                    }
                }.sorted()

                Assertions.assertEquals(cachedValues.size, allValuesUsed.size)
                (0..allValuesUsed.size - 1).forEach {
                    if (allValuesUsed[it] != cachedValues[it]) logger.error { "cache value mismatch: expected[${allValuesUsed[it]}]  actual[${cachedValues[it]}]" }
                }
                (0..allValuesUsed.size - 1).forEach {
                    Assertions.assertEquals(allValuesUsed[it], cachedValues[it])
                }
            } finally {
                serviceCache.stop()
            }
        }
    }
}

data class ServiceCacheTestCacheKey(val value: Int) : CacheKey<Int>
