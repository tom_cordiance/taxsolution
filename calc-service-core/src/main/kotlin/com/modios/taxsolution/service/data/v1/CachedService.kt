package com.modios.taxsolution.service.data.v1

interface CachedService {

    suspend fun clear()

    suspend fun evict()
}
