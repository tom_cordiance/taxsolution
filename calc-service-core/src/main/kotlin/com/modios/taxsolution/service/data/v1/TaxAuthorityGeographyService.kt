package com.modios.taxsolution.service.data.v1

import com.modios.taxsolution.content.model.v1.TaxAuthorityGeography

interface TaxAuthorityGeographyService {

    suspend fun getByGeographyId(geographyId: Long): List<TaxAuthorityGeography>
}
