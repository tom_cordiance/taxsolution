package com.modios.taxsolution.service.data.cache

import org.lpj.some.collection.TrieMap

open class ServiceCacheStandard<V : Any> : ServiceCache<V> {

    protected val map = TrieMap<CacheKey<V>, CacheEntry<V>>()
    // protected val map = ConcurrentHashMap<CacheKey<V>, CacheEntry<V>>()

    protected open suspend fun getCacheEntry(cacheKey: CacheKey<V>): CacheEntry<V>? {
        @Suppress("UNCHECKED_CAST")
        return map.get(cacheKey)
    }

    protected open suspend fun putCacheEntry(cacheKey: CacheKey<V>, cacheEntry: CacheEntry<V>) {
        map.put(cacheKey, cacheEntry)
    }

    private fun emptyCacheEntry() = EmptyCacheEntry<V>()

    private fun singleCacheEntry(value: V) = SingleCacheEntry(value)

    private fun pluralCacheEntry(value: List<V>) = PluralCacheEntry(value)

    override suspend fun getOrPutValue(cacheKey: CacheKey<V>, get: suspend () -> V?): V? {
        // get value from cache
        val cacheEntry: CacheEntry<V>? = getCacheEntry(cacheKey)
        // return value if exists
        return when (cacheEntry) {
            null -> {
                // if null get data externally
                val data = get()
                // put cache entry based on data value
                when (data) {
                    null -> {
                        putCacheEntry(cacheKey, emptyCacheEntry())
                        null
                    }
                    else -> {
                        putCacheEntry(cacheKey, singleCacheEntry(data))
                        data
                    }
                }
            }
            is EmptyCacheEntry<*> -> null
            is SingleCacheEntry<*> -> cacheEntry.value as V
            is PluralCacheEntry<*> -> throw CacheException("A collection cannot be cached with this method: ${cacheEntry::class.qualifiedName}")
            else -> throw CacheException("A CacheEntry of type ${cacheEntry::class.qualifiedName} was found and could not be handled")
        }
    }

    override suspend fun getOrPutCollection(cacheKey: CacheKey<V>, get: suspend () -> List<V>): List<V> {
        // get value from cache
        val cacheEntry = getCacheEntry(cacheKey)
        // return value if exists
        return when (cacheEntry) {
            null -> {
                // if null get data externally
                val dataList = get()
                // put cache entry based on data value
                when (dataList.isEmpty()) {
                    true -> {
                        putCacheEntry(cacheKey, emptyCacheEntry())
                        emptyList()
                    }
                    false -> {
                        putCacheEntry(cacheKey, pluralCacheEntry(dataList))
                        dataList
                    }
                }
            }
            is EmptyCacheEntry<*> -> emptyList()
            is SingleCacheEntry<*> -> throw CacheException("A single value cannot be cached with this method: ${cacheEntry::class.qualifiedName}")
            is PluralCacheEntry<*> -> cacheEntry.valueList as List<V>
            else -> throw CacheException("A CacheEntry of type ${cacheEntry::class.qualifiedName} was found and could not be handled")
        }
    }

    override suspend fun evict(cacheKey: CacheKey<V>) {
        map.remove(cacheKey)
    }

    override suspend fun clear() {
        map.clear()
    }
}
