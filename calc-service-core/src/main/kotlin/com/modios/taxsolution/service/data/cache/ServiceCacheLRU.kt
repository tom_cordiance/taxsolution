package com.modios.taxsolution.service.data.cache

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import mu.KotlinLogging
import java.util.Deque
import java.util.concurrent.ConcurrentLinkedDeque
import java.util.concurrent.Executors
import kotlin.concurrent.fixedRateTimer

class ServiceCacheLRU<V : Any>(private val maxCapacity: Int = 16384) : ServiceCacheStandard<V>() {

    private val logger = KotlinLogging.logger { }

    /**
     * For cache management operations
     */
    private val cacheDispatcher = Executors.newSingleThreadExecutor().asCoroutineDispatcher()

    private val managementChannel =
        Channel<CacheManagementEvent>(capacity = Channel.BUFFERED) { undeliveredManagementEvent ->
            logger.warn { "Undelivered cache management event: $undeliveredManagementEvent" }
        }

    /**
     * head of deque: least recently used
     * tail of deque: most recently used
     */
    private val lruDeque: Deque<LRUDequeEntry<V>> = ConcurrentLinkedDeque()

    private val lruManagementTimer = fixedRateTimer(period = 500) {
        runBlocking { manageLruEntries() }
    }

    init {
        CoroutineScope(cacheDispatcher).launch { start() }
    }

    data class CacheStatistics(
        val maxCapacity: Int,
        val lruEntries: List<LRUDequeEntry<*>>,
        val cacheEntries: List<CacheEntry<*>>
    )

    suspend fun statistics() =
        CacheStatistics(maxCapacity = maxCapacity, lruEntries = lruDeque.toList(), cacheEntries = map.values.toList())

    suspend fun start() {
        logger.trace { "Starting cache management loop..." }

        for (cacheManagementEvent in managementChannel) {
            logger.trace { "Received cache management event: $cacheManagementEvent" }

            logger.trace { "Managing cache entries..." }
            manageCacheEntries()
        }
    }

    suspend fun stop() {
        managementChannel.close()
    }

    private suspend fun manageLruEntries() {
        logger.trace { "Starting LRU purge..." }
        // clear items to be purged. These will already have been updated in the cache, so no cache management needed.
        var count = 0
        lruDeque.removeIf {
            (it.purge == true).also {
                if (it) {
                    count++
                }
            }
        }
        logger.trace { "LRU purge complete, $count entries purged" }
    }

    // TODO: memory based eviction: see java.lang.instrument.Instrumentation
    /**
     * Check cache size versus max capacity. If size is greater than max capacity then purge to max capacity.
     * Purge means remove from the head of the deqque until capacity reached.
     */
    private suspend fun manageCacheEntries() {
        // check cache size and purge as necessary
        val size = map.size
        logger.trace { "Cache size: $size" }
        val difference = size - maxCapacity
        logger.trace { "Cache capacity($maxCapacity) difference: $difference" }
        if (difference > 0) {
            logger.trace { "Evicting least recently used cache entries..." }
            (1..difference).forEach {
                // only evict if entry is not a purge entry
                if (lruDeque.peek()?.purge == false) {
                    // remove key from deque
                    val key = lruDeque.removeFirst()
                    // remove key and value from cache
                    evict(key.cacheKey)
                }
            }
            logger.trace { "Cache eviction complete" }
        }
    }

    /**
     * The LRU deque needs to be managed, the entry should be marked to be purged
     */
    override suspend fun evict(cacheKey: CacheKey<V>) {
        val existingCacheEntry = super.getCacheEntry(cacheKey)
        if (existingCacheEntry != null && existingCacheEntry is LRUCacheEntry<*>) {
            existingCacheEntry.lruEntry.purge = true
        }

        super.evict(cacheKey)
    }

    /**
     * Unwrap the standard cache entry
     */
    override suspend fun getCacheEntry(cacheKey: CacheKey<V>): CacheEntry<V>? {
        val cacheEntry = super.getCacheEntry(cacheKey)
        return when (cacheEntry) {
            null -> null
            is LRUCacheEntry<*> -> cacheEntry.cacheEntry as CacheEntry<V>
            else -> cacheEntry
        }
    }

    /**
     * Wrap the standard cache entry
     */
    override suspend fun putCacheEntry(cacheKey: CacheKey<V>, cacheEntry: CacheEntry<V>) {
        val existingCacheEntry = super.getCacheEntry(cacheKey)
        when (existingCacheEntry == null) {
            true -> {
                // just add to end of deque and super put entry
                val lruDequeEntry = LRUDequeEntry(cacheKey = cacheKey, purge = false)
                lruDeque.addLast(lruDequeEntry)

                val lruCacheEntry = LRUCacheEntry(lruEntry = lruDequeEntry, cacheEntry = cacheEntry)
                super.putCacheEntry(cacheKey, lruCacheEntry)
            }
            false -> {
                // mark existing lru entry as purge, add new lru entry to end of deque, super put entry
                when (existingCacheEntry) {
                    is LRUCacheEntry<*> -> {
                        existingCacheEntry.lruEntry.purge = true

                        val lruDequeEntry = LRUDequeEntry(cacheKey = cacheKey, purge = false)
                        lruDeque.addLast(lruDequeEntry)
                        val lruCacheEntry = LRUCacheEntry(lruEntry = lruDequeEntry, cacheEntry = cacheEntry)

                        super.putCacheEntry(cacheKey, lruCacheEntry)
                    }
                    else -> {
                        // THIS SHOULD NOT HAPPEN
                        throw CacheException("A CacheEntry of type ${cacheEntry::class.qualifiedName} was found, ${LRUCacheEntry::class} was expected")
                    }
                }
            }
        }

        managementChannel.send(CacheUpdatedEvent())
    }
}

data class LRUDequeEntry<T : Any>(val cacheKey: CacheKey<T>, var purge: Boolean = false)
data class LRUCacheEntry<T : Any>(val lruEntry: LRUDequeEntry<T>, val cacheEntry: CacheEntry<T>) : CacheEntry<T>

interface CacheManagementEvent
class CacheUpdatedEvent() : CacheManagementEvent
