package com.modios.taxsolution.service.data.v1

import com.modios.taxsolution.calc.model.v1.TaxDocumentResult

/**
 * Used to store TaxDocumentResult instances
 */
interface TaxDocumentResultStorageService {
    fun write(taxDocumentResult: TaxDocumentResult)
}
