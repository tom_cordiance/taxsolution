package com.modios.taxsolution.service.data.v1

import com.modios.taxsolution.content.model.v1.TaxAuthority

interface TaxAuthorityService {
    suspend fun getByTaxAuthorityId(taxAuthorityId: Long): TaxAuthority?
}
