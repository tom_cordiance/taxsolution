package com.modios.taxsolution.service.data.v1

import com.modios.taxsolution.model.v1.CompanyTaxContentProvider

interface CompanyTaxContentProviderService {

    suspend fun getCompanyTaxContentProviders(companyId: Long): List<CompanyTaxContentProvider>
}
