package com.modios.taxsolution.service.data.v1

import com.modios.taxsolution.model.v1.Company

interface CompanyService {
    suspend fun getCompanies(tenantId: Long): List<Company>

    suspend fun getCompany(tenantId: Long, companyName: String): Company?
}
