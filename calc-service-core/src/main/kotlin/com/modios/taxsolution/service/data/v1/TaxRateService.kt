package com.modios.taxsolution.service.data.v1

import com.modios.taxsolution.content.model.v1.TaxRate

interface TaxRateService {

    suspend fun findByTaxAuthorityId(taxAuthorityId: Long): List<TaxRate>
    suspend fun evictByAuthorityId(taxAuthorityId: Long)
}
