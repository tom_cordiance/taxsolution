package com.modios.taxsolution.service.data.v1

import com.modios.taxsolution.model.v1.Tenant

interface TenantService {
    suspend fun get(name: String): Tenant?
}
