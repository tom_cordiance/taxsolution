package com.modios.taxsolution.service.data.v1

import com.modios.taxsolution.content.model.v1.TaxContentProvider

interface TaxContentProviderService {

    suspend fun get(taxContentProviderId: Long): TaxContentProvider?
}
