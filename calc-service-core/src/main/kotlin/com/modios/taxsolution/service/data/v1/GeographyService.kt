package com.modios.taxsolution.service.data.v1

import com.modios.taxsolution.content.model.v1.Geography

interface GeographyService {

    /**
     * Returns all of the children of 'world' for the supplied tax content provider. This is typically
     * countries.
     */
    suspend fun getTopLevelByTaxContentProviderId(taxContentProviderId: Long): List<Geography>

    suspend fun getByTaxContentProviderIdAndParentIdAndName(
        taxContentProviderId: Long,
        parentId: Long,
        name: String
    ): Geography?

    suspend fun getByTaxContentProviderIdAndParentIdAndCode(
        taxContentProviderId: Long,
        parentId: Long,
        code: String
    ): Geography?
}
