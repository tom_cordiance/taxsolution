package com.modios.taxsolution.service.data.cache

interface CacheKey<T>

interface CacheEntry<T : Any>

class EmptyCacheEntry<T : Any> : CacheEntry<T>

data class SingleCacheEntry<T : Any>(val value: T) : CacheEntry<T>

data class PluralCacheEntry<T : Any>(val valueList: List<T>) : CacheEntry<T>

class CacheEntryException(message: String) : RuntimeException(message)

class CacheException(message: String) : RuntimeException(message)

interface ServiceCache<V : Any> {
    suspend fun getOrPutValue(cacheKey: CacheKey<V>, get: suspend () -> V?): V?

    suspend fun getOrPutCollection(cacheKey: CacheKey<V>, get: suspend () -> List<V>): List<V>

    suspend fun evict(cacheKey: CacheKey<V>)

    suspend fun clear()
}
