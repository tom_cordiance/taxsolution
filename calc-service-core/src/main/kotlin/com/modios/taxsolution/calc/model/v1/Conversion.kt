package com.modios.taxsolution.calc.model.v1

import com.modios.taxsolution.module.v1.TaxCalculation
import com.modios.taxsolution.module.v1.TaxCalculationItem
import com.modios.taxsolution.module.v1.TaxCalculationMessage
import java.math.BigDecimal
import java.time.LocalDateTime

/**
 * Conversion functions
 */

fun TaxDocument.asTaxCalculation(): TaxCalculation {
    return TaxCalculation(
        tenantName = this.tenantName,
        companyName = this.companyName,
        calculationDate = LocalDateTime.now(),
        documentNumber = this.documentNumber,
        shipFromTransactionLocation = this.shipFrom,
        shipToTransactionLocation = this.shipTo,
        persistResult = persistResult,
        hasFailure = false
    ).also {
        it.taxCalculationItemList.addAll(this.taxDocumentItems.map { it.asTaxCalculationItem() })
    }
}

fun TaxDocumentItem.asTaxCalculationItem(): TaxCalculationItem {
    return TaxCalculationItem(
        index = this.index,
        productCode = this.productCode,
        productMappingCode = this.productMappingCode,
        quantity = this.quantity,
        amount = this.amount,
        taxAmount = null,
        messageList = mutableListOf()
    )
}

fun TaxCalculation.asTaxDocumentResult(originalTaxDocument: TaxDocument): TaxDocumentResult {
    return TaxDocumentResult(
        originalTaxDocument = originalTaxDocument,
        tenantName = tenantName,
        companyName = companyName,
        documentNumber = documentNumber,
        shipFromTransactionLocation = shipFromTransactionLocation,
        shipToTransactionLocation = shipToTransactionLocation,
        taxDocumentResultMessages = messageList.map { it.asTaxDocumentMessage() },
        taxDocumentItemResults = taxCalculationItemList.map { it.asTaxDocumentItemResult() }
    )
}

fun TaxCalculationMessage.asTaxDocumentMessage(): TaxDocumentResultMessage {
    return TaxDocumentResultMessage(status = MessageStatus.valueOf(status.toString()), message = message)
}

fun TaxCalculationItem.asTaxDocumentItemResult(): TaxDocumentItemResult {
    return TaxDocumentItemResult(
        index = this.index,
        taxResults = taxResultList.map {
            TaxDocumentItemTaxResult(
                authorityName = it.taxAuthority.name,
                taxRate = it.taxRate.rate,
                taxAmount = it.taxAmount
            )
        },
        taxAmount = taxResultList.fold(BigDecimal.ZERO) { sum, taxResult -> sum.add(taxResult.taxAmount) },
        taxDocumentResultMessages = messageList.map { it.asTaxDocumentMessage() }
    )
}
