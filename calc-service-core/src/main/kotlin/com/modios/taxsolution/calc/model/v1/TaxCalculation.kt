package com.modios.taxsolution.module.v1

import com.modios.taxsolution.calc.model.v1.TransactionLocation
import com.modios.taxsolution.content.model.v1.Geography
import com.modios.taxsolution.content.model.v1.TaxAuthority
import com.modios.taxsolution.content.model.v1.TaxContentProvider
import com.modios.taxsolution.content.model.v1.TaxRate
import com.modios.taxsolution.model.v1.Company
import com.modios.taxsolution.model.v1.Tenant
import java.math.BigDecimal
import java.time.LocalDateTime

enum class TransactionLocationType {
    SHIP_FROM,
    SHIP_TO
}

data class TaxCalculation(
    val tenantName: String,
    val companyName: String,
    val calculationDate: LocalDateTime,
    val documentNumber: String,
    val shipFromTransactionLocation: TransactionLocation? = null,
    val shipToTransactionLocation: TransactionLocation? = null,
    val hasFailure: Boolean = false,
    val persistResult: Boolean = false,
    val tenant: Tenant? = null,
    val company: Company? = null,
    var shipFromTaxingTransactionLocation: TaxingTransactionLocation? = null,
    var shipToTaxingTransactionLocation: TaxingTransactionLocation? = null,
    val taxCalculationItemList: MutableList<TaxCalculationItem> = mutableListOf(),
    val messageList: MutableList<TaxCalculationMessage> = mutableListOf()
) {

    fun getAllTransactionLocationsByType(): List<Pair<TransactionLocationType, TransactionLocation>> {
        return listOf(
            TransactionLocationType.SHIP_FROM to shipFromTransactionLocation,
            TransactionLocationType.SHIP_TO to shipToTransactionLocation
        ).filter { it.second != null }.map { it.first to it.second!! }
    }

    fun getAllTaxingTransactionLocations(): List<TaxingTransactionLocation> {
        return listOf(shipFromTaxingTransactionLocation, shipToTaxingTransactionLocation).filterNotNull()
    }

    fun getTaxingTransactionLocation(transactionLocationType: TransactionLocationType): TaxingTransactionLocation? {
        return when (transactionLocationType) {
            TransactionLocationType.SHIP_FROM -> shipFromTaxingTransactionLocation
            TransactionLocationType.SHIP_TO -> shipToTaxingTransactionLocation
        }
    }

    fun setTaxingTransactionLocation(transactionLocationType: TransactionLocationType, taxingTransactionLocation: TaxingTransactionLocation) {
        when (transactionLocationType) {
            TransactionLocationType.SHIP_FROM -> shipFromTaxingTransactionLocation = taxingTransactionLocation
            TransactionLocationType.SHIP_TO -> shipToTaxingTransactionLocation = taxingTransactionLocation
        }
    }

    companion object {
        fun setTenant(taxCalculation: TaxCalculation, tenant: Tenant): TaxCalculation {
            return taxCalculation.copy(tenant = tenant)
        }

        fun setCompany(taxCalculation: TaxCalculation, company: Company): TaxCalculation {
            return taxCalculation.copy(company = company)
        }

        fun addMessage(taxCalculation: TaxCalculation, taxCalculationMessage: TaxCalculationMessage): TaxCalculation {
            return taxCalculation.also {
                with(it.messageList) {
                    addAll(taxCalculation.messageList)
                    add(taxCalculationMessage)
                }
            }
        }
    }
}

/**
 * Tax is tied to geography, therefore associate the sources of tax results to the location geography.
 */
data class TaxingGeography(
    val geography: Geography,
    val taxAuthorityList: MutableList<TaxAuthority> = mutableListOf()
)

abstract class TaxingTransactionLocation(val taxContentProviderList: MutableList<TaxContentProvider> = mutableListOf()) {

    /**
     * Returns all of the non-null TaxableGeography for this instance. The TaxableGeography should be
     * returned in hierarchical order.
     */
    abstract fun getAllTaxingGeography(): List<TaxingGeography>
}

data class USTaxingTransactionLocation(
    val country: TaxingGeography,
    val state: TaxingGeography? = null,
    val county: TaxingGeography? = null,
    val city: TaxingGeography? = null,
    val district: TaxingGeography? = null,
    val postCode: TaxingGeography? = null,
    val geoCode: TaxingGeography? = null
) : TaxingTransactionLocation() {

    override fun getAllTaxingGeography(): List<TaxingGeography> {
        return listOf(country, state, county, city, district, postCode, geoCode).filterNotNull()
    }
}

data class EUTaxingTransactionLocation(
    val country: TaxingGeography,
    val city: TaxingGeography? = null,
    val postCode: TaxingGeography? = null
) : TaxingTransactionLocation() {

    override fun getAllTaxingGeography(): List<TaxingGeography> {
        return listOf(country, city, postCode).filterNotNull()
    }
}

data class CanadianTaxingTransactionLocation(
    val country: TaxingGeography,
    val province: TaxingGeography? = null,
    val city: TaxingGeography? = null,
    val postCode: TaxingGeography? = null
) : TaxingTransactionLocation() {

    override fun getAllTaxingGeography(): List<TaxingGeography> {
        return listOf(country, province, city, postCode).filter { it != null }.map { it!! }
    }
}

data class TaxCalculationItem(
    val index: Int,
    val productCode: String? = null,
    val productMappingCode: String? = null,
    val quantity: Int,
    val amount: BigDecimal,
    val taxAmount: BigDecimal?,
    val messageList: MutableList<TaxCalculationMessage> = mutableListOf(),
    val taxAuthorityJurisdictionList: MutableList<TaxAuthorityJurisdiction> = mutableListOf(),
    val taxResultList: MutableList<TaxResult> = mutableListOf()
)

data class TaxAuthorityJurisdiction(
    val taxAuthority: TaxAuthority,
    val taxRate: TaxRate
)

data class TaxResult(
    val taxAuthority: TaxAuthority,
    val taxRate: TaxRate,
    val taxAmount: BigDecimal,
    val messageList: MutableList<TaxCalculationMessage> = mutableListOf()
)

enum class TaxCalculationMessageStatus {
    DEBUG,
    INFO,
    WARNING,
    ERROR
}

data class TaxCalculationMessage(val status: TaxCalculationMessageStatus, val message: String)
