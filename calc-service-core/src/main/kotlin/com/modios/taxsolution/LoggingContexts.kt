package com.modios.taxsolution

object LoggingContexts {
    val TENANT = "tenant"
    val COMPANY = "company"
    val DOCUMENT_NUMBER = "documentNumber"
}
