package com.modios.taxsolution.module.v1

import com.modios.taxsolution.calc.model.v1.TaxDocument
import com.modios.taxsolution.calc.model.v1.asTaxDocumentResult
import com.modios.taxsolution.service.data.v1.TaxDocumentResultStorageService
import io.micrometer.core.instrument.MeterRegistry
import io.micrometer.core.instrument.Timer
import kotlin.time.ExperimentalTime
import kotlin.time.measureTimedValue
import kotlin.time.toJavaDuration

/**
 * Conditionally persists the tax result, based upon the document's persist field value
 */
class StoreTaxDocumentResultModule(
    meterRegistry: MeterRegistry,
    private val taxDocumentResultStorageService: TaxDocumentResultStorageService
) :
    TaxModule {

    private val timer = Timer
        .builder("cordiance.module.storeTaxDocumentResult")
        .publishPercentiles(0.5, 0.95)
        .publishPercentileHistogram()
        .register(meterRegistry)

    /**
     * Processes the supplied TaxCalculation instance.  The expectation is that a tax calculation is stateful,
     * with state being stored in the TaxCalculation instance.  A TaxModule instance will expect specific data
     * to be included to function properly.
     */
    @OptIn(ExperimentalTime::class)
    override suspend fun process(taxCalculation: TaxCalculation): TaxCalculation {
        return measureTimedValue {
            if (taxCalculation.persistResult) {
                taxDocumentResultStorageService.write(
                    taxCalculation.asTaxDocumentResult(
                        TaxDocument(
                            tenantName = taxCalculation.tenantName,
                            companyName = taxCalculation.companyName,
                            documentDate = taxCalculation.calculationDate,
                            documentNumber = taxCalculation.documentNumber
                        )
                    )
                )
            }

            taxCalculation
        }.also {
            timer.record(it.duration.toJavaDuration())
        }.value
    }
}
