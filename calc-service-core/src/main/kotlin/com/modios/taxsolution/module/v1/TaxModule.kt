package com.modios.taxsolution.module.v1

interface TaxModule {

    /**
     * Processes the supplied TaxCalculation instance.  The expectation is that a tax calculation is stateful,
     * with state being stored in the TaxCalculation instance.  A TaxModule instance will expect specific data
     * to be included to function properly.
     */
    suspend fun process(taxCalculation: TaxCalculation): TaxCalculation
}
