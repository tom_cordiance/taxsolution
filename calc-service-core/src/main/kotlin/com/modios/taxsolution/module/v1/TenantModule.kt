package com.modios.taxsolution.module.v1

import com.modios.taxsolution.LoggingContexts
import com.modios.taxsolution.service.data.v1.TenantService
import io.micrometer.core.instrument.MeterRegistry
import io.micrometer.core.instrument.Timer
import mu.KotlinLogging
import mu.withLoggingContext
import kotlin.time.ExperimentalTime
import kotlin.time.measureTimedValue
import kotlin.time.toJavaDuration

class TenantModule(meterRegistry: MeterRegistry, val tenantService: TenantService) : TaxModule {

    private val logger = KotlinLogging.logger { }

    private val timer = Timer
        .builder("cordiance.module.tenant")
        .publishPercentiles(0.5, 0.95)
        .publishPercentileHistogram()
        .register(meterRegistry)

    /**
     * Processes the supplied TaxCalculation instance.  The expectation is that a tax calculation is stateful,
     * with state being stored in the TaxCalculation instance.  A TaxModule instance will expect specific data
     * to be included to function properly.
     */
    @OptIn(ExperimentalTime::class)
    override suspend fun process(taxCalculation: TaxCalculation): TaxCalculation {
        return measureTimedValue {
            withLoggingContext(
                LoggingContexts.TENANT to taxCalculation.tenantName,
                LoggingContexts.DOCUMENT_NUMBER to taxCalculation.documentNumber
            ) {

                val tenant = tenantService.get(taxCalculation.tenantName)
                when (tenant) {
                    null -> {
                        val message = TaxCalculationMessage(
                            status = TaxCalculationMessageStatus.ERROR,
                            message = "The tenant ${taxCalculation.tenantName} could not be found"
                        )
                        TaxCalculation.addMessage(taxCalculation, message)
                    }
                    else -> {
                        val message = TaxCalculationMessage(
                            status = TaxCalculationMessageStatus.INFO,
                            message = "Tenant lookup of ${taxCalculation.tenantName} succeeded"
                        )
                        val tc = TaxCalculation.setTenant(taxCalculation, tenant)
                        TaxCalculation.addMessage(tc, message)
                    }
                }
            }
        }.also {
            timer.record(it.duration.toJavaDuration())
        }.value
    }
}
