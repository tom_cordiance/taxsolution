package com.modios.taxsolution.module.v1

import com.modios.taxsolution.service.data.v1.CompanyService
import io.micrometer.core.instrument.MeterRegistry
import io.micrometer.core.instrument.Timer
import kotlin.time.ExperimentalTime
import kotlin.time.measureTimedValue
import kotlin.time.toJavaDuration

/**
 * Performs service calls to get values/validate the tenant and company entities used
 * NOTE: if done this way the TaxDocument instance must contain all Tenant and Company info; internally the result
 * may need to be represented in an intermediate result.
 */
class CompanyModule(meterRegistry: MeterRegistry, private val companyService: CompanyService) : TaxModule {

    private val timer = Timer
        .builder("cordiance.module.company")
        .publishPercentiles(0.5, 0.95)
        .publishPercentileHistogram()
        .register(meterRegistry)

    @OptIn(ExperimentalTime::class)
    override suspend fun process(taxCalculation: TaxCalculation): TaxCalculation {
        assert(
            taxCalculation.tenant != null,
            { "A valid tenant is required for company lookup. Validate that tenant lookup succeeded." }
        )

        return measureTimedValue {
            val company =
                companyService.getCompany(
                    tenantId = taxCalculation.tenant!!.id,
                    companyName = taxCalculation.companyName
                )
            val result = when (company) {
                null -> {
                    val message = TaxCalculationMessage(
                        status = TaxCalculationMessageStatus.ERROR,
                        message = "Company not found for tenant ${taxCalculation.tenantName}: ${taxCalculation.companyName}"
                    )
                    TaxCalculation.addMessage(taxCalculation, message)
                }
                else -> {
                    val message = TaxCalculationMessage(
                        status = TaxCalculationMessageStatus.INFO,
                        message = "Successful Tax Calculation"
                    )
                    val tc = TaxCalculation.setCompany(taxCalculation, company)
                    TaxCalculation.addMessage(tc, message)
                }
            }

            result
        }.also {
            timer.record(it.duration.toJavaDuration())
        }.value
    }
}
