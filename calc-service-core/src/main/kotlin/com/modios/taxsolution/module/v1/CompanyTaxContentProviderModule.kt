package com.modios.taxsolution.module.v1

import com.modios.taxsolution.calc.model.v1.TransactionLocation
import com.modios.taxsolution.content.model.v1.Geography
import com.modios.taxsolution.content.model.v1.TaxContentProvider
import com.modios.taxsolution.service.data.v1.CompanyTaxContentProviderService
import com.modios.taxsolution.service.data.v1.GeographyService
import com.modios.taxsolution.service.data.v1.TaxContentProviderService
import io.micrometer.core.annotation.Timed
import io.micrometer.core.instrument.MeterRegistry
import io.micrometer.core.instrument.Timer
import mu.KotlinLogging
import kotlin.time.ExperimentalTime
import kotlin.time.measureTimedValue
import kotlin.time.toJavaDuration

/**
 * Requirements:
 *   * A valid company
 *   * A valid tax content provider
 *   * A valid company to tax content provider mapping
 *
 * Output:
 *   Zero or more tax content providers that may be applicable to this tax calculation.
 *   A tax content provider is deemed applicable if it has geography that is contained
 *   by any address in the tax calculation.
 *
 *   'Applicable' means that the tax content provider *may* have tax authorities that have
 *   jurisdiction over the tax calculation.
 */
class CompanyTaxContentProviderModule(
    meterRegistry: MeterRegistry,
    private val companyTaxContentProviderService: CompanyTaxContentProviderService,
    private val taxContentProviderService: TaxContentProviderService,
    private val geographyService: GeographyService,
    private val taxingTransactionLocationFactory: TaxingTransactionLocationFactory
) : TaxModule {

    private val logger = KotlinLogging.logger { }

    private val timer = Timer
        .builder("cordiance.module.companyTaxContentProvider")
        .publishPercentiles(0.5, 0.75, 0.95)
        .publishPercentileHistogram()
        .register(meterRegistry)

    /**
     * Really this determines whether the supplied country is applicable for the supplied tax content provider
     */
    suspend fun isApplicableForLocation(
        taxContentProvider: TaxContentProvider,
        transactionLocation: TransactionLocation
    ): Geography? {
        val topLevelGeography = geographyService.getTopLevelByTaxContentProviderId(taxContentProvider.id)
        logger.trace { "Top level geography: $topLevelGeography" }

        val taxableGeography = topLevelGeography
            .filter { it.code == transactionLocation.country }
            .singleOrNull()
        logger.trace { "Taxable geography for $transactionLocation: $taxableGeography" }

        return taxableGeography
    }

    /**
     * Processes the supplied TaxCalculation instance.  The expectation is that a tax calculation is stateful,
     * with state being stored in the TaxCalculation instance.  A TaxModule instance will expect specific data
     * to be included to function properly.
     */
    @OptIn(ExperimentalTime::class)
    @Timed
    override suspend fun process(taxCalculation: TaxCalculation): TaxCalculation {
        assert(
            taxCalculation.company != null,
            { "A valid company is required for a company tax content provider lookup" }
        )

        return measureTimedValue {
            val companyId = taxCalculation.company!!.id
            val taxContentProviders = companyTaxContentProviderService
                .getCompanyTaxContentProviders(companyId)
                // tax content provider should never be null since used from mapping table
                .map { taxContentProviderService.get(it.taxContentProviderId)!! }

            logger.trace { "Tax content providers: $taxContentProviders" }

            // need to tie Location to TaxableLocationGeography here...basically iterate over calculation locations
            // and populate the taxableLocationGeography with the results
            listOf(
                Pair(TransactionLocationType.SHIP_FROM, taxCalculation.shipFromTransactionLocation),
                Pair(TransactionLocationType.SHIP_TO, taxCalculation.shipToTransactionLocation)
            ).forEach { (locationType, location) ->
                logger.trace { "Location type: $locationType  Location: $location" }
                if (location != null) {
                    taxContentProviders.forEach { taxContentProvider ->
                        val geography = isApplicableForLocation(taxContentProvider, location)
                        logger.trace { "Applicable geography: $geography" }
                        if (geography != null) {
                            val taxingTransactionLocation = taxingTransactionLocationFactory.create(
                                taxContentProvider = taxContentProvider,
                                geography = geography
                            )
                            when (locationType) {
                                TransactionLocationType.SHIP_FROM ->
                                    taxCalculation.shipFromTaxingTransactionLocation =
                                        taxingTransactionLocation
                                TransactionLocationType.SHIP_TO ->
                                    taxCalculation.shipToTaxingTransactionLocation =
                                        taxingTransactionLocation
                            }
                        }
                    }
                }
            }

            taxCalculation
        }.also {
            timer.record(it.duration.toJavaDuration())
        }.value
    }
}
