package com.modios.taxsolution.module.v1

import io.micrometer.core.instrument.MeterRegistry
import io.micrometer.core.instrument.Timer
import mu.KotlinLogging
import kotlin.time.ExperimentalTime
import kotlin.time.measureTimedValue
import kotlin.time.toJavaDuration

/**
 * Requires:
 *   * TaxContentProvider
 *
 * Output:
 *   * TaxingTransactionLocation applicable to any location declared in the tax calculation
 */
class TaxingTransactionLocationModule(
    meterRegistry: MeterRegistry,
    private val taxingTransactionLocationFactory: TaxingTransactionLocationFactory
) :
    TaxModule {

    private val logger = KotlinLogging.logger { }

    private val timer = Timer
        .builder("cordiance.module.taxingTransactionLocation")
        .publishPercentiles(0.5, 0.95)
        .publishPercentileHistogram()
        .register(meterRegistry)

    @OptIn(ExperimentalTime::class)
    override suspend fun process(taxCalculation: TaxCalculation): TaxCalculation {
        return measureTimedValue {
            taxCalculation
                .getAllTransactionLocationsByType()
                .filter { it.first != TransactionLocationType.SHIP_FROM }
                .forEach { (transactionLocationType, transactionLocation) ->
                    logger.trace { "Transaction location type for $transactionLocation: $transactionLocationType" }
                    taxCalculation.getTaxingTransactionLocation(transactionLocationType)?.let {
                        val completedTaxingLocationGeography =
                            taxingTransactionLocationFactory.completeFromTransactionLocation(
                                transactionLocation,
                                it
                            )
                        logger.trace { "Completed taxing location geography: $completedTaxingLocationGeography" }
                        taxCalculation.setTaxingTransactionLocation(
                            transactionLocationType,
                            completedTaxingLocationGeography
                        )
                    }
                }

            taxCalculation
        }.also {
            timer.record(it.duration.toJavaDuration())
        }.value
    }
}
