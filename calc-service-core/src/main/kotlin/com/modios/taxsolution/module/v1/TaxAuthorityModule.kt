package com.modios.taxsolution.module.v1

import com.modios.taxsolution.service.data.v1.TaxAuthorityGeographyService
import com.modios.taxsolution.service.data.v1.TaxAuthorityService
import io.micrometer.core.instrument.MeterRegistry
import io.micrometer.core.instrument.Timer
import mu.KotlinLogging
import kotlin.time.ExperimentalTime
import kotlin.time.measureTimedValue
import kotlin.time.toJavaDuration

/**
 * Requires:
 *   * TaxingTransactionLocations completed, which contain the tax content providers
 *
 * Output:
 *   * TaxAuthorities for each TaxingGeography that represents a TaxingTransactionLocation
 */
class TaxAuthorityModule(
    meterRegistry: MeterRegistry,
    private val taxAuthorityService: TaxAuthorityService,
    private val taxAuthorityGeographyService: TaxAuthorityGeographyService
) : TaxModule {

    private val logger = KotlinLogging.logger { }

    private val timer = Timer
        .builder("cordiance.module.taxAuthority")
        .publishPercentiles(0.5, 0.95)
        .publishPercentileHistogram()
        .register(meterRegistry)

    @OptIn(ExperimentalTime::class)
    override suspend fun process(taxCalculation: TaxCalculation): TaxCalculation {
        return measureTimedValue {
            taxCalculation
                .getAllTaxingTransactionLocations()
                .forEach { taxingTransactionLocation ->
                    logger.trace { "Taxing transaction location: $taxingTransactionLocation" }
                    taxingTransactionLocation
                        .getAllTaxingGeography()
                        .forEach { taxableGeography ->
                            logger.trace { "TaxableGeography: $taxableGeography" }
                            taxAuthorityGeographyService
                                .getByGeographyId(taxableGeography.geography.id)
                                .forEach { taxAuthorityGeography ->
                                    logger.trace { "Tax authority geography: $taxAuthorityGeography" }
                                    val taxAuthority =
                                        taxAuthorityService.getByTaxAuthorityId(taxAuthorityGeography.taxAuthorityId)
                                    logger.trace { "Tax authority: $taxAuthority" }
                                    when (taxAuthority) {
                                        null -> throw RuntimeException("The TaxAuthority[id=${taxAuthorityGeography.taxAuthorityId}] was defined in TaxAuthorityGeography and could not be found")
                                        else -> taxableGeography.taxAuthorityList.add(taxAuthority)
                                    }
                                }
                        }
                }

            taxCalculation
        }.also {
            timer.record(it.duration.toJavaDuration())
        }.value
    }
}
