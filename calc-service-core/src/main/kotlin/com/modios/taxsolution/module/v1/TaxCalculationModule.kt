package com.modios.taxsolution.module.v1

import com.modios.Constants
import io.micrometer.core.instrument.MeterRegistry
import io.micrometer.core.instrument.Timer
import kotlin.time.ExperimentalTime
import kotlin.time.measureTimedValue
import kotlin.time.toJavaDuration

class TaxCalculationModule(meterRegistry: MeterRegistry) : TaxModule {

    private val timer = Timer
        .builder("cordiance.module.taxCalculation")
        .publishPercentiles(0.5, 0.95)
        .publishPercentileHistogram()
        .register(meterRegistry)

    /**
     * Processes the supplied TaxCalculation instance.  The expectation is that a tax calculation is stateful,
     * with state being stored in the TaxCalculation instance.  A TaxModule instance will expect specific data
     * to be included to function properly.
     */
    @OptIn(ExperimentalTime::class)
    override suspend fun process(taxCalculation: TaxCalculation): TaxCalculation {
        return measureTimedValue {
            taxCalculation
                .taxCalculationItemList
                .forEach { taxCalculationItem ->
                    taxCalculationItem
                        .taxAuthorityJurisdictionList
                        .forEach { taxAuthorityJurisdiction ->
                            val taxAuthority = taxAuthorityJurisdiction.taxAuthority
                            val taxRate = taxAuthorityJurisdiction.taxRate
                            val taxAmount = taxCalculationItem.amount.multiply(taxAuthorityJurisdiction.taxRate.rate)
                                .setScale(Constants.BIG_DECIMAL_SCALE)
                            val taxResult = TaxResult(
                                taxAuthority = taxAuthority,
                                taxRate = taxRate,
                                taxAmount = taxAmount
                            )

                            with(taxCalculationItem) {
                                taxResultList.add(taxResult)
                                messageList.add(
                                    TaxCalculationMessage(
                                        status = TaxCalculationMessageStatus.INFO,
                                        message = "Authority ${taxAuthority.name} has jurisdiction, applied tax rate ${taxRate.code} of ${taxRate.rate}"
                                    )
                                )
                            }
                        }
                }

            taxCalculation
        }.also {
            timer.record(it.duration.toJavaDuration())
        }.value
    }
}
