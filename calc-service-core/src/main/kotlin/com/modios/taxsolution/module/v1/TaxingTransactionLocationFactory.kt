package com.modios.taxsolution.module.v1

import com.modios.taxsolution.calc.model.v1.TransactionLocation
import com.modios.taxsolution.content.model.v1.Geography
import com.modios.taxsolution.content.model.v1.TaxContentProvider
import com.modios.taxsolution.service.data.v1.GeographyService
import mu.KotlinLogging

interface TaxingTransactionLocationFactory {
    /**
     * This method 'seeds' the TaxingTransactoinLocation with the applicable tax authority and the country geography.
     * It is assumed that completion of
     */
    fun create(taxContentProvider: TaxContentProvider, geography: Geography): TaxingTransactionLocation

    suspend fun completeFromTransactionLocation(
        transactionLocation: TransactionLocation,
        taxingTransactionLocation: TaxingTransactionLocation
    ): TaxingTransactionLocation
}

class DefaultTaxingTransactionLocationFactory(private val geographyService: GeographyService) :
    TaxingTransactionLocationFactory {

    private val logger = KotlinLogging.logger { }

    internal fun isUSCountry(code: String?): Boolean = code == "US"

    val EUCountryList = listOf("FR", "DE", "ES", "IT")
    internal fun isEUCountry(code: String?): Boolean = EUCountryList.contains(code)

    val CanadaProvinceList = listOf("BC", "QC", "AB", "ON")
    internal fun isCanadianProvince(code: String?): Boolean = CanadaProvinceList.contains(code)

    override fun create(taxContentProvider: TaxContentProvider, geography: Geography): TaxingTransactionLocation {
        return when {
            isUSCountry(geography.code) -> {
                USTaxingTransactionLocation(country = TaxingGeography(geography = geography))
            }
            isCanadianProvince(geography.code) -> {
                CanadianTaxingTransactionLocation(country = TaxingGeography(geography = geography))
            }
            isEUCountry(geography.code) -> {
                EUTaxingTransactionLocation(country = TaxingGeography(geography = geography))
            }
            else -> throw RuntimeException("The supplied geoggraphy cannot be converted into a taxable location: ${geography.name}")
        }.apply { taxContentProviderList.add(taxContentProvider) }
    }

    internal suspend fun completeFromUSTransactionLocation(
        transactionLocation: TransactionLocation,
        taxingTransactionLocation: USTaxingTransactionLocation
    ): TaxingTransactionLocation {
        val (taxContentProvider, state) = taxingTransactionLocation
            .taxContentProviderList
            .map { taxContentProvider ->
                val code = transactionLocation.state
                logger.trace { "Geography lookup of $code for ${taxContentProvider.name}" }
                when (code) {
                    null -> {
                        logger.warn { "Transaction location has no state code" }
                        taxContentProvider to null
                    }
                    else -> {
                        taxContentProvider to geographyService
                            .getByTaxContentProviderIdAndParentIdAndCode(
                                taxContentProviderId = taxContentProvider.id,
                                parentId = taxingTransactionLocation.country.geography.id,
                                code = code
                            )
                    }
                }
            }.first()

        logger.trace { "State: $state" }
        return when (state) {
            null -> taxingTransactionLocation
            else -> taxingTransactionLocation.copy(state = TaxingGeography(geography = state)).also { it.taxContentProviderList.add(taxContentProvider) }
        }
    }

    internal suspend fun completeFromEUTransactionLocation(
        transactionLocation: TransactionLocation,
        taxingTransactionLocation: TaxingTransactionLocation
    ): TaxingTransactionLocation {
        return taxingTransactionLocation
        // TODO: completion of EU TaxingLocationGeography goes here, as of now country is the only geography handled
    }

    internal suspend fun completeFromCanadianTransactionLocation(
        transactionLocation: TransactionLocation,
        taxingTransactionLocation: TaxingTransactionLocation
    ): TaxingTransactionLocation {
        return taxingTransactionLocation
        // TODO: completion of Canadian TaxingLocationGeography goes here, as of now country is the only geography handled
    }

    // TODO: expand this into a TransactionLocation service that will properly process a transaction location per region
    override suspend fun completeFromTransactionLocation(
        transactionLocation: TransactionLocation,
        taxingTransactionLocation: TaxingTransactionLocation
    ): TaxingTransactionLocation {
        return when (transactionLocation.getRegion()) {
            TransactionLocationRegion.US -> completeFromUSTransactionLocation(
                transactionLocation,
                taxingTransactionLocation as USTaxingTransactionLocation
            )
            TransactionLocationRegion.EU -> completeFromEUTransactionLocation(
                transactionLocation,
                taxingTransactionLocation
            )
            TransactionLocationRegion.CANADA -> completeFromCanadianTransactionLocation(
                transactionLocation,
                taxingTransactionLocation
            )
        }
    }
}

// TODO: expand this into a TransactionLocation service that will properly process a transaction location per region
enum class TransactionLocationRegion {
    US,
    EU,
    CANADA;

    companion object {

        fun isUSRegion(transactionLocation: TransactionLocation): Boolean {
            return (transactionLocation.country == "US") ||
                listOf("AK", "CA", "FL", "NY", "TX").contains(transactionLocation.state)
        }

        fun isEURegion(transactionLocation: TransactionLocation): Boolean {
            return false // just not implemented yet
        }

        fun isCanadaRegion(transactionLocation: TransactionLocation): Boolean {
            return false // just not implemented yet
        }

        fun getTransactionLocationRegion(transactionLocation: TransactionLocation): TransactionLocationRegion {
            return when {
                isUSRegion(transactionLocation) -> US
                isEURegion(transactionLocation) -> EU
                isCanadaRegion(transactionLocation) -> CANADA
                else -> throw RuntimeException("Unknow TransactionLocation region: $transactionLocation")
            }
        }
    }
}

fun TransactionLocation.getRegion(): TransactionLocationRegion =
    TransactionLocationRegion.getTransactionLocationRegion(this)
