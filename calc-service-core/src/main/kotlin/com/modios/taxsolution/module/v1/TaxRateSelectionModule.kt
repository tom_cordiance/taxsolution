package com.modios.taxsolution.module.v1

import com.modios.taxsolution.service.data.v1.TaxRateService
import io.micrometer.core.instrument.MeterRegistry
import io.micrometer.core.instrument.Timer
import mu.KotlinLogging
import kotlin.time.ExperimentalTime
import kotlin.time.measureTimedValue
import kotlin.time.toJavaDuration

/**
 * Requires: TaxAuthorities set for each TaxingTransactionLocation
 *
 * Output: TaxRates for each TaxingTransactionLocation
 *
 */
class TaxRateSelectionModule(meterRegistry: MeterRegistry, private val taxRateService: TaxRateService) : TaxModule {

    private val logger = KotlinLogging.logger { }

    private val timer = Timer
        .builder("cordiance.module.taxRateSelection")
        .publishPercentiles(0.5, 0.95)
        .publishPercentileHistogram()
        .register(meterRegistry)

    @OptIn(ExperimentalTime::class)
    override suspend fun process(taxCalculation: TaxCalculation): TaxCalculation {
        logger.trace { "TaxCalculation: $taxCalculation" }

        return measureTimedValue {
            val taxingTransactionLocations = taxCalculation.getAllTaxingTransactionLocations()
            logger.trace { "TaxingTransactionLocations: $taxingTransactionLocations" }

            val taxAuthorityJurisdictionList =
                taxingTransactionLocations
                    .flatMap { taxingTransactionLocation ->
                        logger.trace { "TaxingTransactionLocation: ${taxingTransactionLocation.getAllTaxingGeography()}" }
                        taxingTransactionLocation
                            .taxContentProviderList
                            .flatMap { taxContentProvider ->
                                logger.trace { "Tax content provider: $taxContentProvider" }
                                taxingTransactionLocation
                                    .getAllTaxingGeography()
                                    .flatMap { taxingGeography ->
                                        logger.trace { "TaxingGeography: $taxingGeography" }
                                        taxingGeography
                                            .taxAuthorityList
                                            .flatMap { taxAuthority ->
                                                logger.trace { "TaxAuthority: $taxAuthority" }
                                                taxRateService
                                                    .findByTaxAuthorityId(taxAuthorityId = taxAuthority.id)
                                                    .map { taxRate ->
                                                        logger.trace { "TaxRate: $taxRate" }
                                                        TaxAuthorityJurisdiction(
                                                            taxAuthority = taxAuthority,
                                                            taxRate = taxRate
                                                        )
                                                    }
                                            }
                                    }
                            }
                    }

            logger.trace { "Tax authority jurisdiction list: $taxAuthorityJurisdictionList" }

            taxCalculation.taxCalculationItemList.forEach { taxCalculationItem ->
                taxCalculationItem.taxAuthorityJurisdictionList.addAll(
                    taxAuthorityJurisdictionList
                )
            }

            taxCalculation
        }.also {
            timer.record(it.duration.toJavaDuration())
        }.value
    }
}
