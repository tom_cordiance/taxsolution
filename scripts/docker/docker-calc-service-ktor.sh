#!/bin/bash
set -e
set -u

docker run -t -i -e TS_ADMIN_SERVICE_URL="http://admin-service:8001" \
-e TS_TAX_CONTENT_SERVICE_URL="http://tax-content-service:8002" \
-e TS_KAFKA_BOOTSTRAP_SERVERS="kafka:9093" \
-e JAVA_TOOL_OPTIONS="-Xms2048m -Xmx2048m" \
-p 8800:8800 \
--network=taxsolution \
--name calc-service-ktor nexus.dev.modios.io/calc-service-ktor:2021.1.1

#      ADMIN_SERVICE_HOST: "admin-service"
#      ADMIN_SERVICE_PORT: "8001"
#      TS_ADMIN_SERVICE_URL: "http://admin-service:8001"
#      TAX_CONTENT_SERVICE_HOST: "tax-content-service"
#      TAX_CONTENT_SERVICE_PORT: "8002"
#      TS_TAX_CONTENT_SERVICE_URL: "http://tax-content-service:8002"
#      TS_KAFKA_SERVERS: "kafka"
#      TS_KAFKA_PORT: "9093"
#      TS_KAFKA_BOOTSTRAP_SERVERS: "kafka:9093"
#      JAVA_TOOL_OPTIONS: "-Xms1024m -Xmx1024m"
