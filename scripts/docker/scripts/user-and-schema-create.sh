#!/bin/bash
set -e
set -u

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
  CREATE USER taxsolution PASSWORD 'taxsolution';
  CREATE DATABASE taxsolution;
  GRANT ALL PRIVILEGES ON DATABASE taxsolution TO taxsolution;
EOSQL

psql -v ON_ERROR_STOP=1 -U taxsolution -d taxsolution -a <<-EOSQL
  create schema if not exists ts_admin;
  create schema if not exists ts_tax_content;
EOSQL
