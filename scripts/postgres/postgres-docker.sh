#!/bin/bash
set -e
set -u

docker run -d \
    --name postgres:12 \
    -p 5432:5432 \
    -e POSTGRES_USER=taxengine \
    -e POSTGRES_PASSWORD=password \
    -e PGDATA=/var/lib/postgresql/data/pgdata \
    -v /Users/tom/workspace/data/postgres:/var/lib/postgresql/data \
    postgres