#!/bin/bash
set -e
set -u

# intended to be run after installing PostgreSQL via Homebrew
# > brew install postgres
createuser -s postgres
createuser taxsolution
createdb taxsolution -O taxsolution

psql -U taxsolution -d taxsolution -a -f create-schemas.sql