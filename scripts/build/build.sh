#!/bin/bash
set -e
set -u

pushd ../../

./gradlew ktlintFormat clean

./gradlew :admin-service:bootBuildImage

./gradlew :tax-compliance-service:bootBuildImage

./gradlew :tax-content-service:bootBuildImage

./gradlew :calc-service-ktor:assemble

./gradlew :tax-content-client-ktor:assemble

./gradlew :admin-client-ktor:assemble

./gradlew :calc-client:bootJar

popd

docker build -f Dockerfile.calc-service-ktor -t nexus.dev.modios.io/calc-service-ktor:2022.1.1 ../../calc-service-ktor/build/distributions
