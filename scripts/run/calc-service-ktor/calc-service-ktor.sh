#!/bin/bash
set -e
set -u

rm -rf ./calc-service-ktor-2022.1.1
tar -xvf ../../../calc-service-ktor/build/distributions/calc-service-ktor-2022.1.1.tar

calc-service-ktor-2022.1.1/bin/calc-service-ktor
