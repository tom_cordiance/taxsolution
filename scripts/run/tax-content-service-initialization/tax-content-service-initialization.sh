#!/bin/bash
set -e
set -u

rm -rf ./tax-content-client-ktor
tar -xvf ../../../tax-content-client-ktor/build/distributions/tax-content-client-ktor-2022.1.1.tar

tax-content-client-ktor-2022.1.1/bin/tax-content-client-ktor update

