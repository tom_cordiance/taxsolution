#!/bin/bash
set -e
set -u

rm -rf ./admin-client-ktor
tar -xvf ../../../admin-client-ktor/build/distributions/admin-client-ktor-2022.1.1.tar

admin-client-ktor-2022.1.1/bin/admin-client-ktor

