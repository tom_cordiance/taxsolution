#!/bin/bash
set -e
set -u

rm -f ./calc-client-2022.1.1.jar
cp ../../../calc-client/build/libs/calc-client-2022.1.1.jar .

java -jar calc-client-2022.1.1.jar

