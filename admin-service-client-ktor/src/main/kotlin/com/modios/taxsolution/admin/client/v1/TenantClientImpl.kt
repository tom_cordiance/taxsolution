package com.modios.taxsolution.admin.client.v1

import com.modios.taxsolution.model.v1.Tenant
import io.ktor.client.HttpClient
import io.ktor.client.request.get
import io.ktor.client.request.post
import io.ktor.client.request.url
import io.ktor.client.statement.HttpResponse
import io.ktor.client.statement.readText
import io.ktor.http.ContentType
import io.ktor.http.contentType
import java.util.Properties

class TenantClientImpl(private val httpClient: HttpClient, properties: Properties) :
    TenantClient {

    private val urlPropertyName = "cordiance.admin-service.url"
    private val serverAddress: String = properties.getProperty(urlPropertyName)
        ?: throw RuntimeException("$urlPropertyName was not set in configuration")

    private val baseUrl = "$serverAddress/api/v1/tenant"

    override suspend fun createTenant(name: String): Tenant {
        val tenant: Tenant = httpClient.post {
            url("$baseUrl/$name")
            contentType(ContentType.Application.Json)
        }

        return tenant
    }

    override suspend fun getTenant(name: String): Tenant? {
        val response: HttpResponse = httpClient.get("$baseUrl/$name")
        return decodeNullable(response.readText())
    }

    override suspend fun getTenant(name: String, jwt: String): Tenant? {
        TODO("Not yet implemented")
    }
}
