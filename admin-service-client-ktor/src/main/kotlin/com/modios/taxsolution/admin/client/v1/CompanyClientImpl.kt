package com.modios.taxsolution.admin.client.v1

import com.modios.taxsolution.model.v1.Company
import io.ktor.client.HttpClient
import io.ktor.client.request.get
import io.ktor.client.request.post
import io.ktor.client.statement.HttpResponse
import io.ktor.client.statement.readText
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import java.util.Properties

class CompanyClientImpl(private val httpClient: HttpClient, properties: Properties) : CompanyClient {

    private val urlPropertyName = "cordiance.admin-service.url"

    private val serverAddress: String = properties.getProperty(urlPropertyName)
        ?: throw RuntimeException("$urlPropertyName was not set in configuration")

    private val baseUrl = "$serverAddress/api/v1/company"

    override suspend fun create(tenantId: Long, parentCompanyId: Long, name: String): Company {
        val company: Company = httpClient.post("$baseUrl/$tenantId/$parentCompanyId/$name")

        return company
    }

    override suspend fun getCompanies(tenantId: Long): List<Company> {
        val companyList: List<Company> = httpClient.get("$baseUrl/$tenantId")

        return companyList
    }

    override suspend fun getCompany(tenantId: Long, name: String): Company? {
        val response: HttpResponse = httpClient.get("$baseUrl/$tenantId/$name")
        return decodeNullable(response.readText())
    }
}

inline fun <reified T> decodeNullable(json: String): T? {
    return when {
        json.isBlank() -> null
        else -> Json.decodeFromString(json)
    }
}
