package com.modios.taxsolution.admin.client.v1

import com.modios.taxsolution.model.v1.CompanyTaxContentProvider
import io.ktor.client.HttpClient
import io.ktor.client.request.get
import io.ktor.client.request.post
import java.util.Properties

class CompanyTaxContentProviderClientImpl(private val httpClient: HttpClient, properties: Properties) :
    CompanyTaxContentProviderClient {

    private val urlPropertyName = "cordiance.admin-service.url"

    private val serverAddress: String = properties.getProperty(urlPropertyName)
        ?: throw RuntimeException("$urlPropertyName was not set in configuration")

    private val baseUrl = "$serverAddress/api/v1/company-tax-content-provider"

    override suspend fun create(companyId: Long, taxContentProviderId: Long): CompanyTaxContentProvider {
        return httpClient.post("$baseUrl/$companyId/$taxContentProviderId")
    }

    override suspend fun getByCompanyId(companyId: Long): List<CompanyTaxContentProvider> {
        return httpClient.get("$baseUrl/$companyId")
    }
}
