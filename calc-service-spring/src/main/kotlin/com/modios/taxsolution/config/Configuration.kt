package com.modios.taxsolution.config

// import com.fasterxml.jackson.annotation.JsonInclude
// import com.fasterxml.jackson.databind.ObjectMapper
// import com.fasterxml.jackson.databind.module.SimpleModule
// import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
// import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.modios.taxsolution.calc.CalcCommon
// import com.modios.taxsolution.calc.model.v1.TaxDocument
// import com.modios.taxsolution.calc.model.v1.TaxDocumentItem
// import com.modios.taxsolution.calc.model.v1.TaxDocumentItemJsonSerializer
// import com.modios.taxsolution.calc.model.v1.TaxDocumentJsonDeserializer
// import com.modios.taxsolution.calc.model.v1.TaxDocumentJsonSerializer
// import com.modios.taxsolution.calc.model.v1.TransactionLocation
// import com.modios.taxsolution.calc.model.v1.TransactionLocationSerializer
import com.modios.taxsolution.module.v1.CompanyModule
import com.modios.taxsolution.module.v1.CompanyTaxContentProviderModule
import com.modios.taxsolution.module.v1.DefaultTaxingTransactionLocationFactory
import com.modios.taxsolution.module.v1.StoreTaxDocumentResultModule
import com.modios.taxsolution.module.v1.TaxAuthorityModule
import com.modios.taxsolution.module.v1.TaxCalculationModule
import com.modios.taxsolution.module.v1.TaxModule
import com.modios.taxsolution.module.v1.TaxRateSelectionModule
import com.modios.taxsolution.module.v1.TaxingTransactionLocationFactory
import com.modios.taxsolution.module.v1.TaxingTransactionLocationModule
import com.modios.taxsolution.module.v1.TenantModule
import com.modios.taxsolution.service.data.v1.CompanyServiceImpl
import com.modios.taxsolution.service.data.v1.CompanyTaxContentProviderServiceImpl
import com.modios.taxsolution.service.data.v1.GeographyServiceImpl
import com.modios.taxsolution.service.data.v1.TaxAuthorityGeographyServiceImpl
import com.modios.taxsolution.service.data.v1.TaxAuthorityServiceImpl
import com.modios.taxsolution.service.data.v1.TaxContentProviderServiceImpl
import com.modios.taxsolution.service.data.v1.TaxDocumentResultStorageService
import com.modios.taxsolution.service.data.v1.TaxRateServiceImpl
import com.modios.taxsolution.service.data.v1.TenantServiceImpl
import io.micrometer.core.instrument.MeterRegistry
import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf
import org.apache.kafka.clients.admin.NewTopic
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
// import org.springframework.context.annotation.Primary
import org.springframework.kafka.support.converter.RecordMessageConverter
import org.springframework.kafka.support.converter.StringJsonMessageConverter
import org.springframework.web.reactive.function.client.WebClient
import java.util.concurrent.Executors

@Configuration
@EnableConfigurationProperties(CordianceProperties::class)
class Configuration {

    @Bean
    fun adminWebClient(cordianceProperties: CordianceProperties) = WebClient.builder()
        .baseUrl("http://${cordianceProperties.adminService.host}:${cordianceProperties.adminService.port}/").build()

    @Bean
    fun taxContentWebClient(cordianceProperties: CordianceProperties) = WebClient.builder()
        .baseUrl("http://${cordianceProperties.taxContentService.host}:${cordianceProperties.taxContentService.port}/")
        .build()

    /**
     * Standard Engine Beans
     */

    @Bean
    fun taxingTransactionLocationFactory(geographyService: GeographyServiceImpl): TaxingTransactionLocationFactory =
        DefaultTaxingTransactionLocationFactory(geographyService)

    @Bean
    fun tenantModule(meterRegistry: MeterRegistry, tenantService: TenantServiceImpl): TenantModule =
        TenantModule(meterRegistry, tenantService)

    @Bean
    fun companyModule(meterRegistry: MeterRegistry, companyService: CompanyServiceImpl): CompanyModule =
        CompanyModule(meterRegistry, companyService)

    @Bean
    fun companyTaxContentProviderModule(
        meterRegistry: MeterRegistry,
        companyTaxContentProviderService: CompanyTaxContentProviderServiceImpl,
        taxContentProviderService: TaxContentProviderServiceImpl,
        geographyService: GeographyServiceImpl,
        taxingTransactionLocationFactory: TaxingTransactionLocationFactory
    ): CompanyTaxContentProviderModule = CompanyTaxContentProviderModule(
        meterRegistry,
        companyTaxContentProviderService,
        taxContentProviderService,
        geographyService,
        taxingTransactionLocationFactory
    )

    @Bean
    fun taxingTransactionLocationModule(
        meterRegistry: MeterRegistry,
        taxingTransactionLocationFactory: TaxingTransactionLocationFactory
    ): TaxingTransactionLocationModule =
        TaxingTransactionLocationModule(meterRegistry, taxingTransactionLocationFactory)

    @Bean
    fun taxAuthorityModule(
        meterRegistry: MeterRegistry,
        taxAuthorityService: TaxAuthorityServiceImpl,
        taxAuthorityGeographyService: TaxAuthorityGeographyServiceImpl
    ): TaxAuthorityModule = TaxAuthorityModule(meterRegistry, taxAuthorityService, taxAuthorityGeographyService)

    @Bean
    fun taxRateSelectionModule(meterRegistry: MeterRegistry, taxRateService: TaxRateServiceImpl): TaxRateSelectionModule =
        TaxRateSelectionModule(meterRegistry, taxRateService)

    @Bean
    fun taxCalculationModule(meterRegistry: MeterRegistry): TaxCalculationModule = TaxCalculationModule(meterRegistry)

    @Bean
    fun storeTaxDocumentResultModule(
        meterRegistry: MeterRegistry,
        taxDocumentResultStorageService: TaxDocumentResultStorageService
    ): StoreTaxDocumentResultModule = StoreTaxDocumentResultModule(meterRegistry, taxDocumentResultStorageService)

    @Bean
    fun standardTaxModules(
        tenantModule: TenantModule,
        companyModule: CompanyModule,
        companyTaxContentProviderModule: CompanyTaxContentProviderModule,
        taxingTransactionLocationModule: TaxingTransactionLocationModule,
        taxAuthorityModule: TaxAuthorityModule,
        taxRateSelectionModule: TaxRateSelectionModule,
        taxCalculationModule: TaxCalculationModule,
        storeTaxDocumentResultModule: StoreTaxDocumentResultModule
    ): Flow<TaxModule> = flowOf(
        tenantModule,
        companyModule,
        companyTaxContentProviderModule,
        taxingTransactionLocationModule,
        taxAuthorityModule,
        taxRateSelectionModule,
        taxCalculationModule,
        storeTaxDocumentResultModule
    )

    @Bean
    fun calculationDispatcher() = Executors.newFixedThreadPool(16).asCoroutineDispatcher()

    @Bean
    fun taxDocumentResultTopic() = with(CalcCommon) {
        NewTopic(
            TAX_DOCUMENT_RESULT_TOPIC_NAME,
            TAX_DOCUMENT_RESULT_TOPIC_PARTITIONS,
            TAX_DOCUMENT_RESULT_TOPIC_REPLICATION.toShort()
        )
    }

    @Bean
    fun converter(): RecordMessageConverter? {
        return StringJsonMessageConverter()
    }

    // @Bean
    // @Primary
    // fun objectMapper(): ObjectMapper = ObjectMapper().also {
    //     it.setSerializationInclusion(JsonInclude.Include.NON_NULL)
    //     it.registerModule(KotlinModule())
    //     it.registerModule(JavaTimeModule())
    //     it.registerModule(
    //         SimpleModule().also {
    //             it.addSerializer(TaxDocument::class.java, TaxDocumentJsonSerializer())
    //             it.addSerializer(TransactionLocation::class.java, TransactionLocationSerializer())
    //             it.addSerializer(TaxDocumentItem::class.java, TaxDocumentItemJsonSerializer())
    //             it.addDeserializer(TaxDocument::class.java, TaxDocumentJsonDeserializer())
    //         }
    //     )
    // }
}
