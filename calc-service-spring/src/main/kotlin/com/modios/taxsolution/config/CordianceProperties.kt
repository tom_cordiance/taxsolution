package com.modios.taxsolution.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

@ConstructorBinding
@ConfigurationProperties(prefix = "cordiance")
data class CordianceProperties(val adminService: ServiceProperties, val taxContentService: ServiceProperties)

data class ServiceProperties(val host: String = "localhost", val port: String = "8080")
