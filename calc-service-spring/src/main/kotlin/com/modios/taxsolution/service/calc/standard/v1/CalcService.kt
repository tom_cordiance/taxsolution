package com.modios.taxsolution.service.calc.standard.v1

import com.modios.taxsolution.calc.model.v1.TaxDocument
import com.modios.taxsolution.calc.model.v1.TaxDocumentResult
import com.modios.taxsolution.engine.v1.StandardEngine
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import kotlin.time.ExperimentalTime

@RestController
@RequestMapping("/calc/standard/v1")
class CalcService(private val standardEngine: StandardEngine) {

    @ExperimentalTime
    @PostMapping("/jackson")
    suspend fun calculateJackson(@RequestBody taxDocument: TaxDocument): TaxDocumentResult {
        return standardEngine.calculate(taxDocument)
    }

    @ExperimentalTime
    @PostMapping("/kotlin")
    suspend fun calculateKotlin(@RequestBody taxDocumentJson: String): TaxDocumentResult {
        val taxDocument = Json.decodeFromString<TaxDocument>(taxDocumentJson)
        return standardEngine.calculate(taxDocument)
    }
}
