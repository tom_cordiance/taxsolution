package com.modios.taxsolution.service.data.v1

import com.modios.taxsolution.admin.client.v1.CompanyTaxContentProviderClient
import com.modios.taxsolution.model.v1.CompanyTaxContentProvider
import com.modios.taxsolution.service.data.cache.CacheKey
import com.modios.taxsolution.service.data.cache.ServiceCacheStandard
import org.springframework.stereotype.Component

@Component
class CompanyTaxContentProviderServiceImpl(private val companyTaxContentProviderClient: CompanyTaxContentProviderClient) :
    CompanyTaxContentProviderService {

    private val serviceCache = ServiceCacheStandard<CompanyTaxContentProvider>()

    override suspend fun getCompanyTaxContentProviders(companyId: Long): List<CompanyTaxContentProvider> {
        return serviceCache.getOrPutCollection(
            cacheKey = CompanyTaxContentProviderForCompanyIdCacheKey(companyId),
            get = { companyTaxContentProviderClient.getByCompanyId(companyId) }
        )
    }
}

data class CompanyTaxContentProviderForCompanyIdCacheKey(val companyId: Long) : CacheKey<CompanyTaxContentProvider>
