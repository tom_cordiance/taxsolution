package com.modios.taxsolution.service.data.v1

import com.modios.taxsolution.admin.client.v1.TenantClient
import com.modios.taxsolution.model.v1.Tenant
import com.modios.taxsolution.service.data.cache.CacheKey
import com.modios.taxsolution.service.data.cache.ServiceCacheStandard
import org.springframework.stereotype.Component

@Component
class TenantServiceImpl(private val tenantClient: TenantClient) : TenantService {

    private val serviceCache = ServiceCacheStandard<Tenant>()

    override suspend fun get(name: String): Tenant? {
        return serviceCache.getOrPutValue(cacheKey = TenantNameCacheKey(name), get = { tenantClient.getTenant(name) })
    }
}

data class TenantNameCacheKey(val name: String) : CacheKey<Tenant>
