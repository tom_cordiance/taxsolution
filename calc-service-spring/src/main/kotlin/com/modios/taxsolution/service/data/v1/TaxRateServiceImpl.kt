package com.modios.taxsolution.service.data.v1

import com.modios.taxsolution.content.client.v1.TaxRateClient
import com.modios.taxsolution.content.model.v1.TaxRate
import com.modios.taxsolution.service.data.cache.CacheKey
import com.modios.taxsolution.service.data.cache.ServiceCacheStandard
import mu.KotlinLogging
import org.springframework.stereotype.Component

@Component
class TaxRateServiceImpl(private val taxRateClient: TaxRateClient) : TaxRateService {

    private val logger = KotlinLogging.logger { }

    private val serviceCache = ServiceCacheStandard<TaxRate>()

    override suspend fun findByTaxAuthorityId(taxAuthorityId: Long): List<TaxRate> {
        return serviceCache.getOrPutCollection(
            cacheKey = TaxRatesByTaxAuthorityIdCacheKey(taxAuthorityId),
            get = { taxRateClient.findByTaxAuthorityId(taxAuthorityId) }
        )
    }

    override suspend fun evictByAuthorityId(taxAuthorityId: Long) {
        logger.info { "evictByAuthorityId($taxAuthorityId)" }
        serviceCache.evict(TaxRatesByTaxAuthorityIdCacheKey(taxAuthorityId))
    }
}

data class TaxRatesByTaxAuthorityIdCacheKey(val taxAuthorityId: Long) : CacheKey<TaxRate>
