package com.modios.taxsolution.service.data.v1

import com.modios.taxsolution.content.client.v1.TaxAuthorityGeographyClient
import com.modios.taxsolution.content.model.v1.TaxAuthorityGeography
import com.modios.taxsolution.service.data.cache.CacheKey
import com.modios.taxsolution.service.data.cache.ServiceCacheStandard
import org.springframework.stereotype.Component

@Component
class TaxAuthorityGeographyServiceImpl(private val taxAuthorityGeographyClient: TaxAuthorityGeographyClient) :
    TaxAuthorityGeographyService {

    private val serviceCache = ServiceCacheStandard<TaxAuthorityGeography>()

    override suspend fun getByGeographyId(geographyId: Long): List<TaxAuthorityGeography> {
        return serviceCache.getOrPutCollection(
            cacheKey = GeographyIdCacheKey(geographyId = geographyId),
            get = { taxAuthorityGeographyClient.getByGeographyId(geographyId = geographyId) }
        )
    }
}

data class GeographyIdCacheKey(val geographyId: Long) : CacheKey<TaxAuthorityGeography>
