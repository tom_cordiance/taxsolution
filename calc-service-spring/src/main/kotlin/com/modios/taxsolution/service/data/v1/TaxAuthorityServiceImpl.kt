package com.modios.taxsolution.service.data.v1

import com.modios.taxsolution.content.client.v1.TaxAuthorityClient
import com.modios.taxsolution.content.model.v1.TaxAuthority
import com.modios.taxsolution.service.data.cache.CacheKey
import com.modios.taxsolution.service.data.cache.ServiceCacheStandard
import org.springframework.stereotype.Component

@Component
class TaxAuthorityServiceImpl(private val taxAuthorityClient: TaxAuthorityClient) : TaxAuthorityService {

    private val serviceCache = ServiceCacheStandard<TaxAuthority>()

    override suspend fun getByTaxAuthorityId(taxAuthorityId: Long): TaxAuthority? {
        return serviceCache.getOrPutValue(
            cacheKey = TaxAuthorityIdCacheKey(taxAuthorityId),
            get = { taxAuthorityClient.getById(taxAuthorityId) }
        )
    }
}

data class TaxAuthorityIdCacheKey(val taxAuthorityId: Long) : CacheKey<TaxAuthority>
