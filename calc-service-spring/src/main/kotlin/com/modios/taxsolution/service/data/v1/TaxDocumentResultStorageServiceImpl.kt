package com.modios.taxsolution.service.data.v1

import com.modios.taxsolution.calc.CalcCommon
import com.modios.taxsolution.calc.model.v1.TaxDocumentResult
import com.modios.taxsolution.calc.model.v1.TaxDocumentResultKey
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.stereotype.Component

/**
 * The queue is a proof-of-concept level store. The goal is Kafka or something similar.
 */
@Component
class TaxDocumentResultStorageServiceImpl(private val kafkaTemplate: KafkaTemplate<TaxDocumentResultKey, TaxDocumentResult>) :
    TaxDocumentResultStorageService {

    override fun write(taxDocumentResult: TaxDocumentResult) {
        // ignore for now, abstraction to actual service: Kafka etc
        val key = TaxDocumentResultKey(
            timestamp = taxDocumentResult.calculationDate,
            tenantName = taxDocumentResult.tenantName,
            companyName = taxDocumentResult.companyName,
            documentNumber = taxDocumentResult.documentNumber
        )

        kafkaTemplate.send(CalcCommon.TAX_DOCUMENT_RESULT_TOPIC_NAME, key, taxDocumentResult)
    }
}
