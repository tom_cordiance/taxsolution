package com.modios.taxsolution

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.context.properties.EnableConfigurationProperties

@SpringBootApplication
@EnableConfigurationProperties
@ConfigurationPropertiesScan
class CalcServiceApp

fun main(args: Array<String>) {
    SpringApplication.run(CalcServiceApp::class.java, *args)
}
