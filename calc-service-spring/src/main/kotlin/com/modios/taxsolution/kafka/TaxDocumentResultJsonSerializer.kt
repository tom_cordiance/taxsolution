package com.modios.taxsolution.kafka

import com.modios.taxsolution.calc.model.v1.TaxDocumentResult
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.apache.kafka.common.serialization.Serializer

class TaxDocumentResultJsonSerializer : Serializer<TaxDocumentResult> {

    override fun serialize(topic: String, data: TaxDocumentResult): ByteArray {
        return Json.encodeToString(data).toByteArray()
    }
}
