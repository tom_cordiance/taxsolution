package com.modios.taxsolution.kafka

import com.modios.taxsolution.calc.model.v1.TaxDocumentResult
import org.apache.kafka.common.serialization.Serializer
import java.io.ByteArrayOutputStream
import java.io.ObjectOutputStream

class TaxDocumentResultSerializer : Serializer<TaxDocumentResult> {
    override fun serialize(topic: String, data: TaxDocumentResult): ByteArray {
        return ByteArrayOutputStream().use {
            ObjectOutputStream(it).use {
                it.writeObject(data)
                it.flush()
            }
            it.toByteArray()
        }
    }
}
