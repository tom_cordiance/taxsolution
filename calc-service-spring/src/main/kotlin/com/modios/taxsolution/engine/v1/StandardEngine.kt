package com.modios.taxsolution.engine.v1

import com.modios.taxsolution.calc.model.v1.MessageStatus
import com.modios.taxsolution.calc.model.v1.TaxDocument
import com.modios.taxsolution.calc.model.v1.TaxDocumentItem
import com.modios.taxsolution.calc.model.v1.TaxDocumentItemResult
import com.modios.taxsolution.calc.model.v1.TaxDocumentItemTaxResult
import com.modios.taxsolution.calc.model.v1.TaxDocumentResult
import com.modios.taxsolution.calc.model.v1.TaxDocumentResultMessage
import com.modios.taxsolution.module.v1.TaxCalculation
import com.modios.taxsolution.module.v1.TaxCalculationItem
import com.modios.taxsolution.module.v1.TaxCalculationMessage
import com.modios.taxsolution.module.v1.TaxModule
import io.micrometer.core.instrument.MeterRegistry
import io.micrometer.core.instrument.Timer
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.fold
import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.math.BigDecimal
import java.time.LocalDateTime
import kotlin.time.ExperimentalTime
import kotlin.time.measureTimedValue
import kotlin.time.toJavaDuration

/**
 * Note: each engine instance is stateful, since the channels supporting the messaging pipeline are redezvous channels
 * (one message per channel only).  A 'cluster' of engine instances is needed to handle calc loads.
 */
@Component
class StandardEngine(
    meterRegistry: MeterRegistry,
    private val calculationDispatcher: CoroutineDispatcher
) {
    private val logger = KotlinLogging.logger { }
    private val performaceLogger = KotlinLogging.logger("com.modios.performance")

    private val timer = Timer
        .builder("cordiance.engine.standard")
        .publishPercentiles(0.5, 0.95)
        .publishPercentileHistogram()
        .register(meterRegistry)

    @Autowired
    lateinit var moduleFlow: Flow<TaxModule>

    @ExperimentalTime
    suspend fun calculate(taxDocument: TaxDocument): TaxDocumentResult {
        return measureTimedValue {
            val initialTaxCalculation = taxDocument.asTaxCalculation()

            moduleFlow.flowOn(calculationDispatcher).fold(initialTaxCalculation) { taxCalculation, taxModule ->
                logger.trace { "Executing module ${taxModule::class.qualifiedName}" }
                measureTimedValue {
                    taxModule.process(taxCalculation)
                }.let {
                    performaceLogger.trace { "Total time for module ${taxModule::class.qualifiedName}: ${it.duration}" }
                    it.value
                }
            }.asTaxDocumentResult(taxDocument)
        }.also {
            performaceLogger.trace { "Total time for calculation: ${it.duration}" }
            timer.record(it.duration.toJavaDuration())
        }.value
    }
}

/**
 * Conversion functions
 */

fun TaxDocument.asTaxCalculation(): TaxCalculation {
    return TaxCalculation(
        tenantName = this.tenantName,
        companyName = this.companyName,
        calculationDate = LocalDateTime.now(),
        documentNumber = this.documentNumber,
        shipFromTransactionLocation = this.shipFrom,
        shipToTransactionLocation = this.shipTo,
        persistResult = persistResult,
        hasFailure = false
    ).also {
        it.taxCalculationItemList.addAll(this.taxDocumentItems.map { it.asTaxCalculationItem() })
    }
}

fun TaxDocumentItem.asTaxCalculationItem(): TaxCalculationItem {
    return TaxCalculationItem(
        index = this.index,
        productCode = this.productCode,
        productMappingCode = this.productMappingCode,
        quantity = this.quantity,
        amount = this.amount,
        taxAmount = null,
        messageList = mutableListOf()
    )
}

fun TaxCalculation.asTaxDocumentResult(originalTaxDocument: TaxDocument): TaxDocumentResult {
    return TaxDocumentResult(
        originalTaxDocument = originalTaxDocument,
        tenantName = tenantName,
        companyName = companyName,
        documentNumber = documentNumber,
        shipFromTransactionLocation = shipFromTransactionLocation,
        shipToTransactionLocation = shipToTransactionLocation,
        taxDocumentResultMessages = messageList.map { it.asTaxDocumentMessage() },
        taxDocumentItemResults = taxCalculationItemList.map { it.asTaxDocumentItemResult() }
    )
}

fun TaxCalculationMessage.asTaxDocumentMessage(): TaxDocumentResultMessage {
    return TaxDocumentResultMessage(status = MessageStatus.valueOf(status.toString()), message = message)
}

fun TaxCalculationItem.asTaxDocumentItemResult(): TaxDocumentItemResult {
    return TaxDocumentItemResult(
        index = this.index,
        taxResults = taxResultList.map {
            TaxDocumentItemTaxResult(
                authorityName = it.taxAuthority.name,
                taxRate = it.taxRate.rate,
                taxAmount = it.taxAmount
            )
        },
        taxAmount = taxResultList.fold(BigDecimal.ZERO) { sum, taxResult -> sum.add(taxResult.taxAmount) },
        taxDocumentResultMessages = messageList.map { it.asTaxDocumentMessage() }
    )
}
