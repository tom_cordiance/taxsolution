package com.modios.taxsolution.service.data.v1

import com.modios.taxsolution.admin.client.v1.CompanyClient
import com.modios.taxsolution.model.v1.Company
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class CompanyServiceTest {

    @Test
    fun testValidCompanies() = runBlocking {
        val companyList = companyService.getCompanies(tenantId)

        Assertions.assertNotNull(companyList)
        Assertions.assertTrue(companyList.isNotEmpty())
        Assertions.assertEquals(3, companyList.size)

        val companyListAgain = companyService.getCompanies(tenantId)

        Assertions.assertNotNull(companyListAgain)
        Assertions.assertTrue(companyListAgain.isNotEmpty())
        Assertions.assertEquals(3, companyListAgain.size)
    }

    @Test
    fun testValidCompany() = runBlocking {
        val company = companyService.getCompany(tenantId = tenantId, companyName = existingCompany1Name)

        Assertions.assertNotNull(company)
        Assertions.assertEquals(existingCompany1.id, company?.id)
        Assertions.assertEquals(existingCompany1.name, company?.name)

        val companyAgain = companyService.getCompany(tenantId = tenantId, companyName = existingCompany1Name)

        Assertions.assertNotNull(companyAgain)
        Assertions.assertEquals(existingCompany1.id, companyAgain?.id)
        Assertions.assertEquals(existingCompany1.name, companyAgain?.name)
    }

    @Test
    fun testInvalidCompany() = runBlocking {
        val company = companyService.getCompany(tenantId = tenantId, companyName = nonExistentCompanyName)

        Assertions.assertNull(company)
    }

    @Test
    fun testInvalidTenant() = runBlocking {
        val companyList = companyService.getCompanies(invalidTenantId)

        Assertions.assertNotNull(companyList)
        Assertions.assertTrue(companyList.isEmpty())

        val companyListAgain = companyService.getCompanies(invalidTenantId)

        Assertions.assertNotNull(companyListAgain)
        Assertions.assertTrue(companyListAgain.isEmpty())
    }

    companion object {
        val tenantId = 1L
        val invalidTenantId = 99L

        val existingCompany1Name = "Good Company 1"
        val existingCompany1 = Company(
            tenantId = tenantId,
            parentCompanyId = null,
            name = existingCompany1Name,
            id = 1L
        )

        val existingCompany2Name = "Good Company 2"
        val existingCompany2 = Company(
            tenantId = tenantId,
            parentCompanyId = null,
            name = existingCompany2Name,
            id = 2L
        )

        val existingCompany3Name = "Good Company 3"
        val existingCompany3 = Company(
            tenantId = tenantId,
            parentCompanyId = null,
            name = existingCompany3Name,
            id = 3L
        )

        val nonExistentCompanyName = "Bad Company"

        val companyClient = mockk<CompanyClient>().also {
            coEvery { it.getCompanies(tenantId) } returns listOf(
                existingCompany1,
                existingCompany2,
                existingCompany3
            )

            coEvery { it.getCompanies(invalidTenantId) } returns emptyList()

            coEvery { it.getCompany(tenantId, existingCompany1Name) } returns existingCompany1
            coEvery { it.getCompany(tenantId, existingCompany2Name) } returns existingCompany2
            coEvery { it.getCompany(tenantId, existingCompany3Name) } returns existingCompany3
            coEvery { it.getCompany(tenantId, nonExistentCompanyName) } returns null
        }

        val companyService = CompanyServiceImpl(companyClient = companyClient)
    }
}
