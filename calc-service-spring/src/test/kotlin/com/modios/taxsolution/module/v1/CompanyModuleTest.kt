package com.modios.taxsolution.module.v1

import com.modios.taxsolution.calc.model.v1.MessageStatus
import com.modios.taxsolution.model.v1.Company
import com.modios.taxsolution.model.v1.Tenant
import com.modios.taxsolution.service.data.v1.CompanyServiceImpl
import io.micrometer.core.instrument.simple.SimpleMeterRegistry
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import java.time.LocalDate
import java.time.LocalDateTime

class CompanyModuleTest {

    @Test
    fun test() = runBlocking {
        val companyModule = CompanyModule(SimpleMeterRegistry(), companyService)

        val taxCalculation =
            TaxCalculation(
                tenantName = existingTenant.name,
                companyName = existingCompanyName,
                tenant = existingTenant,
                calculationDate = LocalDateTime.now(),
                documentNumber = "123"
            )

        val result = companyModule.process(taxCalculation)

        Assertions.assertNotNull(result)
        Assertions.assertNotNull(result.company)
        Assertions.assertEquals(existingCompanyName, result.company?.name)
    }

    @Test
    fun testBadCompany() = runBlocking {
        val companyModule = CompanyModule(SimpleMeterRegistry(), companyService)

        val taxCalculation =
            TaxCalculation(
                tenantName = existingTenant.name,
                companyName = nonExistantCompany,
                tenant = existingTenant,
                calculationDate = LocalDateTime.now(),
                documentNumber = "123"
            )

        val result = companyModule.process(taxCalculation)

        Assertions.assertNotNull(result)
        Assertions.assertNull(result.company)
        Assertions.assertEquals(1, result.messageList.size)
        Assertions.assertEquals(MessageStatus.ERROR.toString(), result.messageList[0].status.toString())
    }

    companion object {
        val tenantId = 1L
        val existingTenant =
            Tenant(
                id = 1L,
                name = "Good Tenant",
                activeFrom = LocalDate.now(),
                activeTo = null
            )

        val existingCompanyName = "Good Company"
        val existingCompany = Company(
            tenantId = tenantId,
            name = existingCompanyName,
            id = 1L,
            parentCompanyId = null
        )

        val nonExistantCompany = "Bad Company"

        val companyService = mockk<CompanyServiceImpl>().also {
            coEvery { it.getCompany(tenantId, existingCompanyName) } returns existingCompany
            coEvery { it.getCompany(tenantId, nonExistantCompany) } returns null
        }
    }
}
