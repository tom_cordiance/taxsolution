plugins {
    id("org.springframework.boot") apply true
    id("io.spring.dependency-management") apply true
    id("application")
    kotlin("plugin.spring") apply true
    kotlin("plugin.serialization") apply true
}

val logbackVersion: String by project
val logbackEcsEncoderVersion: String by project
val slf4jApiVersion: String by project
val kotlinLoggingVersion: String by project
val junitJupiterVersion: String by project
val mockkVersion: String by project
val kotlinSerializationVersion: String by project

dependencies {
    implementation(enforcedPlatform(org.springframework.boot.gradle.plugin.SpringBootPlugin.BOM_COORDINATES))
    annotationProcessor("org.springframework.boot:spring-boot-configuration-processor")
    annotationProcessor("org.springframework:spring-context-indexer")
    developmentOnly("org.springframework.boot:spring-boot-devtools")

    implementation("org.springframework.boot:spring-boot-starter-actuator")
    implementation("org.springframework.boot:spring-boot-starter-webflux")
    implementation("org.springframework.boot:spring-boot-devtools")
    implementation("io.projectreactor.kotlin:reactor-kotlin-extensions")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-reactor")
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:$kotlinSerializationVersion")

    // Swagger
    implementation("io.springfox:springfox-boot-starter:3.0.0")

    implementation(project(":data-service-common"))
    implementation(project(":calc-service-model"))
    implementation(project(":calc-service-core"))
    implementation(project(":admin-service-model"))
    implementation(project(":admin-service-client"))
    implementation(project(":tax-content-service-model"))
    implementation(project(":tax-content-service-client"))

    // Audit
    implementation("org.springframework.kafka:spring-kafka")

    // Metrics
    implementation("io.micrometer:micrometer-registry-datadog")

    // Logging
    implementation("ch.qos.logback:logback-classic:$logbackVersion")
    implementation("org.slf4j:slf4j-api:$slf4jApiVersion")
    implementation("net.logstash.logback:logstash-logback-encoder:6.6")
    implementation("io.github.microutils:kotlin-logging:$kotlinLoggingVersion")

    testImplementation("org.junit.jupiter:junit-jupiter:$junitJupiterVersion")
    testImplementation("io.mockk:mockk:$mockkVersion")
}

application {
    // Define the main class for the application.
    mainClassName = "com.modios.taxsolution.CalcServiceAppKt"
}

tasks.test {
    useJUnitPlatform()
}

tasks.getByName<org.springframework.boot.gradle.tasks.bundling.BootBuildImage>("bootBuildImage") {
    imageName = "nexus.dev.modios.io/calc-service:${project.version}"
    environment = mapOf()
}
