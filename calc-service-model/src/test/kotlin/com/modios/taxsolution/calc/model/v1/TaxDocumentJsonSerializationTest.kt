package com.modios.taxsolution.calc.model.v1

// import com.fasterxml.jackson.annotation.JsonInclude
// import com.fasterxml.jackson.databind.ObjectMapper
// import com.fasterxml.jackson.databind.module.SimpleModule
// import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
// import com.fasterxml.jackson.module.kotlin.KotlinModule
// import com.modios.Constants
// import org.junit.jupiter.api.Assertions
// import org.junit.jupiter.api.Test
// import java.math.BigDecimal
// import java.time.LocalDateTime
//
// class TaxDocumentJsonSerializationTest {
//
//     @Test
//     fun test() {
//         val taxDocument = TaxDocument(
//             tenantName = "tenant1",
//             companyName = "company1",
//             persistResult = true,
//             documentDate = LocalDateTime.now(),
//             documentNumber = "ABC123",
//             shipFrom = TransactionLocation(
//                 country = "US",
//                 state = "CA"
//             ),
//             shipTo = TransactionLocation(
//                 country = "US",
//                 state = "FL"
//             )
//         ).also {
//             it.taxDocumentItems.add(
//                 TaxDocumentItem(
//                     index = 1,
//                     productCode = "foo",
//                     productMappingCode = null,
//                     quantity = 1,
//                     amount = BigDecimal.valueOf(100L).setScale(Constants.BIG_DECIMAL_SCALE)
//                 )
//             )
//         }
//
//         val objectMapper = ObjectMapper().also {
//             it.setSerializationInclusion(JsonInclude.Include.NON_NULL)
//             it.registerModule(KotlinModule.Builder().build())
//             it.registerModule(JavaTimeModule())
//             it.registerModule(
//                 SimpleModule().also {
//                     it.addSerializer(TaxDocument::class.java, TaxDocumentJsonSerializer())
//                     it.addSerializer(TransactionLocation::class.java, TransactionLocationSerializer())
//                     it.addSerializer(TaxDocumentItem::class.java, TaxDocumentItemJsonSerializer())
//                 }
//             )
//         }
//
//         val json = objectMapper.writeValueAsString(taxDocument)
//         Assertions.assertTrue(json.isNotEmpty())
//
//         val deserializedTaxDocument = objectMapper.readValue(json, TaxDocument::class.java)
//         Assertions.assertEquals(taxDocument.tenantName, deserializedTaxDocument.tenantName)
//         Assertions.assertEquals(taxDocument.taxDocumentItems.size, deserializedTaxDocument.taxDocumentItems.size)
//         Assertions.assertTrue(taxDocument.taxDocumentItems.first().amount.equals(deserializedTaxDocument.taxDocumentItems.first().amount))
//     }
// }
