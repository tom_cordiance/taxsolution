@file:UseSerializers(
    TaxDocumentKSerializer::class,
    TaxDocumentItemKSerializer::class,
    TransactionLocationKSerializer::class,
    BigDecimalKSerializer::class,
    LocalDateTimeKSerializer::class
)

package com.modios.taxsolution.calc.model.v1

import com.modios.Constants
import kotlinx.serialization.UseSerializers
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import java.math.BigDecimal
import java.time.LocalDateTime

class TaxDocumentJsonSerializationKotlinTest {

    // val jsonInstance = Json {
    //     serializersModule = SerializersModule {
    //         contextual(TaxDocument::class, TaxDocumentKSerializer())
    //         contextual(TaxDocumentItem::class, TaxDocumentItemKSerializer())
    //         contextual(TransactionLocation::class, TransactionLocationKSerializer())
    //         contextual(BigDecimal::class, BigDecimalKSerializer())
    //         contextual(LocalDateTime::class, LocalDateTimeKSerializer())
    //     }
    // }

    @Test
    fun test() {
        val taxDocument = TaxDocument(
            tenantName = "tenant1",
            companyName = "company1",
            persistResult = true,
            documentDate = LocalDateTime.now(),
            documentNumber = "ABC123",
            shipFrom = TransactionLocation(
                country = "US",
                state = "CA"
            ),
            shipTo = TransactionLocation(
                country = "US",
                state = "FL"
            )
        ).also {
            it.taxDocumentItems.add(
                TaxDocumentItem(
                    index = 1,
                    productCode = "foo",
                    productMappingCode = null,
                    quantity = 1,
                    amount = BigDecimal.valueOf(100L).setScale(Constants.BIG_DECIMAL_SCALE)
                )
            )
        }

        val json = Json.encodeToString(taxDocument)
        Assertions.assertTrue(json.isNotEmpty())

        val deserializedTaxDocument = Json.decodeFromString<TaxDocument>(json)
        Assertions.assertEquals(taxDocument.tenantName, deserializedTaxDocument.tenantName)
        Assertions.assertEquals(taxDocument.taxDocumentItems.size, deserializedTaxDocument.taxDocumentItems.size)
        Assertions.assertTrue(taxDocument.taxDocumentItems.first().amount.equals(deserializedTaxDocument.taxDocumentItems.first().amount))
    }
}
