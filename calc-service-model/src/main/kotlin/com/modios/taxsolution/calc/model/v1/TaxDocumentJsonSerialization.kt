package com.modios.taxsolution.calc.model.v1

// import com.fasterxml.jackson.core.JsonGenerator
// import com.fasterxml.jackson.core.JsonParser
// import com.fasterxml.jackson.databind.DeserializationContext
// import com.fasterxml.jackson.databind.JsonNode
// import com.fasterxml.jackson.databind.SerializerProvider
// import com.fasterxml.jackson.databind.deser.std.StdDeserializer
// import com.fasterxml.jackson.databind.ser.std.StdSerializer
// import com.modios.Constants
// import java.math.BigDecimal
// import java.time.LocalDateTime
// import java.time.format.DateTimeFormatter
// import kotlin.time.ExperimentalTime
//
// class TaxDocumentJsonSerializer : StdSerializer<TaxDocument>(TaxDocument::class.java) {
//
//     @OptIn(ExperimentalTime::class)
//     override fun serialize(
//         taxDocument: TaxDocument,
//         jsonGenerator: JsonGenerator,
//         serializerProvider: SerializerProvider
//     ) {
//         jsonGenerator.writeStartObject()
//
//         with(TaxDocumentFields) {
//             jsonGenerator.writeStringField(TENANT_NAME, taxDocument.tenantName)
//             jsonGenerator.writeStringField(COMPANY_NAME, taxDocument.companyName)
//             jsonGenerator.writeBooleanField(PERSIST_RESULT, taxDocument.persistResult)
//             jsonGenerator.writeObjectField(
//                 DOCUMENT_DATE,
//                 taxDocument.documentDate.format(DateTimeFormatter.ISO_DATE_TIME)
//             )
//             jsonGenerator.writeStringField(DOCUMENT_NUMBER, taxDocument.documentNumber)
//             jsonGenerator.writeObjectField(SHIP_FROM, taxDocument.shipFrom)
//             jsonGenerator.writeObjectField(SHIP_TO, taxDocument.shipTo)
//             jsonGenerator.writeObjectField(TAX_DOCUMENT_ITEMS, taxDocument.taxDocumentItems)
//         }
//
//         jsonGenerator.writeEndObject()
//     }
// }
//
// class TransactionLocationSerializer : StdSerializer<TransactionLocation>(TransactionLocation::class.java) {
//     override fun serialize(
//         transactionLocation: TransactionLocation,
//         jsonGenerator: JsonGenerator,
//         serializerProvider: SerializerProvider
//     ) {
//         jsonGenerator.writeStartObject()
//
//         with(TransactionLocationFields) {
//             jsonGenerator.writeStringField(COUNTRY, transactionLocation.country)
//             jsonGenerator.writeStringField(PROVINCE, transactionLocation.province)
//             jsonGenerator.writeStringField(STATE, transactionLocation.state)
//             jsonGenerator.writeStringField(COUNTY, transactionLocation.county)
//             jsonGenerator.writeStringField(CITY, transactionLocation.city)
//             jsonGenerator.writeStringField(DISTRICT, transactionLocation.district)
//             jsonGenerator.writeStringField(POSTCODE, transactionLocation.postCode)
//             jsonGenerator.writeStringField(GEOCODE, transactionLocation.geoCode)
//         }
//
//         jsonGenerator.writeEndObject()
//     }
// }
//
// class TaxDocumentItemJsonSerializer : StdSerializer<TaxDocumentItem>(TaxDocumentItem::class.java) {
//     override fun serialize(
//         taxDocumentItem: TaxDocumentItem,
//         jsonGenerator: JsonGenerator,
//         serializerProvider: SerializerProvider
//     ) {
//         jsonGenerator.writeStartObject()
//
//         with(TaxDocumentItemFields) {
//             jsonGenerator.writeNumberField(INDEX, taxDocumentItem.index)
//             jsonGenerator.writeStringField(PRODUCT_CODE, taxDocumentItem.productCode)
//             jsonGenerator.writeStringField(PRODUCT_MAPPING_CODE, taxDocumentItem.productMappingCode)
//             jsonGenerator.writeNumberField(QUANTITY, taxDocumentItem.quantity)
//             jsonGenerator.writeNumberField(AMOUNT, taxDocumentItem.amount)
//         }
//
//         jsonGenerator.writeEndObject()
//     }
// }
//
// class TaxDocumentJsonDeserializer : StdDeserializer<TaxDocument>(TaxDocument::class.java) {
//
//     private fun transactionLocation(jsonNode: JsonNode): TransactionLocation =
//         with(TransactionLocationFields) {
//             TransactionLocation(
//                 country = jsonNode.get(COUNTRY).textValue(),
//                 province = jsonNode.get(PROVINCE).textValue(),
//                 state = jsonNode.get(STATE).textValue(),
//                 county = jsonNode.get(COUNTY).textValue(),
//                 city = jsonNode.get(CITY).textValue(),
//                 district = jsonNode.get(DISTRICT).textValue(),
//                 postCode = jsonNode.get(POSTCODE).textValue(),
//                 geoCode = jsonNode.get(GEOCODE).textValue()
//             )
//         }
//
//     private fun taxDocumentItem(jsonNode: JsonNode): TaxDocumentItem =
//         with(TaxDocumentItemFields) {
//             TaxDocumentItem(
//                 index = jsonNode.get(INDEX).asInt(),
//                 productCode = jsonNode.get(PRODUCT_CODE).asText(),
//                 productMappingCode = jsonNode.get(PRODUCT_MAPPING_CODE).asText(),
//                 quantity = jsonNode.get(QUANTITY).asInt(),
//                 amount = BigDecimal.valueOf(jsonNode.get(AMOUNT).asDouble()).setScale(Constants.BIG_DECIMAL_SCALE)
//             )
//         }
//
//     @OptIn(ExperimentalTime::class)
//     override fun deserialize(jsonParser: JsonParser, deserializationContext: DeserializationContext): TaxDocument {
//         val node: JsonNode = jsonParser.codec.readTree(jsonParser)
//
//         return with(TaxDocumentFields) {
//             TaxDocument(
//                 tenantName = node.get(TENANT_NAME).asText(),
//                 companyName = node.get(COMPANY_NAME).asText(),
//                 persistResult = node.get(PERSIST_RESULT).asBoolean(),
//                 documentDate = LocalDateTime.parse(
//                     node.get(DOCUMENT_DATE).textValue(),
//                     DateTimeFormatter.ISO_DATE_TIME
//                 ),
//                 documentNumber = node.get(DOCUMENT_NUMBER).textValue(),
//                 shipFrom = node.get(SHIP_FROM).let { transactionLocation(it) },
//                 shipTo = node.get(SHIP_TO).let { transactionLocation(it) }
//             ).also {
//                 it.taxDocumentItems.addAll(node.get(TAX_DOCUMENT_ITEMS).map { taxDocumentItem(it) })
//             }
//         }
//     }
// }
