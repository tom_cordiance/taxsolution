package com.modios.taxsolution.calc.model.v1

import java.io.Serializable
import java.math.BigDecimal
import java.time.LocalDateTime

// @JsonSerialize(using = TaxDocumentJsonSerializer::class)
// @JsonDeserialize(using = TaxDocumentJsonDeserializer::class)
@kotlinx.serialization.Serializable // (with = TaxDocumentKSerializer::class)
data class TaxDocument(
    val tenantName: String = "",
    val companyName: String = "",
    val persistResult: Boolean = false,
    @kotlinx.serialization.Serializable(with = LocalDateTimeKSerializer::class)
    val documentDate: LocalDateTime = LocalDateTime.now(),
    val documentNumber: String = "",
    val shipFrom: TransactionLocation? = null,
    val shipTo: TransactionLocation? = null,
    val taxDocumentItems: MutableList<TaxDocumentItem> = mutableListOf()
) : Serializable

// @JsonSerialize(using = TaxDocumentItemJsonSerializer::class)
@kotlinx.serialization.Serializable // (with = TaxDocumentItemKSerializer::class)
data class TaxDocumentItem(
    val index: Int,
    val productCode: String? = null,
    val productMappingCode: String? = null,
    val quantity: Int,
    @kotlinx.serialization.Serializable(with = BigDecimalKSerializer::class)
    val amount: BigDecimal
)

/**
 * Group is intended to be a parent of countries, EU, Gulf Cooperation Council, etc
 */
enum class LocationClassification {
    GROUP,
    COUNTRY,
    PROVINCE,
    STATE,
    COUNTY,
    CITY,
    DISTRICT,
    POSTCODE,
    GEOCODE;
}

// @JsonSerialize(using = TransactionLocationSerializer::class)
@kotlinx.serialization.Serializable // (with = TransactionLocationKSerializer::class)
data class TransactionLocation(
    val country: String? = null,
    val province: String? = null,
    val state: String? = null,
    val county: String? = null,
    val city: String? = null,
    val district: String? = null,
    val postCode: String? = null,
    val geoCode: String? = null
) : Serializable {
    fun getAllLocationLevelsByClassification(): List<Pair<LocationClassification, String>> {
        return listOf(
            LocationClassification.COUNTRY to country,
            LocationClassification.PROVINCE to province,
            LocationClassification.STATE to state,
            LocationClassification.COUNTY to county,
            LocationClassification.CITY to city,
            LocationClassification.DISTRICT to district,
            LocationClassification.POSTCODE to postCode,
            LocationClassification.GEOCODE to geoCode
        )
            .filter { it.second != null }
            .map { it.first to it.second!! }
    }
}
