package com.modios.taxsolution.calc.model.v1

import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.KSerializer
import kotlinx.serialization.SerializationException
import kotlinx.serialization.Serializer
import kotlinx.serialization.builtins.ListSerializer
import kotlinx.serialization.builtins.nullable
import kotlinx.serialization.builtins.serializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.descriptors.buildClassSerialDescriptor
import kotlinx.serialization.descriptors.element
import kotlinx.serialization.encoding.CompositeDecoder.Companion.DECODE_DONE
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import kotlinx.serialization.encoding.decodeStructure
import kotlinx.serialization.encoding.encodeStructure
import java.math.BigDecimal
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

@OptIn(ExperimentalSerializationApi::class)
@Serializer(forClass = BigDecimal::class)
class BigDecimalKSerializer : KSerializer<BigDecimal> {
    override val descriptor: SerialDescriptor = PrimitiveSerialDescriptor("BigDecimal", PrimitiveKind.STRING)

    override fun deserialize(decoder: Decoder): BigDecimal {
        val text = decoder.decodeString()
        return BigDecimal(text)
    }

    override fun serialize(encoder: Encoder, value: BigDecimal) {
        encoder.encodeString(value.toPlainString())
    }
}

@OptIn(ExperimentalSerializationApi::class)
@Serializer(forClass = LocalDateTime::class)
class LocalDateTimeKSerializer : KSerializer<LocalDateTime> {
    override val descriptor: SerialDescriptor = PrimitiveSerialDescriptor("LocalDateTime", PrimitiveKind.STRING)

    override fun deserialize(decoder: Decoder): LocalDateTime {
        val text = decoder.decodeString()
        return LocalDateTime.parse(text, DateTimeFormatter.ISO_DATE_TIME)
    }

    override fun serialize(encoder: Encoder, value: LocalDateTime) {
        encoder.encodeString(value.format(DateTimeFormatter.ISO_DATE_TIME))
    }
}

@OptIn(ExperimentalSerializationApi::class)
@Serializer(forClass = MessageStatus::class)
class MessageStatusKSerializer : KSerializer<MessageStatus> {
    override fun deserialize(decoder: Decoder): MessageStatus {
        val text = decoder.decodeString()
        return MessageStatus.valueOf(text)
    }

    override val descriptor: SerialDescriptor = PrimitiveSerialDescriptor("MessageStatus", PrimitiveKind.STRING)

    override fun serialize(encoder: Encoder, value: MessageStatus) {
        encoder.encodeString(value.toString())
    }
}

@OptIn(ExperimentalSerializationApi::class)
@Serializer(forClass = TaxDocument::class)
class TaxDocumentKSerializer : KSerializer<TaxDocument> {
    override fun deserialize(decoder: Decoder): TaxDocument {
        return decoder.decodeStructure(descriptor) {
            var tenantName = ""
            var companyName = ""
            var persistResult = false
            var documentDate = LocalDateTime.now()
            var documentNumber = ""
            var shipFrom: TransactionLocation? = null
            var shipTo: TransactionLocation? = null
            var taxDocumentItems: List<TaxDocumentItem> = listOf()

            loop@ while (true) {
                when (val i = decodeElementIndex(descriptor)) {
                    DECODE_DONE -> break@loop

                    0 -> tenantName = decodeStringElement(descriptor, 0)
                    1 -> companyName = decodeStringElement(descriptor, 1)
                    2 -> persistResult = decodeBooleanElement(descriptor, 2)
                    3 -> documentDate = decodeSerializableElement(descriptor, 3, LocalDateTimeKSerializer())
                    4 -> documentNumber = decodeStringElement(descriptor, 4)
                    5 ->
                        shipFrom =
                            decodeNullableSerializableElement(descriptor, 5, TransactionLocationKSerializer().nullable)
                    6 ->
                        shipTo =
                            decodeNullableSerializableElement(descriptor, 6, TransactionLocationKSerializer().nullable)
                    7 ->
                        taxDocumentItems =
                            decodeSerializableElement(descriptor, 7, ListSerializer(TaxDocumentItemKSerializer()))

                    else -> throw SerializationException("Unexpected index deserializing ${this.javaClass} $i")
                }
            }

            TaxDocument(
                tenantName = tenantName,
                companyName = companyName,
                persistResult = persistResult,
                documentDate = documentDate,
                documentNumber = documentNumber,
                shipFrom = shipFrom,
                shipTo = shipTo,
                taxDocumentItems = taxDocumentItems.toMutableList()
            )
        }
    }

    override val descriptor: SerialDescriptor = buildClassSerialDescriptor("TaxDocument") {
        element<String>(TaxDocumentFields.TENANT_NAME)
        element<String>(TaxDocumentFields.COMPANY_NAME)
        element<Boolean>(TaxDocumentFields.PERSIST_RESULT)
        element<LocalDateTime>(TaxDocumentFields.DOCUMENT_DATE)
        element<String>(TaxDocumentFields.DOCUMENT_NUMBER)
        element<TransactionLocation>(TaxDocumentFields.SHIP_FROM)
        element<TransactionLocation>(TaxDocumentFields.SHIP_TO)
        element<List<TaxDocumentItem>>(TaxDocumentFields.TAX_DOCUMENT_ITEMS)
    }

    override fun serialize(encoder: Encoder, value: TaxDocument) {
        encoder.encodeStructure(descriptor) {
            encodeStringElement(descriptor, 0, value.tenantName)
            encodeStringElement(descriptor, 1, value.companyName)
            encodeBooleanElement(descriptor, 2, value.persistResult)
            encodeSerializableElement(descriptor, 3, LocalDateTimeKSerializer(), value.documentDate)
            encodeStringElement(descriptor, 4, value.documentNumber)
            encodeNullableSerializableElement(descriptor, 5, TransactionLocationKSerializer(), value.shipFrom)
            encodeNullableSerializableElement(descriptor, 6, TransactionLocationKSerializer(), value.shipTo)
            encodeSerializableElement(
                descriptor,
                4,
                ListSerializer(TaxDocumentItemKSerializer()),
                value.taxDocumentItems.toList()
            )
        }
    }
}

@OptIn(ExperimentalSerializationApi::class)
@Serializer(forClass = TransactionLocation::class)
class TransactionLocationKSerializer : KSerializer<TransactionLocation> {
    override fun deserialize(decoder: Decoder): TransactionLocation {
        return decoder.decodeStructure(descriptor) {
            var country: String? = null
            var province: String? = null
            var state: String? = null
            var county: String? = null
            var city: String? = null
            var district: String? = null
            var postCode: String? = null
            var geoCode: String? = null

            loop@ while (true) {
                when (val index = decodeElementIndex(descriptor)) {
                    DECODE_DONE -> break@loop

                    0 -> country = decodeNullableSerializableElement(descriptor, 0, String.serializer().nullable)
                    1 -> province = decodeNullableSerializableElement(descriptor, 1, String.serializer().nullable)
                    2 -> state = decodeNullableSerializableElement(descriptor, 2, String.serializer().nullable)
                    3 -> county = decodeNullableSerializableElement(descriptor, 3, String.serializer().nullable)
                    4 -> city = decodeNullableSerializableElement(descriptor, 4, String.serializer().nullable)
                    5 -> district = decodeNullableSerializableElement(descriptor, 5, String.serializer().nullable)
                    6 -> postCode = decodeNullableSerializableElement(descriptor, 6, String.serializer().nullable)
                    7 -> geoCode = decodeNullableSerializableElement(descriptor, 7, String.serializer().nullable)

                    else -> throw SerializationException("Unexpected index deserializing ${this.javaClass} $index")
                }
            }

            TransactionLocation(
                country = country,
                province = province,
                state = state,
                county = county,
                city = city,
                district = district,
                postCode = postCode,
                geoCode = geoCode
            )
        }
    }

    override val descriptor: SerialDescriptor = buildClassSerialDescriptor("TransactionLocation") {
        element<String?>(TransactionLocationFields.COUNTRY, isOptional = true)
        element<String?>(TransactionLocationFields.PROVINCE, isOptional = true)
        element<String?>(TransactionLocationFields.STATE, isOptional = true)
        element<String?>(TransactionLocationFields.COUNTY, isOptional = true)
        element<String?>(TransactionLocationFields.CITY, isOptional = true)
        element<String?>(TransactionLocationFields.DISTRICT, isOptional = true)
        element<String?>(TransactionLocationFields.POSTCODE, isOptional = true)
        element<String?>(TransactionLocationFields.GEOCODE, isOptional = true)
    }

    override fun serialize(encoder: Encoder, value: TransactionLocation) {
        encoder.encodeStructure(descriptor) {
            encodeNullableSerializableElement(descriptor, 0, String.serializer(), value.country)
            encodeNullableSerializableElement(descriptor, 1, String.serializer(), value.state)
            encodeNullableSerializableElement(descriptor, 2, String.serializer(), value.county)
            encodeNullableSerializableElement(descriptor, 3, String.serializer(), value.city)
            encodeNullableSerializableElement(descriptor, 4, String.serializer(), value.district)
            encodeNullableSerializableElement(descriptor, 5, String.serializer(), value.postCode)
            encodeNullableSerializableElement(descriptor, 6, String.serializer(), value.geoCode)
        }
    }
}

@OptIn(ExperimentalSerializationApi::class)
@Serializer(forClass = TaxDocumentItem::class)
class TaxDocumentItemKSerializer : KSerializer<TaxDocumentItem> {
    override fun deserialize(decoder: Decoder): TaxDocumentItem {
        return decoder.decodeStructure(descriptor) {
            var index = 0
            var productCode: String? = null
            var productMappingCode: String? = null
            var quantity = 0
            var amount = BigDecimal.ZERO

            loop@ while (true) {
                when (val i = decodeElementIndex(descriptor)) {
                    DECODE_DONE -> break@loop

                    0 -> index = decodeIntElement(descriptor, 0)
                    1 -> productCode = decodeNullableSerializableElement(descriptor, 1, String.serializer().nullable)
                    2 ->
                        productMappingCode =
                            decodeNullableSerializableElement(descriptor, 2, String.serializer().nullable)
                    3 -> quantity = decodeIntElement(descriptor, 3)
                    4 -> amount = decodeSerializableElement(descriptor, 4, BigDecimalKSerializer())

                    else -> throw SerializationException("Unexpected index deserializing ${this.javaClass} $i")
                }
            }

            TaxDocumentItem(
                index = index,
                productCode = productCode,
                productMappingCode = productMappingCode,
                quantity = quantity,
                amount = amount
            )
        }
    }

    override val descriptor: SerialDescriptor = buildClassSerialDescriptor("TaxDocumentItem") {
        element<Int>(TaxDocumentItemFields.INDEX)
        element<String?>(TaxDocumentItemFields.PRODUCT_CODE, isOptional = true)
        element<String?>(TaxDocumentItemFields.PRODUCT_MAPPING_CODE, isOptional = true)
        element<Int>(TaxDocumentItemFields.QUANTITY)
        element<BigDecimal>(TaxDocumentItemFields.AMOUNT)
    }

    override fun serialize(encoder: Encoder, value: TaxDocumentItem) {
        encoder.encodeStructure(descriptor) {
            encodeIntElement(descriptor, 0, value.index)
            encodeNullableSerializableElement(descriptor, 1, String.serializer(), value.productCode)
            encodeNullableSerializableElement(descriptor, 2, String.serializer(), value.productMappingCode)
            encodeIntElement(descriptor, 3, value.quantity)
            encodeSerializableElement(descriptor, 4, BigDecimalKSerializer(), value.amount)
        }
    }
}

@OptIn(ExperimentalSerializationApi::class)
@Serializer(forClass = TaxDocumentResult::class)
class TaxDocumentResultKSerializer : KSerializer<TaxDocumentResult> {
    override fun deserialize(decoder: Decoder): TaxDocumentResult {
        return decoder.decodeStructure(descriptor) {
            var originalTaxDocument = TaxDocument()
            var documentNumber = ""
            var calculationDate = LocalDateTime.now()
            var success = true
            var resultPersisted = false
            var tenantName = ""
            var companyName = ""
            var shipFromTransactionLocation: TransactionLocation? = null
            var shipToTransactionLocation: TransactionLocation? = null
            var taxDocumentResultMessages: List<TaxDocumentResultMessage> = emptyList()
            var taxDocumentItemResults: List<TaxDocumentItemResult> = emptyList()

            loop@ while (true) {
                when (val i = decodeElementIndex(descriptor)) {
                    DECODE_DONE -> break@loop

                    0 -> originalTaxDocument = decodeSerializableElement(descriptor, 0, TaxDocumentKSerializer())
                    1 -> documentNumber = decodeStringElement(descriptor, 1)
                    2 -> calculationDate = decodeSerializableElement(descriptor, 2, LocalDateTimeKSerializer())
                    3 -> success = decodeBooleanElement(descriptor, 3)
                    4 -> resultPersisted = decodeBooleanElement(descriptor, 4)
                    5 -> tenantName = decodeStringElement(descriptor, 5)
                    6 -> companyName = decodeStringElement(descriptor, 6)
                    7 ->
                        shipFromTransactionLocation =
                            decodeNullableSerializableElement(descriptor, 7, TransactionLocationKSerializer().nullable)
                    8 ->
                        shipToTransactionLocation =
                            decodeNullableSerializableElement(descriptor, 8, TransactionLocationKSerializer().nullable)
                    9 ->
                        taxDocumentResultMessages =
                            decodeSerializableElement(descriptor, 9, ListSerializer(TaxDocumentResultMessageKSerializer()))
                    10 ->
                        taxDocumentItemResults =
                            decodeSerializableElement(descriptor, 9, ListSerializer(TaxDocumentItemResultKSerializer()))

                    else -> throw SerializationException("Unexpected index deserializing ${this.javaClass} $i")
                }
            }

            TaxDocumentResult(
                originalTaxDocument = originalTaxDocument,
                documentNumber = documentNumber,
                calculationDate = calculationDate,
                success = success,
                resultPersisted = resultPersisted,
                tenantName = tenantName,
                companyName = companyName,
                shipFromTransactionLocation = shipFromTransactionLocation,
                shipToTransactionLocation = shipToTransactionLocation,
                taxDocumentResultMessages = taxDocumentResultMessages.toMutableList(),
                taxDocumentItemResults = taxDocumentItemResults.toMutableList()
            )
        }
    }

    override val descriptor: SerialDescriptor = buildClassSerialDescriptor("TaxDocumentResult") {
        with(TaxDocumentResultFields) {
            element<TaxDocument>(ORIGINAL_TAX_DOCUMENT)
            element<String>(DOCUMENT_NUMBER)
            element<LocalDateTime>(CALCULATION_DATE)
            element<Boolean>(SUCCESS)
            element<Boolean>(RESULT_PERSISTED)
            element<String>(TENANT_NAME)
            element<String>(COMPANY_NAME)
            element<TransactionLocation>(SHIP_FROM_TRANSACTION_LOCATION)
            element<TransactionLocation>(SHIP_TO_TRANSACTION_LOCATION)
            element<List<TaxDocumentResultMessage>>(TAX_DOCUMENT_RESULT_MESSAGES)
            element<List<TaxDocumentItemResult>>(TAX_DOCUMENT_ITEM_RESULTS)
        }
    }

    override fun serialize(encoder: Encoder, value: TaxDocumentResult) {
        encoder.encodeStructure(descriptor) {
            encodeSerializableElement(descriptor, 0, TaxDocumentKSerializer(), value.originalTaxDocument)
            encodeStringElement(descriptor, 1, value.documentNumber)
            encodeSerializableElement(descriptor, 2, LocalDateTimeKSerializer(), value.calculationDate)
            encodeBooleanElement(descriptor, 3, value.success)
            encodeBooleanElement(descriptor, 4, value.resultPersisted)
            encodeStringElement(descriptor, 5, value.tenantName)
            encodeStringElement(descriptor, 6, value.companyName)
            encodeNullableSerializableElement(
                descriptor,
                7,
                TransactionLocationKSerializer(),
                value.shipFromTransactionLocation
            )
            encodeNullableSerializableElement(
                descriptor,
                8,
                TransactionLocationKSerializer(),
                value.shipToTransactionLocation
            )
            encodeSerializableElement(
                descriptor,
                9,
                ListSerializer(TaxDocumentResultMessageKSerializer()),
                value.taxDocumentResultMessages
            )
            encodeSerializableElement(
                descriptor,
                10,
                ListSerializer(TaxDocumentItemResultKSerializer()),
                value.taxDocumentItemResults
            )
        }
    }
}

@OptIn(ExperimentalSerializationApi::class)
@Serializer(forClass = TaxDocumentItemResult::class)
class TaxDocumentItemResultKSerializer : KSerializer<TaxDocumentItemResult> {
    override fun deserialize(decoder: Decoder): TaxDocumentItemResult {
        return decoder.decodeStructure(descriptor) {
            var index = 0
            var taxDocumentResultMessages: List<TaxDocumentResultMessage> = emptyList()
            var taxResults: List<TaxDocumentItemTaxResult> = emptyList()
            var taxAmount = BigDecimal.ZERO

            loop@ while (true) {
                when (val i = decodeElementIndex(descriptor)) {
                    DECODE_DONE -> break@loop

                    0 -> index = decodeIntElement(descriptor, 0)
                    1 ->
                        taxDocumentResultMessages =
                            decodeSerializableElement(descriptor, 1, ListSerializer(TaxDocumentResultMessageKSerializer()))
                    2 ->
                        taxResults =
                            decodeSerializableElement(descriptor, 1, ListSerializer(TaxDocumentItemTaxResultKSerializer))
                    3 -> taxAmount = decodeSerializableElement(descriptor, 2, BigDecimalKSerializer())

                    else -> throw SerializationException("Unexpected index deserializing ${this.javaClass} $i")
                }
            }

            TaxDocumentItemResult(
                index = index,
                taxDocumentResultMessages = taxDocumentResultMessages.toMutableList(),
                taxResults = taxResults.toMutableList(),
                taxAmount = taxAmount
            )
        }
    }

    override val descriptor: SerialDescriptor = buildClassSerialDescriptor("TaxDocumentItemResult") {
        with(TaxDocumentItemResultFields) {
            element<String>(INDEX)
            element<List<TaxDocumentResultMessage>>(TAX_DOCUMENT_RESULT_MESSAGES)
            element<List<TaxDocumentItemTaxResult>>(TAX_RESULTS)
            element<BigDecimal>(TAX_AMOUNT)
        }
    }

    override fun serialize(encoder: Encoder, value: TaxDocumentItemResult) {
        encoder.encodeStructure(descriptor) {
            encodeIntElement(descriptor, 0, value.index)
            encodeSerializableElement(
                descriptor,
                1,
                ListSerializer(TaxDocumentResultMessageKSerializer()),
                value.taxDocumentResultMessages
            )
            encodeSerializableElement(
                descriptor,
                2,
                ListSerializer(TaxDocumentItemTaxResultKSerializer),
                value.taxResults
            )
            encodeSerializableElement(descriptor, 3, BigDecimalKSerializer(), value.taxAmount)
        }
    }
}

@OptIn(ExperimentalSerializationApi::class)
@Serializer(forClass = TaxDocumentItemTaxResult::class)
object TaxDocumentItemTaxResultKSerializer : KSerializer<TaxDocumentItemTaxResult> {
    override fun deserialize(decoder: Decoder): TaxDocumentItemTaxResult {
        return decoder.decodeStructure(descriptor) {
            var authorityName = ""
            var taxRate = BigDecimal.ZERO
            var taxAmount = BigDecimal.ZERO

            loop@ while (true) {
                when (val i = decodeElementIndex(descriptor)) {
                    DECODE_DONE -> break@loop

                    0 -> authorityName = decodeStringElement(descriptor, 0)
                    1 -> taxRate = decodeSerializableElement(descriptor, 1, BigDecimalKSerializer())
                    2 -> taxAmount = decodeSerializableElement(descriptor, 2, BigDecimalKSerializer())

                    else -> throw SerializationException("Unexpected index deserializing ${this.javaClass} $i")
                }
            }

            TaxDocumentItemTaxResult(
                authorityName = authorityName,
                taxRate = taxRate,
                taxAmount = taxAmount
            )
        }
    }

    override val descriptor: SerialDescriptor = buildClassSerialDescriptor("TaxDocumentItemTaxResult") {
        element<String>(TaxDocumentItemTaxResultFields.AUTHORITY_NAME)
        element<BigDecimal>(TaxDocumentItemTaxResultFields.TAX_RATE)
        element<BigDecimal>(TaxDocumentItemTaxResultFields.TAX_AMOUNT)
    }

    override fun serialize(encoder: Encoder, value: TaxDocumentItemTaxResult) {
        encoder.encodeStructure(descriptor) {
            encodeStringElement(descriptor, 0, value.authorityName)
            encodeSerializableElement(descriptor, 1, BigDecimalKSerializer(), value.taxRate)
            encodeSerializableElement(descriptor, 2, BigDecimalKSerializer(), value.taxAmount)
        }
    }
}

@OptIn(ExperimentalSerializationApi::class)
@Serializer(forClass = TaxDocumentResultMessage::class)
class TaxDocumentResultMessageKSerializer : KSerializer<TaxDocumentResultMessage> {
    override fun deserialize(decoder: Decoder): TaxDocumentResultMessage {
        return decoder.decodeStructure(descriptor) {
            var status = MessageStatus.INFO
            var message = ""

            loop@ while (true) {
                when (val i = decodeElementIndex(descriptor)) {
                    DECODE_DONE -> break@loop

                    0 -> status = decodeSerializableElement(descriptor, 0, MessageStatusKSerializer())
                    1 -> message = decodeStringElement(descriptor, 1)

                    else -> throw SerializationException("Unexpected index deserializing ${this.javaClass} $i")
                }
            }

            TaxDocumentResultMessage(
                status = status,
                message = message
            )
        }
    }

    override val descriptor: SerialDescriptor = buildClassSerialDescriptor("TaxDocumentResultMessage") {
        element<MessageStatus>(TaxDocumentResultMesageFields.STATUS)
        element<String>(TaxDocumentResultMesageFields.MESSAGE)
    }

    override fun serialize(encoder: Encoder, value: TaxDocumentResultMessage) {
        encoder.encodeStructure(descriptor) {
            encodeSerializableElement(descriptor, 0, MessageStatusKSerializer(), value.status)
            encodeStringElement(descriptor, 1, value.message)
        }
    }
}
