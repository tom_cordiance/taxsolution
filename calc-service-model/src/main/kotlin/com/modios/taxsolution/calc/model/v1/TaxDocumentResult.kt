package com.modios.taxsolution.calc.model.v1

import java.io.Serializable
import java.math.BigDecimal
import java.time.LocalDateTime

@kotlinx.serialization.Serializable
data class TaxDocumentResultKey(
    @kotlinx.serialization.Serializable(with = LocalDateTimeKSerializer::class)
    val timestamp: LocalDateTime,
    val tenantName: String,
    val companyName: String,
    val documentNumber: String
)

@kotlinx.serialization.Serializable
data class TaxDocumentResult(
    val originalTaxDocument: TaxDocument,
    val documentNumber: String,
    @kotlinx.serialization.Serializable(with = LocalDateTimeKSerializer::class)
    val calculationDate: LocalDateTime = LocalDateTime.now(),
    val success: Boolean = true,
    val resultPersisted: Boolean = false,
    val tenantName: String,
    val companyName: String,
    val shipFromTransactionLocation: TransactionLocation? = null,
    val shipToTransactionLocation: TransactionLocation? = null,
    val taxDocumentResultMessages: List<TaxDocumentResultMessage> = emptyList(),
    val taxDocumentItemResults: List<TaxDocumentItemResult> = emptyList()
) : Serializable {
    companion object {
        fun addMessage(
            taxDocumentResult: TaxDocumentResult,
            taxDocumentResultMessage: TaxDocumentResultMessage
        ): TaxDocumentResult {
            return taxDocumentResult.copy(
                taxDocumentResultMessages = mutableListOf<TaxDocumentResultMessage>().apply {
                    this.addAll(taxDocumentResult.taxDocumentResultMessages)
                    this.add(taxDocumentResultMessage)
                }
            )
        }
    }
}

@kotlinx.serialization.Serializable
data class TaxDocumentItemResult(
    val index: Int,
    val taxDocumentResultMessages: List<TaxDocumentResultMessage>,
    val taxResults: List<TaxDocumentItemTaxResult>,
    @kotlinx.serialization.Serializable(with = BigDecimalKSerializer::class)
    val taxAmount: BigDecimal
) : Serializable

@kotlinx.serialization.Serializable
data class TaxDocumentItemTaxResult(
    val authorityName: String,
    @kotlinx.serialization.Serializable(with = BigDecimalKSerializer::class)
    val taxRate: BigDecimal,
    @kotlinx.serialization.Serializable(with = BigDecimalKSerializer::class)
    val taxAmount: BigDecimal
) : Serializable

@kotlinx.serialization.Serializable
enum class MessageStatus {
    DEBUG,
    INFO,
    WARNING,
    ERROR
}

@kotlinx.serialization.Serializable
data class TaxDocumentResultMessage(val status: MessageStatus, val message: String) : Serializable
