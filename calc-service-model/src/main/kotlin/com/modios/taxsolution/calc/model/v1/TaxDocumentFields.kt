package com.modios.taxsolution.calc.model.v1

object TaxDocumentFields {
    const val TENANT_NAME = "tenantName"
    const val COMPANY_NAME = "companyName"
    const val PERSIST_RESULT = "persistResult"
    const val DOCUMENT_DATE = "documentDate"
    const val DOCUMENT_NUMBER = "documentNumber"
    const val SHIP_FROM = "shipFrom"
    const val SHIP_TO = "shipTo"
    const val TAX_DOCUMENT_ITEMS = "taxDocumentItems"
}

object TransactionLocationFields {
    const val COUNTRY = "country"
    const val PROVINCE = "province"
    const val STATE = "state"
    const val COUNTY = "county"
    const val CITY = "city"
    const val DISTRICT = "district"
    const val POSTCODE = "postCode"
    const val GEOCODE = "geoCode"
}

object TaxDocumentItemFields {
    const val INDEX = "index"
    const val PRODUCT_CODE = "productCode"
    const val PRODUCT_MAPPING_CODE = "productMappingCode"
    const val QUANTITY = "quantity"
    const val AMOUNT = "amount"
}

object TaxDocumentResultFields {
    const val ORIGINAL_TAX_DOCUMENT = "originalTaxDocument"
    const val DOCUMENT_NUMBER = "documentNumber"
    const val CALCULATION_DATE = "calculationDate"
    const val SUCCESS = "success"
    const val RESULT_PERSISTED = "resultPersisted"
    const val TENANT_NAME = "tenantName"
    const val COMPANY_NAME = "companyName"
    const val SHIP_FROM_TRANSACTION_LOCATION = "shipFromTransactionLocation"
    const val SHIP_TO_TRANSACTION_LOCATION = "shipToTransactionLocation"
    const val TAX_DOCUMENT_RESULT_MESSAGES = "taxDocumentResultMessages"
    const val TAX_DOCUMENT_ITEM_RESULTS = "taxDocumentItemResults"
}

object TaxDocumentResultMesageFields {
    const val STATUS = "status"
    const val MESSAGE = "message"
}

object TaxDocumentItemResultFields {
    const val INDEX = "index"
    const val TAX_DOCUMENT_RESULT_MESSAGES = "taxDocumentResultMessages"
    const val TAX_RESULTS = "taxResults"
    const val TAX_AMOUNT = "taxAmount"
}

object TaxDocumentItemTaxResultFields {
    const val AUTHORITY_NAME = "authorityName"
    const val TAX_RATE = "taxRate"
    const val TAX_AMOUNT = "taxAmount"
}
