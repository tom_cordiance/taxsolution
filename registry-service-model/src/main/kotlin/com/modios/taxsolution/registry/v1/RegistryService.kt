package com.modios.taxsolution.registry.v1

import com.modios.taxsolution.registry.model.v1.IdBlock
import java.util.UUID

interface RegistryService {

    /**
     * Need a way to identify valid service instances
     *
     * @param serviceName - the name of the service
     */
    suspend fun registerService(serviceName: String): UUID

    /**
     * Allows a service to reserve a block of IDs.  These are permanently reserved and unavailable to other callers.
     *
     * @param serviceUUID the key of the registered service
     * @param blockName the name of the counter, i.e. tablename
     * @param blockSize the size of the block
     */
    suspend fun reserveIdBlock(serviceUUID: UUID, blockName: String, blockSize: Long): IdBlock

    /**
     * Allow a reserved ID block to be able to be released and reused
     */
    suspend fun releaseIdBlock(serviceKey: UUID, blockUUID: UUID)
}

/**
 * Thrown by any of the registry methods if the service has not been registered
 */
class ServiceNotRegisteredException(serviceName: String) : RuntimeException("The service $serviceName has not been registered")
