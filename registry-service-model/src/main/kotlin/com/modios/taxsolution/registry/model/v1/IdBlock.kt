package com.modios.taxsolution.registry.model.v1

import java.util.UUID

/**
 * A 1000 id block would have a blockStart of 501 and a blockEnd of 1500
 *
 * @param serviceKey key of the registered service
 * @param blockUUID unique identifier of the block reserved
 * @param name the name of the block, i.e. tablename
 * @param blockStart the starting ID of the block
 * @param blockEnd the ending ID, inclusive, of the block
 */
data class IdBlock(val serviceKey: UUID, val blockUUID: UUID, val name: String, val blockStart: Long, val blockEnd: Long)
