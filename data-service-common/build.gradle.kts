plugins {
    id("java-library")
    kotlin("plugin.serialization") apply true
}

val logbackVersion: String by project
val slf4jApiVersion: String by project
val kotlinLoggingVersion: String by project
val junitJupiterVersion: String by project
val bugSnagVersion: String by project
val mockkVersion: String by project
val kotlinCoroutineVersion: String by project
val kotlinSerializationVersion: String by project

dependencies {
    implementation("org.springframework.data:spring-data-commons:2.5.5")

    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:$kotlinSerializationVersion")

    // Logging
    implementation("ch.qos.logback:logback-classic:$logbackVersion")
    implementation("org.slf4j:slf4j-api:$slf4jApiVersion")
    implementation("io.github.microutils:kotlin-logging:$kotlinLoggingVersion")
    implementation("com.bugsnag:bugsnag:$bugSnagVersion")

    // Testing
    testImplementation("org.junit.jupiter:junit-jupiter:$junitJupiterVersion")
    testImplementation("io.mockk:mockk:$mockkVersion")

    testImplementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$kotlinCoroutineVersion")
}

tasks.test {
    useJUnitPlatform()
}

tasks.getByName<Jar>("jar") {
    enabled = true
}
