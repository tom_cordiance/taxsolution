package com.modios.service

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class IdGeneratorTest {

    @Test
    fun testNodeId() {
        val nodeId = IdGenerator.getNodeId()
        Assertions.assertTrue(nodeId != 0)

        (0..10).forEach { Assertions.assertEquals(nodeId, IdGenerator.getNodeId()) }
    }

    @Test
    fun testGetNextId() {
        val idGenerator = IdGenerator.instance

        Assertions.assertTrue(idGenerator.getNextId() != 0L)

        val idSet = mutableSetOf<Long>()
        (0..100000).forEach {
            val id = idGenerator.getNextId()
            val unique = idSet.add(id)
            Assertions.assertTrue(unique)
        }
    }

    fun getNextId(): Deferred<Long> {
        return CoroutineScope(Dispatchers.Default).async {
            IdGenerator.instance.getNextId()
        }
    }

    @Test
    fun testGetNextIdThreaded() = runBlocking {
        val idSet = mutableSetOf<Long>()
        (0..100000).forEach {
            val id = getNextId().await()
            val unique = idSet.add(id)
            Assertions.assertTrue(unique)
        }
    }
}
