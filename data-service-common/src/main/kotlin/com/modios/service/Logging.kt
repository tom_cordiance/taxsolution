package com.modios.service

import mu.KLogger
import mu.KotlinLogging
import mu.withLoggingContext

inline fun <reified T> logWithDebug(
    logger: KLogger = KotlinLogging.logger { },
//    bugsnag: Bugsnag? = null,
    vararg loggingContext: Pair<String, String> = emptyArray(),
    message: String,
    block: () -> T
): T {
    withLoggingContext(*loggingContext) {
        try {
            logger.debug { message }

            return block()
        } catch (throwable: Throwable) {
//            bugsnag?.notify(throwable)

            logger.error(throwable) { "'$message' generated an exception" }

            throw throwable
        }
    }
}
