package org.lpj.some

import org.lpj.some.collection.TrieMap
import kotlin.test.Test
import kotlin.test.assertEquals

class SingleInsertTest {

    @Test
    fun test() {
        val trieMap = TrieMap<Any, Any>()

        val key = "foo"
        val value = "bar"
        trieMap.put(key, value)

        val fetchedValue = trieMap.get("foo")

        assertEquals(value, fetchedValue)
    }
}
