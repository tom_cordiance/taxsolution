package com.modios.taxsolution.config

import org.springframework.beans.factory.BeanClassLoaderAware
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration
import org.springframework.core.io.ClassPathResource
import org.springframework.data.cassandra.config.AbstractCassandraConfiguration
import org.springframework.data.cassandra.core.cql.keyspace.CreateKeyspaceSpecification
import org.springframework.data.cassandra.core.cql.keyspace.KeyspaceOption
import org.springframework.data.cassandra.core.cql.session.init.KeyspacePopulator
import org.springframework.data.cassandra.core.cql.session.init.ResourceKeyspacePopulator
import org.springframework.data.cassandra.repository.config.EnableCassandraRepositories

@Configuration
@EnableCassandraRepositories(basePackages = ["com.modios.taxsolution.data"])
class CassandraConfiguration(
    @Value("\${spring.data.cassandra.contact-points}") private val cassandraServers: String,
    @Value("\${spring.data.cassandra.port}") private val cassadraPort: Int
) : AbstractCassandraConfiguration(), BeanClassLoaderAware {
    override fun getContactPoints(): String = cassandraServers

    override fun getPort(): Int = cassadraPort

    override fun getKeyspaceName(): String = KEYSPACE_NAME

    override fun getKeyspaceCreations(): MutableList<CreateKeyspaceSpecification> =
        mutableListOf(
            CreateKeyspaceSpecification
                .createKeyspace(KEYSPACE_NAME)
                .ifNotExists()
                .with(KeyspaceOption.DURABLE_WRITES, true)
                .withSimpleReplication()
        )

    override fun keyspacePopulator(): KeyspacePopulator =
        ResourceKeyspacePopulator(ClassPathResource("cassandra/taxsolution-schema.cql"))

    companion object {
        const val KEYSPACE_NAME = "taxsolution"
    }
}
