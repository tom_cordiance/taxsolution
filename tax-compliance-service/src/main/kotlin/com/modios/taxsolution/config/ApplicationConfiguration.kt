package com.modios.taxsolution.config

import com.modios.taxsolution.taxcompliance.modules.TaxAuthorityResultModule
import com.modios.taxsolution.taxcompliance.modules.TaxDocumentItemResultModule
import com.modios.taxsolution.taxcompliance.modules.TaxDocumentItemTaxResultModule
import com.modios.taxsolution.taxcompliance.modules.TaxDocumentResultModule
import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.flow.flowOf
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.util.concurrent.Executors

@Configuration
class ApplicationConfiguration {

    @Autowired
    private lateinit var taxDocumentResultModule: TaxDocumentResultModule

    @Autowired
    private lateinit var taxDocumentItemResultModule: TaxDocumentItemResultModule

    @Autowired
    private lateinit var taxDocumentItemTaxResultModule: TaxDocumentItemTaxResultModule

    @Autowired
    private lateinit var taxAuthorityResultModule: TaxAuthorityResultModule

    @Bean
    fun taxResultModules() =
        flowOf(
            taxDocumentResultModule,
            taxDocumentItemResultModule,
            taxDocumentItemTaxResultModule,
            taxAuthorityResultModule
        )

    @Bean
    fun taxResultDispatcher() = Executors.newFixedThreadPool(16).asCoroutineDispatcher()
}
