package com.modios.taxsolution

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class TaxComplianceServiceApp

fun main(args: Array<String>) {
    SpringApplication.run(TaxComplianceServiceApp::class.java, *args)
}
