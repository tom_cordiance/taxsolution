package com.modios.taxsolution.taxcompliance.modules

import com.modios.taxsolution.calc.model.v1.TaxDocumentResult
import com.modios.taxsolution.data.TaxDocumentItemResultRepository
import com.modios.taxsolution.data.entity.TaxDocumentItemResultEntity
import com.modios.taxsolution.data.entity.TaxDocumentItemResultPrimaryKey
import org.springframework.stereotype.Component

@Component
class TaxDocumentItemResultModule(private val taxDocumentItemResultRepository: TaxDocumentItemResultRepository) :
    TaxResultModule {
    override suspend fun process(taxDocumentResult: TaxDocumentResult) {
        taxDocumentResult.toTaxDocumentItemResultEntities().forEach {
            taxDocumentItemResultRepository.save(it)
        }
    }
}

private fun TaxDocumentResult.toTaxDocumentItemResultEntities() =
    this.taxDocumentItemResults.map {
        val primaryKey = TaxDocumentItemResultPrimaryKey(
            tenantName = this.tenantName,
            companyName = this.companyName,
            documentNumber = this.documentNumber,
            documentDate = this.calculationDate,
            itemIndex = it.index
        )

        TaxDocumentItemResultEntity(
            primaryKey = primaryKey,
            shipFromCountry = this.shipFromTransactionLocation?.country.orEmpty(),
            shipFromProvince = this.shipFromTransactionLocation?.province.orEmpty(),
            shipFromState = this.shipFromTransactionLocation?.state.orEmpty(),
            shipToCountry = this.shipToTransactionLocation?.country.orEmpty(),
            shipToProvince = this.shipToTransactionLocation?.province.orEmpty(),
            shipToState = this.shipToTransactionLocation?.state.orEmpty(),
            totalTaxAmount = this.taxDocumentItemResults.sumOf { it.taxAmount }
        )
    }
