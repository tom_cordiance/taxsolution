package com.modios.taxsolution.taxcompliance.modules

import com.modios.taxsolution.calc.model.v1.TaxDocumentResult
import com.modios.taxsolution.data.TaxDocumentItemTaxResultRepository
import com.modios.taxsolution.data.entity.TaxDocumentItemTaxResultEntity
import com.modios.taxsolution.data.entity.TaxDocumentItemTaxResultPrimaryKey
import org.springframework.stereotype.Component

@Component
class TaxDocumentItemTaxResultModule(private val taxDocumentItemTaxResultRepository: TaxDocumentItemTaxResultRepository) :
    TaxResultModule {
    override suspend fun process(taxDocumentResult: TaxDocumentResult) {
        taxDocumentResult.toTaxDocumentItemTaxResultEntities().forEach {
            taxDocumentItemTaxResultRepository.save(it)
        }
    }
}

private fun TaxDocumentResult.toTaxDocumentItemTaxResultEntities() =
    this.taxDocumentItemResults.flatMap { taxDocumentItemResult ->
        taxDocumentItemResult.taxResults.map { taxDocumentItemTaxResult ->
            val primaryKey = TaxDocumentItemTaxResultPrimaryKey(
                tenantName = this.tenantName,
                companyName = this.companyName,
                documentNumber = this.documentNumber,
                documentDate = this.calculationDate,
                itemIndex = taxDocumentItemResult.index,
                authorityName = taxDocumentItemTaxResult.authorityName
            )

            TaxDocumentItemTaxResultEntity(
                primaryKey = primaryKey,
                shipFromCountry = this.shipFromTransactionLocation?.country.orEmpty(),
                shipFromProvince = this.shipFromTransactionLocation?.province.orEmpty(),
                shipFromState = this.shipFromTransactionLocation?.state.orEmpty(),
                shipToCountry = this.shipToTransactionLocation?.country.orEmpty(),
                shipToProvince = this.shipToTransactionLocation?.province.orEmpty(),
                shipToState = this.shipToTransactionLocation?.state.orEmpty(),
                taxRate = taxDocumentItemTaxResult.taxRate,
                taxAmount = taxDocumentItemTaxResult.taxAmount
            )
        }
    }
