package com.modios.taxsolution.taxcompliance.modules

import com.modios.taxsolution.calc.model.v1.TaxDocumentResult
import com.modios.taxsolution.data.TaxDocumentResultRepository
import com.modios.taxsolution.data.entity.TaxDocumentResultEntity
import com.modios.taxsolution.data.entity.TaxDocumentResultPrimaryKey
import org.springframework.stereotype.Component

@Component
class TaxDocumentResultModule(private val taxDocumentRespository: TaxDocumentResultRepository) : TaxResultModule {
    override suspend fun process(taxDocumentResult: TaxDocumentResult) {
        taxDocumentRespository.save(taxDocumentResult.toTaxResultEntity())
    }
}

private fun TaxDocumentResult.toTaxResultEntity() =
    TaxDocumentResultEntity(
        primaryKey = TaxDocumentResultPrimaryKey(
            tenantName = this.tenantName,
            companyName = this.companyName,
            documentNumber = this.documentNumber,
            documentDate = this.calculationDate,
        ),
        shipFromCountry = this.shipFromTransactionLocation?.country.orEmpty(),
        shipFromProvince = this.shipFromTransactionLocation?.province.orEmpty(),
        shipFromState = this.shipFromTransactionLocation?.state.orEmpty(),
        shipToCountry = this.shipToTransactionLocation?.country.orEmpty(),
        shipToProvince = this.shipToTransactionLocation?.province.orEmpty(),
        shipToState = this.shipToTransactionLocation?.state.orEmpty(),
        totalTaxAmount = this.taxDocumentItemResults.sumOf { it.taxAmount }
    )
