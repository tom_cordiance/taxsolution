package com.modios.taxsolution.taxcompliance.modules

import com.modios.taxsolution.calc.model.v1.TaxDocumentResult
import com.modios.taxsolution.data.TaxAuthorityResultRepository
import com.modios.taxsolution.data.entity.CompanyTenantTransactionDateAuthorityPrimaryKey
import com.modios.taxsolution.data.entity.TaxAuthorityResultEntity
import org.springframework.stereotype.Component

@Component
class TaxAuthorityResultModule(private val taxAuthorityResultRepository: TaxAuthorityResultRepository) :
    TaxResultModule {
    override suspend fun process(taxDocumentResult: TaxDocumentResult) {
        taxDocumentResult.toTaxAuthorityResultEntity().forEach {
            taxAuthorityResultRepository.save(it)
        }
    }
}

private fun TaxDocumentResult.toTaxAuthorityResultEntity() =
    this.taxDocumentItemResults.flatMap { taxDocumentItemResult ->
        taxDocumentItemResult.taxResults.map { taxDocumentItemTaxResult ->
            val primaryKey = CompanyTenantTransactionDateAuthorityPrimaryKey(
                tenantName = this.tenantName,
                companyName = this.companyName,
                authorityName = taxDocumentItemTaxResult.authorityName,
                transactionDate = this.calculationDate,
                documentNumber = this.documentNumber,
                itemIndex = taxDocumentItemResult.index
            )

            TaxAuthorityResultEntity(
                primaryKey = primaryKey,
                taxRate = taxDocumentItemTaxResult.taxRate,
                taxAmount = taxDocumentItemTaxResult.taxAmount
            )
        }
    }
