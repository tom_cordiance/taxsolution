package com.modios.taxsolution.taxcompliance.service

import com.modios.taxsolution.calc.CalcCommon
import com.modios.taxsolution.calc.model.v1.TaxDocumentResult
import com.modios.taxsolution.taxcompliance.modules.TaxResultModule
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import mu.KotlinLogging
import org.springframework.beans.factory.InitializingBean
import org.springframework.kafka.annotation.KafkaHandler
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.stereotype.Service

@Service
@KafkaListener(id = CalcCommon.TAX_DOCUMENT_RESULT_GROUP_NAME, topics = [CalcCommon.TAX_DOCUMENT_RESULT_TOPIC_NAME])
class TaxDocumentResultService(
    private val taxResultDispatcher: CoroutineDispatcher,
    private val taxDocumentModules: Flow<TaxResultModule>
) : InitializingBean {

    private val logger = KotlinLogging.logger { }

    val taxDocumentResultChannel = Channel<TaxDocumentResult>(capacity = Channel.BUFFERED)

    @KafkaHandler
    fun taxResultListener(taxDocumentResult: TaxDocumentResult) = runBlocking {
        taxDocumentResultChannel.send(taxDocumentResult)
    }

    suspend fun taxDocumentProcessing() {
        for (taxDocumentResult in taxDocumentResultChannel) {
            taxDocumentModules.flowOn(taxResultDispatcher).collect { it.process(taxDocumentResult) }
        }
    }

    override fun afterPropertiesSet() {
        CoroutineScope(taxResultDispatcher).launch { taxDocumentProcessing() }
    }
}
