package com.modios.taxsolution.taxcompliance.service

import com.modios.taxsolution.calc.CalcCommon
import mu.KotlinLogging
import org.springframework.kafka.annotation.KafkaHandler
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.stereotype.Service

@Service
@KafkaListener(id = CalcCommon.TAX_DOCUMENT_DEAD_LETTER_GROUP_NAME, topics = ["${CalcCommon.TAX_DOCUMENT_RESULT_TOPIC_NAME}.DLT"])
class KafkaDeadLetterListener {

    private val logger = KotlinLogging.logger { }

    @KafkaHandler
    fun listen(message: String) {
        logger.warn { "Received from DLT: $message" }
    }
}
