package com.modios.taxsolution.taxcompliance.modules

import com.modios.taxsolution.calc.model.v1.TaxDocumentResult

interface TaxResultModule {
    suspend fun process(taxDocumentResult: TaxDocumentResult)
}
