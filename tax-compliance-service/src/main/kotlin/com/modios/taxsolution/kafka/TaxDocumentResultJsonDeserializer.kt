package com.modios.taxsolution.kafka

import com.modios.taxsolution.calc.model.v1.TaxDocumentResult
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import org.apache.kafka.common.serialization.Deserializer

class TaxDocumentResultJsonDeserializer : Deserializer<TaxDocumentResult> {

    override fun deserialize(topic: String, data: ByteArray): TaxDocumentResult {
        val json = String(data)
        return Json.decodeFromString(json)
    }
}
