package com.modios.taxsolution.kafka

import com.modios.taxsolution.calc.model.v1.TaxDocumentResultKey
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import org.apache.kafka.common.serialization.Deserializer

class TaxDocumentResultKeyJsonDeserializer : Deserializer<TaxDocumentResultKey> {
    override fun deserialize(topic: String, data: ByteArray): TaxDocumentResultKey {
        val json = String(data)
        return Json.decodeFromString(json)
    }
}
