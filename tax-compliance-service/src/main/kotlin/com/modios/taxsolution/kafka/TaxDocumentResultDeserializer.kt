package com.modios.taxsolution.kafka

import com.modios.taxsolution.calc.model.v1.TaxDocumentResult
import org.apache.kafka.common.serialization.Deserializer
import java.io.ByteArrayInputStream
import java.io.ObjectInputStream

class TaxDocumentResultDeserializer : Deserializer<TaxDocumentResult> {
    override fun deserialize(topic: String, data: ByteArray): TaxDocumentResult {
        return ByteArrayInputStream(data).use {
            ObjectInputStream(it).use {
                it.readObject()
            }
        } as TaxDocumentResult
    }
}
