package com.modios.taxsolution.data.entity

import org.springframework.data.cassandra.core.cql.PrimaryKeyType
import org.springframework.data.cassandra.core.mapping.PrimaryKeyClass
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn
import java.time.LocalDateTime

@PrimaryKeyClass
data class TaxDocumentResultPrimaryKey(
    @get:PrimaryKeyColumn(type = PrimaryKeyType.PARTITIONED, name = "company_name", ordinal = 0)
    val companyName: String,
    @get:PrimaryKeyColumn(type = PrimaryKeyType.CLUSTERED, name = "tenant_name", ordinal = 1)
    val tenantName: String,
    @get:PrimaryKeyColumn(type = PrimaryKeyType.CLUSTERED, name = "document_date", ordinal = 2)
    val documentDate: LocalDateTime,
    @get:PrimaryKeyColumn(type = PrimaryKeyType.CLUSTERED, name = "document_number", ordinal = 3)
    val documentNumber: String
)

@PrimaryKeyClass
data class TaxDocumentItemResultPrimaryKey(
    @get:PrimaryKeyColumn(type = PrimaryKeyType.PARTITIONED, name = "company_name", ordinal = 0)
    val companyName: String,
    @get:PrimaryKeyColumn(type = PrimaryKeyType.CLUSTERED, name = "tenant_name", ordinal = 1)
    val tenantName: String,
    @get:PrimaryKeyColumn(type = PrimaryKeyType.CLUSTERED, name = "document_date", ordinal = 2)
    val documentDate: LocalDateTime,
    @get:PrimaryKeyColumn(type = PrimaryKeyType.CLUSTERED, name = "document_number", ordinal = 3)
    val documentNumber: String,
    @get:PrimaryKeyColumn(type = PrimaryKeyType.CLUSTERED, name = "item_index", ordinal = 4)
    val itemIndex: Int
)

@PrimaryKeyClass
data class TaxDocumentItemTaxResultPrimaryKey(
    @get:PrimaryKeyColumn(type = PrimaryKeyType.PARTITIONED, name = "company_name", ordinal = 0)
    val companyName: String,
    @get:PrimaryKeyColumn(type = PrimaryKeyType.CLUSTERED, name = "tenant_name", ordinal = 1)
    val tenantName: String,
    @get:PrimaryKeyColumn(type = PrimaryKeyType.CLUSTERED, name = "document_date", ordinal = 2)
    val documentDate: LocalDateTime,
    @get:PrimaryKeyColumn(type = PrimaryKeyType.CLUSTERED, name = "document_number", ordinal = 3)
    val documentNumber: String,
    @get:PrimaryKeyColumn(type = PrimaryKeyType.CLUSTERED, name = "item_index", ordinal = 4)
    val itemIndex: Int,
    @get:PrimaryKeyColumn(type = PrimaryKeyType.CLUSTERED, name = "authority_name", ordinal = 5)
    val authorityName: String,
)
