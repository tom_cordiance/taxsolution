package com.modios.taxsolution.data

import com.modios.taxsolution.data.entity.TaxDocumentResultEntity
import com.modios.taxsolution.data.entity.TaxDocumentResultPrimaryKey
import org.springframework.data.cassandra.repository.CassandraRepository

interface TaxDocumentResultRepository : CassandraRepository<TaxDocumentResultEntity, TaxDocumentResultPrimaryKey>
