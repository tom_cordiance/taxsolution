package com.modios.taxsolution.data.entity

import org.springframework.data.cassandra.core.mapping.Column
import org.springframework.data.cassandra.core.mapping.PrimaryKey
import org.springframework.data.cassandra.core.mapping.Table
import java.math.BigDecimal

@Table("tax_document_results")
data class TaxDocumentResultEntity(
    @PrimaryKey
    val primaryKey: TaxDocumentResultPrimaryKey,
    @get:Column("ship_from_country")
    val shipFromCountry: String,
    @get:Column("ship_from_province")
    val shipFromProvince: String,
    @get:Column("ship_from_state")
    val shipFromState: String,
    @get:Column("ship_to_country")
    val shipToCountry: String,
    @get:Column("ship_to_province")
    val shipToProvince: String,
    @get:Column("ship_to_state")
    val shipToState: String,
    @get:Column("total_tax_amount")
    val totalTaxAmount: BigDecimal
)
