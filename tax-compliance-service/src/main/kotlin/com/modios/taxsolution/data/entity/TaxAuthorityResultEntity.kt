package com.modios.taxsolution.data.entity

import org.springframework.data.cassandra.core.mapping.Column
import org.springframework.data.cassandra.core.mapping.PrimaryKey
import org.springframework.data.cassandra.core.mapping.Table
import java.math.BigDecimal

@Table("tax_authority_results")
data class TaxAuthorityResultEntity(
    @PrimaryKey
    val primaryKey: CompanyTenantTransactionDateAuthorityPrimaryKey,
    @get:Column("tax_rate")
    val taxRate: BigDecimal,
    @get:Column("tax_amount")
    val taxAmount: BigDecimal
)
