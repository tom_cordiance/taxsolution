package com.modios.taxsolution.data.entity

import org.springframework.data.cassandra.core.cql.PrimaryKeyType
import org.springframework.data.cassandra.core.mapping.PrimaryKeyClass
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn
import java.time.LocalDateTime

@PrimaryKeyClass
data class CompanyTenantTransactionDateAuthorityPrimaryKey(
    @get:PrimaryKeyColumn(type = PrimaryKeyType.PARTITIONED, name = "company_name", ordinal = 0)
    val companyName: String,
    @get:PrimaryKeyColumn(type = PrimaryKeyType.CLUSTERED, name = "tenant_name", ordinal = 1)
    val tenantName: String,
    @get:PrimaryKeyColumn(type = PrimaryKeyType.CLUSTERED, name = "authority_name", ordinal = 2)
    val authorityName: String,
    @get:PrimaryKeyColumn(type = PrimaryKeyType.CLUSTERED, name = "transaction_date", ordinal = 3)
    val transactionDate: LocalDateTime,
    @get:PrimaryKeyColumn(type = PrimaryKeyType.CLUSTERED, name = "document_number", ordinal = 4)
    val documentNumber: String,
    @get:PrimaryKeyColumn(type = PrimaryKeyType.CLUSTERED, name = "item_index", ordinal = 5)
    val itemIndex: Int
)
