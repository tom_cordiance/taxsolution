package com.modios.taxsolution.data

import com.modios.taxsolution.data.entity.TaxAuthorityResultEntity
import com.modios.taxsolution.data.entity.TaxDocumentResultPrimaryKey
import org.springframework.data.cassandra.repository.CassandraRepository

interface TaxAuthorityResultRepository : CassandraRepository<TaxAuthorityResultEntity, TaxDocumentResultPrimaryKey>
