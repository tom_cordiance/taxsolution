package com.modios

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.modios.taxsolution.calc.model.v1.TaxDocument
import com.modios.taxsolution.calc.model.v1.TaxDocumentItem
import com.modios.taxsolution.calc.model.v1.TransactionLocation
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import java.math.BigDecimal
import java.time.LocalDateTime

class SerializationTest {

    private fun createTaxingLocation(country: String, state: String): TransactionLocation {
        return TransactionLocation(country = country, state = state)
    }

    @Test
    fun test() {
        val objectMapper = ObjectMapper()
            .registerModule(KotlinModule())
            .registerModule(JavaTimeModule())
        // .registerModule(
        //     SimpleModule()
        //         .addSerializer(TaxDocumentResultJacksonSerializer())
        //         .addSerializer(TaxDocumentJacksonSerializer())
        // )

        val taxDocument = TaxDocument(
            tenantName = "Tenant",
            companyName = "Company",
            persistResult = false,
            documentDate = LocalDateTime.now(),
            documentNumber = "abc123",
            shipFrom = createTaxingLocation("US", "CA"),
            shipTo = createTaxingLocation("US", "OR")
        ).apply {
            taxDocumentItems.add(TaxDocumentItem(index = 1, quantity = 1, amount = BigDecimal.valueOf(100)))
        }

        val json = objectMapper.writeValueAsString(taxDocument)
        Assertions.assertNotNull(json)

        val deserializedTaxDocument = objectMapper.readValue(json, TaxDocument::class.java)
        Assertions.assertNotNull(deserializedTaxDocument)
        Assertions.assertEquals(taxDocument.documentDate, deserializedTaxDocument.documentDate)
    }
}
