plugins {
    id("org.springframework.boot") apply true
    id("io.spring.dependency-management") apply true
    id("application")
    kotlin("plugin.spring") apply true
    kotlin("plugin.serialization") apply true
}

val logbackVersion: String by project
val logbackEcsEncoderVersion: String by project
val slf4jApiVersion: String by project
val kotlinLoggingVersion: String by project
val junitJupiterVersion: String by project
val kotlinSerializationVersion: String by project

dependencies {
    implementation(enforcedPlatform(org.springframework.boot.gradle.plugin.SpringBootPlugin.BOM_COORDINATES))
    implementation("javax.validation:validation-api:2.0.1.Final")

    implementation("org.springframework.boot:spring-boot-starter-actuator")
    implementation("org.springframework.boot:spring-boot-starter-webflux")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-reactor")
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:$kotlinSerializationVersion")

    implementation("org.springframework.kafka:spring-kafka")
    implementation("org.springframework.data:spring-data-cassandra")

    implementation(project(":calc-service-model"))

    // Logging
    implementation("ch.qos.logback:logback-classic:$logbackVersion")
    implementation("org.slf4j:slf4j-api:$slf4jApiVersion")
    implementation("io.github.microutils:kotlin-logging:$kotlinLoggingVersion")

    testImplementation("org.junit.jupiter:junit-jupiter:$junitJupiterVersion")
}

application {
    // Define the main class for the application.
    mainClassName = "com.modios.taxsolution.TaxComplianceServiceAppKt"
}

tasks.test {
    useJUnitPlatform()
}

tasks.getByName<org.springframework.boot.gradle.tasks.bundling.BootBuildImage>("bootBuildImage") {
    imageName = "nexus.dev.modios.io/tax-compliance-service:${project.version}"
    environment = mapOf()
}
