plugins {
    id("org.springframework.boot") apply true
    id("io.spring.dependency-management") apply true
    id("application")
    kotlin("plugin.spring") apply true
}

val logbackVersion: String by project
val logbackEcsEncoderVersion: String by project
val slf4jApiVersion: String by project
val kotlinLoggingVersion: String by project
val r2dbcMigrateVersion: String by project

dependencies {
    implementation(enforcedPlatform(org.springframework.boot.gradle.plugin.SpringBootPlugin.BOM_COORDINATES))
    implementation("javax.validation:validation-api:2.0.1.Final")

    implementation("org.springframework.boot:spring-boot-starter-actuator")
    implementation("org.springframework.boot:spring-boot-starter-data-r2dbc")
    implementation("org.springframework.boot:spring-boot-starter-webflux")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-reactor")

    // Swagger
    implementation("io.springfox:springfox-boot-starter:3.0.0")

//    implementation(project(":registry-service-model"))
    implementation(project(":registry-service-dataaccess"))
    implementation(project(":admin-service-model"))

    // Logging
    implementation("ch.qos.logback:logback-classic:$logbackVersion")
    implementation("org.slf4j:slf4j-api:$slf4jApiVersion")
    implementation("io.github.microutils:kotlin-logging:$kotlinLoggingVersion")

    implementation("io.r2dbc:r2dbc-postgresql")
    // db migrations for R2DBC
    implementation("name.nkonev.r2dbc-migrate:r2dbc-migrate-spring-boot-starter:$r2dbcMigrateVersion")
}

application {
    // Define the main class for the application.
    mainClassName = "com.modios.taxengine.DataServiceAppKt"
}
