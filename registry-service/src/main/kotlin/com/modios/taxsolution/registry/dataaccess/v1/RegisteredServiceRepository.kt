package com.modios.taxsolution.registry.dataaccess.v1

import org.springframework.data.annotation.Id
import org.springframework.data.r2dbc.repository.Query
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.repository.kotlin.CoroutineCrudRepository
import java.util.UUID

interface RegisteredServiceRepository : CoroutineCrudRepository<RegisteredService, String> {

    @Query("select * from CE_REGISTERED_SERVCIE where serviceUUID=:serviceUUID")
    suspend fun findByUUID(serviceUUID: UUID): RegisteredService?
}

data class RegisteredService(@Id val serviceName: String, @Column val serviceUUID: UUID)
