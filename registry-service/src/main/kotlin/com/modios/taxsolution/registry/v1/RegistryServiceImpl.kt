package com.modios.taxsolution.registry.v1

import com.modios.taxsolution.registry.dataaccess.v1.RegisteredService
import com.modios.taxsolution.registry.dataaccess.v1.RegisteredServiceRepository
import com.modios.taxsolution.registry.dataaccess.v1.ServiceIdBlock
import com.modios.taxsolution.registry.dataaccess.v1.ServiceIdBlockRepository
import com.modios.taxsolution.registry.model.v1.IdBlock
import mu.KotlinLogging
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.util.UUID

@RestController
@RequestMapping("/api/v1/id")
class RegistryServiceImpl(
    private val registeredServiceRepository: RegisteredServiceRepository,
    private val serviceIdBlockRepository: ServiceIdBlockRepository
) : RegistryService {

    private val logger = KotlinLogging.logger { }

    /**
     * Need a way to identify valid service instances
     *
     * @param serviceName - the name of the service
     */
    @PostMapping("/api/v1/id/reserve/{serviceName}")
    override suspend fun registerService(@PathVariable serviceName: String): UUID {
        val registeredService = RegisteredService(serviceName = serviceName, serviceUUID = UUID.randomUUID())

        registeredServiceRepository.save(registeredService)

        return registeredService.serviceUUID
    }

    /**
     * Allows a service to reserve a block of IDs.  These are permanently reserved and unavailable to other callers.
     *
     * Note: this is intended to be a private service that does not validate duplicate calls. Be sure the service
     *  1) IS NOT PUBLICLY AVAILABLE
     *  2) IS CALLED IN A PREDICTABLE MANNER
     *
     * @param serviceUUID the key of the registered service
     * @param blockName the name of the counter, i.e. tablename
     * @param blockSize the size of the block
     */
    @PostMapping("/api/v1/id/reserve/{serviceUUID}/{blockName}/{blockSize}")
    override suspend fun reserveIdBlock(
        @PathVariable serviceUUID: UUID,
        @PathVariable blockName: String,
        @PathVariable blockSize: Long
    ): IdBlock {
        val registeredService = registeredServiceRepository.findByUUID(serviceUUID)

        return when (registeredService) {
            null -> {
                val message = "The service $serviceUUID has not been registered"
                logger.error { message }
                throw ServiceNotRegisteredException(message)
            }
            else -> {
                val existingServiceIdBlock = serviceIdBlockRepository.findById(serviceUUID)
                val newServiceIdBlock = when (existingServiceIdBlock) {
                    null -> ServiceIdBlock.create(
                        serviceUUID = serviceUUID,
                        name = blockName,
                        blockStart = 1L,
                        blockEnd = blockSize
                    )
                    else -> existingServiceIdBlock.copy(
                        blockStart = existingServiceIdBlock.blockEnd + 1,
                        blockEnd = existingServiceIdBlock.blockEnd + blockSize
                    )
                }

                serviceIdBlockRepository.save(newServiceIdBlock)

                IdBlock(
                    serviceKey = newServiceIdBlock.serviceUUID,
                    blockUUID = newServiceIdBlock.blockUUID,
                    name = newServiceIdBlock.name,
                    blockStart = newServiceIdBlock.blockStart,
                    blockEnd = newServiceIdBlock.blockEnd
                )
            }
        }
    }

    /**
     * Allow a reserved ID block to be able to be released and reused
     *
     * NOTE: only implement this if necessary
     */
    @PostMapping("/api/v1/id/release/{serviceKey}/{blockUUID}")
    override suspend fun releaseIdBlock(@PathVariable serviceKey: UUID, @PathVariable blockUUID: UUID) {
        TODO("Not yet implemented")
    }
}
