package com.modios.taxsolution.registry.dataaccess.v1

import org.springframework.data.annotation.Id
import org.springframework.data.r2dbc.repository.Query
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import org.springframework.data.repository.kotlin.CoroutineCrudRepository
import org.springframework.transaction.annotation.Transactional
import java.util.UUID

@Transactional
interface ServiceIdBlockRepository : CoroutineCrudRepository<ServiceIdBlock, UUID> {

    @Query("select * from CE_SERVICE_ID_BLOCK where name=:name")
    suspend fun findByName(name: String): List<ServiceIdBlock>

    @Query("insert into CE_SERVICE_ID_BLOCK (blockUUID,serviceUUID,name,blockStart,blockEnd) values (:blockUUID,:serviceUUID,:name,(select max(blockStart) from CE_SERVICE_ID_BLOCK where name=:name)) set blockStart=blockEnd+1 where name=:name")
    suspend fun reserveBlock(name: String): ServiceIdBlock
}

@Table("CE_SERVICE_ID_BLOCK")
data class ServiceIdBlock(
    @Id val blockUUID: UUID,
    @Column val serviceUUID: UUID,
    @Column val name: String,
    @Column val blockStart: Long,
    @Column val blockEnd: Long
) {
    companion object {
        fun create(serviceUUID: UUID, name: String, blockStart: Long, blockEnd: Long) =
            ServiceIdBlock(
                blockUUID = UUID.randomUUID(),
                serviceUUID = serviceUUID,
                name = name,
                blockStart = blockStart,
                blockEnd = blockEnd
            )
    }
}
