package com.modios.taxsolution

object ServiceCommon {

    const val TAX_CONTENT_PROVIDER = "Cordiance US Tax Content"

    const val TENANT_NAME = "Modios"

    const val COMPANY_NAME = "ModCo"
}
