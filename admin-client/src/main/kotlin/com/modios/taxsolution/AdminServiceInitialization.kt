package com.modios.taxsolution

import com.modios.taxsolution.admin.client.v1.AdminClientCommon
import com.modios.taxsolution.admin.client.v1.CompanyClient
import com.modios.taxsolution.admin.client.v1.CompanyTaxContentProviderClient
import com.modios.taxsolution.admin.client.v1.TenantClient
import com.modios.taxsolution.content.client.v1.TaxContentProviderClient
import com.modios.taxsolution.model.v1.Company
import com.modios.taxsolution.model.v1.Tenant
import kotlinx.coroutines.runBlocking
import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

fun main(args: Array<String>) {
    SpringApplication(AdminServiceInitialization::class.java).run(*args)
}

@SpringBootApplication
class AdminServiceInitialization : CommandLineRunner {

    private val logger = KotlinLogging.logger { }

    @Autowired
    private lateinit var tenantClient: TenantClient

    @Autowired
    private lateinit var companyClient: CompanyClient

    @Autowired
    private lateinit var taxContentProviderClient: TaxContentProviderClient

    @Autowired
    private lateinit var companyTaxContentProviderClient: CompanyTaxContentProviderClient

    private suspend fun initializeTenants(): Tenant {
        val tenantName = ServiceCommon.TENANT_NAME
        logger.info { "getting tenant $tenantName..." }
        var tenant: Tenant? = tenantClient.getTenant(tenantName)

        when (tenant) {
            null -> {
                logger.info { "tenant does not exist, creating tenant $tenantName..." }
                tenant = tenantClient.createTenant(tenantName)
                logger.info { "Tenant created: $tenant" }
            }
            else -> {
                logger.info { "tenant exists: $tenant" }
            }
        }

        return tenant
    }

    private suspend fun initializeCompanies(tenantId: Long): Company {
        val companyName = ServiceCommon.COMPANY_NAME
        val parentCompanyId = AdminClientCommon.TOP_LEVEL_PARENT_ID

        logger.info { "getting company: tenantId[$tenantId], name[$companyName]..." }
        var company: Company? = companyClient.getCompany(tenantId, companyName)

        when (company) {
            null -> {
                logger.info { "company does not exist, creating company: tenantId[$tenantId], parentCompanyId[$parentCompanyId], name[$companyName]" }
                company =
                    companyClient.create(tenantId = tenantId, parentCompanyId = parentCompanyId, name = companyName)
                logger.info { "company created: $company" }
            }
            else -> {
                logger.info { "company exists: $company" }
            }
        }

        return company
    }

    private suspend fun initializeCompanyContentProviders(companyId: Long) {
        val taxConentProviderName = ServiceCommon.TAX_CONTENT_PROVIDER

        val taxContentProvider = taxContentProviderClient.get(taxConentProviderName)
        when (taxContentProvider) {
            null -> throw RuntimeException("Could not find the tax content provider $taxConentProviderName")
            else -> {
                logger.info { "found content provider $taxConentProviderName[${taxContentProvider.id}], creating company tax content provider..." }
                val companyTaxContentProvider = companyTaxContentProviderClient.create(companyId, taxContentProvider.id)
                logger.info { "created company tax content provider: $companyTaxContentProvider" }
            }
        }
    }

    override fun run(vararg args: String?) = runBlocking {

        val tenant = initializeTenants()

        val company = initializeCompanies(tenant.id)

        initializeCompanyContentProviders(company.id)

        System.exit(0)
    }
}
