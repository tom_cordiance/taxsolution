package com.modios.taxsolution.engine.v1

import com.modios.taxsolution.calc.model.v1.MessageStatus
import com.modios.taxsolution.calc.model.v1.TaxDocument
import com.modios.taxsolution.calc.model.v1.TaxDocumentItem
import com.modios.taxsolution.calc.model.v1.TaxDocumentItemResult
import com.modios.taxsolution.calc.model.v1.TaxDocumentItemTaxResult
import com.modios.taxsolution.calc.model.v1.TaxDocumentResult
import com.modios.taxsolution.calc.model.v1.TaxDocumentResultMessage
import com.modios.taxsolution.module.v1.TaxCalculation
import com.modios.taxsolution.module.v1.TaxCalculationItem
import com.modios.taxsolution.module.v1.TaxCalculationMessage
import com.modios.taxsolution.module.v1.TaxModule
import io.micrometer.core.instrument.MeterRegistry
import io.micrometer.core.instrument.Timer
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.fold
import java.math.BigDecimal
import java.time.LocalDateTime
import kotlin.time.ExperimentalTime
import kotlin.time.measureTimedValue
import kotlin.time.toJavaDuration

class StandardEngine(
    meterRegistry: MeterRegistry,
    private val moduleFlow: Flow<TaxModule>,
    private val calculationDispatcher: CoroutineDispatcher
) {
    private val totalCalcTimer = Timer
        .builder("cordiance.engine.standard")
        .register(meterRegistry)

    private val taxDocumentResultTimer = Timer
        .builder("cordiance.engine.standard.taxCalcConversion")
        .register(meterRegistry)

    @ExperimentalTime
    suspend fun calculate(taxDocument: TaxDocument): TaxDocumentResult {
        return measureTimedValue {
            val initialTaxCalculation = taxDocument.asTaxCalculation()

            val taxCalculation = moduleFlow.flowOn(calculationDispatcher).fold(initialTaxCalculation) { taxCalculation, taxModule ->
                taxModule.process(taxCalculation)
            }

            measureTimedValue {
                taxCalculation.asTaxDocumentResult(taxDocument)
            }.also {
                taxDocumentResultTimer.record(it.duration.toJavaDuration())
            }.value
        }.also {
            totalCalcTimer.record(it.duration.toJavaDuration())
        }.value
    }
}

/**
 * Conversion functions
 */

fun TaxDocument.asTaxCalculation(): TaxCalculation {
    return TaxCalculation(
        tenantName = this.tenantName,
        companyName = this.companyName,
        calculationDate = LocalDateTime.now(),
        documentNumber = this.documentNumber,
        shipFromTransactionLocation = this.shipFrom,
        shipToTransactionLocation = this.shipTo,
        persistResult = persistResult,
        hasFailure = false
    ).also {
        it.taxCalculationItemList.addAll(this.taxDocumentItems.map { it.asTaxCalculationItem() })
    }
}

fun TaxDocumentItem.asTaxCalculationItem(): TaxCalculationItem {
    return TaxCalculationItem(
        index = this.index,
        productCode = this.productCode,
        productMappingCode = this.productMappingCode,
        quantity = this.quantity,
        amount = this.amount,
        taxAmount = null,
        messageList = mutableListOf()
    )
}

fun TaxCalculation.asTaxDocumentResult(originalTaxDocument: TaxDocument): TaxDocumentResult {
    return TaxDocumentResult(
        originalTaxDocument = originalTaxDocument,
        tenantName = tenantName,
        companyName = companyName,
        documentNumber = documentNumber,
        shipFromTransactionLocation = shipFromTransactionLocation,
        shipToTransactionLocation = shipToTransactionLocation,
        taxDocumentResultMessages = messageList.map { it.asTaxDocumentMessage() },
        taxDocumentItemResults = taxCalculationItemList.map { it.asTaxDocumentItemResult() }
    )
}

fun TaxCalculationMessage.asTaxDocumentMessage(): TaxDocumentResultMessage {
    return TaxDocumentResultMessage(status = MessageStatus.valueOf(status.toString()), message = message)
}

fun TaxCalculationItem.asTaxDocumentItemResult(): TaxDocumentItemResult {
    return TaxDocumentItemResult(
        index = this.index,
        taxResults = taxResultList.map {
            TaxDocumentItemTaxResult(
                authorityName = it.taxAuthority.name,
                taxRate = it.taxRate.rate,
                taxAmount = it.taxAmount
            )
        },
        taxAmount = taxResultList.fold(BigDecimal.ZERO) { sum, taxResult -> sum.add(taxResult.taxAmount) },
        taxDocumentResultMessages = messageList.map { it.asTaxDocumentMessage() }
    )
}
