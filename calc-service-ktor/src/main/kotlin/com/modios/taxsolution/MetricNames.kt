package com.modios.taxsolution

object MetricNames {
    val nameSpace = "cordiance"

    val names = listOf(
        "cordiance.engine.standard",
        "cordiance.module.company",
        "cordiance.module.companyTaxContentProvider",
        "cordiance.module.storeTaxDocumentResult",
        "cordiance.module.taxAuthority",
        "cordiance.module.taxCalculation",
        "cordiance.module.taxingTransactionLocation",
        "cordiance.module.taxRateSelection",
        "cordiance.module.tenant"
    )
}
