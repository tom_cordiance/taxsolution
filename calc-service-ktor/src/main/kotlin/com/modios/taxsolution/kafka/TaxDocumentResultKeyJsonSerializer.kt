package com.modios.taxsolution.kafka

import com.modios.taxsolution.calc.model.v1.TaxDocumentResultKey
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.apache.kafka.common.serialization.Serializer

class TaxDocumentResultKeyJsonSerializer : Serializer<TaxDocumentResultKey> {
    override fun serialize(topic: String, data: TaxDocumentResultKey): ByteArray {
        return Json.encodeToString(data).toByteArray()
    }
}
