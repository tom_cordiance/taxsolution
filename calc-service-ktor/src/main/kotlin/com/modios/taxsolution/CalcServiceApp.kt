package com.modios.taxsolution

import com.modios.taxsolution.calc.model.v1.TaxDocument
import com.modios.taxsolution.config.calcModules
import com.modios.taxsolution.content.model.v1.TaxContentMessage
import com.modios.taxsolution.service.calc.standard.v1.CalcService
import com.modios.taxsolution.service.data.v1.TaxContentMessageService
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.ContentNegotiation
import io.ktor.features.DefaultHeaders
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.request.receiveText
import io.ktor.response.respond
import io.ktor.response.respondText
import io.ktor.routing.Routing
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.routing
import io.ktor.serialization.json
import io.micrometer.core.instrument.MeterRegistry
import io.micrometer.core.instrument.Timer
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import mu.KotlinLogging
import org.koin.ktor.ext.Koin
import org.koin.ktor.ext.inject
import java.util.concurrent.TimeUnit
import kotlin.time.ExperimentalTime
import kotlin.time.measureTimedValue
import kotlin.time.toJavaDuration

fun main(args: Array<String>): Unit = io.ktor.server.cio.EngineMain.main(args)

val logger = KotlinLogging.logger { }

fun foo(metricsRegistry: MeterRegistry): String {
    val timers = MetricNames
        .names
        .map { it to metricsRegistry.timer(it) }
        .toMap()

    val table = "<table>" +
        "<tr>" +
        "<th>Metric Name</th>\n" +
        "<th>Count</th>\n" +
        "<th>Mean</th>\n" +
        "<th>Max</th>\n" +
        "</tr>" +
        timers
            .map { (name, timer) ->
                String.format(
                    "<tr>\n<td>$name</td>\n<td>${timer.count()}</td>\n<td>%.2fus</td>\n<td>%.2fus</td></tr>\n",
                    timer.mean(TimeUnit.MICROSECONDS),
                    timer.max(TimeUnit.MICROSECONDS)
                )
            }
            .joinToString("") +
        "</table>"

    return table
}

fun generateDisplayableMetrics(metricsRegistry: MeterRegistry) =
    MetricNames
        .names
        .map {
            val timer = metricsRegistry.timer(it)
            with(timer) {
                "$it\t\t\t count[${count()}]\t  mean[${mean(TimeUnit.MICROSECONDS)}us]\t  max[${max(TimeUnit.MICROSECONDS)}us]"
            }
        }.joinToString("\n")

@OptIn(ExperimentalTime::class)
fun Application.module() {
    install(DefaultHeaders)
    install(Routing)
    install(ContentNegotiation) {
        json()
    }
    install(Koin) {
        modules(calcModules)
    }

    val taxContentMessageService: TaxContentMessageService by inject()
    val calcService: CalcService by inject()
    val meterRegistry: MeterRegistry by inject()

    val deserializationTimer = Timer
        .builder("cordiance.calc.deserialization")
        .register(meterRegistry)

    val serializationTimer = Timer
        .builder("cordiance.calc.serialization")
        .register(meterRegistry)

    // Routing section
    routing {
        get("/") {
            call.respondText("Hello")
        }
        get("/calc/standard/v1") {
            val response = foo(meterRegistry)
            call.respondText(response, ContentType.Text.Html)
        }
        post("/calc/standard/v1") {
            val taxDocumentJson = call.receiveText()
            val taxDocument: TaxDocument = measureTimedValue {
                Json.decodeFromString(taxDocumentJson) as TaxDocument
            }.also {
                deserializationTimer.record(it.duration.toJavaDuration())
            }.value

            val taxDocumentResult = calcService.calculate(taxDocument)

            val taxDocumentResultJson = measureTimedValue {
                Json.encodeToString(taxDocumentResult)
            }.also {
                serializationTimer.record(it.duration.toJavaDuration())
            }.value

            call.respondText(contentType = ContentType.Application.Json) { taxDocumentResultJson }
        }
        post("/calc/standard/v1/taxcontentmessage") {
            val taxContentUpdate: TaxContentMessage = call.receive()
            logger.info { "tax content message: $taxContentUpdate" }
            taxContentMessageService.handleMessage(taxContentUpdate)
            call.respond(status = HttpStatusCode.OK, "")
        }
    }
}
