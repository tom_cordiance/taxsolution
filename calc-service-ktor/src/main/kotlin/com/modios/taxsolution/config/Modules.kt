package com.modios.taxsolution.config

import com.modios.taxsolution.admin.client.v1.CompanyClient
import com.modios.taxsolution.admin.client.v1.CompanyClientImpl
import com.modios.taxsolution.admin.client.v1.CompanyTaxContentProviderClient
import com.modios.taxsolution.admin.client.v1.CompanyTaxContentProviderClientImpl
import com.modios.taxsolution.admin.client.v1.TenantClient
import com.modios.taxsolution.admin.client.v1.TenantClientImpl
import com.modios.taxsolution.content.client.v1.GeographyClient
import com.modios.taxsolution.content.client.v1.GeographyClientImpl
import com.modios.taxsolution.content.client.v1.TaxAuthorityClient
import com.modios.taxsolution.content.client.v1.TaxAuthorityClientImpl
import com.modios.taxsolution.content.client.v1.TaxAuthorityGeographyClient
import com.modios.taxsolution.content.client.v1.TaxAuthorityGeographyClientImpl
import com.modios.taxsolution.content.client.v1.TaxContentProviderClient
import com.modios.taxsolution.content.client.v1.TaxContentProviderClientImpl
import com.modios.taxsolution.content.client.v1.TaxRateClient
import com.modios.taxsolution.content.client.v1.TaxRateClientImpl
import com.modios.taxsolution.engine.v1.StandardEngine
import com.modios.taxsolution.kafka.TaxDocumentResultJsonSerializer
import com.modios.taxsolution.kafka.TaxDocumentResultKeyJsonSerializer
import com.modios.taxsolution.module.v1.CompanyModule
import com.modios.taxsolution.module.v1.CompanyTaxContentProviderModule
import com.modios.taxsolution.module.v1.DefaultTaxingTransactionLocationFactory
import com.modios.taxsolution.module.v1.StoreTaxDocumentResultModule
import com.modios.taxsolution.module.v1.TaxAuthorityModule
import com.modios.taxsolution.module.v1.TaxCalculationModule
import com.modios.taxsolution.module.v1.TaxModule
import com.modios.taxsolution.module.v1.TaxRateSelectionModule
import com.modios.taxsolution.module.v1.TaxingTransactionLocationFactory
import com.modios.taxsolution.module.v1.TaxingTransactionLocationModule
import com.modios.taxsolution.module.v1.TenantModule
import com.modios.taxsolution.service.calc.standard.v1.CalcService
import com.modios.taxsolution.service.data.v1.CompanyService
import com.modios.taxsolution.service.data.v1.CompanyServiceImpl
import com.modios.taxsolution.service.data.v1.CompanyTaxContentProviderService
import com.modios.taxsolution.service.data.v1.CompanyTaxContentProviderServiceImpl
import com.modios.taxsolution.service.data.v1.GeographyService
import com.modios.taxsolution.service.data.v1.GeographyServiceImpl
import com.modios.taxsolution.service.data.v1.TaxAuthorityGeographyService
import com.modios.taxsolution.service.data.v1.TaxAuthorityGeographyServiceImpl
import com.modios.taxsolution.service.data.v1.TaxAuthorityService
import com.modios.taxsolution.service.data.v1.TaxAuthorityServiceImpl
import com.modios.taxsolution.service.data.v1.TaxContentMessageService
import com.modios.taxsolution.service.data.v1.TaxContentMessageServiceImpl
import com.modios.taxsolution.service.data.v1.TaxContentProviderService
import com.modios.taxsolution.service.data.v1.TaxContentProviderServiceImpl
import com.modios.taxsolution.service.data.v1.TaxDocumentResultStorageService
import com.modios.taxsolution.service.data.v1.TaxDocumentResultStorageServiceImpl
import com.modios.taxsolution.service.data.v1.TaxRateService
import com.modios.taxsolution.service.data.v1.TaxRateServiceImpl
import com.modios.taxsolution.service.data.v1.TenantService
import com.modios.taxsolution.service.data.v1.TenantServiceImpl
import io.ktor.client.HttpClient
import io.ktor.client.engine.cio.CIO
import io.ktor.client.features.json.JsonFeature
import io.micrometer.core.instrument.Clock
import io.micrometer.core.instrument.MeterRegistry
import io.micrometer.datadog.DatadogConfig
import io.micrometer.datadog.DatadogMeterRegistry
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf
import org.apache.kafka.clients.producer.KafkaProducer
import org.apache.kafka.clients.producer.ProducerConfig
import org.koin.core.qualifier.named
import org.koin.dsl.module
import java.util.Properties

@OptIn(ExperimentalCoroutinesApi::class)
val calcModules = module {
    single { Configuration().properties as Properties }

    single {
        HttpClient(CIO) {
            install(JsonFeature)
            expectSuccess = true
            engine {
            }
        }
    }

    single { TenantClientImpl(get(), get()) as TenantClient }
    single { CompanyClientImpl(get(), get()) as CompanyClient }
    single { CompanyTaxContentProviderClientImpl(get(), get()) as CompanyTaxContentProviderClient }

    single { GeographyClientImpl(get(), get()) as GeographyClient }
    single { TaxAuthorityClientImpl(get(), get()) as TaxAuthorityClient }
    single { TaxAuthorityGeographyClientImpl(get(), get()) as TaxAuthorityGeographyClient }
    single { TaxContentProviderClientImpl(get(), get()) as TaxContentProviderClient }
    single { TaxRateClientImpl(get(), get()) as TaxRateClient }

    single { CompanyServiceImpl(get()) as CompanyService }
    single { CompanyTaxContentProviderServiceImpl(get()) as CompanyTaxContentProviderService }
    single { GeographyServiceImpl(get()) as GeographyService }
    single { TaxAuthorityGeographyServiceImpl(get()) as TaxAuthorityGeographyService }
    single { TaxAuthorityServiceImpl(get()) as TaxAuthorityService }
    single { TaxContentProviderServiceImpl(get()) as TaxContentProviderService }
    single { TaxDocumentResultStorageServiceImpl(get()) as TaxDocumentResultStorageService }
    single { TaxRateServiceImpl(get()) as TaxRateService }
    single { TenantServiceImpl(get()) as TenantService }

    single { TaxContentMessageServiceImpl(taxRateService = get()) as TaxContentMessageService }

    single {
        object : DatadogConfig {

            val properties: Properties = get()

            override fun apiKey(): String {
                return properties.getProperty("datadog.api-key") ?: ""
            }

            override fun applicationKey(): String {
                return properties.getProperty("datadog.aplication-key") ?: ""
            }

            override fun get(key: String): String? {
                return properties.getProperty(key)
            }
        } as DatadogConfig
    }

    single {
        val configProps: Properties = get()

        val properties = Properties()

        // kafka bootstrap server
        properties.setProperty(
            ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,
            configProps.getProperty("kafka.bootstrap-servers")
        )
        properties.setProperty(
            ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
            TaxDocumentResultKeyJsonSerializer::class.java.name
        )
        properties.setProperty(
            ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
            TaxDocumentResultJsonSerializer::class.java.name
        )
        // producer acks
        properties.setProperty(ProducerConfig.ACKS_CONFIG, "all") // strongest producing guarantee
        properties.setProperty(ProducerConfig.RETRIES_CONFIG, "3")
        properties.setProperty(ProducerConfig.LINGER_MS_CONFIG, "1")
        // leverage idempotent producer from Kafka 0.11 !
        properties.setProperty(ProducerConfig.ENABLE_IDEMPOTENCE_CONFIG, "true") // ensure we don't push duplicates

        KafkaProducer<String, String>(properties)
    }

    single { DefaultTaxingTransactionLocationFactory(get()) as TaxingTransactionLocationFactory }

    single { TenantModule(get(), get()) }
    single { CompanyModule(get(), get()) }
    single { CompanyTaxContentProviderModule(get(), get(), get(), get(), get()) }
    single { TaxingTransactionLocationModule(get(), get()) }
    single { TaxAuthorityModule(get(), get(), get()) }
    single { TaxRateSelectionModule(get(), get()) }
    single { TaxCalculationModule(get()) }
    single { StoreTaxDocumentResultModule(get(), get()) }

    factory(named("standardModules")) {
        flowOf(
            get<TenantModule>(),
            get<CompanyModule>(),
            get<CompanyTaxContentProviderModule>(),
            get<TaxingTransactionLocationModule>(),
            get<TaxAuthorityModule>(),
            get<TaxRateSelectionModule>(),
            get<TaxCalculationModule>(),
            get<StoreTaxDocumentResultModule>(),
        ) as Flow<TaxModule>
    }

    single(named("calculationDispatcher")) {
        Dispatchers.Default.limitedParallelism(16) as CoroutineDispatcher
    }

    single { DatadogMeterRegistry(get(), Clock.SYSTEM) as MeterRegistry }

    single { StandardEngine(get(), get(named("standardModules")), get(named("calculationDispatcher"))) }

    single { CalcService(get()) }
}
