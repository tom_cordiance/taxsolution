package com.modios.taxsolution.config

import com.typesafe.config.ConfigFactory
import io.ktor.config.HoconApplicationConfig
import mu.KotlinLogging
import java.util.Properties

class Configuration {
    private val logger = KotlinLogging.logger { }

    val config = HoconApplicationConfig(ConfigFactory.load())

    val properties = Properties().also { p ->
        logger.info { "Loading properties..." }
        ConfigFactory.load().entrySet().forEach {
            logger.info { "Property: ${it.key}: ${it.value.unwrapped()}" }
            p.setProperty(it.key, it.value.unwrapped().toString())
        }
    }

    fun getProperty(key: String): String? = config.propertyOrNull(key)?.getString()
}
