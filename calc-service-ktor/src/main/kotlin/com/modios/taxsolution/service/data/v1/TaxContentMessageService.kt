package com.modios.taxsolution.service.data.v1

import com.modios.taxsolution.content.model.v1.TaxContentMessage
import com.modios.taxsolution.content.model.v1.TaxContentType
import mu.KotlinLogging

interface TaxContentMessageService {
    suspend fun handleMessage(taxContentMessage: TaxContentMessage)
}

class TaxContentMessageServiceImpl(
    private val taxRateService: TaxRateService
) : TaxContentMessageService {

    private val logger = KotlinLogging.logger { }

    override suspend fun handleMessage(taxContentMessage: TaxContentMessage) {
        logger.info { "handleMessage($taxContentMessage)" }
        when (taxContentMessage.taxContentType) {
            TaxContentType.TAX_RATE -> {
                taxContentMessage
                    .taxContentIdList
                    .forEach {
                        taxRateService.evictByAuthorityId(it.parentId)
                    }
            }
            else -> {}
        }
    }
}
