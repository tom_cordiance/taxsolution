package com.modios.taxsolution.service.data.v1

import com.modios.taxsolution.content.client.v1.GeographyClient
import com.modios.taxsolution.content.model.v1.Geography
import com.modios.taxsolution.service.data.cache.CacheKey
import com.modios.taxsolution.service.data.cache.ServiceCacheStandard

class GeographyServiceImpl(private val geographyClient: GeographyClient) : GeographyService {

    private val serviceCache = ServiceCacheStandard<Geography>()

    /**
     * Returns all of the children of 'world' for the supplied tax content provider. This is typically
     * countries.
     */
    override suspend fun getTopLevelByTaxContentProviderId(taxContentProviderId: Long): List<Geography> {
        return serviceCache.getOrPutCollection(
            cacheKey = GeographyByTaxContentProviderIdAndParentIdCacheKey(
                taxContentProviderId = taxContentProviderId,
                parentId = Geography.WORLD_ID
            ),
            get = {
                geographyClient.getByTaxContentProviderIdAndParentId(
                    taxContentProviderId = taxContentProviderId,
                    parentId = Geography.WORLD_ID
                )
            }
        )
    }

    override suspend fun getByTaxContentProviderIdAndParentIdAndName(
        taxContentProviderId: Long,
        parentId: Long,
        name: String
    ): Geography? {
        return serviceCache.getOrPutValue(
            cacheKey = GeographyByTaxContentProviderIdAndParentIdAndNameCacheKey(
                taxContentProviderId = taxContentProviderId,
                parentId = parentId,
                name = name
            ),
            get = {
                geographyClient.getByTaxContentProviderIdAndParentIdAndName(
                    taxContentProviderId = taxContentProviderId,
                    parentId = parentId,
                    name = name
                )
            }
        )
    }

    override suspend fun getByTaxContentProviderIdAndParentIdAndCode(
        taxContentProviderId: Long,
        parentId: Long,
        code: String
    ): Geography? {
        return serviceCache.getOrPutValue(
            cacheKey = GeographyByTaxContentProviderIdAndParentIdAndCodeCacheKey(
                taxContentProviderId = taxContentProviderId,
                parentId = parentId,
                code = code
            ),
            get = {
                geographyClient.getByTaxContentProviderIdAndParentIdAndCode(
                    taxContentProviderId = taxContentProviderId,
                    parentId = parentId,
                    code = code
                )
            }
        )
    }
}

data class GeographyByTaxContentProviderIdAndParentIdCacheKey(val taxContentProviderId: Long, val parentId: Long) :
    CacheKey<Geography>

data class GeographyByTaxContentProviderIdAndParentIdAndCodeCacheKey(
    val taxContentProviderId: Long,
    val parentId: Long,
    val code: String
) : CacheKey<Geography>

data class GeographyByTaxContentProviderIdAndParentIdAndNameCacheKey(
    val taxContentProviderId: Long,
    val parentId: Long,
    val name: String
) : CacheKey<Geography>
