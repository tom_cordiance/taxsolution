package com.modios.taxsolution.service.data.v1

import com.modios.taxsolution.admin.client.v1.CompanyClient
import com.modios.taxsolution.model.v1.Company
import com.modios.taxsolution.service.data.cache.CacheKey
import com.modios.taxsolution.service.data.cache.ServiceCacheStandard

class CompanyServiceImpl(private val companyClient: CompanyClient) : CompanyService {

    private val cache = ServiceCacheStandard<Company>()

    override suspend fun getCompanies(tenantId: Long): List<Company> {
        return cache.getOrPutCollection(
            cacheKey = TenantIdCacheKey(tenantId),
            get = { companyClient.getCompanies(tenantId) }
        )
    }

    override suspend fun getCompany(tenantId: Long, companyName: String): Company? {
        return cache.getOrPutValue(
            cacheKey = TenantIdCompanyNameCacheKey(tenantId = tenantId, companyName = companyName),
            get = { companyClient.getCompany(tenantId = tenantId, name = companyName) }
        )
    }
}

data class TenantIdCacheKey(val tenantId: Long) : CacheKey<Company>

data class TenantIdCompanyNameCacheKey(val tenantId: Long, val companyName: String) : CacheKey<Company>
