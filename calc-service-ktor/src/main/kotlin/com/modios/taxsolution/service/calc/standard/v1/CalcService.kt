package com.modios.taxsolution.service.calc.standard.v1

import com.modios.taxsolution.calc.model.v1.TaxDocument
import com.modios.taxsolution.calc.model.v1.TaxDocumentResult
import com.modios.taxsolution.engine.v1.StandardEngine
import kotlin.time.ExperimentalTime

class CalcService(private val standardEngine: StandardEngine) {

    @OptIn(ExperimentalTime::class)
    suspend fun calculate(taxDocument: TaxDocument): TaxDocumentResult {
        return standardEngine.calculate(taxDocument)
    }
}
