package com.modios.taxsolution.service.data.v1

import com.modios.taxsolution.content.client.v1.TaxContentProviderClient
import com.modios.taxsolution.content.model.v1.TaxContentProvider
import com.modios.taxsolution.service.data.cache.CacheKey
import com.modios.taxsolution.service.data.cache.ServiceCacheStandard

class TaxContentProviderServiceImpl(private val taxContentProviderClient: TaxContentProviderClient) : TaxContentProviderService {

    private val serviceCache = ServiceCacheStandard<TaxContentProvider>()

    override suspend fun get(taxContentProviderId: Long): TaxContentProvider? {
        return serviceCache.getOrPutValue(
            cacheKey = TaxContentProviderIdCacheKey(taxContentProviderId),
            get = { taxContentProviderClient.get(taxContentProviderId) }
        )
    }
}

data class TaxContentProviderIdCacheKey(val taxContentProviderId: Long) : CacheKey<TaxContentProvider>
