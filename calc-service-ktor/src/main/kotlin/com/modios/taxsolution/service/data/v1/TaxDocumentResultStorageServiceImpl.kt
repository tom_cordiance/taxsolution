package com.modios.taxsolution.service.data.v1

import com.modios.taxsolution.calc.CalcCommon
import com.modios.taxsolution.calc.model.v1.TaxDocumentResult
import com.modios.taxsolution.calc.model.v1.TaxDocumentResultKey
import org.apache.kafka.clients.producer.KafkaProducer
import org.apache.kafka.clients.producer.ProducerRecord

class TaxDocumentResultStorageServiceImpl(private val kafkaProducer: KafkaProducer<TaxDocumentResultKey, TaxDocumentResult>) :
    TaxDocumentResultStorageService {

    override fun write(taxDocumentResult: TaxDocumentResult) {
        val key = TaxDocumentResultKey(
            timestamp = taxDocumentResult.calculationDate,
            tenantName = taxDocumentResult.tenantName,
            companyName = taxDocumentResult.companyName,
            documentNumber = taxDocumentResult.documentNumber
        )

        kafkaProducer.send(ProducerRecord(CalcCommon.TAX_DOCUMENT_RESULT_TOPIC_NAME, key, taxDocumentResult))
    }
}
