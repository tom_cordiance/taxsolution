package com.modios.taxsolution.service.data.v1

import com.modios.taxsolution.admin.client.v1.TenantClient
import com.modios.taxsolution.model.v1.Tenant
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import java.time.LocalDate

class TenantServiceTest {

    @Test
    fun testValidTenant() = runBlocking {
        val tenant = tenantService.get(existingTenantName)

        Assertions.assertNotNull(tenant)
        Assertions.assertEquals(existingTenantName, tenant?.name)

        // this is the cached value, should yield the same results
        val sameTenant = tenantService.get(existingTenantName)

        Assertions.assertNotNull(sameTenant)
        Assertions.assertEquals(existingTenantName, sameTenant?.name)
    }

    @Test
    fun testInvalidTenant() = runBlocking {
        val tenant = tenantService.get(nonExistingTenantName)

        Assertions.assertNull(tenant)

        val sameTenant = tenantService.get(nonExistingTenantName)

        Assertions.assertNull(sameTenant)
    }

    companion object {
        val existingTenantName = "Good Tenant"
        val existingTenant =
            Tenant(
                id = 1L,
                name = existingTenantName,
                activeFrom = LocalDate.now(),
                activeTo = null
            )

        val nonExistingTenantName = "Bad Tenant"

        val tenantClient = mockk<TenantClient>().also {
            coEvery { it.getTenant(existingTenantName) } returns existingTenant
            coEvery { it.getTenant(nonExistingTenantName) } returns null
        }

        val tenantService = TenantServiceImpl(tenantClient = tenantClient)
    }
}
