package com.modios.taxsolution.engine.v1

import com.modios.taxsolution.calc.model.v1.TaxDocument
import com.modios.taxsolution.model.v1.Company
import com.modios.taxsolution.model.v1.Tenant
import com.modios.taxsolution.module.v1.CompanyModule
import com.modios.taxsolution.module.v1.CompanyModuleTest
import com.modios.taxsolution.module.v1.StoreTaxDocumentResultModule
import com.modios.taxsolution.module.v1.TaxCalculation
import com.modios.taxsolution.module.v1.TenantModule
import io.micrometer.core.instrument.simple.SimpleMeterRegistry
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import java.time.LocalDate
import java.time.LocalDateTime
import kotlin.time.ExperimentalTime

class StandardEngineTest {

    @ExperimentalTime
    @Test
    fun test() = runBlocking {
        val standardEngine = StandardEngine(
            meterRegistry = SimpleMeterRegistry(),
            moduleFlow = listOf(tenantModule, companyModule, storeTaxDocumentResultModule).asFlow(),
            calculationDispatcher = Dispatchers.Default
        )

        val taxDocument = TaxDocument(
            tenantName = existingTenantName,
            companyName = existingCompanyName,
            documentDate = calculationDate,
            documentNumber = documentNumber
        )

        val result = standardEngine.calculate(taxDocument)

        Assertions.assertNotNull(result)
    }

    companion object {
        val existingTenantName = "Good Tenant"
        val existingTenant =
            Tenant(
                id = 1L,
                name = existingTenantName,
                activeFrom = LocalDate.now(),
                activeTo = null
            )

        val existingCompanyName = "Good Company"
        val existingCompany =
            Company(
                tenantId = CompanyModuleTest.tenantId,
                name = existingCompanyName,
                id = 1L,
                parentCompanyId = null
            )

        val calculationDate = LocalDateTime.now()
        val documentNumber = "123"
        val taxCalculation1 = TaxCalculation(
            tenantName = existingTenantName,
            companyName = existingCompanyName,
            calculationDate = calculationDate,
            documentNumber = documentNumber
        )
        val taxCalculation1After =
            TaxCalculation(
                tenantName = existingTenantName,
                companyName = existingCompanyName,
                tenant = existingTenant,
                calculationDate = calculationDate,
                documentNumber = documentNumber
            )

        val tenantModule = mockk<TenantModule>().also {
            coEvery { it.process(ofType(TaxCalculation::class)) } returns taxCalculation1After
        }

        val taxCalculation2After = TaxCalculation(
            tenantName = existingTenantName,
            companyName = existingCompanyName,
            tenant = existingTenant,
            company = existingCompany,
            calculationDate = calculationDate,
            documentNumber = documentNumber
        )

        val companyModule = mockk<CompanyModule>().also {
            coEvery { it.process(ofType(TaxCalculation::class)) } returns taxCalculation2After
        }

        val storeTaxDocumentResultModule = mockk<StoreTaxDocumentResultModule>().also {
            coEvery { it.process(ofType(TaxCalculation::class)) } returns taxCalculation2After
        }
    }
}
