package com.modios.taxsolution

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.fold
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class FlowTest {

    @Test
    fun test() = runBlocking {
        val flow = flowOf(1, 2, 3, 4, 5, 6)

        val coroutineScope = CoroutineScope(Dispatchers.Default)
        val results = (1..10).map {
            async { flow.fold(0) { total, element -> total + element * 10 } }
        }.awaitAll()

        results.forEach {
            Assertions.assertEquals(210, it)
        }
    }
}
