package com.modios.taxsolution.module.v1

import com.modios.taxsolution.calc.model.v1.MessageStatus
import com.modios.taxsolution.model.v1.Tenant
import com.modios.taxsolution.service.data.v1.TenantServiceImpl
import io.micrometer.core.instrument.simple.SimpleMeterRegistry
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import java.time.LocalDate
import java.time.LocalDateTime

class TenantModuleTest {

    @Test
    fun test() = runBlocking {
        val tenantModule = TenantModule(SimpleMeterRegistry(), tenantService)

        val taxCalculation = TaxCalculation(
            tenantName = existingTenantName,
            companyName = "",
            calculationDate = LocalDateTime.now(),
            documentNumber = "123"
        )

        val result = tenantModule.process(taxCalculation)

        Assertions.assertNotNull(result)
        Assertions.assertEquals(existingTenantName, result.tenant?.name)
    }

    @Test
    fun testInvalidTenant() = runBlocking {
        val tenantModule = TenantModule(SimpleMeterRegistry(), tenantService)

        val taxCalculation = TaxCalculation(
            tenantName = nonExistingTenantName,
            companyName = "",
            calculationDate = LocalDateTime.now(),
            documentNumber = "123"
        )

        val result = tenantModule.process(taxCalculation)
        Assertions.assertNotNull(result)
        Assertions.assertNull(result.tenant)
        Assertions.assertEquals(1, result.messageList.size)
        Assertions.assertEquals(MessageStatus.ERROR.toString(), result.messageList[0].status.toString())
    }

    companion object {
        val existingTenantName = "Good Tenant"
        val existingTenant =
            Tenant(
                id = 1L,
                name = existingTenantName,
                activeFrom = LocalDate.now(),
                activeTo = null
            )

        val nonExistingTenantName = "Bad Tenant"

        val tenantService = mockk<TenantServiceImpl>().also {
            coEvery { it.get(existingTenantName) } returns existingTenant
            coEvery { it.get(nonExistingTenantName) } returns null
        }
    }
}
