package com.modios.taxsolution.module.v1

import com.modios.taxsolution.calc.model.v1.TransactionLocation
import com.modios.taxsolution.content.model.v1.Geography
import com.modios.taxsolution.content.model.v1.GeographyClassification
import com.modios.taxsolution.content.model.v1.TaxContentProvider
import com.modios.taxsolution.model.v1.Company
import com.modios.taxsolution.model.v1.CompanyTaxContentProvider
import com.modios.taxsolution.service.data.v1.CompanyTaxContentProviderServiceImpl
import com.modios.taxsolution.service.data.v1.GeographyServiceImpl
import com.modios.taxsolution.service.data.v1.TaxContentProviderServiceImpl
import io.micrometer.core.instrument.simple.SimpleMeterRegistry
import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import java.time.LocalDateTime

class CompanyTaxContentProviderModuleTest {

    val companyTaxContentProviderModule = CompanyTaxContentProviderModule(
        meterRegistry = SimpleMeterRegistry(),
        companyTaxContentProviderService = companyTaxContentProviderService,
        taxContentProviderService = taxContentProviderService,
        geographyService = geographyService,
        taxingTransactionLocationFactory = testTaxableLocationGeographyFactory
    )

    @Test
    fun testIsApplicableForLocation() = runBlocking {
        val applicableLocation = companyTaxContentProviderModule.isApplicableForLocation(
            taxContentProvider = applicableTaxContentProvider,
            transactionLocation = taxCalculation.shipFromTransactionLocation!!
        )

        Assertions.assertNotNull(applicableLocation)

        val nonApplicableLocation = companyTaxContentProviderModule.isApplicableForLocation(
            taxContentProvider = nonApplicableTaxContentProvider,
            transactionLocation = taxCalculation.shipFromTransactionLocation!!
        )
        Assertions.assertNull(nonApplicableLocation)

        val emptyLocation = companyTaxContentProviderModule.isApplicableForLocation(
            taxContentProvider = emptyTaxContentProvider,
            transactionLocation = taxCalculation.shipFromTransactionLocation!!
        )
        Assertions.assertNull(emptyLocation)
    }

    @Test
    fun testProcess() = runBlocking {
        val applicableTaxableTaxCalculation = taxCalculation.copy(company = companyApplicableTaxContentProviders)

        val applicableResult = companyTaxContentProviderModule.process(applicableTaxableTaxCalculation)
        val applicableShipFromLocationGeography = applicableResult.shipFromTaxingTransactionLocation
        Assertions.assertTrue(applicableShipFromLocationGeography is TestTaxingTransactionLocation)
        Assertions.assertTrue(
            (applicableShipFromLocationGeography as TestTaxingTransactionLocation).taxContentProviderList.contains(
                applicableTaxContentProvider
            )
        )
        Assertions.assertEquals(
            applicableGeographyList.first(),
            applicableShipFromLocationGeography.taxingGeography.geography
        )

        val nonApplicableTaxableTaxCalculation = taxCalculation.copy(company = companyNoApplicableTaxContentProviders)
        val nonApplicableResult = companyTaxContentProviderModule.process(nonApplicableTaxableTaxCalculation)
        val nonApplicableShipFromLocationGeography = nonApplicableResult.shipFromTaxingTransactionLocation
        Assertions.assertNull(nonApplicableShipFromLocationGeography)
    }

    companion object {
        val companyNoTaxContentProviders =
            Company(tenantId = 1L, parentCompanyId = -1L, id = 1L, name = "no tax content providers")

        val companyApplicableTaxContentProviders =
            Company(tenantId = 1L, parentCompanyId = -1L, id = 2L, name = "has applicable content providers")
        val applicableTaxContentProvider = TaxContentProvider(id = 1L, name = "applicable tax content provider")
        val applicableGeographyList = (1L..3L).map {
            Geography(
                taxContentProviderId = applicableTaxContentProvider.id,
                classification = GeographyClassification.COUNTRY,
                id = it,
                parentId = Geography.WORLD_ID,
                name = "geography$it",
                code = "g$it"
            )
        }
        val applicableCompanyTaxContentProvider = CompanyTaxContentProvider(
            id = 1L,
            companyId = companyApplicableTaxContentProviders.id,
            taxContentProviderId = applicableTaxContentProvider.id
        )

        val companyNoApplicableTaxContentProviders =
            Company(tenantId = 1L, parentCompanyId = -1L, id = 3L, name = "no applicable tax content providers")
        val nonApplicableTaxContentProvider = TaxContentProvider(id = 2L, name = "nonapplicable tax content provider")
        val nonApplicableGeographyList = (10L..15L).map {
            Geography(
                taxContentProviderId = applicableTaxContentProvider.id,
                classification = GeographyClassification.COUNTRY,
                id = it,
                parentId = Geography.WORLD_ID,
                name = "geography$it",
                code = "g$it"
            )
        }
        val nonApplicableCompanyTaxContentProvider = CompanyTaxContentProvider(
            id = 2L,
            companyId = companyNoApplicableTaxContentProviders.id,
            taxContentProviderId = nonApplicableTaxContentProvider.id
        )

        val emptyTaxContentProvider = TaxContentProvider(id = 3L, name = "empty tax content provider")

        val companyTaxContentProviderService = mockk<CompanyTaxContentProviderServiceImpl>().also {
            coEvery { it.getCompanyTaxContentProviders(companyNoTaxContentProviders.id) } returns emptyList()
            coEvery { it.getCompanyTaxContentProviders(companyNoApplicableTaxContentProviders.id) } returns listOf(
                nonApplicableCompanyTaxContentProvider
            )
            coEvery { it.getCompanyTaxContentProviders(companyApplicableTaxContentProviders.id) } returns listOf(
                applicableCompanyTaxContentProvider
            )
        }

        val taxContentProviderService = mockk<TaxContentProviderServiceImpl>().also {
            coEvery { it.get(applicableCompanyTaxContentProvider.id) } returns applicableTaxContentProvider
            coEvery { it.get(nonApplicableCompanyTaxContentProvider.id) } returns nonApplicableTaxContentProvider
            coEvery { it.get(emptyTaxContentProvider.id) } returns null
        }

        val geographyService = mockk<GeographyServiceImpl>().also {
            coEvery { it.getTopLevelByTaxContentProviderId(eq(applicableTaxContentProvider.id)) } returns applicableGeographyList
            coEvery { it.getTopLevelByTaxContentProviderId(eq(nonApplicableTaxContentProvider.id)) } returns nonApplicableGeographyList
            coEvery { it.getTopLevelByTaxContentProviderId(eq(emptyTaxContentProvider.id)) } returns emptyList()
        }

        data class TestTaxingTransactionLocation(val taxingGeography: TaxingGeography) : TaxingTransactionLocation() {
            /**
             * Returns all of the non-null TaxableGeography for this instance. The TaxableGeography should be
             * returned in hierarchical order.
             */
            override fun getAllTaxingGeography(): List<TaxingGeography> {
                return listOf(taxingGeography)
            }
        }

        val testTaxableLocationGeographyFactory = mockk<TaxingTransactionLocationFactory>().also {
            applicableGeographyList.forEach { geography ->
                every { it.create(applicableTaxContentProvider, eq(geography)) } returns TestTaxingTransactionLocation(
                    taxingGeography = TaxingGeography(geography = geography)
                ).also {
                    it.taxContentProviderList.add(
                        applicableTaxContentProvider
                    )
                }
            }

            nonApplicableGeographyList.forEach { geography ->
                every {
                    it.create(
                        nonApplicableTaxContentProvider,
                        eq(geography)
                    )
                } returns TestTaxingTransactionLocation(
                    taxingGeography = TaxingGeography(geography = geography)
                ).also {
                    it.taxContentProviderList.add(
                        nonApplicableTaxContentProvider
                    )
                }
            }
        }

        val taxCalculation = TaxCalculation(
            tenantName = "test",
            companyName = "test",
            calculationDate = LocalDateTime.now(),
            documentNumber = "123",
            shipFromTransactionLocation = TransactionLocation(country = applicableGeographyList.first().code)
        )
    }
}
