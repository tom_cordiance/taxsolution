plugins {
    id("application")
    kotlin("plugin.serialization") apply true
}

val logbackVersion: String by project
val logbackEcsEncoderVersion: String by project
val slf4jApiVersion: String by project
val kotlinLoggingVersion: String by project
val junitJupiterVersion: String by project
val mockkVersion: String by project
val ktorVersion: String by project
val koinVersion: String by project
val kafkaVersion: String by project
val kotlinCoroutineVersion: String by project
val kotlinSerializationVersion: String by project

dependencies {
    // Align versions of all Kotlin components
    implementation(platform("org.jetbrains.kotlin:kotlin-bom"))

    implementation("io.ktor:ktor:$ktorVersion")
    implementation("io.ktor:ktor-server-cio:$ktorVersion")
    implementation("io.ktor:ktor-serialization:$ktorVersion")
    implementation("io.ktor:ktor-client-core:$ktorVersion")
    implementation("io.ktor:ktor-client-cio:$ktorVersion")
    implementation("io.ktor:ktor-client-serialization:$ktorVersion")

    implementation("io.insert-koin:koin-core:$koinVersion")
    implementation("io.insert-koin:koin-ktor:$koinVersion")

    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$kotlinCoroutineVersion")
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:$kotlinSerializationVersion")

    implementation(project(":data-service-common"))
    implementation(project(":calc-service-model"))
    implementation(project(":calc-service-core"))
    implementation(project(":admin-service-model"))
    implementation(project(":admin-service-client-ktor"))
    implementation(project(":tax-content-service-model"))
    implementation(project(":tax-content-service-client-ktor"))

    implementation("org.apache.kafka:kafka-clients:$kafkaVersion")

    // Metrics
    implementation("io.micrometer:micrometer-registry-datadog:1.7.5")

    // Logging
    implementation("ch.qos.logback:logback-classic:$logbackVersion")
    implementation("org.slf4j:slf4j-api:$slf4jApiVersion")
    implementation("net.logstash.logback:logstash-logback-encoder:6.6")
    implementation("io.github.microutils:kotlin-logging:$kotlinLoggingVersion")

    testImplementation("org.junit.jupiter:junit-jupiter:$junitJupiterVersion")
    testImplementation("io.mockk:mockk:$mockkVersion")
}

application {
    // Define the main class for the application.
    mainClassName = "com.modios.taxsolution.CalcServiceAppKt"
}

tasks.test {
    useJUnitPlatform()
}
