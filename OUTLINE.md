 
# Cloud Based
All of the below is assumed to be a Cloud-based solution.  No on-premise requirements 
unless they are required, such as local tax engines for the sake of performance that 
send calc results to the cloud (Gary, remember Giza?)
 
# Tax Engine
The tax engine that I’ve designed so far is intended to answer questions regarding scalability, 
performance, time-to-market for emerging requirements, tax content/data ease-of-management.
It is focused upon tax engine building blocks and component loose coupling.  The high-level ideas are:
- There are Web services for serving up tax data – content, company config, administrative, whatever
- All data is inherently multi-tenant.  No magic incantations required for collocating data.
- The Web services are the source of data for all actors in the system: UI, 
data automation, tax engine, report generation, return generation, etc
- The tax engine is made of up tax modules, each module sourcing its data independent of another.
Company lookup/validation is done mutually exclusively of rate lookup for example.  
Each has its own method of sourcing data, which does not have dependencies.  A way 
to visualize this is that there are a chain of these modules that are put together 
that complete a calculation.  They are reusable and can be reassembled to create 
multiple tax engine implementations based upon the requirement.
- The tax engine has versioned service endpoints for the calculations, each able to have
its own model/schema.  Customers/integrations migrate to the appropriate calc endpoints at
their pace.  One version of a calculation model is not dependent upon another.  This means
that new calculation requirements can be rolled out more quickly.  This does cause a calc
endpoint management requirement, as the number of versions of each endpoint could become unwieldly.
An N-1 strategy could be implemented to help keep versions down.
- The Web services are versioned, so new versions of data can be exposed readily.  
Each tax engine building block gets its data from explicit web services.  The Web services
hide the data implementation, so work can be done internally without causing a data version
incompatibility.  Example: a telecom Web service can be created and populated with data
without causing an issue for existing functionality.  A new telecom-specific tax engine
could be created that uses most of the existing tax calc modules but has some required for telecom.
- Tax data is managed directly via the solution.  Tax data export/import is no longer a concern/requirement.
Tax content ‘releases’ take on a new meaning, which either means internal promotion or 
management via effective dates.  Note that all data will have unique identifiers so can be
migrated from system to system, but this would be for internal housekeeping not regular maintenance.
 
# Tax Return Generation
It would be ‘easy’ to create a tax return solution from the above.  The ‘easy’ has heavy quotes:
it isn’t a trivial task, however has an engine architecture that would allow for things
like incremental return generation, incremental reports to show tax liabilities, 
and also data analysis that could become predictive.  It would be cloud-based,
so globally available.
 
# Available to Many Markets
Since the engine is essentially based upon building blocks that can be assembled as needed,
many markets could be addressed globally.  This is both in terms of company size as well 
as company business domain.
 
